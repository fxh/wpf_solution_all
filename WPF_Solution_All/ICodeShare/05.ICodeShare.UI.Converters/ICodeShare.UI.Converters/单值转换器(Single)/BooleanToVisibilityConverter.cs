﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// Boolean值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:BooleanToVisibilityConverter x:Key="cvtsBooleanToVisibility"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsBooleanToVisibility},ConverterParameter='UseHidden=true,IsInversed=true'}"></TextBox>
    /// </example>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        #region Members
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        /// <summary>
        /// 控件不可见时,如果条件为真则仍为控件分配位置，条件为假则不分配位置。
        /// </summary>
        private bool _useHidden = false;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public BooleanToVisibilityConverter()
        {

        }

        /// <summary>
        /// 实例化一个BooleanToVisibilityConverter类
        /// </summary>
        /// <param name="isInversed">值相反</param>
        /// <param name="useHidden">是否为控件分配位置</param>
        public BooleanToVisibilityConverter(bool isInversed, bool useHidden)
        {
            this.IsInversed = isInversed;
            this.UseHidden = useHidden;
        }
        #endregion

        #region Properties
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = System.Convert.ToBoolean(value, CultureInfo.InvariantCulture);

            if (this.IsInversed)
            {
                val = !val;
            }

            if (val)
            {
                return Visibility.Visible;
            }
            return this.UseHidden ? Visibility.Hidden : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Visibility))
            {
                return DependencyProperty.UnsetValue;
            }

            var visibility = (Visibility)value;
            var result = visibility == Visibility.Visible;

            if (this.IsInversed)
            {
                result = !result;
            }

            return result;
        }
        #endregion
    }
    #endregion
}
