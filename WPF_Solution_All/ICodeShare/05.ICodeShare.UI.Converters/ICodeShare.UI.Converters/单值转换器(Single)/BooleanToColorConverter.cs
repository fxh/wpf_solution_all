﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// Boolean值转换成Color值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:BooleanToColorConverter x:Key="cvtsBooleanToColor"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsBooleanToColor},ConverterParameter='UseHidden=true,IsInversed=true'}"></TextBox>
    /// </example>
    [ValueConversion(typeof(bool), typeof(Color))]
    public sealed class BooleanToColorConverter : IValueConverter
    {
        #region Members
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        /// <summary>
        /// 条件为真时，颜色值#FFFFFFFF
        /// </summary>
        private Color _trueColor = Colors.White;
        /// <summary>
        /// 条件为假时，颜色值#FF000000
        /// </summary>
        private Color _falseColor = Colors.Black;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public BooleanToColorConverter()
        {

        }

        /// <summary>
        /// 实例化一个BooleanToColorConverter类
        /// </summary>
        /// <param name="isInversed">值相反</param>
        /// <param name="trueColor">条件为真的颜色</param>
        /// <param name="falseColor">条件为假的颜色</param>
        public BooleanToColorConverter(bool isInversed, Color trueColor, Color falseColor)
        {
            this.IsInversed = isInversed;
            this.TrueColor = trueColor;
            this.FalseColor = falseColor;
        }
        #endregion

        #region Properties
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 条件为真时，颜色值
        /// </summary>
        public Color TrueColor
        {
            get { return this._trueColor; }
            set { this._trueColor = value; }
        }

        /// <summary>
        /// 条件为假时，颜色值
        /// </summary>
        public Color FalseColor
        {
            get { return this._falseColor; }
            set { this._falseColor = value; }
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = System.Convert.ToBoolean(value, CultureInfo.InvariantCulture);

            if (this.IsInversed)
            {
                val = !val;
            }

            if (val)
            {
                return TrueColor;
            }
            return FalseColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Color))
            {
                return DependencyProperty.UnsetValue;
            }

            var color = (Color)value;
            var result = (color == TrueColor);

            if (this.IsInversed)
            {
                result = !result;
            }

            return result;
        }
        #endregion
    }
    #endregion
}
