﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Converters
{
    #region  IValueConverter
    /// <summary>
    /// String值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.引用该程序集，在xmal文件添加命名空间引用。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:StringToBooleanConverter x:Key="cvtsStringToBoolean"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsStringToBoolean},ConverterParameter='是'}"></TextBox>
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsStringToBoolean},ConverterParameter='否;是'}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个字符串，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个字符串，这个字符串则表示条件为真的值，条件为假的值用空字符串表示. 
    /// </remarks>
    [ValueConversion(typeof(String), typeof(bool))]
    public sealed class StringToBooleanConverter : IValueConverter
    {
        #region Members
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public StringToBooleanConverter()
        {
        }

        /// <summary>
        /// 实例化一个BooleanToVisibilityConverter类
        /// </summary>
        /// <param name="isInversed">值相反</param>
        public StringToBooleanConverter(bool isInversed)
        {
            this.IsInversed = isInversed;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 取反
        /// </summary>
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Enum) && !(value is string))
            {
                return DependencyProperty.UnsetValue;
            }
            string falseValue, trueValue;
            ParseParameter(parameter, out falseValue, out trueValue);
            var result = System.Convert.ToString(value, CultureInfo.InvariantCulture).ToLower().Equals(trueValue.ToLower());

            if (this.IsInversed)
            {
                result = !result;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
            {
                return DependencyProperty.UnsetValue;
            }

            string falseValue, trueValue;
            ParseParameter(parameter, out falseValue, out trueValue);

            var result = (bool)value;
            if (this.IsInversed)
            {
                result = !result;
            }
            if (!(targetType is Enum))
            {
                return result ? trueValue : falseValue;
            }
            else
            {
                return result ? (int)Enum.Parse(targetType, trueValue.ToString()) : (int)Enum.Parse(targetType, falseValue.ToString());
            }
        }
        #endregion

        #region Methods
        private void ParseParameter(object parameter, out string falseValue, out string trueValue)
        {
            falseValue = string.Empty;
            trueValue = string.Empty;
            var stringValue = parameter as string;
            if (string.IsNullOrWhiteSpace(stringValue))
                return;
            var stringValues = stringValue.Split(';');
            if (stringValues.Length == 0)
                return;

            if (stringValues.Length == 1)
            {
                trueValue = stringValues[0];
            }
            else
            {
                falseValue = stringValues[0];
                trueValue = stringValues[1];
            }
        }
        #endregion
    }
    #endregion
}
