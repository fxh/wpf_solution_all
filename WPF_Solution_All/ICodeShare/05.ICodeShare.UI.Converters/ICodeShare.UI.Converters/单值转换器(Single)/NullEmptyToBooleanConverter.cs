﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    ///  NULL值|Empty值转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:NullEmptyToBooleanConverter x:Key="cvtsNullEmptyToBoolean"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsNullEmptyToBoolean}}"></TextBox>
    /// </example>
    [ValueConversion(typeof(object), typeof(bool))]
    public sealed class NullEmptyToBooleanConverter : IValueConverter
    {
        #region Members
        /// <summary>
        /// 空值的返回结果
        /// </summary>
        private bool _emptyResult = false;
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public NullEmptyToBooleanConverter()
        {
        }

        /// <summary>
        /// 实例化一个NullEmptyToBooleanConverter类
        /// </summary>
        /// <param name="careEmpty">空值返回结果</param>
        /// <param name="isInversed">是否取相反值</param>
        public NullEmptyToBooleanConverter(bool emptyResult, bool isInversed)
        {
            this.EmptyResult = emptyResult;
            this.IsInversed = isInversed;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool EmptyResult
        {
            get { return this._emptyResult; }
            set { this._emptyResult = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }
        #endregion

        #region Base Class Overrides 基类方法重写

        /// <summary>
        /// The convert.
        /// </summary>
        /// <param name="value"> The value. </param>
        /// <param name="targetType"> The target type. </param>
        /// <param name="parameter"> The parameter. </param>
        /// <param name="culture"> The culture. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool result = true;
            if (value == null)
            {
                result = false;
            }
            if ((value is Brush) && ((Brush)value) == Brushes.Transparent)
            {
                result = false;
            }
            if ((value is string) && (((string)value) == string.Empty))
            {
                result = EmptyResult;
            }

            if (this.IsInversed)
            {
                result = !result;
            }

            return result;
        }

        /// <summary>
        /// The convert back.
        /// </summary>
        /// <param name="value"> The value. </param>
        /// <param name="targetType"> The target type. </param>
        /// <param name="parameter"> The parameter. </param>
        /// <param name="culture"> The culture. </param>
        /// <returns> The <see cref="object"/>. </returns>
        /// <exception cref="NotImplementedException">Any Exception </exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
    #endregion 
}
