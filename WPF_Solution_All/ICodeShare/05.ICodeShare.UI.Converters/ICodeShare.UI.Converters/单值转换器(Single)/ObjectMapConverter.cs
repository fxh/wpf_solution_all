﻿using ICodeShare.UI.Converters.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// 单值映射转换器，用于多种类型输入对应多类型输出的情况
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="clr-namespace:ICodeShare.UI.Converters"
    /// 2.在xaml文件中指定Binding值的Converter，此时需要使用的Binding.Converter来指定多组Binding。
    /// <CheckBox x:Name="_checkBox"/>
    /// <Label Content="Here is the label.">
    ///     <Label.Visibility>
    ///         <Binding Path="IsChecked" ElementName="_checkBox" FallbackValue="Collapsed">
    ///             <Binding.Converter>
    ///                 <cvts:MapConverter>
    ///                     <cvts:Mapping To="{x:Static Visibility.Visible}">
    ///                         <cvts:Mapping.From>
    ///                             <sys:Boolean>True</sys:Boolean>
    ///                         </cvts:Mapping.From>
    ///                     </cvts:Mapping>
    ///                 </cvts:MapConverter>
    ///             </Binding.Converter>
    ///         </Binding>
    ///     </Label.Visibility>
    /// </Label>
    /// 
    /// <Label>
    ///        <Label.Content>
    ///            <Binding Path="UriFormat">
    ///                <Binding.Converter>
    ///                    <cvts:MapConverter FallbackBehavior="ReturnOriginalValue">
    ///                        <cvts:Mapping From="{x:Static sys:UriFormat.SafeUnescaped}" To="Safe unescaped"/>
    ///                        <cvts:Mapping From="{x:Static sys:UriFormat.UriEscaped}" To="URI escaped"/>
    ///                    </cvts:MapConverter>
    ///                </Binding.Converter>
    ///            </Binding>
    ///        </Label.Content>
    /// </Label>
    /// </example>
    [ContentProperty("Mappings")]
    [ValueConversion(typeof(object), typeof(object))]
    public sealed class ObjectMapConverter : IValueConverter
    {
        #region  Members 成员变量
        private readonly Collection<Mapping> mappings;
        private FallbackBehavior fallbackBehavior;
        private object fallbackValue;
        #endregion

        #region Constructors 构造函数
         /// <summary>
        /// Initializes a new instance of the MapConverter class.
        /// </summary>
        public ObjectMapConverter()
        {
            this.mappings = new Collection<Mapping>();
        }
        #endregion

        #region  Properties 属性
         /// <summary>
        /// Gets or sets the fallback behavior for this <c>MapConverter</c>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The fallback behavior determines how this <c>MapConverter</c> treats failed conversions. <c>ReturnUnsetValue</c> (the default)
        /// specifies that any failed conversions should return <see cref="DependencyProperty.UnsetValue"/>, which can be used in combination with
        /// <c>Binding.FallbackValue</c> to default bindings to a specific value.
        /// </para>
        /// <para>
        /// Alternatively, <c>FallbackBehavior.ReturnOriginalValue</c> can be specified so that failed conversions result in the original value
        /// being returned. This is useful where mappings are only necessary for a subset of the total possible values. Mappings can be specified
        /// where necessary and other values can be returned as is by the <c>MapConverter</c> by setting the fallback behavior to
        /// <c>ReturnOriginalValue</c>.
        /// </para>
        /// </remarks>
        public FallbackBehavior FallbackBehavior
        {
            get
            {
                return this.fallbackBehavior;
            }

            set
            {
                value.AssertEnumMember("value", FallbackBehavior.ReturnFallbackValue, FallbackBehavior.ReturnOriginalValue, FallbackBehavior.ReturnUnsetValue);
                this.fallbackBehavior = value;
            }
        }

        /// <summary>
        /// Gets or sets the value that will be returned if <see cref="FallbackBehavior"/> is set to <see cref="FallbackBehavior.ReturnFallbackValue"/>
        /// and no mapping is present for the converted value.
        /// </summary>
        public object FallbackValue
        {
            get { return this.fallbackValue; }
            set { this.fallbackValue = value; }
        }

        /// <summary>
        /// Gets the collection of <see cref="Mapping"/>s configured for this <c>MapConverter</c>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Each <see cref="Mapping"/> defines a relationship between a source object (see <see cref="Mapping.From"/>) and a destination (see
        /// <see cref="Mapping.To"/>). The <c>MapConverter</c> uses these mappings whilst attempting to convert values.
        /// </para>
        /// </remarks>
        public Collection<Mapping> Mappings
        {
            get { return this.mappings; }
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        /// <summary>
        /// Attempts to convert the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type of the binding target property.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var mapping in this.Mappings)
            {
                if (object.Equals(value, mapping.From))
                {
                    return mapping.To;
                }
            }

            if (FallbackBehavior == FallbackBehavior.ReturnUnsetValue)
            {
                return DependencyProperty.UnsetValue;
            }
            else if (FallbackBehavior == FallbackBehavior.ReturnOriginalValue)
            {
                return value;
            }
            else
            {
                return this.FallbackValue;
            }
        }

        /// <summary>
        /// Attempts to convert the specified value back.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type of the binding target property.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var mapping in this.Mappings)
            {
                if (object.Equals(value, mapping.To))
                {
                    return mapping.From;
                }
            }

            if (FallbackBehavior == FallbackBehavior.ReturnUnsetValue)
            {
                return DependencyProperty.UnsetValue;
            }
            else if (FallbackBehavior == FallbackBehavior.ReturnOriginalValue)
            {
                return value;
            }
            else
            {
                return this.FallbackValue;
            }
        }
        #endregion
    }
    #endregion
}
