﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// Boolean值转换成Double双精度值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:BooleanToDoubleConverter x:Key="cvtsBooleanToDouble"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsBooleanToDouble},ConverterParameter='1.0'}"></TextBox>
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsBooleanToDouble},ConverterParameter='0;1.0'}"></TextBox>
    ///    <TextBox Text="{Binding ShowTheText,Converter={StaticResource cvtsBooleanToDouble}}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个参数，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个参数，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// 如果参数parameter未声明，则用1表示条件为真的值，用0表示条件为假的值.
    /// </remarks>
    [ValueConversion(typeof(bool), typeof(double), ParameterType = typeof(string))]
    public sealed class BooleanToDoubleConverter : IValueConverter
    {

        #region  Base Class Overrides 基类方法重写
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>The converted value.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                throw new ArgumentException("Cannot convert from type " + value.GetType().Name, "value");
            double falseValue, trueValue;
            ParseParameter(parameter, out falseValue, out trueValue);
            return (bool)value ? trueValue : falseValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>The converted value.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is double))
            {
                return DependencyProperty.UnsetValue;
            }
            var doubleValue = (double)value;
            double falseValue, trueValue;
            ParseParameter(parameter, out falseValue, out trueValue);
            return doubleValue == trueValue;
        }


        #endregion

        #region Methods 方法
        private void ParseParameter(object parameter, out double falseValue, out double trueValue)
        {
            falseValue = 0.0;
            trueValue = 1.0;
            var stringValue = parameter as string;
            if (string.IsNullOrWhiteSpace(stringValue))
                return;
            var stringValues = stringValue.Split(';');
            if (stringValues.Length == 0)
                return;

            if (stringValues.Length == 1)
            {
                trueValue = double.Parse(stringValues[0]);
            }
            else
            {
                falseValue = double.Parse(stringValues[0]);
                trueValue = double.Parse(stringValues[1]);
            }
        }
        #endregion
    }
    #endregion
}
