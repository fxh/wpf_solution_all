﻿using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// String值转换成Visibility值
    /// </summary>
    /// <example>
    /// 第一种：
    /// 1.引用该程序集，在xmal文件中xmlns默认命名空间必须为2006/2007/2008版本,否则采用第二种方法或往AssemblyInfo中添加映射(不建议)。
    /// xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2007/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2008/xaml/presentation"
    /// 2.在xaml文件中指定Binding的Converter
    ///   <TextBox Visibility="{Binding ShowTheTextBox, Converter={StringToVisibilityConverter},ConverterParameter='是'}"/>
    ///   <TextBox Visibility="{Binding ShowTheTextBox, Converter={StringToVisibilityConverter UseHidden=true},ConverterParameter='否;是'}"/>
    ///    
    /// or
    /// 
    /// 第二种：
    /// 1.引用该程序集，在xmal文件添加命名空间引用。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:StringToVisibilityConverter},ConverterParameter='是'}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:StringToVisibilityConverter UseHidden=true},ConverterParameter='否;是'}"/>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个字符串，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个字符串，这个字符串则表示条件为真的值，条件为假的值用空字符串表示. 
    /// </remarks>
    [MarkupExtensionReturnType(typeof(StringToVisibilityConverter))]
    public sealed class StringToVisibilityConverterExtension : MarkupExtension
    {
        #region Members
        private bool _isInversed = false;
        private bool _useHidden = false;
        #endregion

        #region Construction
        public StringToVisibilityConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个StringToVisibilityConverterExtension类
        /// </summary>
        /// <param name="isInversed">值是否相反</param>
        /// <param name="useHidden">是否为控件分配位置</param>
        public StringToVisibilityConverterExtension(bool isInversed, bool useHidden)
        {
            this.IsInversed = isInversed;
            this.UseHidden = useHidden;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 取相反值
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 隐藏控件
        /// </summary>
        [ConstructorArgument("useHidden")]
        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new StringToVisibilityConverter(this._isInversed, this._useHidden);
        }
        #endregion
    }
    #endregion
}
