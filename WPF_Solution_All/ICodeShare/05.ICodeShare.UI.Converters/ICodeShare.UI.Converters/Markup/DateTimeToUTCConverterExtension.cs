﻿using ICodeShare.UI.Converters.Common;
using System;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 世界时间转换
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <Label Content="{Binding DateTimeUtc, ElementName=UtcTimer, Converter={cvts:DateTimeToUTCConverter TargetKind=Local}}"/>
    ///    <Label Content="{Binding DateTimeUtc, ElementName=UtcTimer, Converter={cvts:DateTimeToUTCConverter TargetKind=Local,TargetAdjustment=1000:1:1:1}}"/>
    ///    <Label Content="{Binding DateTimeUtc, ElementName=UtcTimer, Converter={cvts:DateTimeToUTCConverter TargetKind=Local,TargetAdjustment=0:1,ConversionMode=SpecifyKindOnly}}"/>
    ///    <Label Content="{Binding DateTimeUtc, ElementName=UtcTimer, Converter={cvts:DateTimeToUTCConverter SourceKind=Utc,TargetKind=Local,SourceAdjustment=0:1,TargetAdjustment=0:1}}"/>
    ///    <Label Content="{Binding DateTimeUtc, ElementName=UtcTimer, Converter={cvts:DateTimeToUTCConverter SourceKind=Utc,TargetKind=Local,SourceAdjustment=0:1,TargetAdjustment=0:1,ConversionMode=SpecifyKindOnly}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(DateTimeToUTCConverter))]
    public sealed class DateTimeToUTCConverterExtension : MarkupExtension
    {
        #region  Members 成员变量
        private DateTimeKind _sourceKind;
        private DateTimeKind _targetKind;
        private DateTimeConversionMode _conversionMode;
        private TimeSpan _sourceAdjustment;
        private TimeSpan _targetAdjustment;
        #endregion

        #region  Properties 属性

        #region SourceKind
        /// <summary>
        /// 源时间格式
        /// </summary>
        [ConstructorArgument("sourceKind")]
        public DateTimeKind SourceKind
        {
            get
            {
                return this._sourceKind;
            }

            set
            {
                value.AssertEnumMember("value", DateTimeKind.Local, DateTimeKind.Unspecified, DateTimeKind.Utc);
                this._sourceKind = value;
            }
        }
        #endregion

        #region
        /// <summary>
        /// 目标时间格式
        /// </summary>
        /// <remarks>
        [ConstructorArgument("targetKind")]
        public DateTimeKind TargetKind
        {
            get
            {
                return this._targetKind;
            }

            set
            {
                value.AssertEnumMember("value", DateTimeKind.Local, DateTimeKind.Unspecified, DateTimeKind.Utc);
                this._targetKind = value;
            }
        }
        #endregion

        #region ConversionMode
        /// <summary>
        /// 时间格式间的转换方式
        /// </summary>
        /// <remarks>
        /// 如果调整方式为DateTimeConversionMode.DoConversion，则转换中时间会被调整。
        /// 如果调整方式为DateTimeConversionMode.SpecifyKindOnly，则转换中时间不被调整。
        /// </remarks>
        public DateTimeConversionMode ConversionMode
        {
            get
            {
                return this._conversionMode;
            }

            set
            {
                value.AssertEnumMember("value", DateTimeConversionMode.DoConversion, DateTimeConversionMode.SpecifyKindOnly);
                this._conversionMode = value;
            }
        }
        #endregion

        #region SourceAdjustment
        /// <summary>
        /// 源时间调整值TimeSpan.
        /// </summary>
        public TimeSpan SourceAdjustment
        {
            get { return this._sourceAdjustment; }
            set { this._sourceAdjustment = value; }
        }
        #endregion

        #region TargetAdjustment
        /// <summary>
        /// 目标时间调整值TargetAdjustment
        /// </summary>
        public TimeSpan TargetAdjustment
        {
            get { return this._targetAdjustment; }
            set { this._targetAdjustment = value; }
        }
        #endregion

        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the DateTimeConverterExtension class.
        /// </summary>
        public DateTimeToUTCConverterExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DateTimeConverterExtension class with the specified source and target kinds.
        /// </summary>
        /// <param name="sourceKind">
        /// The source kind for the <see cref="DateTimeConverter"/>.
        /// </param>
        /// <param name="targetKind">
        /// The target kind for the <see cref="DateTimeConverter"/>.
        /// </param>
        public DateTimeToUTCConverterExtension(DateTimeKind sourceKind, DateTimeKind targetKind)
        {
            this.SourceKind = sourceKind;
            this.TargetKind = targetKind;
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        /// <summary>
        /// Provides an instance of <see cref="DateTimeConverter"/> based on this <c>DateTimeConverterExtension</c>.
        /// </summary>
        /// <param name="serviceProvider">
        /// An object that can provide services.
        /// </param>
        /// <returns>
        /// The instance of <see cref="DateTimeConverter"/>.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var dateTimeConverter = new DateTimeToUTCConverter(this.SourceKind, this.TargetKind);
            dateTimeConverter.ConversionMode = this.ConversionMode;
            dateTimeConverter.SourceAdjustment = this.SourceAdjustment;
            dateTimeConverter.TargetAdjustment = this.TargetAdjustment;

            return dateTimeConverter;
        }
        #endregion

        #region Methods 方法
        #endregion
    }
    #endregion
}
