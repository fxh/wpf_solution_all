﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成string字符串
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToStringConverter},ConverterParameter='是'}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToStringConverter},ConverterParameter='否;是'}"/>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个字符串，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个字符串，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(BooleanToStringConverter))]
    public sealed class BooleanToStringConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public BooleanToStringConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new BooleanToStringConverter();
        }
        #endregion
    }
    #endregion
}
