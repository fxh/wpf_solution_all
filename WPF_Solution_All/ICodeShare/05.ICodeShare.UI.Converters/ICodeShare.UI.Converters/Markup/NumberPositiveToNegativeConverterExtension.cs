﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 正数转换成负数
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox  Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Right,Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter},  RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    /// </example>
    /// <remarks>
    /// </remarks>
    [MarkupExtensionReturnType(typeof(NumberPositiveToNegativeConverter))]
    public sealed class NumberPositiveToNegativeConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public NumberPositiveToNegativeConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new NumberPositiveToNegativeConverter();
        }
        #endregion
    }
    #endregion
}
