﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成double双精度值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding ShowTheText,Converter={cvts:BooleanToDoubleConverter},ConverterParameter='1.0'}"></TextBox>
    ///    <TextBox Text="{Binding ShowTheText,Converter={cvts:BooleanToDoubleConverter},ConverterParameter='0;1.0'}"></TextBox>
    ///    <TextBox Text="{Binding ShowTheText,Converter={cvts:BooleanToDoubleConverter}}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个参数，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个参数，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// 如果参数parameter未声明，则用1表示条件为真的值，用0表示条件为假的值.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(BooleanToDoubleConverter))]
    public sealed class BooleanToDoubleConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public BooleanToDoubleConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new BooleanToDoubleConverter();
        }
        #endregion
    }
    #endregion
}
