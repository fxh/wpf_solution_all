﻿using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region  MarkupExtension
    /// <summary>
    /// NULL值|Empty值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToVisibilityConverter}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToVisibilityConverter EmptyResult=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToVisibilityConverter IsInversed=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToVisibilityConverter UseHidden=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToVisibilityConverter EmptyResult=true,IsInversed=true,UseHidden=true}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(NullEmptyToVisibilityConverter))]
    public sealed class NullEmptyToVisibilityConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 空值的返回结果
        /// </summary>
        private bool _emptyResult = false;
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        /// <summary>
        /// 是否仅隐藏控件
        /// </summary>
        private bool _useHidden = false;
        #endregion

        #region Construction
        public NullEmptyToVisibilityConverterExtension()
        {
        }

        /// <summary>
        /// 实例化一个NullEmptyToVisibilityConverterExtension类
        /// </summary>
        /// <param name="careEmpty">空值返回结果</param>
        /// <param name="isInversed">是否取相反值</param>
        public NullEmptyToVisibilityConverterExtension(bool emptyResult, bool isInversed, bool useHidden)
        {
            this.EmptyResult = emptyResult;
            this.IsInversed = isInversed;
            this.UseHidden = useHidden;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [ConstructorArgument("emptyResult"), DefaultValue(false)]
        public bool EmptyResult
        {
            get { return this._emptyResult; }
            set { this._emptyResult = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [ConstructorArgument("useHidden"), DefaultValue(false)]
        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new NullEmptyToVisibilityConverter(_emptyResult, _isInversed, _useHidden);
        }
        #endregion
    }
    #endregion
}
