﻿using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Byte[]值转换成BitmapImage值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <Image Source="{Binding photo, Converter={cvts:ByteToBitmapImageConverter}}"/>
    ///    <Image Source="{Binding photo, Converter={cvts:ByteToBitmapImageConverter DefaultImage='/Psap.Client.Themes;component/Images/Common/headphoto.png'}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(ByteToBitmapImageConverter))]
    public sealed class ByteToBitmapImageConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 默认图片
        /// </summary>
        private string _defaultImage = string.Empty;
        #endregion

        #region Construction
        public ByteToBitmapImageConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个BooleanToVisibilityConverterExtension类
        /// </summary>
        /// <param name="defaultImage">默认图片路径</param>
        public ByteToBitmapImageConverterExtension(string defaultImage)
        {
            this.DefaultImage = defaultImage;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 默认照片
        /// </summary>
       [ConstructorArgument("defaultImage"), DefaultValue("")]
        public string DefaultImage
        {
            get { return this._defaultImage; }
            set { this._defaultImage = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new ByteToBitmapImageConverter(this._defaultImage);
        }
        #endregion
    }
    #endregion
}
