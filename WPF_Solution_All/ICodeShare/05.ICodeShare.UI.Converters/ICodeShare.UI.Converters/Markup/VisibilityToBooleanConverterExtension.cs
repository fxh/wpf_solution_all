﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:VisibilityToBooleanConverter}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:VisibilityToBooleanConverter UseHidden=true}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(VisibilityToBooleanConverter))]
    public sealed class VisibilityToBooleanConverterExtension : MarkupExtension
    {
        #region Members
        private bool _isInversed = false;
        private bool _useHidden = false;
        #endregion

        #region Construction
        public VisibilityToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个VisibilityToBooleanConverterExtension类
        /// </summary>
        /// <param name="isInversed">值是否相反</param>
        /// <param name="useHidden">是否为控件分配位置</param>
        public VisibilityToBooleanConverterExtension(bool isInversed, bool useHidden)
        {
            this._isInversed = isInversed;
            this._useHidden = useHidden;
        }
        #endregion

        #region Properties
        [ConstructorArgument("isInversed")]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        [ConstructorArgument("useHidden")]
        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new VisibilityToBooleanConverter(this._isInversed, this._useHidden);
        }
        #endregion
    }
    #endregion
}
