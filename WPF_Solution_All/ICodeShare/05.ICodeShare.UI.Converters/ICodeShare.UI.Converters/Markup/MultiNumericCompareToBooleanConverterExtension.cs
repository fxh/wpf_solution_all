﻿using System;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 比较两个数值型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=Equal}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=LessThan}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=GreaterThan}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=LessThanOrEqual}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=GreaterThanOrEqual}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=NotEqual}" Mode=OneWay>
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual
    /// </remarks>
    [MarkupExtensionReturnType(typeof(MultiNumericCompareToBooleanConverter))]
    public sealed class MultiNumericCompareToBooleanConverterExtension : MarkupExtension
    {
         #region  Members 成员变量
        private MNCompareSymbol _symbol = MNCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public MultiNumericCompareToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public MultiNumericCompareToBooleanConverterExtension(MNCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public MNCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
                value.AssertEnumMember("value", MNCompareSymbol.Equal, MNCompareSymbol.LessThan, MNCompareSymbol.GreaterThan,
                    MNCompareSymbol.LessThanOrEqual, MNCompareSymbol.GreaterThanOrEqual, MNCompareSymbol.NotEqual);
                this._symbol = value;
            }
        }
        #endregion

        #region Base Abstract Methods 抽象方法
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new MultiNumericCompareToBooleanConverter(this.Symbol);
        }
        #endregion
    }
    #endregion
}
