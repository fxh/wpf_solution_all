﻿using System;
using System.ComponentModel;
using System.Windows.Markup;
using System.Windows.Media;


namespace ICodeShare.UI.Converters.Markup
{

    #region MarkupExtension
    /// <summary>
    /// Brush值转换成相近的黑色画刷或白色画刷
    /// </summary>
    /// <example>
    /// 
    /// </example>
    [MarkupExtensionReturnType(typeof(BrushToRoundBrushConverter))]
    public sealed class BrushToRoundBrushConverterExtension : MarkupExtension
    {
        #region Members
        private Brush _lightBrush = Brushes.White;
        private Brush _deepBrush = Brushes.Black;
        #endregion

        #region Construction
        public BrushToRoundBrushConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个BooleanToColorConverterExtension类
        /// </summary>
        /// <param name="lightBrush">浅色</param>
        /// <param name="deepBrush">深色</param>
        public BrushToRoundBrushConverterExtension(Brush lightBrush, Brush deepBrush)
        {
            this.LightBrush = lightBrush;
            this.DeepBrush = deepBrush;
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// 浅色近似值
        /// </summary>
        [ConstructorArgument("highBrush")]
        public Brush LightBrush
        {
            get { return _lightBrush; }
            set { _lightBrush = value; }
        }

     
         /// <summary>
         /// 深色近似值
         /// </summary>
         [ConstructorArgument("lowBrush")]
        public Brush DeepBrush
        {
            get { return _deepBrush; }
            set { _deepBrush = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new BrushToRoundBrushConverter(this._lightBrush, this._deepBrush);
        }
        #endregion
    }
    #endregion
}
