﻿using System;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 比较两个字符型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=Equal}" Mode=OneWay>
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=LessThan}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=GreaterThan}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=LessThanOrEqual}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=GreaterThanOrEqual}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=NotEqual}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=StartWith}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding>
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=EndWith}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding>
    ///   <MultiBinding Converter="{cvts:MultiNumericCompareToBooleanConverter Symbol=Contains}">
    ///       <Binding Path= "Text" ElementName="txtPassword"/>   
    ///       <Binding Path= "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding>
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual,StartWith,EndWith,Contains
    /// </remarks>
    [MarkupExtensionReturnType(typeof(MultiStringCompareToBooleanConverter))]
    public sealed class MultiStringCompareToBooleanConverterExtension : MarkupExtension
    {
        #region  Members 成员变量
        private MSCompareSymbol _symbol = MSCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public MultiStringCompareToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public MultiStringCompareToBooleanConverterExtension(MSCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public MSCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
                value.AssertEnumMember("value", MSCompareSymbol.Equal, MSCompareSymbol.LessThan, MSCompareSymbol.GreaterThan,
                   MSCompareSymbol.LessThanOrEqual, MSCompareSymbol.GreaterThanOrEqual, MSCompareSymbol.NotEqual,
                   MSCompareSymbol.StartWith, MSCompareSymbol.EndWith, MSCompareSymbol.Contains);
                this._symbol = value;
            }
        }
        #endregion

        #region Base Abstract Methods 抽象方法
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new MultiStringCompareToBooleanConverter(this.Symbol);
        }
        #endregion
    }
    #endregion
}
