﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{

    #region MarkupExtension
    /// <summary>
    /// Thickness值转换成倍数的Thickness值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <Ellipse x:Name="PART_Decor" Grid.Row="0" Fill="{TemplateBinding Background}" Stroke="{TemplateBinding BorderBrush}" Stretch="Fill" HorizontalAlignment="Center" VerticalAlignment="Center" 
    ///    StrokeThickness="{Binding BorderThickness,RelativeSource={RelativeSource TemplatedParent},Converter={cvts:ThicknessToDoubleConverter}}" />
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该0个到4个以分号(,)分隔的参数.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(ThicknessToMultipleThicknessConverter))]
    public sealed class ThicknessToMultipleThicknessConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 倍数
        /// </summary>
        private double _multiple = 1.0;

        /// <summary>
        /// 左倍数
        /// </summary>
        private double _multipleLeft = 1.0;
        /// <summary>
        /// 上倍数
        /// </summary>
        private double _multipleTop = 1.0;
        /// <summary>
        /// 右倍数
        /// </summary>
        private double _multipleRight = 1.0;
        /// <summary>
        /// 下倍数
        /// </summary>
        private double _multipleBottom = 1.0;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public ThicknessToMultipleThicknessConverterExtension()
        {
        }

        /// <summary>
        ///  实例化一个<see cref="ThicknessToMultipleThicknessConverterExtension"/>实例
        /// </summary>
        /// <param name="multiple">倍数，用于一次设置全部</param>
        /// <param name="multipleLeft"></param>
        /// <param name="multipleTop"></param>
        /// <param name="multipleRight"></param>
        /// <param name="multipleBottom"></param>
        public ThicknessToMultipleThicknessConverterExtension(double multiple, double multipleLeft, double multipleTop, double multipleRight, double multipleBottom)
        {
            this.Multiple = multiple;
            this.MultipleLeft = multipleLeft;
            this.MultipleTop = multipleTop;
            this.MultipleRight = multipleRight;
            this.MultipleBottom = multipleBottom;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 倍数
        /// </summary>
         [ConstructorArgument("multiple"), DefaultValue(1.0)]
        public double Multiple
        {
            get { return this._multiple; }
            set{this._multiple = value;}
        }

        /// <summary>
        /// 左倍数
        /// </summary>
         [ConstructorArgument("multipleLeft"), DefaultValue(1.0)]
        public double MultipleLeft
        {
            get { return this._multipleLeft; }
            set { this._multipleLeft = value; }
        }

        /// <summary>
        /// 上倍数
        /// </summary>
        [ConstructorArgument("multipleTop"), DefaultValue(1.0)]
        public double MultipleTop
        {
            get { return this._multipleTop; }
            set { this._multipleTop = value; }
        }

        /// <summary>
        /// 右倍数
        /// </summary>
         [ConstructorArgument("multipleRight"), DefaultValue(1.0)]
        public double MultipleRight
        {
            get { return this._multipleRight; }
            set { this._multipleRight = value; }
        }

        /// <summary>
        /// 下倍数
        /// </summary>
         [ConstructorArgument("multipleBottom"), DefaultValue(1.0)]
        public double MultipleBottom
        {
            get { return this._multipleBottom; }
            set { this._multipleBottom = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new ThicknessToMultipleThicknessConverter(this._multiple, this._multipleLeft, this._multipleTop, this._multipleRight, this._multipleBottom);
        }
        #endregion
    }
    #endregion
}
