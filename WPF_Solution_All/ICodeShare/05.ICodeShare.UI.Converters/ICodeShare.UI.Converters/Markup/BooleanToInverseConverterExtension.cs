﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成相反Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox IsEnable="{Binding ShowTheText,Converter={cvts:BooleanToInverseConverter}}"></TextBox>
    /// </example>
    [MarkupExtensionReturnType(typeof(BooleanToInverseConverter))]
    public sealed class BooleanToInverseConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public BooleanToInverseConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new BooleanToInverseConverter();
        }
        #endregion
    }
    #endregion
}
