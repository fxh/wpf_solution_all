﻿using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToVisibilityConverter}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToVisibilityConverter UseHidden=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToVisibilityConverter IsInversed=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:BooleanToVisibilityConverter UseHidden=true,IsInversed=true}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(BooleanToVisibilityConverter))]
    public sealed class BooleanToVisibilityConverterExtension : MarkupExtension
    {
        #region Members
        private bool _isInversed = false;
        private bool _useHidden = false;
        #endregion

        #region Construction
        public BooleanToVisibilityConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个BooleanToVisibilityConverterExtension类
        /// </summary>
        /// <param name="isInversed">值是否相反</param>
        /// <param name="useHidden">是否为控件分配位置</param>
        public BooleanToVisibilityConverterExtension(bool isInversed, bool useHidden)
        {
            this._isInversed = isInversed;
            this._useHidden = useHidden;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 取相反值
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 隐藏控件
        /// </summary>
        [ConstructorArgument("useHidden"), DefaultValue(false)]
        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new BooleanToVisibilityConverter(this._isInversed, this._useHidden);
        }
        #endregion
    }
    #endregion
}
