﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成string字符串
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding SelectedDate,ElementName=datePicker, Converter={cvts:DateTimeToStringConverter},ConverterParameter='yyyyMMddHHmmss'}"/>
    ///    <TextBox Text="{Binding SelectedDate,ElementName=datePicker, Converter={cvts:DateTimeToStringConverter},ConverterParameter='yyyyMMdd'}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(DateTimeToStringConverter))]
    public sealed class DateTimeToStringConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DateTimeToStringConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DateTimeToStringConverter();
        }
        #endregion
    }
    #endregion
}
