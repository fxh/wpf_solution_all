﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Double值转换成Thickness双精度值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox  Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToCornerRadiusConverter}, ConverterParameter='TopLeft, TopRight, BottomRight,BottomLeft', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToCornerRadiusConverter}, ConverterParameter='TopLeft, TopRight, BottomRight', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToCornerRadiusConverter},  RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该0个到4个以分号(,)分隔的参数.
    /// 如果参数parameter包含两个参数，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个参数，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// 如果参数parameter未声明，则用1表示条件为真的值，用0表示条件为假的值.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(DoubleToCornerRadiusConverter))]
    public sealed class DoubleToCornerRadiusConverterExtension : MarkupExtension
    {
        #region Members
         /// <summary>
        /// 倍数
        /// </summary>
        private double _multiple = 1.0;

        /// <summary>
        /// 左倍数
        /// </summary>
        private double _multipleTopLeft = 1.0;
        /// <summary>
        /// 上倍数
        /// </summary>
        private double _multipleTopRight = 1.0;
        /// <summary>
        /// 右倍数
        /// </summary>
        private double _multipleBottomRight = 1.0;
        /// <summary>
        /// 下倍数
        /// </summary>
        private double _multipleBottomLeft = 1.0;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DoubleToCornerRadiusConverterExtension():this(1.0,1.0,1.0,1.0,1.0)
        {
        }

        /// <summary>
        ///  实例化一个<see cref="ThicknessToMultipleThicknessConverterExtension"/>实例
        /// </summary>
        /// <param name="multiple">倍数，用于一次设置全部</param>
        /// <param name="multipleTopLeft"></param>
        /// <param name="multipleTopRight"></param>
        /// <param name="multipleBottomRight"></param>
        /// <param name="multipleBottomLeft"></param>
        public DoubleToCornerRadiusConverterExtension(double multiple, double multipleTopLeft, double multipleTopRight, double multipleBottomRight, double multipleBottomLeft)
        {
            this.Multiple = multiple;
            this.MultipleTopLeft = multipleTopLeft;
            this.MultipleTopRight = multipleTopRight;
            this.MultipleBottomRight = multipleBottomRight;
            this.MultipleBottomLeft = multipleBottomLeft;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 倍数
        /// </summary>
         [ConstructorArgument("multiple"), DefaultValue(1.0)]
        public double Multiple
        {
            get { return this._multiple; }
            set{this._multiple = value;}
        }

        /// <summary>
        /// 左倍数
        /// </summary>
         [ConstructorArgument("multipleTopLeft"), DefaultValue(1.0)]
         public double MultipleTopLeft
         {
             get { return this._multipleTopLeft; }
             set { this._multipleTopLeft = value; }
         }

        /// <summary>
        /// 上倍数
        /// </summary>
        [ConstructorArgument("multipleTopRight"), DefaultValue(1.0)]
         public double MultipleTopRight
         {
             get { return this._multipleTopRight; }
             set { this._multipleTopRight = value; }
         }

        /// <summary>
        /// 右倍数
        /// </summary>
         [ConstructorArgument("multipleBottomRight"), DefaultValue(1.0)]
        public double MultipleBottomRight
        {
            get { return this._multipleBottomRight; }
            set { this._multipleBottomRight = value; }
        }

        /// <summary>
        /// 下倍数
        /// </summary>
         [ConstructorArgument("multipleBottomLeft"), DefaultValue(1.0)]
         public double MultipleBottomLeft
         {
             get { return this._multipleBottomLeft; }
             set { this._multipleBottomLeft = value; }
         }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToCornerRadiusConverter(this._multiple, this._multipleTopLeft, this._multipleTopRight, this._multipleBottomRight, this._multipleBottomLeft);
        }
        #endregion
    }
    #endregion
}
