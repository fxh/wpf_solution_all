﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Double值转换成Thickness双精度值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox  Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Right,Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter},  RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该0个到4个以分号(,)分隔的参数.
    /// 如果参数parameter包含两个参数，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个参数，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// 如果参数parameter未声明，则用1表示条件为真的值，用0表示条件为假的值.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(DoubleToGridLengthConverter))]
    public sealed class DoubleToGridLengthConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DoubleToGridLengthConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToGridLengthConverter();
        }
        #endregion
    }
    #endregion
}
