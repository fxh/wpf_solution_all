﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 统计值转换器-多值转换器。对绑定值进行相加求和操作。
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///   <MultiBinding Converter="{cvts:MultiCompareToColorConverter}">
    ///       <Binding Path = "Value" ElementName="slVer"/>   
    ///       <Binding Path = "Value" ElementName="slHor"/>
    ///   </MultiBinding> 
    /// </example>
    [MarkupExtensionReturnType(typeof(MultiCompareToColorConverter))]
    public sealed class MultiCompareToColorConverterExtension : MarkupExtension
    {
        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new MultiCompareToColorConverter();
        }
        #endregion
    }
    #endregion
}
