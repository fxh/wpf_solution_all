﻿using System;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 比较字符型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=Equal},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=LessThan},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=GreaterThan},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=LessThanOrEqual},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=GreaterThanOrEqual},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=NotEqual},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=StartWith},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=EndWith},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=Contains},ConverterParameter='abc',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=Between},ConverterParameter='abc;def',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=BetweenOrEqual},ConverterParameter='abc;def',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text,ElementName=txtCompare,Converter={cvts:StringCompareToBooleanConverter Symbol=StdIn},ConverterParameter='abc;abd;aef;def',Mode=OneWay}"/>
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual,StartWith,EndWith,Contains
    /// 三目比较符包括：Between,BetweenOrEqual
    /// 多目比较符包括：StdIn
    /// 参数parameter应该包含该一个参数或多个以分号(;)分隔的参数.
    /// 如果参数parameter只包含一个参数时，这个参数表示双目比较符的比较对象。
    /// 如果参数parameter包含两个以上参数时，第一个参数表示三目比较符的下限值，第二个字符串表示双目比较符的比较对象或三目比较符的上限值。
    /// 进行StdIn比较时，参数parameter表示一组枚举值。
    /// 如果参数parameter未声明，则总返回false.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(StringCompareToBooleanConverter))]
    public sealed class StringCompareToBooleanConverterExtension : MarkupExtension
    {
        #region  Members 成员变量
        private SCompareSymbol _symbol = SCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public StringCompareToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public StringCompareToBooleanConverterExtension(SCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public SCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
                value.AssertEnumMember("value", SCompareSymbol.Equal, SCompareSymbol.LessThan, SCompareSymbol.GreaterThan,
                    SCompareSymbol.LessThanOrEqual, SCompareSymbol.GreaterThanOrEqual, SCompareSymbol.NotEqual,
                    SCompareSymbol.Between, SCompareSymbol.BetweenOrEqual, SCompareSymbol.StdIn,
                    SCompareSymbol.StartWith, SCompareSymbol.EndWith, SCompareSymbol.Contains);
                this._symbol = value;
            }
        }
        #endregion

        #region Base Abstract Methods 抽象方法
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new StringCompareToBooleanConverter(this.Symbol);
        }
        #endregion
    }
    #endregion
}
