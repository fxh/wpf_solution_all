﻿#region 头部注释

/*************************************************************************************
     * 目标框架：       .NET Framework 4.5 - CLR 4.0.30319.42000 
     * 命名空间：       ICodeShare.UI.Converters.Markup
     * 一类名称：       DateTimeToStringConverterExtension
     * 一版本号：       v1.0.0.0 
*************************************************************************************    
     * 机器名称：       DESKTOP-CS9U887
     * 一一作者：       FXH_PC
     * 创建时间：       2017/3/17 14:03:48
*************************************************************************************
     * 功能描述：       1.
*************************************************************************************
     * 修 改 人：     
     * 修改时间：
     * 修改描述：
     * 一一版本：
*************************************************************************************
 *Copyright @ FXH_PC 2017. All rights reserved.
*************************************************************************************/

#endregion //头部注释

using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Double值转换成百分比字符串
    /// </summary>
    /// <example>
    /// 第一种：
    /// 1.引用该程序集，在xmal文件中xmlns默认命名空间必须为2006/2007/2008版本,否则采用第二种方法或往AssemblyInfo中添加映射(不建议)。
    /// xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2007/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2008/xaml/presentation"
    /// 2.在xaml文件中指定Binding的Converter
    ///     <TextBox Text="{Binding Width,ElementName=txtCompare, Converter={DoubleToStringPercentConverter}}"/>
    ///    
    /// or
    /// 
    /// 第二种：
    /// 1.引用该程序集，在xmal文件添加命名空间引用。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding Width,ElementName=txtCompare, Converter={cvts:DoubleToStringPercentConverter}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(DoubleToStringPercentConverter))]
    public sealed class DoubleToStringPercentConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DoubleToStringPercentConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToStringPercentConverter();
        }
        #endregion
    }
    #endregion
}
