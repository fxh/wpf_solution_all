﻿using System;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 统计值转换器-多值转换器。对绑定值进行相加求和操作。
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///   <MultiBinding Converter="{cvts:MultiSumToTotalConverter Decimals=1}">
    ///       <Binding Path = "Value" ElementName="slVer"/>   
    ///       <Binding Path = "Value" ElementName="slHor"/>
    ///   </MultiBinding> 
    ///   
    ///   <MultiBinding Converter="{cvts:MultiSumToTotalConverter}">
    ///       <Binding Path = "Value" ElementName="slVer"/>   
    ///       <Binding Path = "Value" ElementName="slHor"/>
    ///   </MultiBinding> 
    /// </example>
    [MarkupExtensionReturnType(typeof(MultiSumToTotalConverter))]
    public sealed class MultiSumToTotalConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 小数位数
        /// </summary>
        private int _decimals = 2;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public MultiSumToTotalConverterExtension()
        {
        }

        /// <summary>
        /// 实例化一个MultiSumToTotalConverter类
        /// </summary>
        /// <param name="decimals">小数位数</param>
        public MultiSumToTotalConverterExtension(int decimals)
        {
            this._decimals = decimals;
        }
        #endregion

        #region Properties

        [ConstructorArgument("_decimals")]
        public int Decimals
        {
            get { return this._decimals; }
            set { this._decimals = value; }
        }

        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new MultiSumToTotalConverter(this._decimals);
        }
        #endregion
    }
    #endregion
}
