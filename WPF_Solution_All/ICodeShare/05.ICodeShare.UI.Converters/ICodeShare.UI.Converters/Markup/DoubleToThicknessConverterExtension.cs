﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Double值转换成Thickness双精度值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox  Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Right,Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter}, ConverterParameter='Left, Top, Bottom', RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    ///    <TextBox Margin="{Binding Path=Padding.Left, Converter={cvts:DoubleToThicknessConverter},  RelativeSource={RelativeSource TemplatedParent}}"></TextBox>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该0个到4个以分号(,)分隔的参数.
    /// 如果参数parameter包含两个参数，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个参数，这个字符串则表示条件为真的值，条件为假的值用空字符串表示.
    /// 如果参数parameter未声明，则用1表示条件为真的值，用0表示条件为假的值.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(DoubleToThicknessConverter))]
    public sealed class DoubleToThicknessConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 倍数
        /// </summary>
        private double _multiple = 1.0;

        /// <summary>
        /// 左倍数
        /// </summary>
        private double _multipleLeft = 1.0;
        /// <summary>
        /// 上倍数
        /// </summary>
        private double _multipleTop = 1.0;
        /// <summary>
        /// 右倍数
        /// </summary>
        private double _multipleRight = 1.0;
        /// <summary>
        /// 下倍数
        /// </summary>
        private double _multipleBottom = 1.0;
        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DoubleToThicknessConverterExtension():this(1.0,1.0,1.0,1.0,1.0)
        {
        }

        /// <summary>
        ///  实例化一个<see cref="ThicknessToMultipleThicknessConverterExtension"/>实例
        /// </summary>
        /// <param name="multiple">倍数，用于一次设置全部</param>
        /// <param name="multipleLeft"></param>
        /// <param name="multipleTop"></param>
        /// <param name="multipleRight"></param>
        /// <param name="multipleBottom"></param>
        public DoubleToThicknessConverterExtension(double multiple, double multipleLeft, double multipleTop, double multipleRight, double multipleBottom)
        {
            this.Multiple = multiple;
            this.MultipleLeft = multipleLeft;
            this.MultipleTop = multipleTop;
            this.MultipleRight = multipleRight;
            this.MultipleBottom = multipleBottom;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 倍数
        /// </summary>
         [ConstructorArgument("multiple"), DefaultValue(1.0)]
        public double Multiple
        {
            get { return this._multiple; }
            set{this._multiple = value;}
        }

        /// <summary>
        /// 左倍数
        /// </summary>
         [ConstructorArgument("multipleLeft"), DefaultValue(1.0)]
        public double MultipleLeft
        {
            get { return this._multipleLeft; }
            set { this._multipleLeft = value; }
        }

        /// <summary>
        /// 上倍数
        /// </summary>
        [ConstructorArgument("multipleTop"), DefaultValue(1.0)]
        public double MultipleTop
        {
            get { return this._multipleTop; }
            set { this._multipleTop = value; }
        }

        /// <summary>
        /// 右倍数
        /// </summary>
         [ConstructorArgument("multipleRight"), DefaultValue(1.0)]
        public double MultipleRight
        {
            get { return this._multipleRight; }
            set { this._multipleRight = value; }
        }

        /// <summary>
        /// 下倍数
        /// </summary>
         [ConstructorArgument("multipleBottom"), DefaultValue(1.0)]
        public double MultipleBottom
        {
            get { return this._multipleBottom; }
            set { this._multipleBottom = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToThicknessConverter(this._multiple, this._multipleLeft, this._multipleTop, this._multipleRight, this._multipleBottom);
        }
        #endregion
    }
    #endregion
}
