﻿using System;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// 比较数值型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=Equal},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=LessThan},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=GreaterThan},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=LessThanOrEqual},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=GreaterThanOrEqual},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=NotEqual},ConverterParameter='100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=Between},ConverterParameter='0;100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=BetweenOrEqual},ConverterParameter='0;100',Mode=OneWay}"/>
    ///    <CheckBox IsChecked="{Binding Path=Text.Length,ElementName=txtCompare,Converter={cvts:NumericCompareToBooleanConverter Symbol=StdIn},ConverterParameter='0;10;20;40',Mode=OneWay}"/>
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual
    /// 三目比较符包括：Between,BetweenOrEqual
    /// 多目比较符包括：StdIn
    /// 参数parameter应该包含该一个参数或多个以分号(;)分隔的参数.
    /// 如果参数parameter只包含一个参数时，这个参数表示双目比较符的比较对象。
    /// 如果参数parameter包含两个以上参数时，第一个参数表示三目比较符的下限值，第二个字符串表示双目比较符的比较对象或三目比较符的上限值。
    /// 进行StdIn比较时，参数parameter表示一组枚举值。
    /// 如果参数parameter未声明，则总返回false.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(NumericCompareToBooleanConverter))]
    public sealed class NumericCompareToBooleanConverterExtension : MarkupExtension
    {
        #region  Members 成员变量
        private NCompareSymbol _symbol = NCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public NumericCompareToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public NumericCompareToBooleanConverterExtension(NCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public NCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
                value.AssertEnumMember("value", NCompareSymbol.Equal, NCompareSymbol.LessThan, NCompareSymbol.GreaterThan,
                    NCompareSymbol.LessThanOrEqual, NCompareSymbol.GreaterThanOrEqual, NCompareSymbol.NotEqual,
                    NCompareSymbol.Between, NCompareSymbol.BetweenOrEqual, NCompareSymbol.StdIn);
                this._symbol = value;
            }
        }
        #endregion

        #region Base Abstract Methods 抽象方法
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new NumericCompareToBooleanConverter(this.Symbol);
        }
        #endregion
    }
    #endregion
}
