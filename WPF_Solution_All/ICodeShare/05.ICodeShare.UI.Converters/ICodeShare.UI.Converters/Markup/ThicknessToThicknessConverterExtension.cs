﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Thickness值转换成缺省的Thickness值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <Ellipse x:Name="PART_Decor" Grid.Row="0" Fill="{TemplateBinding Background}" Stroke="{TemplateBinding BorderBrush}" Stretch="Fill" HorizontalAlignment="Center" VerticalAlignment="Center" 
    ///    StrokeThickness="{Binding BorderThickness,RelativeSource={RelativeSource TemplatedParent},Converter={cvts:ThicknessToThicknessConverter}}" />
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该0个到4个以分号(,)分隔的参数.
    /// </remarks>
    [MarkupExtensionReturnType(typeof(ThicknessToThicknessConverter))]
    public sealed class ThicknessToThicknessConverterExtension : MarkupExtension
    {
        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public ThicknessToThicknessConverterExtension()
        {
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new ThicknessToThicknessConverter();
        }
        #endregion
    }
    #endregion
}
