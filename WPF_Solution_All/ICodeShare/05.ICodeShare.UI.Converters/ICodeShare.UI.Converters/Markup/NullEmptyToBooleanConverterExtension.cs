﻿using System;
using System.Windows.Markup;
using System.ComponentModel;

namespace ICodeShare.UI.Converters.Markup
{
    #region  MarkupExtension
    /// <summary>
    /// NULL值|Empty值转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToBooleanConverter}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToBooleanConverter EmptyResult=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToBooleanConverter IsInversed=true}}"/>
    ///    <TextBox Visibility="{Binding ShowTheTextBox, Converter={cvts:NullEmptyToBooleanConverter EmptyResult=true,IsInversed=true}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(NullEmptyToBooleanConverter))]
    public sealed class NullEmptyToBooleanConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 空值的返回结果
        /// </summary>
        private bool _emptyResult = false;
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        #endregion

        #region Construction
        public NullEmptyToBooleanConverterExtension()
        {
        }

        /// <summary>
        /// 实例化一个NullEmptyToBooleanConverterExtension类
        /// </summary>
        /// <param name="careEmpty">空值返回结果</param>
        /// <param name="isInversed">是否取相反值</param>
        public NullEmptyToBooleanConverterExtension(bool emptyResult, bool isInversed)
        {
            this.EmptyResult = emptyResult;
            this.IsInversed = isInversed;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [ConstructorArgument("emptyResult"), DefaultValue(false)]
        public bool EmptyResult
        {
            get { return this._emptyResult; }
            set { this._emptyResult = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new NullEmptyToBooleanConverter(this._emptyResult, this._isInversed);
        }
        #endregion
    }
    #endregion
}
