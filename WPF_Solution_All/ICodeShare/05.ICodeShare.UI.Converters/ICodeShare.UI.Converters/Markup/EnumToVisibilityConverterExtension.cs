﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Enum值转换成Visibility值
    /// </summary>
    /// <example>
    /// 第一种：
    /// 1.引用该程序集，在xmal文件中xmlns默认命名空间必须为2006/2007/2008版本,否则采用第二种方法或往AssemblyInfo中添加映射(不建议)。
    /// xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2007/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2008/xaml/presentation"
    /// 2.在xaml文件中指定Binding的Converter
    ///   <TextBox Visibility="{Binding ShowTheTextBox, Converter={StringToVisibilityConverter},ConverterParameter='是'}"/>
    ///   <TextBox Visibility="{Binding ShowTheTextBox, Converter={StringToVisibilityConverter UseHidden=true},ConverterParameter='否;是'}"/>
    /// </remarks>
    [MarkupExtensionReturnType(typeof(EnumToVisibilityConverter))]
    public sealed class EnumToVisibilityConverterExtension : MarkupExtension
    {
        #region Members
        /// <summary>
        /// 是否按位枚举
        /// </summary>
        private bool _isFlags = false;
        /// <summary>
        /// 是否取相反值
        /// </summary>
        private bool _isInversed = false;
        /// <summary>
        /// 控件不可见时,如果条件为真则仍为控件分配位置，条件为假则不分配位置。
        /// </summary>
        private bool _useHidden = false;
        #endregion

          #region Construction
        /// <summary>
        /// 
        /// </summary>
        public EnumToVisibilityConverterExtension()
        {

        }

        /// <summary>
        /// 实例化一个BooleanToVisibilityConverter类
        /// </summary>
        /// <param name="isFlags">是否按位枚举</param>
        /// <param name="isInversed">值相反</param>
        /// <param name="useHidden">是否为控件分配位置</param>
        public EnumToVisibilityConverterExtension(bool isFlags, bool isInversed, bool useHidden)
        {
            this.IsFlags = isFlags;
            this.IsInversed = isInversed;
            this.UseHidden = useHidden;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 是否按位枚举
        /// </summary>
        [ConstructorArgument("isFlags"), DefaultValue(false)]
        public bool IsFlags
        {
            get { return this._isFlags; }
            set { this._isFlags = value; }
        }

        /// <summary>
        /// 取相反值
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 隐藏控件
        /// </summary>
        [ConstructorArgument("useHidden")]
        public bool UseHidden
        {
            get { return this._useHidden; }
            set { this._useHidden = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new EnumToVisibilityConverter(this._isFlags,this._isInversed, this._useHidden);
        }
        #endregion
    }
    #endregion
}
