﻿using System;
using System.ComponentModel;
using System.Windows.Markup;
using System.Windows.Media;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// String值转换成Color值
    /// </summary>
    /// <example>
    /// 第一种：
    /// 1.引用该程序集，在xmal文件中xmlns默认命名空间必须为2006/2007/2008版本,否则采用第二种方法或往AssemblyInfo中添加映射(不建议)。
    /// xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2007/xaml/presentation" 或者
    /// xmlns="http://schemas.microsoft.com/winfx/2008/xaml/presentation"
    /// 2.在xaml文件中指定Binding的Converter
    ///   <TextBox Color="{Binding ShowTheTextBox, Converter={StringToColorConverter},ConverterParameter='是'}"/>
    ///   <TextBox Color="{Binding ShowTheTextBox, Converter={StringToColorConverter UseHidden=true},ConverterParameter='否;是'}"/>
    ///    
    /// or
    /// 
    /// 第二种：
    /// 1.引用该程序集，在xmal文件添加命名空间引用。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Color="{Binding ShowTheTextBox, Converter={cvts:StringToColorConverter},ConverterParameter='是'}"/>
    ///    <TextBox Color="{Binding ShowTheTextBox, Converter={cvts:StringToColorConverter UseHidden=true},ConverterParameter='否;是'}"/>
    /// </example>
    /// <remarks>
    /// 参数parameter应该包含该一个参数或两个以分号(;)分隔的参数.
    /// 如果参数parameter包含两个字符串，第一个字符串表示条件为假的值，第二个字符串表示条件为真的值。.
    /// 如果参数parameter包含一个字符串，这个字符串则表示条件为真的值，条件为假的值用空字符串表示. 
    /// </remarks>
    [MarkupExtensionReturnType(typeof(StringToColorConverter))]
    public sealed class StringToColorConverterExtension : MarkupExtension
    {
        #region Members
        private bool _isInversed = false;
        /// <summary>
        /// 条件为真时，颜色值#FFFFFFFF
        /// </summary>
        private Color _trueColor = Colors.White;
        /// <summary>
        /// 条件为假时，颜色值#FF000000
        /// </summary>
        private Color _falseColor = Colors.Black;
        #endregion

        #region Construction
        public StringToColorConverterExtension()
        {
        }

        /// <summary>
        /// 初始化一个StringToColorConverterExtension类
        /// </summary>
        /// <param name="isInversed">值是否相反</param>
        /// <param name="trueColor">条件为真的颜色</param>
        /// <param name="falseColor">条件为假的颜色</param>
        public StringToColorConverterExtension(bool isInversed, Color trueColor, Color falseColor)
        {
            this.IsInversed = isInversed;
            this.TrueColor = trueColor;
            this.FalseColor = falseColor;
        }
        #endregion

        #region Properties
        /// <summary>
        /// 取相反值
        /// </summary>
        [ConstructorArgument("isInversed"), DefaultValue(false)]
        public bool IsInversed
        {
            get { return this._isInversed; }
            set { this._isInversed = value; }
        }

        /// <summary>
        /// 条件为真时，颜色值
        /// </summary>
         [ConstructorArgument("trueColor")]
        public Color TrueColor
        {
            get { return this._trueColor; }
            set { this._trueColor = value; }
        }

        /// <summary>
        /// 条件为假时，颜色值
        /// </summary>
        [ConstructorArgument("falseColor")]
        public Color FalseColor
        {
            get { return this._falseColor; }
            set { this._falseColor = value; }
        }
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new StringToColorConverter(this._isInversed, this._trueColor,this._falseColor);
        }
        #endregion
    }
    #endregion
}
