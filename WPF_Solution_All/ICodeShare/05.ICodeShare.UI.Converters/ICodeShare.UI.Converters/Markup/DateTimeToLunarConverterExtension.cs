﻿using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Markup
{
    #region MarkupExtension
    /// <summary>
    /// Boolean值转换成Visibility值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter
    ///    <TextBox Visibility="{Binding Time, Converter={cvts:DateTimeToLunarConverter}}"/>
    /// </example>
    [MarkupExtensionReturnType(typeof(DateTimeToLunarConverter))]
    public sealed class DateTimeToLunarConverterExtension : MarkupExtension
    {
        #region Members

        #endregion

        #region Construction
        public DateTimeToLunarConverterExtension()
        {
        }
        #endregion

        #region Properties
      
        #endregion

        #region Base Class Overrides
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DateTimeToLunarConverter();
        }
        #endregion
    }
    #endregion
}
