﻿注意：转换器的命名空间都为ICodeShare.UI.Converters 根据绑定类型不同，放入不同的文件夹中

编号  转换器名称              作用说明

单绑定（Single）
1  BooleanToDoubleConverter  Boolean值转换成Double双精度值
2  BooleanToInverseConverter  Boolean值转换成相反Boolean值
3  BooleanToStringConverter  Boolean值转换成Visibility值
4  BooleanToVisibilityConverter  Boolean值转换成Visibility值  
5  CharacterCasingConverter  大小写转换，对输入字符串进行大小写转换
6  DateTimeToStringConverter 日期格式化转换类 DateTime转换成yyyy/MM/dd（年/月/日）格式
7  DateTimeToUTCConverter  世界时间转换
8  NullEmptyToBooleanConverter  NULL|Empty值转换成Boolean值
9  NullEmptyToVisibilityConverter NULL|Empty值转换成Visibility值
10 NumericCompareToBooleanConverter  比较数值型输入转换成Boolean值
11 ObjectMapConverter  单值映射转换器，用于多种类型输入对应多类型输出的情况
12 ObjectTypeConverter  类型转换器，在设定的两种类型间进行转换
13 StringCompareToBooleanConverter  比较字符型输入转换成Boolean值
14 StringToVisibilityConverter  String值转换成Visibility值
15 VisibilityToBooleanConverter  Boolean值转换成Visibility值

多绑定（Multiple）
1  MultiCompareToColorConverter  多值颜色转换器。当纵向流量大于横向流量时指示灯应为绿色，当纵向流量小于横向流量时指示灯应为红色，否则指示灯为黄色。
2  MultiNumericCompareToBooleanConverter  比较两个数值型输入转换成Boolean值 
3  MultiStringCompareToBooleanConverter  比较两个字符型输入转换成Boolean值
4  MultiSumToTotalConverter  多值统计转换器。对绑定值进行相加求和操作。

复合绑定（Complex）
1  ExpressionConverter  表达式转换器
2  InverseColorToBinaryConverter  二值化反颜色转换器
3  StringFormatConverter 格式字符串输出转换器,根据对任意输入进行格式化字符串转换

组合容器（Combine）
1  ConverterGroup 单值组合转换器，用于多种转换器的组合使用
2  MultiConverterGroup  多值多步骤转换器，用于对多个值进行不同方式的转换，然后整合输出


