﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Converters
{
    [ValueConversion(typeof(double), typeof(CornerRadius))]
    public sealed class DoubleToCornerRadiusConverter : IValueConverter
    {
        #region Members
        /// <summary>
        /// 倍数
        /// </summary>
        private double _multiple = 1.0;

        /// <summary>
        /// 左倍数
        /// </summary>
        private double _multipleTopLeft = 1.0;
        /// <summary>
        /// 上倍数
        /// </summary>
        private double _multipleTopRight = 1.0;
        /// <summary>
        /// 右倍数
        /// </summary>
        private double _multipleBottomRight = 1.0;
        /// <summary>
        /// 下倍数
        /// </summary>
        private double _multipleBottomLeft = 1.0;
        #endregion

          #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public DoubleToCornerRadiusConverter():this(1.0,1.0,1.0,1.0,1.0)
        {

        }

        /// <summary>
        ///  实例化一个<see cref="ThicknessToMultipleThicknessConverter"/>实例
        /// </summary>
        /// <param name="multiple">倍数，用于一次设置全部</param>
        /// <param name="multipleTopLeft"></param>
        /// <param name="multipleTopRight"></param>
        /// <param name="multipleBottomRight"></param>
        /// <param name="multipleBottomLeft"></param>
        public DoubleToCornerRadiusConverter(double multiple, double multipleTopLeft, double multipleTopRight, double multipleBottomRight, double multipleBottomLeft)
        {
            this.Multiple = multiple;
            this.MultipleTopLeft = multipleTopLeft;
            this.MultipleTopRight = multipleTopRight;
            this.MultipleBottomRight = multipleBottomRight;
            this.MultipleBottomLeft = multipleBottomLeft;
        }
        #endregion

       #region Properties
        /// <summary>
        /// 倍数
        /// </summary>
        public double Multiple
        {
            get { return this._multiple; }
            set 
            {
                this._multiple = value;
                    _multipleTopLeft = value;
                    _multipleTopRight = value;
                    _multipleBottomRight = value;
                    _multipleBottomLeft = value;
             }
        }

        /// <summary>
        /// 左倍数
        /// </summary>
        public double MultipleTopLeft
        {
            get { return this._multipleTopLeft; }
            set { this._multipleTopLeft = value; }
        }

        /// <summary>
        /// 上倍数
        /// </summary>
        public double MultipleTopRight
        {
            get { return this._multipleTopRight; }
            set { this._multipleTopRight = value; }
        }

        /// <summary>
        /// 右倍数
        /// </summary>
        public double MultipleBottomRight
        {
            get { return this._multipleBottomRight; }
            set { this._multipleBottomRight = value; }
        }

        /// <summary>
        /// 下倍数
        /// </summary>
        public double MultipleBottomLeft
        {
            get { return this._multipleBottomLeft; }
            set { this._multipleBottomLeft = value; }
        }

        #endregion

         #region Base Class Overrides 基类方法重写
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is double))
            {
                return DependencyProperty.UnsetValue;
            }

            var parameters = parameter as string;
            if (parameters != null)
            {
                var propertyNames = parameters.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (propertyNames.Length > 0)
                {
                    var cornerRadius = new CornerRadius(0);

                    var changed = false;

                    foreach (var propertyName in propertyNames)
                    {
                        switch (propertyName.Trim().ToLower())
                        {
                            case "topleft":
                                cornerRadius.TopLeft = (double)value * _multipleTopLeft;
                                changed = true;
                                break;
                            case "topright":
                                cornerRadius.TopRight = (double)value * _multipleTopRight;
                                changed = true;
                                break;
                            case "bottomright":
                                cornerRadius.BottomRight = (double)value * _multipleBottomRight;
                                changed = true;
                                break;
                            case "bottomleft":
                                cornerRadius.BottomLeft = (double)value * _multipleBottomLeft;
                                changed = true;
                                break;
                        }
                    }

                    if (changed)
                    {
                        return cornerRadius;
                    }
                }
            }

            return new CornerRadius((double)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CornerRadius))
            {
                return DependencyProperty.UnsetValue;
            }

            var cornerRadius = (CornerRadius)value;

            var propertyName = parameter as string;
            switch (propertyName)
            {
                case "topleft":
                    return cornerRadius.TopLeft;
                case "topright":
                    return cornerRadius.TopRight;
                case "bottomright":
                    return cornerRadius.BottomRight;
                case "bottomleft":
                    return cornerRadius.BottomLeft;
                default:
                    return (cornerRadius.TopLeft + cornerRadius.TopRight + cornerRadius.BottomRight + cornerRadius.BottomLeft) / 4;
            }
        }
        #endregion
    }
}
