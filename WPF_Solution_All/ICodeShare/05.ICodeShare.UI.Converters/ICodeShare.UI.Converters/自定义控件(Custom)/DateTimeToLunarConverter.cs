﻿using ICodeShare.UI.Converters.Helpers;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;


namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// DateTime转换成阴历时间
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:DateTimeToLunarConverter x:Key="cvtsDateTimeToLunar"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding Time,Converter={StaticResource cvtsDateTimeToLunar}}"></TextBox>
    /// </example>
    [ValueConversion(typeof(DateTime), typeof(string))]
    public sealed class DateTimeToLunarConverter : IValueConverter
    {
        #region Members

        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public DateTimeToLunarConverter()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime))
                throw new ArgumentException("Cannot convert from type " + value.GetType().Name, "value");

            DateTime time = System.Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            bool isHoliday=false;
            string strLunar = LunarShow(time,out isHoliday);

            return strLunar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
        #endregion

        /// <summary>
        /// 阴历显示什么
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string LunarShow(DateTime dt, out bool isHoliday)
        {
            LunarHelper chineseCalendar = new LunarHelper(dt);
            isHoliday = true;
            string res = "";
            if (chineseCalendar.ChineseTwentyFourDay != "")
            {
                res = chineseCalendar.ChineseTwentyFourDay;
            }
            else if (chineseCalendar.ChineseCalendarHoliday != "")
            {
                res = chineseCalendar.ChineseCalendarHoliday;
            }
            else if (chineseCalendar.DateHoliday != "")
            {
                res = chineseCalendar.DateHoliday;
            }
            else
            {
                isHoliday = false;
                res = chineseCalendar.ChineseShortDateString;
            }
            return res;
        }
    }
    #endregion
}
