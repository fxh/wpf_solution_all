﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Converters
{
    [ValueConversion(typeof(Thickness), typeof(Thickness))]
    public sealed class ThicknessToThicknessConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Thickness))
            {
                return DependencyProperty.UnsetValue;
            }

            var source = (Thickness)value;

            var parameters = parameter as string;
            if (parameters != null)
            {
                var propertyNames = parameters.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (propertyNames.Length > 0)
                {
                    var thickness = new Thickness(0);

                    var changed = false;

                    foreach (var propertyName in propertyNames)
                    {
                        switch (propertyName.Trim().ToLower())
                        {
                            case "left":
                                thickness.Left = source.Left;
                                changed = true;
                                break;
                            case "top":
                                thickness.Top = source.Top;
                                changed = true;
                                break;
                            case "right":
                                thickness.Right = source.Right;
                                changed = true;
                                break;
                            case "bottom":
                                thickness.Bottom = source.Bottom;
                                changed = true;
                                break;
                        }
                    }

                    if (changed)
                    {
                        return thickness;
                    }
                }
            }

            return new Thickness(source.Left, source.Top, source.Right, source.Bottom);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is double))
            {
                return DependencyProperty.UnsetValue;
            }
            var source = (Thickness)value;
            var parameters = parameter as string;
            if (parameter != null)
            {
                var propertyNames = parameters.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (propertyNames.Length > 0)
                {
                    var thickness = new Thickness(0);

                    var changed = false;


                    foreach (var propertyName in propertyNames)
                    {
                        switch (propertyName.Trim().ToLower())
                        {
                            case "left":
                                thickness.Left = source.Left;
                                if (source.Right == 0)
                                {
                                    thickness.Right = source.Left;
                                }
                                changed = true;
                                break;
                            case "top":
                                thickness.Top = source.Top;
                                if (source.Bottom == 0)
                                {
                                    thickness.Bottom = source.Top;
                                }
                                changed = true;
                                break;
                            case "right":
                                thickness.Right = source.Right;
                               if (source.Left == 0)
                               {
                                   thickness.Left = source.Right;
                               }
                                changed = true;
                                break;
                            case "bottom":
                                thickness.Bottom = source.Bottom;
                                if (source.Bottom == 0)
                                {
                                    thickness.Top = source.Bottom;
                                }
                                changed = true;
                                break;
                        }
                    }

                    if (changed)
                    {
                        return thickness;
                    }
                }
            }
            return new Thickness(source.Left, source.Top, source.Right, source.Bottom);
        }
    }
}
