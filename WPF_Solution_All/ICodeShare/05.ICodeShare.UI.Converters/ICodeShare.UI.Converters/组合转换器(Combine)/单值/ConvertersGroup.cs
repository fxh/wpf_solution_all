﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters
{
    #region IMultiValueConverter
    /// <summary>
    /// 单值组合转换器，用于多种转换器的组合使用
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件中指定Binding值的Converter，此时需要使用的Binding.Converter来指定多组Binding。
    ///   <Label>
    ///     <Label.Content>
    ///         <Binding Path="StartDateTime">
    ///             <Binding.Converter>
    ///                 <cvts:ConverterGroup>
    ///                     <cvts:DateTimeConverter TargetKind="Local"/>
    ///                     <cvts:FormatConverter FormatString="{0:t}"/>
    ///                 </cvt:ConverterGroup>
    ///             </Binding.Converter>
    ///         </Binding>
    ///     </Label.Content>
    ///   </Label> 
    ///   
    ///   <TextBox x:Name="_textBox"/>
    ///   <CheckBox IsThreeState="True">
    ///     <CheckBox.IsChecked>
    ///         <Binding Path="Text" ElementName="_textBox" FallbackValue="{x:Null}">
    ///             <Binding.Converter>
    ///                 <cvts:ConverterGroup>
    ///                     <cvts:CaseConverter Casing="Upper"/>
    ///                     <cvts:MapConverter>
    ///                         <cvts:Mapping From="YES">
    ///                             <cvts:Mapping.To>
    ///                                 <sys:Boolean>True</sys:Boolean>
    ///                             </cvt:Mapping.To>
    ///                         </cvt:Mapping>
    ///                         <cvts:Mapping From="NO">
    ///                             <cvts:Mapping.To>
    ///                                 <sys:Boolean>False</sys:Boolean>
    ///                             </cvt:Mapping.To>
    ///                         </cvt:Mapping>
    ///                     </cvt:MapConverter>
    ///                 </cvt:ConverterGroup>
    ///             </Binding.Converter>
    ///         </Binding>
    ///     </CheckBox.IsChecked>
    ///    </CheckBox>
    ///    
    ///     <TextBox x:Name="_textBox7"/>
    ///     <Label>
    ///         <Label.Content>
    ///            <Binding Path="Text" ElementName="_textBox7">
    ///                <Binding.Converter>
    ///                     <cvts:ConverterGroup>
    ///                           <cvts:TypeConverter TargetType="{x:Type sys:DateTime}"/>
    ///                           <cvts:DateTimeConverter TargetAdjustment="0:10"/>
    ///                           <cvts:FormatConverter FormatString="Adding 10 minutes to that we get a time of '{0:t}'."/>
    ///                       </cvt:ConverterGroup>
    ///                  </Binding.Converter>
    ///             </Binding>
    ///         </Label.Content>
    ///      </Label>
    /// </example>
    /// <remarks>
    /// 按顺序执行转换器组
    /// </remarks>
    [ContentProperty("Converters")]
    [ValueConversion(typeof(object), typeof(object))]
    public sealed class ConverterGroup : IValueConverter
    {
        #region Members
        private readonly Collection<IValueConverter> converters;
        #endregion

        #region Construction
        /// <summary>
        /// Initializes a new instance of the ConverterGroup class.
        /// </summary>
        public ConverterGroup()
        {
            this.converters = new Collection<IValueConverter>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the collection of <see cref="IValueConverter"/>s in this <c>ConverterGroup</c>.
        /// </summary>
        public Collection<IValueConverter> Converters
        {
            get { return this.converters; }
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        /// <summary>
        /// Attempts to convert the specified value.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type of the binding target property.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.Converters.Count == 0)
            {
                return DependencyProperty.UnsetValue;
            }

            var convertedValue = value;

            foreach (var valueConverter in this.Converters)
            {
                convertedValue = valueConverter.Convert(convertedValue, targetType, parameter, culture);
            }

            return convertedValue;
        }

        /// <summary>
        /// Attempts to convert the specified value back.
        /// </summary>
        /// <param name="value">
        /// The value to convert.
        /// </param>
        /// <param name="targetType">
        /// The type of the binding target property.
        /// </param>
        /// <param name="parameter">
        /// The converter parameter to use.
        /// </param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// </param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.Converters.Count == 0)
            {
                return DependencyProperty.UnsetValue;
            }

            var convertedValue = value;

            for (var i = this.Converters.Count - 1; i >= 0; --i)
            {
                convertedValue = this.Converters[i].ConvertBack(convertedValue, targetType, parameter, culture);
            }

            return convertedValue;
        }
        #endregion
    }
    #endregion
}
