using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ICodeShare.UI.Converters.Helpers
{
    /// <summary>
    /// �������԰�����
    /// </summary>
	public static class ArgumentHelper
	{
		[DebuggerHidden]
		public static void AssertNotNull<T>(T arg, string argName) where T : class
		{
			if (arg == null)
			{
				throw new ArgumentNullException(argName);
			}
		}
		[DebuggerHidden]
		public static void AssertNotNull<T>(T? arg, string argName) where T : struct
		{
			if (!arg.HasValue)
			{
				throw new ArgumentNullException(argName);
			}
		}
		[DebuggerHidden]
		public static void AssertGenericArgumentNotNull<T>(T arg, string argName)
		{
			Type typeFromHandle = typeof(T);
			if (!typeFromHandle.IsValueType || (typeFromHandle.IsGenericType && typeFromHandle.GetGenericTypeDefinition() == typeof(Nullable<>)))
			{
				ArgumentHelper.AssertNotNull<object>(arg, argName);
			}
		}
		[DebuggerHidden]
		public static void AssertNotNull<T>(IEnumerable<T> arg, string argName, bool assertContentsNotNull)
		{
			ArgumentHelper.AssertNotNull<IEnumerable<T>>(arg, argName);
			if (assertContentsNotNull && typeof(T).IsClass)
			{
				foreach (T current in arg)
				{
					if (current == null)
					{
						throw new ArgumentException("An item inside the enumeration was null.", argName);
					}
				}
			}
		}
		[DebuggerHidden]
		public static void AssertNotNullOrEmpty(string arg, string argName)
		{
			ArgumentHelper.AssertNotNullOrEmpty(arg, argName, false);
		}
		[DebuggerHidden]
		public static void AssertNotNullOrEmpty(string arg, string argName, bool trim)
		{
			if (string.IsNullOrEmpty(arg) || (trim && ArgumentHelper.IsOnlyWhitespace(arg)))
			{
				throw new ArgumentException("Cannot be null or empty.", argName);
			}
		}
		[DebuggerHidden]
		public static void AssertNotNullOrEmpty(ICollection arg, string argName)
		{
			if (arg == null || arg.Count == 0)
			{
				throw new ArgumentException("Cannot be null or empty.", argName);
			}
		}
		[DebuggerHidden]
		public static void AssertEnumMember<TEnum>(TEnum enumValue, string argName) where TEnum : struct, IConvertible
		{
			if (Attribute.IsDefined(typeof(TEnum), typeof(FlagsAttribute), false))
			{
				long num = enumValue.ToInt64(CultureInfo.InvariantCulture);
				bool flag;
				if (num == 0L)
				{
					flag = !Enum.IsDefined(typeof(TEnum), ((IConvertible)0).ToType(Enum.GetUnderlyingType(typeof(TEnum)), CultureInfo.InvariantCulture));
				}
				else
				{
					IEnumerator enumerator = Enum.GetValues(typeof(TEnum)).GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							TEnum tEnum = (TEnum)((object)enumerator.Current);
							num &= ~tEnum.ToInt64(CultureInfo.InvariantCulture);
						}
					}
					finally
					{
						IDisposable disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
					flag = (num != 0L);
				}
				if (flag)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum value '{0}' is not valid for flags enumeration '{1}'.", new object[]
					{
						enumValue,
						typeof(TEnum).FullName
					}), argName);
				}
			}
			else
			{
				if (!Enum.IsDefined(typeof(TEnum), enumValue))
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum value '{0}' is not defined for enumeration '{1}'.", new object[]
					{
						enumValue,
						typeof(TEnum).FullName
					}), argName);
				}
			}
		}
		[DebuggerHidden]
		public static void AssertEnumMember<TEnum>(TEnum enumValue, string argName, params TEnum[] validValues) where TEnum : struct, IConvertible
		{
			ArgumentHelper.AssertNotNull<TEnum[]>(validValues, "validValues");
			if (Attribute.IsDefined(typeof(TEnum), typeof(FlagsAttribute), false))
			{
				long num = enumValue.ToInt64(CultureInfo.InvariantCulture);
				bool flag;
				if (num == 0L)
				{
					flag = true;
					for (int i = 0; i < validValues.Length; i++)
					{
						TEnum tEnum = validValues[i];
						if (tEnum.ToInt64(CultureInfo.InvariantCulture) == 0L)
						{
							flag = false;
							break;
						}
					}
				}
				else
				{
					for (int j = 0; j < validValues.Length; j++)
					{
						TEnum tEnum2 = validValues[j];
						num &= ~tEnum2.ToInt64(CultureInfo.InvariantCulture);
					}
					flag = (num != 0L);
				}
				if (flag)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum value '{0}' is not allowed for flags enumeration '{1}'.", new object[]
					{
						enumValue,
						typeof(TEnum).FullName
					}), argName);
				}
			}
			else
			{
				for (int k = 0; k < validValues.Length; k++)
				{
					TEnum tEnum3 = validValues[k];
					if (enumValue.Equals(tEnum3))
					{
						return;
					}
				}
				if (!Enum.IsDefined(typeof(TEnum), enumValue))
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum value '{0}' is not defined for enumeration '{1}'.", new object[]
					{
						enumValue,
						typeof(TEnum).FullName
					}), argName);
				}
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum value '{0}' is defined for enumeration '{1}' but it is not permitted in this context.", new object[]
				{
					enumValue,
					typeof(TEnum).FullName
				}), argName);
			}
		}
		private static bool IsOnlyWhitespace(string arg)
		{
			for (int i = 0; i < arg.Length; i++)
			{
				char c = arg[i];
				if (!char.IsWhiteSpace(c))
				{
					return false;
				}
			}
			return true;
		}
	}
}
