﻿using System;
using System.Windows.Data;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// 比较两个数值型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:MultiNumericCompareToBooleanConverter x:Key="cvtsMultiNumericCompareToBoolean"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <MultiBinding Converter="{StaticResource cvtsMultiNumericCompareToBoolean}">
    ///       <Binding Path= "Text.Length" ElementName="txtCompare1"/>   
    ///       <Binding Path= "Text.Length" ElementName="txtCompare2"/>
    ///   </MultiBinding> 
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual
    /// </remarks>
    [ValueConversion(typeof(object[]), typeof(Boolean))]
    public sealed class MultiNumericCompareToBooleanConverter:IMultiValueConverter
    {
         #region  Members 成员变量
        private MNCompareSymbol _symbol = MNCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public MultiNumericCompareToBooleanConverter()
        {
          
        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public MultiNumericCompareToBooleanConverter(MNCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public MNCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
               value.AssertEnumMember("value", MNCompareSymbol.Equal, MNCompareSymbol.LessThan, MNCompareSymbol.GreaterThan,
                    MNCompareSymbol.LessThanOrEqual, MNCompareSymbol.GreaterThanOrEqual, MNCompareSymbol.NotEqual);
                this._symbol = value;
            }
        }
        #endregion

          #region  Base Class Overrides 基类方法重写
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length != 2) return false;
            bool result = false;
            IComparable sourceValue = (IComparable)values[0];
            IComparable targetValue = (IComparable)System.Convert.ChangeType(values[1], values[0].GetType(), culture);
            if (sourceValue == null || targetValue == null)
            {
                result = false;
            }
            switch (Symbol)
            {
                case MNCompareSymbol.Equal:
                    {
                        result = sourceValue.CompareTo(targetValue) == 0;
                        break;
                    }
                case MNCompareSymbol.LessThan:
                    {
                        result = sourceValue.CompareTo(targetValue) < 0;
                        break;
                    }
                case MNCompareSymbol.GreaterThan:
                    {
                        result = sourceValue.CompareTo(targetValue) > 0;
                        break;
                    }
                case MNCompareSymbol.LessThanOrEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) <= 0;
                        break;
                    }
                case MNCompareSymbol.GreaterThanOrEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) >= 0;
                        break;
                    }
                case MNCompareSymbol.NotEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) != 0;
                        break;
                    }
                default: break;
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    #endregion
}
