﻿using System;
using System.Windows.Data;
using System.Windows.Markup;
using ICodeShare.UI.Converters.Common;
using ICodeShare.UI.Converters.Extensions;

namespace ICodeShare.UI.Converters
{
    #region IValueConverter
    /// <summary>
    /// 比较两个字符型输入转换成Boolean值
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:MultiStringCompareToBooleanConverter x:Key="cvtsMultiStringCompareToBoolean"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///  <CheckBox>
    ///  <CheckBox.IsChecked>
    ///   <MultiBinding Converter="{StaticResource cvtsMultiStringCompareToBoolean}">
    ///       <Binding Path = "Text" ElementName="txtPassword"/>   
    ///       <Binding Path = "Text" ElementName="txtConfirmPassword"/>
    ///   </MultiBinding> 
    ///   </CheckBox.IsChecked>
    ///   </CheckBox>
    /// </example>
    /// <remarks>
    /// Symbol种类：
    /// 双目比较符包括：Equal,LessThan,GreaterThan,LessThanOrEqual,GreaterThanOrEqual,NotEqual,StartWith,EndWith,Contains
    /// </remarks>
    [ValueConversion(typeof(object[]), typeof(Boolean))]
    public sealed class MultiStringCompareToBooleanConverter : IMultiValueConverter
    {
        #region  Members 成员变量
        private MSCompareSymbol _symbol = MSCompareSymbol.Equal;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the CaseConverter class.
        /// </summary>
        public MultiStringCompareToBooleanConverter()
        {

        }

        /// <summary>
        /// Initializes a new instance of the CaseConverter class with the specified source and target casings.
        /// </summary>
        /// <param name="casing">
        /// The source and target casings for the converter (see <see cref="Casing"/>).
        /// </param>
        public MultiStringCompareToBooleanConverter(MSCompareSymbol symbol)
        {
            this.Symbol = symbol;
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// 比较运算符号
        /// </summary>
        [ConstructorArgument("symbol")]
        public MSCompareSymbol Symbol
        {
            get
            {
                return this._symbol;
            }

            set
            {
                value.AssertEnumMember("value", MSCompareSymbol.Equal, MSCompareSymbol.LessThan, MSCompareSymbol.GreaterThan,
                    MSCompareSymbol.LessThanOrEqual, MSCompareSymbol.GreaterThanOrEqual, MSCompareSymbol.NotEqual,
                    MSCompareSymbol.StartWith, MSCompareSymbol.EndWith, MSCompareSymbol.Contains);
                this._symbol = value;
            }
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length != 2) return false;
            bool result = false;
            IComparable sourceValue = (IComparable)values[0];
            IComparable targetValue = (IComparable)System.Convert.ChangeType(values[1], values[0].GetType(), culture);
            if (sourceValue == null || targetValue == null)
            {
                result = false;
            }
            switch (Symbol)
            {
                case MSCompareSymbol.Equal:
                    {
                        result = sourceValue.CompareTo(targetValue) == 0;
                        break;
                    }
                case MSCompareSymbol.LessThan:
                    {
                        result = sourceValue.CompareTo(targetValue) < 0;
                        break;
                    }
                case MSCompareSymbol.GreaterThan:
                    {
                        result = sourceValue.CompareTo(targetValue) > 0;
                        break;
                    }
                case MSCompareSymbol.LessThanOrEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) <= 0;
                        break;
                    }
                case MSCompareSymbol.GreaterThanOrEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) >= 0;
                        break;
                    }
                case MSCompareSymbol.NotEqual:
                    {
                        result = sourceValue.CompareTo(targetValue) != 0;
                        break;
                    }
                case MSCompareSymbol.StartWith:
                    {
                        if ((values[0] is string))
                        {
                            result = ((string)values[0]).StartsWith(((string)targetValue), false, culture);
                        }
                        else
                        {
                            result = false;
                        }
                        break;
                    }
                case MSCompareSymbol.EndWith:
                    {
                        if ((values[0] is string))
                        {
                            result = ((string)values[0]).EndsWith(((string)targetValue), false, culture);
                        }
                        else
                        {
                            result = false;
                        }
                        break;
                    }
                case MSCompareSymbol.Contains:
                    {
                        if ((values[0] is string))
                        {
                            result = ((string)values[0]).Contains(((string)targetValue));
                        }
                        else
                        {
                            result = false;
                        }
                        break;
                    }
                default: break;
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    #endregion
}
