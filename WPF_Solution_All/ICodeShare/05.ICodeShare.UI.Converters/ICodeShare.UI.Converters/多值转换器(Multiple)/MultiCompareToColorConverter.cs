﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ICodeShare.UI.Converters
{
    #region IMultiValueConverter
    /// <summary>
    /// 颜色转换器-多值转换器。当纵向流量大于横向流量时指示灯应为绿色，当纵向流量小于横向流量时指示灯应为红色，否则指示灯为黄色。
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:MultiCompareToColorConverter x:Key="cvtsMultiCompareToColor"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter，此时需要使用的MultiBinding来指定多组Binding。
    ///  <MultiBinding Converter="{StaticResource cvtsMultiCompareToColor}">
    ///       <Binding Path = "Value" ElementName="slVer"/>   
    ///       <Binding Path = "Value" ElementName="slHor"/>
    ///   </MultiBinding>                
    /// </example>
    [ValueConversion(typeof(double[]), typeof(SolidColorBrush))]
    public sealed class MultiCompareToColorConverter : IMultiValueConverter
    {
        #region  Base Class Overrides 基类方法重写
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double verValue = (double)values[0];
            double horValue = (double)values[1];
            if (verValue > horValue)
            {
                return new SolidColorBrush(Colors.Green);
            }
            else if (verValue < horValue)
            {
                return new SolidColorBrush(Colors.Red);
            }
            return new SolidColorBrush(Colors.Yellow);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
        #endregion
    }
    #endregion 
}
