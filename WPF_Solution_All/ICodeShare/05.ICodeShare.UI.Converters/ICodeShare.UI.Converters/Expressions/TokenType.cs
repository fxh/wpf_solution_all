namespace ICodeShare.UI.Converters.Expressions
{
    internal enum TokenType
    {
        Symbol,
        Number,
        String,
        Word
    }
}
