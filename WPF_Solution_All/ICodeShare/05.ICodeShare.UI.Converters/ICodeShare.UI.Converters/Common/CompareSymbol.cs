﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Converters.Common
{
    /// <summary>
    /// 数值型比较运算符
    /// </summary>
    public enum NCompareSymbol
    {
        /// <summary>
        /// 等于
        /// </summary>
        [Description("等于")]
        Equal = 0,

        /// <summary>
        /// 小于
        /// </summary>
        [Description("小于")]
        LessThan = 1,

        /// <summary>
        /// 大于
        /// </summary>
        [Description("大于")]
        GreaterThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("小于等于")]
        LessThanOrEqual = 3,

        /// <summary>
        /// 大于等于
        /// </summary>
        // [GlobalCode(">=", OnlyAttribute = true)]
        [Description("大于等于")]
        GreaterThanOrEqual = 4,

        /// <summary>
        /// 不等于
        /// </summary>
        [Description("不等于")]
        NotEqual = 5,

        /// <summary>
        /// 处理（大于m小于m+）的问题
        /// </summary>
        [Description("在范围内不包含等于")]
        Between = 6,

        /// <summary>
        /// 处理（大于等于m小于等于m+）的问题
        /// </summary>
        [Description("在范围内包含等于")]
        BetweenOrEqual = 7,

        /// <summary>
        /// 处理In的问题
        /// </summary>
        [Description("在此范围内")]
        StdIn = 8
    }

    /// <summary>
    /// 字符型比较运算符
    /// </summary>
    public enum SCompareSymbol
    {
        /// <summary>
        /// 等于
        /// </summary>
        [Description("等于")]
        Equal = 0,

        /// <summary>
        /// 小于
        /// </summary>
        [Description("小于")]
        LessThan = 1,

        /// <summary>
        /// 大于
        /// </summary>
        [Description("大于")]
        GreaterThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("小于等于")]
        LessThanOrEqual = 3,

        /// <summary>
        /// 大于等于
        /// </summary>
        // [GlobalCode(">=", OnlyAttribute = true)]
        [Description("大于等于")]
        GreaterThanOrEqual = 4,

        /// <summary>
        /// 不等于
        /// </summary>
        [Description("不等于")]
        NotEqual = 5,

        /// <summary>
        /// 处理（大于m小于m+）的问题
        /// </summary>
        [Description("在范围内不包含等于")]
        Between = 6,

        /// <summary>
        /// 处理（大于等于m小于等于m+）的问题
        /// </summary>
        [Description("在范围内包含等于")]
        BetweenOrEqual = 7,

        /// <summary>
        /// 处理In的问题
        /// </summary>
        [Description("在此范围内")]
        StdIn = 8,

        /// <summary>
        /// 开头等于(仅String类型可用)
        /// </summary>
        [Description("开头等于")]
        StartWith = 9,

        /// <summary>
        /// 结尾等于(仅String类型可用)
        /// </summary>
        [Description("结尾等于")]
        EndWith = 10,

        /// <summary>
        /// 处理Like的问题(仅String类型可用)
        /// </summary>
        [Description("包含")]
        Contains = 11
    }

    /// <summary>
    /// 数值型比较运算符
    /// </summary>
    public enum MNCompareSymbol
    {
        /// <summary>
        /// 等于
        /// </summary>
        [Description("等于")]
        Equal = 0,

        /// <summary>
        /// 小于
        /// </summary>
        [Description("小于")]
        LessThan = 1,

        /// <summary>
        /// 大于
        /// </summary>
        [Description("大于")]
        GreaterThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("小于等于")]
        LessThanOrEqual = 3,

        /// <summary>
        /// 大于等于
        /// </summary>
        // [GlobalCode(">=", OnlyAttribute = true)]
        [Description("大于等于")]
        GreaterThanOrEqual = 4,

        /// <summary>
        /// 不等于
        /// </summary>
        [Description("不等于")]
        NotEqual = 5
    }

    /// <summary>
    /// 字符型比较运算符
    /// </summary>
    public enum MSCompareSymbol
    {
        /// <summary>
        /// 等于
        /// </summary>
        [Description("等于")]
        Equal = 0,

        /// <summary>
        /// 小于
        /// </summary>
        [Description("小于")]
        LessThan = 1,

        /// <summary>
        /// 大于
        /// </summary>
        [Description("大于")]
        GreaterThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        [Description("小于等于")]
        LessThanOrEqual = 3,

        /// <summary>
        /// 大于等于
        /// </summary>
        // [GlobalCode(">=", OnlyAttribute = true)]
        [Description("大于等于")]
        GreaterThanOrEqual = 4,

        /// <summary>
        /// 不等于
        /// </summary>
        [Description("不等于")]
        NotEqual = 5,

        /// <summary>
        /// 开头等于(仅String类型可用)
        /// </summary>
        [Description("开头等于")]
        StartWith = 6,

        /// <summary>
        /// 结尾等于(仅String类型可用)
        /// </summary>
        [Description("结尾等于")]
        EndWith = 7,

        /// <summary>
        /// 处理Like的问题(仅String类型可用)
        /// </summary>
        [Description("包含")]
        Contains = 8
    }
}
