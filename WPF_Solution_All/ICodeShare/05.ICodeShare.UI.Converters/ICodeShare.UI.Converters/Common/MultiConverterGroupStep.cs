﻿using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Markup;

namespace ICodeShare.UI.Converters.Common
{
    /// <summary>
    /// Represents a single step in a <see cref="MultiConverterGroup"/>.
    /// </summary>
    [ContentProperty("Converters")]
    public class MultiConverterGroupStep
    {
        #region  Members 成员变量
        private readonly Collection<IMultiValueConverter> converters;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// Initializes a new instance of the MultiConverterGroupStep class.
        /// </summary>
        public MultiConverterGroupStep()
        {
            this.converters = new Collection<IMultiValueConverter>();
        }
        #endregion

        #region  Properties 属性
        /// <summary>
        /// Gets the collection of <see cref="IMultiValueConverter"/>s in this <c>MultiConverterGroupStep</c>.
        /// </summary>
        public Collection<IMultiValueConverter> Converters
        {
            get { return this.converters; }
        }
        #endregion
    }
}
