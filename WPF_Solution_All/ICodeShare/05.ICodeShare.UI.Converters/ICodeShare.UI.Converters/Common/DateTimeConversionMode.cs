﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Converters.Common
{
    /// <summary>
    /// 声明不同时间格式间如何转换
    /// </summary>
    public enum DateTimeConversionMode
    {
        /// <summary>
        /// DateTime.ToLocalTime或者DateTime.ToUniversalTime方法会被调用，时间格式更改且进行时区转换.
        /// </summary>
        DoConversion,

        /// <summary>
        /// DateTime.SpecifyKind方法被调用，时间格式更改，时间不进行时区转换。
        /// </summary>
        SpecifyKindOnly
    }
}
