﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Converters.Common
{
    /// <summary>
    /// 字符串大小写转换格式
    /// </summary>
    public enum CharacterCasing
    {
        /// <summary>
        /// 正常不需要进行转换
        /// </summary>
        Normal,

        /// <summary>
        /// 转换为小写
        /// </summary>
        Lower,

        /// <summary>
        /// 转换为大小
        /// </summary>
        Upper
    }
}
