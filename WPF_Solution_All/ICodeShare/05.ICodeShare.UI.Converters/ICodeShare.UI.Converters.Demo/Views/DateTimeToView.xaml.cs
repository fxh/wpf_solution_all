﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Converters.Demo.Views
{
    /// <summary>
    /// DateTimeToUTCView.xaml 的交互逻辑
    /// </summary>
    public partial class DateTimeToView : UserControl
    {
        public static readonly DependencyProperty DateTimeUtcProperty = DependencyProperty.Register("DateTimeUtc",
           typeof(DateTime),
           typeof(DateTimeToView));


        public DateTime DateTimeUtc
        {
            get
            {
                return (DateTime)GetValue(DateTimeUtcProperty);
            }
            set
            {
                SetValue(DateTimeUtcProperty, value);
            }
        }

        public DateTimeToView()
        {
            InitializeComponent();
            txtUTC1.Text = DateTime.Now.ToString("g");
        }

        private void updateDateTime_Click(object sender, EventArgs e)
        {
            DateTimeUtc = DateTime.UtcNow;
            txtUTC1.Text = DateTime.Now.ToString("g");
        }
    }
}
