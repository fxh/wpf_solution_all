﻿using System.Common;

namespace System.Windows
{
    public static class ThicknessExtension
    {
        public static Thickness Multiply(this Thickness thickness, double value)
        {
            return new Thickness(thickness.Left * value, thickness.Top * value, thickness.Right * value, thickness.Bottom * value);
        }

        public static double GetValue(this Thickness thickness, Side side)
        {
            switch (side)
            {
                case Side.Left:
                    return thickness.Left;

                case Side.Top:
                    return thickness.Top;

                case Side.Right:
                    return thickness.Right;

                case Side.Bottom:
                    return thickness.Bottom;

                default:
                    return double.NaN;
            }
        }

        public static void SetValue(ref Thickness thickness, Side side, double value)
        {
            switch (side)
            {
                case Side.Left:
                    thickness.Left = value;
                    break;

                case Side.Top:
                    thickness.Top = value;
                    break;

                case Side.Right:
                    thickness.Right = value;
                    break;

                case Side.Bottom:
                    thickness.Bottom = value;
                    break;
            }
        }

        public static Thickness ChangeValue(this Thickness thickness, Side side, double value)
        {
            Thickness result = thickness;
            SetValue(ref result, side, value);
            return result;
        }
    }
}