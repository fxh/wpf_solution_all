﻿using System.Windows.Controls;

namespace System.Windows
{
    public static class OrientationExtension
    {
        public static Orientation OrthogonalValue(this Orientation value)
        {
            return value == Orientation.Horizontal ? Orientation.Vertical : Orientation.Horizontal;
        }
    }
}