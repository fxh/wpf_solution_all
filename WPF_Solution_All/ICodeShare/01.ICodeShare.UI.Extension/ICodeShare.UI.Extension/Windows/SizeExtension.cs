﻿namespace System.Windows
{
    public static class SizeExtension
    {
        /// <summary>
        /// 是否长宽为零
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static bool IsZero(this Size size)
        {
            return size.Width == 0 && size.Height == 0;
        }

        /// <summary>
        /// 返回一个<see cref="Point"/>
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Point ToPoint(this Size size)
        {
            return new Point(size.Width, size.Height);
        }

        public static Rect ToRect(this Size size)
        {
            return new Rect(0, 0, size.Width, size.Height);
        }
    }
}