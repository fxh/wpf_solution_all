﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;

namespace System.Windows
{
    /// <summary>
    /// Helper methods for UI-related tasks.
    /// This class was obtained from Philip Sumi (a fellow WPF Disciples blog)
    /// http://www.hardcodet.net/uploads/2009/06/UIHelper.cs
    /// </summary>
    public static class DependencyObjectExtension
    {
        private struct DependencyPropertyValueInfo
        {
            private object _LocalValue;
            private object _Value;

            public DependencyPropertyValueInfo(DependencyObject o, DependencyProperty property)
            {
                _LocalValue = o.ReadLocalValue(property);
                _Value = o.GetValue(property);
            }

            public override bool Equals(object obj)
            {
                if (obj is DependencyPropertyValueInfo)
                {
                    var info = (DependencyPropertyValueInfo)obj;
                    return IsAssigned == info.IsAssigned &&
                        (_Value == null && info._Value == null || _Value != null && _Value.Equals(info._Value));
                }
                else
                    return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public void RestorePropertyValue(DependencyObject o, DependencyProperty property)
            {
                if (IsAssigned)
                    if (_LocalValue is BindingExpressionBase)
                        ((FrameworkElement)o).SetBinding(property, ((BindingExpressionBase)_LocalValue).ParentBindingBase);
                    else
                        o.SetValue(property, _LocalValue);
                else
                {
                    o.ClearValue(property);
                    o.InvalidateProperty(property);
                }
            }

            private bool IsAssigned { get { return _LocalValue != DependencyProperty.UnsetValue; } }
        }

        #region DataContext

        public static readonly DependencyProperty DataContextProperty =
          DependencyProperty.RegisterAttached("DataContext", typeof(object), typeof(DependencyObjectExtension), null);

        public static object GetDataContext(DependencyObject obj)
        {
            return obj.GetValue(DataContextProperty);
        }

        public static void SetDataContext(DependencyObject obj, object value)
        {
            obj.SetValue(DataContextProperty, value);
        }

        #endregion DataContext

        #region Bool 判断

        public static bool HasDefaultValue(this DependencyObject o, DependencyProperty property)
        {
            object value = o.GetValue(property);
            object defaultValue = property.GetMetadata(o.GetType()).DefaultValue;
            return object.Equals(value, defaultValue) || defaultValue == DependencyProperty.UnsetValue && value == null;
        }

        public static bool IsPropertyAssigned(this DependencyObject o, DependencyProperty property)
        {
            return o.ReadLocalValue(property) != DependencyProperty.UnsetValue;
        }

        public static bool IsPropertySet(this DependencyObject o, DependencyProperty property)
        {
            return o.IsPropertyAssigned(property) || !o.HasDefaultValue(property);
        }

        #endregion Bool 判断

        public static void ReadPropertyFromXML(this DependencyObject o, XmlReader xml, DependencyProperty property, string propertyName, Type propertyType)
        {
            object localValue = o.ReadLocalValue(property);
            if (localValue is BindingExpression)
            {
                BindingMode mode = ((BindingExpression)localValue).ParentBinding.Mode;
                if (mode == BindingMode.Default)
                {
                    var metadata = property.GetMetadata(o) as FrameworkPropertyMetadata;
                    mode = metadata != null && metadata.BindsTwoWayByDefault ? BindingMode.TwoWay : BindingMode.OneWay;
                }
                if (!(mode == BindingMode.TwoWay || mode == BindingMode.OneWayToSource))
                    return;
            }
            if (xml[propertyName] != null)
            {
                string s = xml[propertyName];
                object value;
                if (propertyType.IsEnum)
                    value = Enum.Parse(propertyType, s, true);
                else
                    if (typeof(FrameworkElement).IsAssignableFrom(propertyType))
                        value = o is FrameworkElement ? ((FrameworkElement)o).FindName(s) : null;
                    else
                    {
                        value = null;
                        var typeConverterAttributes = propertyType.GetCustomAttributes(typeof(TypeConverterAttribute), true);
                        if (typeConverterAttributes.Length > 0)
                        {
                            var converterType = Type.GetType(((TypeConverterAttribute)typeConverterAttributes[0]).ConverterTypeName);
                            if (converterType != null)
                            {
                                var converter = (TypeConverter)converterType.GetConstructor(Type.EmptyTypes).Invoke(null);
                                if (converter.CanConvertFrom(typeof(string)))
                                    if (propertyType == typeof(Thickness))
                                        value = converter.ConvertFromInvariantString(s);
                                    else
                                        value = converter.ConvertFromString(s);
                            }
                        }
                        if (value == null)
                        {
                            value = ((IConvertible)s).ToType(propertyType, CultureInfo.InvariantCulture);
                            if (value is string && localValue != null && localValue != DependencyProperty.UnsetValue &&
                                !(localValue is string) && !(localValue is BindingExpression))
                                return;
                        }
                    }
                o.SetValue(property, value);
            }
            else
                o.ClearValue(property);
        }

        public static void WritePropertyToXML(this DependencyObject o, XmlWriter xml, DependencyProperty property, string propertyName)
        {
            if (!o.IsPropertyAssigned(property))
                return;
            object value = o.GetValue(property);
            string s;
            if (value == null)
                s = null;
            else
                if (value is double)
                    s = ((double)value).ToString(CultureInfo.InvariantCulture);
                else
                    if (value is FrameworkElement)
                        s = ((FrameworkElement)value).Name;
                    else
                        s = value.ToString();
            xml.WriteAttributeString(propertyName, s);
        }

        public static object GetCoerceOldValue(this DependencyObject o, DependencyProperty dp)
        {
            return o.GetValue(dp);
        }

        public static void SetBinding(this FrameworkElement o, DependencyObject bindingSource, string bindingPath, DependencyProperty property)
        {
            Binding b = new Binding(bindingPath);
            b.Source = bindingSource;
            o.SetBinding(property, b);
        }

        public static void SetCurrentValueIfDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            if (!o.IsPropertyAssigned(property))
                o.SetCurrentValue(property, value);
        }

        public static void SetValueIfDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            if (!o.IsPropertyAssigned(property))
                o.SetValue(property, value);
        }

        public static void SetValueIfNotDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            object defaultValue = property.GetMetadata(o.GetType()).DefaultValue;
            if (value == defaultValue || value == null && defaultValue == DependencyProperty.UnsetValue)
            {
                if (o.IsPropertyAssigned(property))
                    o.ClearValue(property);
            }
            else
                o.SetValue(property, value);
        }

        public static object StorePropertyValue(this DependencyObject o, DependencyProperty property)
        {
            return new DependencyPropertyValueInfo(o, property);
        }

        public static object StoreAndAssignPropertyValue(this DependencyObject o, DependencyProperty property)
        {
            var result = o.StorePropertyValue(property);
            o.SetValue(property, o.GetValue(property));
            return result;
        }

        public static void RestorePropertyValue(this DependencyObject o, DependencyProperty property, object storedInfo)
        {
            ((DependencyPropertyValueInfo)storedInfo).RestorePropertyValue(o, property);
        }

        public static UIElement GetElementByName(this DependencyObject owner, string elementName)
        {
            if (VisualTreeHelper.GetChildrenCount(owner) == 0)
                return null;
            FrameworkElement element = (FrameworkElement)VisualTreeHelper.GetChild(owner, 0);
            return element.FindName(elementName) as UIElement;
        }

        public static T FindElementByTypeInParents<T>(this DependencyObject element, DependencyObject stopElement) where T : FrameworkElement
        {
            if (element == null || element == stopElement)
                return null;
            if (element is T)
                return element as T;
            return VisualTreeHelper.GetParent(element).FindElementByTypeInParents<T>(stopElement);
        }

        public static bool FindIsInParents(this DependencyObject child, DependencyObject parent)
        {
            if (child == null) return false;
            if (parent == null) return false;
            if (VisualTreeHelper.GetParent(child) == null) return false;
            if (child == parent) return true;
            return FindIsInParents(VisualTreeHelper.GetParent(child), parent);
        }

        public static bool IsInDesignTool(this DependencyObject o)
        {
            return DesignerProperties.GetIsInDesignMode(o);
        }

        #region Get

        #region Parent
        /// <summary>
        /// Finds immediate parent of the child control
        /// </summary>
        /// <typeparam name="T">Finds specific Type of parent control</typeparam>
        /// <param name="child">Child control in use</param>
        /// <returns></returns>
        public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = child.GetParentObject();// VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            var parent = parentObject as T;
            return parent ?? TryFindParent<T>(parentObject);
        }

        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Keep in mind that for content element,
        /// this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise
        /// null.</returns>
        public static DependencyObject GetParentObject(this DependencyObject child)
        {
            if (child == null) return null;

            //handle content elements separately
            var contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                var fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            //also try searching for parent in framework elements (such as DockPanel, etc)
            var frameworkElement = child as FrameworkElement;
            if (frameworkElement != null)
            {
                DependencyObject parent = frameworkElement.Parent;
                if (parent != null) return parent;
            }

            //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }

        #endregion

        #region Child
        /// <summary>
        /// Finds child of specific type of specific name
        /// </summary>
        /// <typeparam name="T">Type of child</typeparam>
        /// <param name="parent">Current parent control</param>
        /// <param name="childName">Name of the child control to be found</param>
        /// <returns></returns>
        public static T FindChild<T>(this DependencyObject parent, string childName) where T : DependencyObject
        {
            // Confirm parent and childName are valid.
            if (parent == null) return null;
            T foundChild = null;
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);

                // If the child is not of the request child type child
                var childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);
                    // If the child is found, break so we do not overwrite the found child.
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child control found.
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }

        #endregion

        #endregion

        /// <summary>
        /// Analyzes both visual and logical tree in order to find all elements of a given
        /// type that are descendants of the <paramref name="source"/> item.
        /// </summary>
        /// <typeparam name="T">The type of the queried items.</typeparam>
        /// <param name="source">The root element that marks the source of the search. If the
        /// source is already of the requested type, it will not be included in the result.</param>
        /// <param name="forceUsingTheVisualTreeHelper">Sometimes it's better to search in the VisualTree (e.g. in tests)</param>
        /// <returns>All descendants of <paramref name="source"/> that match the requested type.</returns>
        public static IEnumerable<T> FindChildren<T>(this DependencyObject source, bool forceUsingTheVisualTreeHelper = false) where T : DependencyObject
        {
            if (source != null)
            {
                var childs = GetChildObjects(source, forceUsingTheVisualTreeHelper);
                foreach (DependencyObject child in childs)
                {
                    //analyze if children match the requested type
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    //recurse tree
                    foreach (T descendant in FindChildren<T>(child))
                    {
                        yield return descendant;
                    }
                }
            }
        }



        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetChild"/> method, which also
        /// supports content elements. Keep in mind that for content elements,
        /// this method falls back to the logical tree of the element.
        /// </summary>
        /// <param name="parent">The item to be processed.</param>
        /// <param name="forceUsingTheVisualTreeHelper">Sometimes it's better to search in the VisualTree (e.g. in tests)</param>
        /// <returns>The submitted item's child elements, if available.</returns>
        public static IEnumerable<DependencyObject> GetChildObjects(this DependencyObject parent, bool forceUsingTheVisualTreeHelper = false)
        {
            if (parent == null) yield break;

            if (!forceUsingTheVisualTreeHelper && (parent is ContentElement || parent is FrameworkElement))
            {
                //use the logical tree for content / framework elements
                foreach (object obj in LogicalTreeHelper.GetChildren(parent))
                {
                    var depObj = obj as DependencyObject;
                    if (depObj != null) yield return (DependencyObject)obj;
                }
            }
            else
            {
                //use the visual tree per default
                int count = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < count; i++)
                {
                    yield return VisualTreeHelper.GetChild(parent, i);
                }
            }
        }

        //or

        /// <summary>
        /// Get collection of child controls of specific types
        /// </summary>
        /// <typeparam name="T">Type of controls to be fetched</typeparam>
        /// <param name="parent">Current parent</param>
        /// <returns></returns>
        //public static List<T> GetVisualChildCollection<T>(this DependencyObject parent) where T : DependencyObject
        //{
        //    var visualCollection = new List<T>();
        //    GetVisualChildCollection(parent, visualCollection);
        //    return visualCollection;
        //}


        //private static void GetVisualChildCollection<T>(DependencyObject parent, List<T> visualCollection)
        //    where T : DependencyObject
        //{
        //    int count = VisualTreeHelper.GetChildrenCount(parent);
        //    for (int i = 0; i < count; i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(parent, i);
        //        if (child is T)
        //        {
        //            visualCollection.Add(child as T);
        //        }
        //        else if (child != null)
        //        {
        //            GetVisualChildCollection(child, visualCollection);
        //        }
        //    }
        //}

        /// <summary>
        /// Tries to locate a given item within the visual tree,
        /// starting with the dependency object at a given position. 
        /// </summary>
        /// <typeparam name="T">The type of the element to be found
        /// on the visual tree of the element at the given location.</typeparam>
        /// <param name="reference">The main element which is used to perform
        /// hit testing.</param>
        /// <param name="point">The position to be evaluated on the origin.</param>
        public static T TryFindFromPoint<T>(UIElement reference, Point point)
            where T : DependencyObject
        {
            var element = reference.InputHitTest(point) as DependencyObject;

            if (element == null)
                return null;
            if (element is T)
                return (T)element;
            return TryFindParent<T>(element);
        }
    }
}