﻿using System.Windows.Interop;

namespace System.Windows
{
    public static class InteropExtension
    {
        public static bool? ShowDialog(this Window win, IntPtr handle)
        {
            var helper = new WindowInteropHelper(win) { Owner = handle };
            return win.ShowDialog();
        }
    }
}