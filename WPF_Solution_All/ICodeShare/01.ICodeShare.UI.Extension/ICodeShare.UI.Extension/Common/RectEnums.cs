﻿namespace System.Common
{
    /// <summary>
    ///
    /// </summary>
    public enum RectCorner { TopLeft, TopRight, BottomRight, BottomLeft }

    public enum RectKeyPoint { TopLeft, TopRight, BottomRight, BottomLeft, LeftMiddle, RightMiddle, TopMiddle, BottomMiddle }

    public enum RectTruncatorResultType { InvalidLine, MindlessCorner, Path, Rectangle, TangentLine }
}