﻿namespace System.Common
{
    [AttributeUsage(AttributeTargets.Field)]
    public class IgnoreDependencyPropertiesConsistencyCheckerAttribute : Attribute
    {
    }
}