﻿using ICodeShare.UI.Reports.Barcode;
using ICodeShare.UI.Reports.Interfaces;

namespace ICodeShare.UI.Reports.Document.Barcode
{
    /// <summary>
    /// Table cell barcode C128
    /// </summary>
    public class TableCellBarcodeC128 : BarcodeC128, ITableCellValue
    {
    }
}
