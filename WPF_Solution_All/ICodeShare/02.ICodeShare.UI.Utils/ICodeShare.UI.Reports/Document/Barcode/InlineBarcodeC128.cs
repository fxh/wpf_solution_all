﻿using ICodeShare.UI.Reports.Barcode;
using ICodeShare.UI.Reports.Interfaces;
using System.Windows;

namespace ICodeShare.UI.Reports.Document.Barcode
{
    /// <summary>
    /// Inline barcode C128
    /// </summary>
    public class InlineBarcodeC128 : BarcodeC128, IInlineDocumentValue
    {
    }
}
