﻿using System.Windows.Documents;
using ICodeShare.UI.Reports.Interfaces;

namespace ICodeShare.UI.Reports.Document
{
    /// <summary>
    /// Class for fillable table row values
    /// </summary>
    public class TableRowForDataTable : TableRow, ITableRowForDataTable
    {
        private string _tableName;
        /// <summary>
        /// Gets or sets the table name
        /// </summary>
        public string TableName
        {
            get { return _tableName; }
            set { _tableName = value; }
        }
    }
}
