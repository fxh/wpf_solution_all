﻿namespace ICodeShare.UI.Reports.Interfaces
{
    /// <summary>
    /// Interface for inline context values
    /// </summary>
    public interface IInlineContextValue : IPropertyValue, IAggregateValue
    {
    }
}
