﻿namespace ICodeShare.UI.Reports.Interfaces
{
    /// <summary>
    /// Interface for inline property values
    /// </summary>
    public interface IInlinePropertyValue : IPropertyValue
    {
    }
}
