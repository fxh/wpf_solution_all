﻿namespace ICodeShare.UI.Reports.Interfaces
{
    /// <summary>
    /// Interface for table cell values
    /// </summary>
    public interface ITableCellValue : IHasValue, IAggregateValue
    {
    }
}
