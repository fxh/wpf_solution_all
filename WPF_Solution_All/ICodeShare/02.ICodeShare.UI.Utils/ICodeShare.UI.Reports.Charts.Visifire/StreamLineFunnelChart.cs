﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stream line funnel chart
    /// </summary>
    public class StreamLineFunnelChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StreamLineFunnelChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StreamLineFunnel;
        }
    }
}
