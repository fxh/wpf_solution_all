﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked column 100 chart
    /// </summary>
    public class StackedColumn100Chart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedColumn100Chart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedColumn100;
        }
    }
}
