﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a area chart
    /// </summary>
    public class AreaChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public AreaChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Area;
        }
    }
}
