﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a doughnut stick chart
    /// </summary>
    public class DoughnutChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DoughnutChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Doughnut;
        }
    }
}
