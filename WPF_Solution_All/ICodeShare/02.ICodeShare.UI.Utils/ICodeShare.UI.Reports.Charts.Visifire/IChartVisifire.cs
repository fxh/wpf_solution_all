﻿using ICodeShare.UI.Reports.Interfaces;

namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Interface for all Visifire charts
    /// </summary>
    public interface IChartVisifire : IChart
    {
    }
}
