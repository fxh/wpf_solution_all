﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a bubble chart
    /// </summary>
    public class BubbleChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public BubbleChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Bubble;
        }
    }
}
