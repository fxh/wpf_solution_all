﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a point chart
    /// </summary>
    public class PointChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PointChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Point;
        }
    }
}
