﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked area chart
    /// </summary>
    public class StackedAreaChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedAreaChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedArea;
        }
    }
}
