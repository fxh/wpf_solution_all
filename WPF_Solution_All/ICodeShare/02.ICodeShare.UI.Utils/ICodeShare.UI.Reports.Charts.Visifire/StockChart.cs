﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stock chart
    /// </summary>
    public class StockChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StockChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Stock;
        }
    }
}
