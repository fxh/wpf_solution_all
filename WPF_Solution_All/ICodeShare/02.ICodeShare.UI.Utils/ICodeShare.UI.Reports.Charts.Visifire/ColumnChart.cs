﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a column chart
    /// </summary>
    public class ColumnChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ColumnChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Column;
        }
    }
}
