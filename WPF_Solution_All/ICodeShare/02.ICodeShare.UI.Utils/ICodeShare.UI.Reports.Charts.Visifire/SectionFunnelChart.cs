﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a section funnel chart
    /// </summary>
    public class SectionFunnelChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SectionFunnelChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.SectionFunnel;
        }
    }
}
