﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked bar chart
    /// </summary>
    public class StackedBarChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedBarChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedBar;
        }
    }
}
