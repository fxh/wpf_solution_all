﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked area 100 chart
    /// </summary>
    public class StackedArea100Chart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedArea100Chart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedArea100;
        }
    }
}
