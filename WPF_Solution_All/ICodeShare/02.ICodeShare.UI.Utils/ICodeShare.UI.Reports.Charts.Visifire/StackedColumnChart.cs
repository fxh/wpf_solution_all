﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked column chart
    /// </summary>
    public class StackedColumnChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedColumnChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedColumn;
        }
    }
}
