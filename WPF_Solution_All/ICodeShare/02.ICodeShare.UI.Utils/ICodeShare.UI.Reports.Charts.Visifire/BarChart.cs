﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a bar chart
    /// </summary>
    public class BarChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public BarChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Bar;
        }
    }
}
