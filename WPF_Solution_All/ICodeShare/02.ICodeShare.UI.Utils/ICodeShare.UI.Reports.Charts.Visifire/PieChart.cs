﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a pie chart
    /// </summary>
    public class PieChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PieChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Pie;
        }
    }
}
