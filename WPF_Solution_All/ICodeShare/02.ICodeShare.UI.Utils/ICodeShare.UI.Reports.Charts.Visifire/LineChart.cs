﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a line chart
    /// </summary>
    public class LineChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LineChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.Line;
        }
    }
}
