﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a candle stick chart
    /// </summary>
    public class CandleStickChart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CandleStickChart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.CandleStick;
        }
    }
}
