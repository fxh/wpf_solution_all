﻿namespace ICodeShare.UI.Reports.Charts.Visifire
{
    /// <summary>
    /// Represents a stacked bar 100 chart
    /// </summary>
    public class StackedBar100Chart : ChartBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StackedBar100Chart()
        {
            RenderAs = global::Visifire.Charts.RenderAs.StackedBar100;
        }
    }
}
