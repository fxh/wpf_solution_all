﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace  Extended.WPF.Core
{
    public delegate void ThemeChangingRoutedEventHandler(DependencyObject sender, ThemeChangingRoutedEventArgs e);
    public delegate void ThemeChangedRoutedEventHandler(DependencyObject sender, ThemeChangedRoutedEventArgs e);
    public class ThemeRoutedEventArgs : RoutedEventArgs
    {
        public ThemeRoutedEventArgs(string themeName)
        {
            ThemeName = themeName;
        }
        public string ThemeName { get; private set; }
    }
    public class ThemeChangedRoutedEventArgs : ThemeRoutedEventArgs
    {
        public ThemeChangedRoutedEventArgs(string themeName) : base(themeName) { }
    }
    public class ThemeChangingRoutedEventArgs : ThemeRoutedEventArgs
    {
        public ThemeChangingRoutedEventArgs(string themeName) : base(themeName) { }
    }
}
