﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Extented.UI.Core
{
    internal static class ThemeNameHelper
    {
        public static readonly string ThemeAssemblyPrefix = "/DevExpress.Xpf.Themes.";
        public static string GetThemeName(IServiceProvider serviceProvider)
        {
            IUriContext uriContext = (IUriContext)serviceProvider.GetService(typeof(IUriContext));
            if (uriContext != null && uriContext.BaseUri != null && uriContext.BaseUri.IsAbsoluteUri)
                return EnsureThemeName(uriContext.BaseUri);
            return null;
        }
        public static string GetAssemblyName(IServiceProvider serviceProvider)
        {
            IUriContext uriContext = (IUriContext)serviceProvider.GetService(typeof(IUriContext));
            if (uriContext != null && uriContext.BaseUri != null && uriContext.BaseUri.IsAbsoluteUri && uriContext.BaseUri.LocalPath.Contains(';'))
                return uriContext.BaseUri.LocalPath.Split(';')[0];
            return null;
        }
        static string EnsureThemeName(Uri baseUri)
        {
            try
            {
                string localPath = baseUri.OriginalString;
                if (string.IsNullOrEmpty(localPath)) return null;
                int startIndex = localPath.IndexOf(ThemeAssemblyPrefix);
                if (startIndex >= 0)
                {
                    startIndex += ThemeAssemblyPrefix.Length;
                    int endIndex = localPath.IndexOf('.', startIndex);
                    if (endIndex > startIndex) return localPath.Substring(startIndex, endIndex - startIndex);
                }
            }
            catch
            {
            }
            return null;
        }
    }
}
