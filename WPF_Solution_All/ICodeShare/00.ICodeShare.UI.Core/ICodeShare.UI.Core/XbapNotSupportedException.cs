﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core
{
    [Serializable]
    public class XbapNotSupportedException : ApplicationException
    {
        internal const string Text = @"Starting from v2011 vol 2, XBAP applications are not supported. Instead, we recommend that you use ClickOnce deployment (the most preferable way) or migrate your application to Silverlight. For more information, please refer to http://go.devexpress.com/SupportXBAP.aspx

If you still want to continue using DevExpress controls in XBAP applications, set the OptionsXBAP.SuppressNotSupportedException property to True.";
        public XbapNotSupportedException(string message)
            : base(message)
        {
        }
        [System.Security.SecuritySafeCritical]
        public XbapNotSupportedException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
