﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Extented.UI.Core
{
    public class ThemePartLoaderExtension : MarkupExtension
    {
        public string AssemblyName { get; set; }
        public string Path { get; set; }
        public string PathCore { get; set; }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            string path = Path;
            string themeName = ThemeNameHelper.GetThemeName(serviceProvider);
            if (String.IsNullOrEmpty(themeName))
                return new Uri(string.Format(ThemeNameHelper.GetAssemblyName(serviceProvider) + ";component{0}", PathCore), UriKind.RelativeOrAbsolute);
            return new Uri(string.Format(ThemeNameHelper.ThemeAssemblyPrefix + "{0}" + AssemblyInfo.VSuffix + ";component{1}", themeName, path), UriKind.RelativeOrAbsolute);
        }
    }
}
