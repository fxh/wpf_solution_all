﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core
{
    public class ThemePartKeyExtension : ThemePartLoaderExtension
    {
        internal Uri Uri { get; set; }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            Uri = base.ProvideValue(serviceProvider) as Uri;
            return this;
        }
        public override int GetHashCode()
        {
            if (AssemblyName == null)
                return 0;
            return AssemblyName.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            ThemePartKeyExtension key = obj as ThemePartKeyExtension;
            if (key == null)
                return false;
            return string.Equals(AssemblyName, key.AssemblyName, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
