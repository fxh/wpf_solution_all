﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
namespace Extended.WPF.Core
{
    public class ThemeExtension : MarkupExtension
    {
        public ThemeExtension() { }
        public string Name { get; set; }
        public string AssemblyName { get; set; }
        public string Version { get; set; }
        public string PublicKeyToken { get; set; }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            Theme existantTheme = Theme.FindTheme(Name);
            if (existantTheme != null)
                return existantTheme;
            Theme theme = new Theme(Name) { AssemblyName = this.AssemblyName, PublicKeyToken = this.PublicKeyToken, Version = this.Version };
            Theme.RegisterTheme(theme);
            return theme;
        }
    }
}
