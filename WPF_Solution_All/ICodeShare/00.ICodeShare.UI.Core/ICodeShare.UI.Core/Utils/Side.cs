﻿using System;
namespace Extented.UI.Core.Utils
{
    [Flags]
    public enum Side
    {
        Left = 0x0,
        Top = 0x1,
        Right = 0x2,
        Bottom = 0x4,
        LeftRight = Left | Right,
        TopBottom = Top | Bottom,
        All = LeftRight | TopBottom,
    }
}