﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Utils
{
    public static class ComparingUtils
    {
        public static int CompareDoubles(double number1, double number2, double epsilon)
        {
            double difference = number1 - number2;
            if (Math.Abs(difference) <= epsilon)
                return 0;
            return difference > 0 ? 1 : -1;
        }
        public static int CompareDoubleArrays(double[] numbers1, double[] numbers2, double epsilon)
        {
            if (numbers1.Length != numbers2.Length)
                throw new ArgumentException();
            for (int i = 0; i < numbers1.Length; i++)
            {
                int result = CompareDoubles(numbers1[i], numbers2[i], epsilon);
                if (result != 0)
                    return result;
            }
            return 0;
        }
    }
}
