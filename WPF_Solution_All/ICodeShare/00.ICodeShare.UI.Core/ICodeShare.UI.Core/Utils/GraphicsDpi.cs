﻿using Extented.UI.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Utils
{
    public class GraphicsDpi
    {
        public const float Display = 75f,
            Inch = 1f,
            Document = 300f,
            Millimeter = 25.4f,
            Point = 72f,
            HundredthsOfAnInch = 100f,
            TenthsOfAMillimeter = 254f,
            Twips = 1440f,
            EMU = 914400f,
            DeviceIndependentPixel = 96f;
        public static readonly float Pixel = 96f;
        static GraphicsDpi()
        {
#if !SL
            if (!PSNativeMethods.AspIsRunning)
                using (Bitmap bmp = new Bitmap(1, 1))
                using (Graphics graph = Graphics.FromImage(bmp))
                    Pixel = graph.DpiX;
#endif
        }
        public static float GetGraphicsDpi(Graphics gr)
        {
            if (gr.PageUnit == GraphicsUnit.Display)
                return gr.DpiX;
            return UnitToDpi(gr.PageUnit);
        }
        public static float UnitToDpi(GraphicsUnit unit)
        {
            switch (unit)
            {
                case GraphicsUnit.Display:
                    return Display;
                case GraphicsUnit.Inch:
                    return Inch;
                case GraphicsUnit.Document:
                    return Document;
                case GraphicsUnit.Millimeter:
                    return Millimeter;
                case GraphicsUnit.Pixel:
                case GraphicsUnit.World:
                    return Pixel;
                case GraphicsUnit.Point:
                    return Point;
            }
            throw new ArgumentException("unit");
        }
        public static float UnitToDpiI(GraphicsUnit unit)
        {
            switch (unit)
            {
                case GraphicsUnit.Display:
                    return Display;
                case GraphicsUnit.Inch:
                    return Inch;
                case GraphicsUnit.Document:
                    return Document;
                case GraphicsUnit.Millimeter:
                    return Millimeter;
                case GraphicsUnit.Pixel:
                case GraphicsUnit.World:
                    return DeviceIndependentPixel;
                case GraphicsUnit.Point:
                    return Point;
            }
            throw new ArgumentException("unit");
        }
        public static GraphicsUnit DpiToUnit(float dpi)
        {
            if (dpi.Equals(Display))
                return GraphicsUnit.Display;
            if (dpi.Equals(Inch))
                return GraphicsUnit.Inch;
            if (dpi.Equals(Document))
                return GraphicsUnit.Document;
            if (dpi.Equals(Millimeter))
                return GraphicsUnit.Millimeter;
            if (dpi.Equals(Pixel))
                return GraphicsUnit.Pixel;
            if (dpi.Equals(Point))
                return GraphicsUnit.Point;
            throw new ArgumentException("dpi");
        }
    }
}
