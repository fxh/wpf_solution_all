﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Themes
{
    public abstract class ThemeKeyExtensionInternalBase<T> : ThemeKeyExtensionGeneric, IDisposable
    {
        bool isVisibleInBlendCore = true;
        public T ResourceKey
        {
            get { return (T)ResourceKeyCore; }
            set { SetResourceKey(value); }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsVisibleInBlend
        {
            get { return isVisibleInBlendCore; }
            set { isVisibleInBlendCore = value; }
        }
        protected ThemeKeyExtensionInternalBase()
            : base()
        {
        }
        protected override bool Equals(ThemeKeyExtensionGeneric other)
        {
            var baseResult = base.Equals(other);
            if (!baseResult)
                return false;
            var key = (ThemeKeyExtensionInternalBase<T>)other;
            return object.Equals(ResourceKeyCore, key.ResourceKeyCore);
        }
        void SetResourceKey(T value)
        {
            if (value == null)
                throw new NullReferenceException("ThemeKeyExtensionBase");
            if (typeof(T).IsEnum && !Enum.IsDefined(typeof(T), value))
                throw new ArgumentException("Resource key isn`t defined");
            ResourceKeyCore = value;
            SetHashCode();
        }
        public override string ToString()
        {
            return GetType().Name + "_" + ResourceKeyCore;
        }
        protected override int GenerateHashCode()
        {
            return base.GenerateHashCode() ^ typeof(T).GetHashCode() ^ ResourceKeyCore.GetHashCode();
        }
        #region IDisposable Members
        void IDisposable.Dispose()
        {
        }
        #endregion
    }
}
