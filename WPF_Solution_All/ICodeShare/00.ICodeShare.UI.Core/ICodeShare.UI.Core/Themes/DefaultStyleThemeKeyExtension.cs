﻿using Extended.WPF.Core;
using Infrastructure.Utils.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Markup;

namespace Extented.UI.Core.Themes
{
    public class DefaultStyleThemeKeyExtension : ThemeKeyExtensionInternalBase<string>
    {
        string fullName;
        string traceString = null;
        public DefaultStyleThemeKeyExtension() { }
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string FullName
        {
            get { return fullName; }
            set { SetFullName(value); }
        }
        public Type Type { set { SetFullName(value != null ? value.FullName : null); } }
        internal protected string AssemblyName { get; set; }
        protected virtual bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(FullName) && ThemeName != null;
            }
        }
        protected override int GenerateHashCode()
        {
            return base.GenerateHashCode() ^ (FullName == null ? 0 : FullName.GetHashCode());
        }
        protected override bool Equals(ThemeKeyExtensionGeneric other)
        {
            var baseResult = base.Equals(other);
            if (!baseResult)
                return false;
            var themeKey = (DefaultStyleThemeKeyExtension)other;
            return FullName == themeKey.FullName && object.Equals(Assembly, themeKey.Assembly);
        }
        void SetFullName(string value)
        {
            this.fullName = value;
            SetHashCode();
        }
        public override Assembly Assembly
        {
            get
            {
                if (AssemblyName != null)
                {
                    Assembly assembly = AssemblyHelper.GetLoadedAssembly(AssemblyName);
                    if (assembly != null)
                        return assembly;
                }
                return base.Assembly;
            }
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (IsInitialized(serviceProvider))
            {
                RegisterThemeType();
                return this;
            }
            return base.ProvideValue(serviceProvider);
        }
        void RegisterThemeType()
        {
            SetHashCode();
            if (!IsValid)
                return;
            GenerateTrace();
            TraceHelper.Write(ThemeManager.TraceSwitch, "Registering ThemedType", this);
            ThemedElementsDictionary.RegisterThemeType(ThemeName, FullName, this);
        }
        bool IsInitialized(IServiceProvider serviceProvider)
        {
            IProvideValueTarget provideValueTarget = (IProvideValueTarget)serviceProvider.GetService(typeof(IProvideValueTarget));
            return provideValueTarget != null && provideValueTarget.TargetObject != null;
        }
        [System.Diagnostics.Conditional("DEBUG")]
        void GenerateTrace()
        {
            StringBuilder traceStringBuilder = new StringBuilder();
            traceStringBuilder.Append(ThemeName);
            traceStringBuilder.Append(" ");
            traceStringBuilder.Append(FullName);
            traceStringBuilder.Append(" ");
            traceStringBuilder.Append((Assembly != null ? Assembly.FullName : null));
            traceStringBuilder.Append(" ");
            traceStringBuilder.Append((ResourceKeyCore != defaultResourceCore ? ResourceKeyCore.ToString() : null));
            traceStringBuilder.ToString();
            this.traceString = string.Intern(traceStringBuilder.ToString());
        }
        public override string ToString()
        {
            return traceString ?? base.ToString();
        }
    }
}
