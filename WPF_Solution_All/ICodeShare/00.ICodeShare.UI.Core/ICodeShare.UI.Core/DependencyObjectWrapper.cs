﻿using System.Windows;
using System;
using Extended.WPF.Core.Helpers;

namespace Extented.UI.Core
{
    public class DependencyObjectWrapper
    {
        readonly WeakReference wRef;
        public DependencyObjectWrapper(DependencyObject dObj)
        {
            wRef = new WeakReference(dObj);
        }
        public DependencyObject Object { get { return wRef.Target as DependencyObject; } }
        public ResourceDictionary Resources
        {
            get
            {
                if (Object is FrameworkElement)
                    return ((FrameworkElement)Object).Resources;
                if (Object is FrameworkContentElement)
                    return ((FrameworkContentElement)Object).Resources;
                throw new ArgumentException("Resources");
            }
        }
        public bool HasDefaultStyleKeyProperty { get { return (Object is FrameworkElement) || (Object is FrameworkContentElement); } }
        public bool IsLoaded
        {
            get
            {
                if (Object is FrameworkElement)
                    return ((FrameworkElement)Object).IsLoaded;
                if (Object is FrameworkContentElement)
                    return ((FrameworkContentElement)Object).IsLoaded;
                throw new ArgumentException("IsLoaded");
            }
        }
        public bool OverridesDefaultStyleKey
        {
            get
            {
                if (Object is FrameworkElement)
                    return ((FrameworkElement)Object).OverridesDefaultStyle;
                if (Object is FrameworkContentElement)
                    return ((FrameworkContentElement)Object).OverridesDefaultStyle;
                throw new ArgumentException("OverridesDefaultStyleKey");
            }
        }
        string ThemeName { get; set; }
        public object GetDefaultStyleKey()
        {
            if (Object is FrameworkElement)
                return ((FrameworkElement)Object).GetDefaultStyleKey();
            if (Object is FrameworkContentElement)
                return ((FrameworkContentElement)Object).GetDefaultStyleKey();
            throw new ArgumentException("DefaultStyleKey");
        }
        public void SetDefaultStyleKey(object value)
        {
            if (Object is FrameworkElement)
            {
                ((FrameworkElement)Object).SetDefaultStyleKey(value);
                return;
            }
            if (Object is FrameworkContentElement)
            {
                ((FrameworkContentElement)Object).SetDefaultStyleKey(value);
                return;
            }
            throw new ArgumentException("DefaultStyleKey");
        }
        public void AddHandler(RoutedEvent routedEvent, Delegate handler)
        {
            FrameworkElement uie = Object as FrameworkElement;
            if (uie != null)
            {
                uie.AddHandler(routedEvent, handler);
                return;
            }
            FrameworkContentElement fce = Object as FrameworkContentElement;
            if (fce != null)
            {
                fce.AddHandler(routedEvent, handler);
                return;
            }
            throw new ArgumentException("AddHandler");
        }
        public void RemoveHandler(RoutedEvent routedEvent, Delegate handler)
        {
            FrameworkElement uie = Object as FrameworkElement;
            if (uie != null)
            {
                uie.RemoveHandler(routedEvent, handler);
                return;
            }
            FrameworkContentElement fce = Object as FrameworkContentElement;
            if (fce != null)
            {
                fce.RemoveHandler(routedEvent, handler);
                return;
            }
            throw new ArgumentException("RemoveHandler");
        }
        public void RaiseEvent(RoutedEventArgs e)
        {
            FrameworkElement uie = Object as FrameworkElement;
            if (uie != null)
            {
                uie.RaiseEvent(e);
                return;
            }
            FrameworkContentElement fce = Object as FrameworkContentElement;
            if (fce != null)
            {
                fce.RaiseEvent(e);
                return;
            }
            throw new ArgumentException("RaiseEvent");
        }
        public bool InVisualTree(DependencyObject obj)
        {
            return LayoutHelper.GetParent(obj, true) != null;
        }
        public void ClearDefaultStyleKey()
        {
            if (Object is FrameworkElement)
            {
                ((FrameworkElement)Object).ClearDefaultStyleKey();
                return;
            }
            if (Object is FrameworkContentElement)
            {
                ((FrameworkContentElement)Object).ClearDefaultStyleKey();
                return;
            }
            throw new ArgumentException("ClearDefaultStyleKey");
        }
        public System.Windows.Data.BindingExpression GetBindingExpression(DependencyObject dObj, DependencyProperty dProp)
        {
            FrameworkElement uie = Object as FrameworkElement;
            if (uie != null)
            {
                return uie.GetBindingExpression(dProp);
            }
            FrameworkContentElement fce = Object as FrameworkContentElement;
            if (fce != null)
            {
                return fce.GetBindingExpression(dProp);
            }
            throw new ArgumentException("GetBindingExpression");
        }
    }
}
