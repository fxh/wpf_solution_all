﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public abstract class SettableRenderTriggerContextBase : RenderTriggerContextBase
    {
        new SettableRenderTriggerBase Factory { get { return base.Factory as SettableRenderTriggerBase; } }
        protected SettableRenderTriggerContextBase(SettableRenderTriggerBase factory)
            : base(factory)
        {
        }
        public abstract bool IsValid();
        public override void Invalidate()
        {
            if (IsValid())
            {
                foreach (var setter in Factory.Setters)
                {
                    setter.SetValue(Namescope, ElementHost);
                }
            }
            else
            {
                foreach (var setter in Factory.Setters)
                {
                    setter.ResetValue(Namescope, ElementHost);
                }
            }
        }
        public override bool Matches(FrameworkRenderElementContext context, string propertyName)
        {
            foreach (var setter in Factory.Setters)
            {
                if (setter.Matches(context, propertyName))
                    return true;
            }
            return false;
        }
    }
}
