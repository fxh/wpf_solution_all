﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class RenderSetter
    {
        public string Property { get; set; }
        public object Value { get; set; }
        public string TargetName { get; set; }
        public object ConvertedValue { get; private set; }
        bool isInitialized;
        public void SetValue(INamescope namescope, IElementHost elementHost)
        {
            var context = namescope.GetElement(TargetName);
            if (context == null)
                throw new ArgumentException(String.Format("Cannot find element with name '{0}'", TargetName));
            InitializeConvertedValue(context);
            context.Do(x => x.SetValue(Property, ConvertedValue));
        }
        void InitializeConvertedValue(object context)
        {
            if (isInitialized)
                return;
            ConvertedValue = RenderTriggerHelper.GetConvertedValue(context, Property, Value);
            isInitialized = true;
        }
        public void ResetValue(INamescope namescope, IElementHost elementHost)
        {
            var context = namescope.GetElement(TargetName);
            if (context == null)
                throw new ArgumentException(String.Format("Cannot find element with name '{0}'", TargetName));
            context.Do(x => x.ResetValue(Property));
        }
        public bool Matches(FrameworkRenderElementContext context, string propertyName)
        {
            return string.Equals(Property, propertyName) && string.Equals(TargetName, context.Name);
        }
    }
}
