﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class RenderEventArgsBase : EventArgs
    {
        protected RenderEventArgsBase(IFrameworkRenderElementContext source, EventArgs originalEventArgs)
        {
            this.Source = source;
            this.OriginalEventArgs = originalEventArgs;
        }
        public bool Handled { get; set; }
        public IFrameworkRenderElementContext Source { get; private set; }
        public EventArgs OriginalEventArgs { get; private set; }
        protected internal virtual void InvokeEventHandler(IFrameworkRenderElementContext target) { }
    }
}
