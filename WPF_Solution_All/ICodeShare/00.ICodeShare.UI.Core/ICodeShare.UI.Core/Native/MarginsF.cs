﻿using Extented.UI.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    [TypeConverter(typeof(MarginsFConverter))]
    public class MarginsF : ICloneable
    {
        #region static
        static float scale = 3;
        public static int ToHundredths(float val)
        {
            return Convert.ToInt32(val / scale);
        }
        public static float FromHundredths(float val)
        {
            return val * scale;
        }
        public static Margins ToMargins(MarginsF value)
        {
            return new Margins(ToHundredths(value.Left), ToHundredths(value.Right), ToHundredths(value.Top), ToHundredths(value.Bottom));
        }
        #endregion
        float left;
        float right;
        float top;
        float bottom;
        public float Left
        {
            get { return left; }
            set { left = value; }
        }
        public float Right
        {
            get { return right; }
            set { right = value; }
        }
        public float Top
        {
            get { return top; }
            set { top = value; }
        }
        public float Bottom
        {
            get { return bottom; }
            set { bottom = value; }
        }
        public Margins Margins
        {
            get
            {
                return ToMargins(this);
            }
        }
        public MarginsF()
        {
        }
        public MarginsF(float left, float right, float top, float bottom)
        {
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }
        public MarginsF(Margins margins)
        {
            this.left = FromHundredths(margins.Left);
            this.right = FromHundredths(margins.Right);
            this.top = FromHundredths(margins.Top);
            this.bottom = FromHundredths(margins.Bottom);
        }
        public Margins Round()
        {
            return new Margins(Convert.ToInt32(left), Convert.ToInt32(right), Convert.ToInt32(top), Convert.ToInt32(bottom));
        }
        public object Clone()
        {
            return new MarginsF(Left, Right, Top, Bottom);
        }
        public override bool Equals(object obj)
        {
            MarginsF margins = obj as MarginsF;
            return margins != null &&
                left == margins.left &&
                right == margins.right &&
                top == margins.top &&
                bottom == margins.bottom;
        }
        public override int GetHashCode()
        {
            return HashCodeHelper.CalcHashCode((int)left, (int)right, (int)top, (int)bottom);
        }
    }
}
