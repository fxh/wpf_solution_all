﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    [Flags]
    enum RenderEventKinds
    {
        Mouse =
            RenderEvents.PreviewMouseUp |
            RenderEvents.PreviewMouseDown |
            RenderEvents.MouseDown |
            RenderEvents.MouseUp |
            RenderEvents.MouseEnter |
            RenderEvents.MouseLeave |
            RenderEvents.MouseMove,
    }
}
