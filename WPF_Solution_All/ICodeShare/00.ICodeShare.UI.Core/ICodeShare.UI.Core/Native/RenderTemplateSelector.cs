﻿using Extended.WPF.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Extented.UI.Core.Native
{
    public class RenderTemplateSelector : MarkupExtension
    {
        public static readonly RenderTemplateSelector Default = new RenderTemplateSelector();
        public virtual RenderTemplate SelectTemplate(Chrome chrome, object content)
        {
            return chrome.RenderTemplate;
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
