﻿using Extented.UI.Core.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class FloatsComparer
    {
        static FloatsComparer()
        {
            Default = new FloatsComparer(0.001);
        }
        public static FloatsComparer Default;
        double epsilon;
        protected FloatsComparer(double epsilon)
        {
            this.epsilon = epsilon;
        }
        public bool FirstEqualsSecond(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon) == 0;
        }
        public bool FirstLessSecond(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon) < 0;
        }
        public bool FirstLessOrEqualSecond(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon) <= 0;
        }
        public bool FirstGreaterSecond(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon) > 0;
        }
        public bool FirstGreaterOrEqualSecond(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon) >= 0;
        }
        public bool FirstGreaterSecondLessThird(double first, double second, double third)
        {
            return FirstGreaterSecond(first, second) && FirstLessSecond(first, third);
        }
        public bool SizeFEquals(SizeF size1, SizeF size2)
        {
            return ComparingUtils.CompareDoubles(size1.Width, size2.Width, epsilon) == 0 && ComparingUtils.CompareDoubles(size1.Height, size2.Height, epsilon) == 0;
        }
        public bool RectangleIsEmpty(RectangleF rect)
        {
            return FirstGreaterSecond(rect.Width, 0) ? (FirstLessSecond(rect.Height, 0) || FirstEqualsSecond(rect.Height, 0)) : true;
        }
        public int CompareDoubles(double first, double second)
        {
            return ComparingUtils.CompareDoubles(first, second, epsilon);
        }
    }
}
