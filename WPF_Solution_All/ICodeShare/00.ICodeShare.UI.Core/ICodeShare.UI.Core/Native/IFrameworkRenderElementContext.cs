﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core.Native
{
    public interface IFrameworkRenderElementContext
    {
        Size DesiredSize { get; }
        Size RenderSize { get; }
        Rect RenderRect { get; }
        Vector VisualOffset { get; }
        object DataContext { get; set; }
        int RenderChildrenCount { get; }
        void SetValue(string propertyName, object value);
        object GetValue(string propertyName);
        FrameworkRenderElementContext GetRenderChild(int index);
        void OnMouseDown(MouseRenderEventArgs args);
        void OnMouseUp(MouseRenderEventArgs args);
        void OnMouseMove(MouseRenderEventArgs args);
        void OnMouseEnter(MouseRenderEventArgs args);
        void OnMouseLeave(MouseRenderEventArgs args);
        void OnPreviewMouseDown(MouseRenderEventArgs args);
        void OnPreviewMouseUp(MouseRenderEventArgs args);
    }
}
