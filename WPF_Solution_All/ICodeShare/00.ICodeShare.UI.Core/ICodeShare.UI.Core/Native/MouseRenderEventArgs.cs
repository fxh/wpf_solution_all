﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core.Native
{
    public class MouseRenderEventArgs : RenderEventArgs
    {
        public Point Position { get; private set; }
        public MouseRenderEventArgs(IFrameworkRenderElementContext source, EventArgs originalEventArgs, RenderEvents renderEvent, Point position)
            : base(source, originalEventArgs, renderEvent)
        {
            if (((int)RenderEventKinds.Mouse & (int)renderEvent) == 0)
                throw new ArgumentException("renderEvent");
            this.Position = position;
        }
        protected internal override void InvokeEventHandler(IFrameworkRenderElementContext target)
        {
            base.InvokeEventHandler(target);
            switch (RenderEvent)
            {
                case RenderEvents.MouseMove:
                    target.OnMouseMove(this);
                    return;
                case RenderEvents.MouseEnter:
                    target.OnMouseEnter(this);
                    return;
                case RenderEvents.MouseLeave:
                    target.OnMouseLeave(this);
                    return;
                case RenderEvents.MouseDown:
                    target.OnMouseDown(this);
                    return;
                case RenderEvents.MouseUp:
                    target.OnMouseUp(this);
                    return;
                case RenderEvents.PreviewMouseDown:
                    target.OnPreviewMouseDown(this);
                    return;
                case RenderEvents.PreviewMouseUp:
                    target.OnPreviewMouseUp(this);
                    return;
            }
        }
    }
}
