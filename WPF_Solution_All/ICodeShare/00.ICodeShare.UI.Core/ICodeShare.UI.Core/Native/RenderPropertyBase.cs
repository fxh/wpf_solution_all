﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class RenderConditionCollection : ObservableCollection<RenderPropertyBase> { }
    [Browsable(false)]
    public abstract class RenderPropertyBase
    {
        public abstract RenderPropertyContextBase CreateContext();
    }
}
