﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public abstract class RenderPropertyContextBase : IRenderPropertyContext
    {
        public RenderPropertyBase Factory { get; private set; }
        public IElementHost ElementHost { get; private set; }
        public INamescope Namescope { get; private set; }
        public IPropertyChangedListener PropertyChangedListener { get; private set; }
        public RenderTriggerContextBase TriggerContext { get; private set; }
        protected bool IsAttached { get; private set; }
        protected RenderPropertyContextBase(RenderPropertyBase factory)
        {
            this.Factory = factory;
        }
        public void Attach(INamescope scope, IElementHost elementHost, IPropertyChangedListener listener, RenderTriggerContextBase context)
        {
            ElementHost = elementHost;
            Namescope = scope;
            PropertyChangedListener = listener;
            TriggerContext = context;
            AttachOverride(scope, elementHost, listener, context);
            IsAttached = true;
        }
        public void Detach()
        {
            if (!IsAttached)
                return;
            IsAttached = false;
            DetachOverride();
            ElementHost = null;
            PropertyChangedListener = null;
            Namescope = null;
            TriggerContext = null;
        }
        public virtual void Reset()
        {
            var namescope = Namescope;
            var elementHost = ElementHost;
            var triggetContext = TriggerContext;
            var listener = PropertyChangedListener;
            Detach();
            Attach(namescope, elementHost, listener, triggetContext);
            triggetContext.Invalidate();
        }
        protected abstract void AttachOverride(INamescope scope, IElementHost elementHost, IPropertyChangedListener listener, RenderTriggerContextBase context);
        protected abstract void DetachOverride();
    }
}
