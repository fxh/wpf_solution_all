﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    [Flags]
    public enum FREInvalidateOptions
    {
        None = 0x1,
        AffectsMeasure = 0x2,
        AffectsArrange = 0x4,
        AffectsVisual = 0x8,
        AffectsRenderCaches = 0x10,
        AffectsGeneralCaches = 0x20,
        AffectsChildrenCaches = 0x40,
        AffectsOpacity = 0x80,
        AffectsMeasureAndVisual = AffectsMeasure | AffectsVisual,
        UpdateLayout = AffectsMeasureAndVisual | AffectsRenderCaches,
        UpdateSubTree = UpdateLayout | AffectsGeneralCaches | AffectsChildrenCaches,
    }
}
