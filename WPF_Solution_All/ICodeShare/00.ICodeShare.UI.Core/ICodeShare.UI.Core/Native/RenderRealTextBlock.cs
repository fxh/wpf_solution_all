﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Extented.UI.Core.Native
{
    public class RenderRealTextBlock : RenderControlBase
    {
        protected override FrameworkElement CreateFrameworkElement(FrameworkRenderElementContext context)
        {
            return new TextBlock() { TextWrapping = TextWrapping.WrapWithOverflow };
        }
        protected override FrameworkRenderElementContext CreateContextInstance()
        {
            return new RenderRealTextBlockContext(this);
        }
    }
}
