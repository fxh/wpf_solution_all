﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public abstract class RenderTriggerBase
    {
        public IElementHost ElementHost { get; private set; }
        public INamescope Namescope { get; private set; }
        public abstract RenderTriggerContextBase CreateContext(INamescope namescope, IElementHost elementHost);
    }
}
