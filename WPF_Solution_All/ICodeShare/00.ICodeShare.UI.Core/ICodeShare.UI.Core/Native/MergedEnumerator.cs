﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class MergedEnumerator : IEnumerator
    {
        readonly IEnumerator<IEnumerator> sourceEnumerator;
        public MergedEnumerator(params IEnumerator[] args)
        {
            sourceEnumerator = args.Where(x => x != null).ToList().GetEnumerator();
        }
        public object Current
        {
            get { return sourceEnumerator.Current.With(x => x.Current); }
        }
        public void Dispose()
        {
            sourceEnumerator.Dispose();
        }
        object IEnumerator.Current
        {
            get { return Current; }
        }
        public bool MoveNext()
        {
            if (sourceEnumerator.Current == null)
                sourceEnumerator.MoveNext();
            if (sourceEnumerator.Current == null)
                return false;
            if (!sourceEnumerator.Current.MoveNext())
            {
                sourceEnumerator.MoveNext();
                if (sourceEnumerator.Current == null)
                    return false;
            }
            else
                return true;
            return sourceEnumerator.Current.MoveNext();
        }
        public void Reset()
        {
            sourceEnumerator.Reset();
            while (sourceEnumerator.MoveNext())
                sourceEnumerator.Current.Reset();
            sourceEnumerator.Reset();
        }
    }
}
