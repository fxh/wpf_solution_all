﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Extented.UI.Core.Native
{
    public class RenderRealTextBlockContext : RenderControlBaseContext
    {
        TextBlock TextBlock { get { return (TextBlock)Control; } }
        string text;
        string highlightedText;
        HighlightedTextCriteria criteria;
        public string Text
        {
            get { return text; }
            set
            {
                SetProperty(ref text, value);
                TextBlockService.UpdateTextBlock(TextBlock, text, highlightedText, criteria);
            }
        }
        public string HighlightedText
        {
            get { return highlightedText; }
            set
            {
                SetProperty(ref highlightedText, value);
                TextBlockService.UpdateTextBlock(TextBlock, text, highlightedText, criteria);
            }
        }
        public HighlightedTextCriteria HighlightedTextCriteria
        {
            get { return criteria; }
            set
            {
                SetProperty(ref criteria, value);
                TextBlockService.UpdateTextBlock(TextBlock, text, highlightedText, criteria);
            }
        }
        public TextWrapping TextWrapping
        {
            get { return TextBlock.TextWrapping; }
            set { TextBlock.TextWrapping = value; }
        }
        public TextTrimming TextTrimming
        {
            get { return TextBlock.TextTrimming; }
            set { TextBlock.TextTrimming = value; }
        }
        public TextAlignment TextAlignment
        {
            get { return TextBlock.TextAlignment; }
            set { TextBlock.TextAlignment = value; }
        }
        public TextDecorationCollection TextDecorations
        {
            get { return TextBlock.TextDecorations; }
            set { TextBlock.TextDecorations = value; }
        }
        public RenderRealTextBlockContext(RenderRealTextBlock factory)
            : base(factory)
        {
        }
    }
}
