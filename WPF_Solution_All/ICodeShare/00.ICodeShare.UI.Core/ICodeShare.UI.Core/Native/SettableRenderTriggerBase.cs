﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace Extented.UI.Core.Native
{
    [ContentProperty("Setters")]
    public abstract class SettableRenderTriggerBase : RenderTriggerBase
    {
        public RenderSetterCollection Setters { get; private set; }
        protected SettableRenderTriggerBase()
        {
            Setters = new RenderSetterCollection();
        }
    }
}
