﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    [Flags]
    public enum RenderEvents
    {
        PreviewMouseUp = 0x1,
        PreviewMouseDown = 0x2,
        MouseDown = 0x4,
        MouseUp = 0x8,
        MouseEnter = 0x10,
        MouseLeave = 0x20,
        MouseMove = 0x40,
    }
}
