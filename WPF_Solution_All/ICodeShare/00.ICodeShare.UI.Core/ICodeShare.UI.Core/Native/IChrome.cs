﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core.Native
{
    public interface IChrome
    {
        void AddChild(FrameworkElement element);
        void RemoveChild(FrameworkElement element);
        FrameworkRenderElementContext Root { get; }
        void InvalidateMeasure();
        void InvalidateArrange();
        void InvalidateVisual();
        void GoToState(string stateName);
    }
}
