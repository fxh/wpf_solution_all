﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core.Native
{
    [Browsable(false)]
    public class RenderPropertyChangedListener : RenderPropertyBase
    {
        string property;
        public string Property { get { return property ?? DependencyProperty.With(x => x.Name); } set { property = value; } }
        [IgnoreDependencyPropertiesConsistencyChecker]
        private DependencyProperty dependencyProperty;
        public DependencyProperty DependencyProperty
        {
            get { return dependencyProperty; }
            set { dependencyProperty = value; }
        }
        public RenderValueSource ValueSource { get; set; }
        public string TargetName { get; set; }
        public string SourceName { get; set; }
        public RenderPropertyChangedListener()
        {
            ValueSource = RenderValueSource.DataContext;
        }
        public override RenderPropertyContextBase CreateContext()
        {
            return new RenderPropertyChangedListenerContext(this);
        }
    }
}
