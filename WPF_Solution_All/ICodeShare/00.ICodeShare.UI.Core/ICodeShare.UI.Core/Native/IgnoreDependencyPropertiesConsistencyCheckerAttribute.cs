﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class IgnoreDependencyPropertiesConsistencyCheckerAttribute : Attribute
    {
    }
}
