﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public interface INamescope
    {
        FrameworkRenderElementContext GetElement(string name);
        IEnumerable<RenderTriggerContextBase> Triggers { get; set; }
        FrameworkRenderElementContext RootElement { get; }
        void RegisterElement(FrameworkRenderElementContext context);
        void ReleaseElement(FrameworkRenderElementContext context);
        void AddChild(FrameworkRenderElementContext context);
        void RemoveChild(FrameworkRenderElementContext context);
        void GoToState(string state);
    }
}
