﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public interface IRenderPropertyContext
    {
        void Attach(INamescope scope, IElementHost elementHost, IPropertyChangedListener listener, RenderTriggerContextBase context);
        void Detach();
        void Reset();
    }
}
