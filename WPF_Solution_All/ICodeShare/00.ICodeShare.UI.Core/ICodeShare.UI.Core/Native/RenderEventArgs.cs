﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class RenderEventArgs : RenderEventArgsBase
    {
        public RenderEvents RenderEvent { get; private set; }
        public RenderEventArgs(IFrameworkRenderElementContext source, EventArgs originalEventArgs, RenderEvents renderEvent)
            : base(source, originalEventArgs)
        {
            this.RenderEvent = renderEvent;
        }
        protected internal override void InvokeEventHandler(IFrameworkRenderElementContext target)
        {
        }
    }
}
