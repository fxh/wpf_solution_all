﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public abstract class RenderTriggerContextBase
    {
        protected RenderTriggerContextBase(RenderTriggerBase factory)
        {
            Factory = factory;
        }
        public RenderTriggerBase Factory { get; private set; }
        public IElementHost ElementHost { get; private set; }
        public INamescope Namescope { get; private set; }
        public IPropertyChangedListener Listener { get; private set; }
        protected bool IsAttached { get; private set; }
        public void Attach(INamescope scope, IElementHost elementHost, IPropertyChangedListener listener)
        {
            ElementHost = elementHost;
            Namescope = scope;
            Listener = listener;
            AttachOverride(scope, elementHost, listener);
            IsAttached = true;
        }
        public void Detach()
        {
            if (!IsAttached)
                return;
            IsAttached = false;
            DetachOverride();
            ElementHost = null;
            Namescope = null;
            Listener = null;
        }
        public virtual void Invalidate() { }
        public void Reset()
        {
            if (IsAttached)
                ResetOverride();
        }
        protected virtual void ResetOverride() { }
        public abstract bool Matches(FrameworkRenderElementContext context, string propertyName);
        protected virtual void DetachOverride() { }
        protected virtual void AttachOverride(INamescope scope, IElementHost elementHost, IPropertyChangedListener listener) { }
    }
}
