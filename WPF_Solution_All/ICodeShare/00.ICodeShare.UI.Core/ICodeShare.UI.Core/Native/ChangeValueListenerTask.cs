﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public class ChangeValueListenerTask
    {
        public RenderPropertyChangedListenerContext Context { get; set; }
        public Action Action { get; set; }
    }
}
