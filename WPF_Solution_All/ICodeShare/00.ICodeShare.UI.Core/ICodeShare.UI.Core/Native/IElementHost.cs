﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core.Native
{
    public interface IElementHost
    {
        int ChildrenCount { get; }
        FrameworkElement GetChild(int index);
        IEnumerator LogicalChildren { get; }
        FrameworkElement TemplatedParent { get; }
        FrameworkElement Parent { get; }
        void InvalidateMeasure();
        void InvalidateArrange();
        void InvalidateVisual();
    }
}
