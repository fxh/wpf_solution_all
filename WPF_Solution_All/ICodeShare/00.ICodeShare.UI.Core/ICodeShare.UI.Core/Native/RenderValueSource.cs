﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public enum RenderValueSource 
    {
        DataContext,
        ElementName, 
        TemplatedParent 
    }
}
