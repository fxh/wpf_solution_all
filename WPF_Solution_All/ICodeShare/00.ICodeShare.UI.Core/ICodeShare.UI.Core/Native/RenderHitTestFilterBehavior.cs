﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    [Flags]
    public enum RenderHitTestFilterBehavior
    {
        Continue = 0x1,
        ContinueSkipChildren = Continue | 0x2,
        ContinueSkipSelf = Continue | 0x4,
        ContinueSkipSelfAndChildren = ContinueSkipChildren | ContinueSkipSelf,
        Stop = 0x8
    }
}
