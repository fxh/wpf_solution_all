﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extented.UI.Core.Native
{
    public interface IPropertyChangedListener
    {
        void SubscribeValueChangedAsync(RenderPropertyChangedListenerContext context);
        void UnsubscribeValueChanged(object target, RenderPropertyChangedListenerContext context);
        void SubscribeValueChanged(object target, RenderPropertyChangedListenerContext context);
        void Flush();
    }
}
