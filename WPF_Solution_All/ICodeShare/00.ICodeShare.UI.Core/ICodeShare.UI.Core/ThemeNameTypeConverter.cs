﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace  Extended.WPF.Core
{
    public class ThemeNameTypeConverter : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            List<string> themeNames = new List<string>(Theme.Themes.Count);
            foreach (Theme theme in Theme.Themes)
            {
                if (theme.ShowInThemeSelector)
                    themeNames.Add(theme.Name);
            }
            themeNames.Add(Theme.NoneName);
            return new StandardValuesCollection(themeNames);
        }
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                return value;
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}
