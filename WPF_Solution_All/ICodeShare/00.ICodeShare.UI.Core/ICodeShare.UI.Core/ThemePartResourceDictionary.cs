﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extented.UI.Core
{
    public class ThemePartResourceDictionary : ResourceDictionary
    {
        bool isEnabled;
        static List<ThemePartResourceDictionary> resourceDictionaries = new List<ThemePartResourceDictionary>();
        Uri mutableSource;
        object key;
        public ThemePartResourceDictionary()
        {
            AllowExnernalSetIsEnabled = true;
        }
        internal static List<ThemePartResourceDictionary> ResourceDictionaries { get { return resourceDictionaries; } }
        protected bool AllowExnernalSetIsEnabled { get; set; }
        protected bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled == value)
                    return;
                isEnabled = value;
                OnIsEnabledChanged();
            }
        }
        public Uri DisabledSource
        {
            get { return mutableSource; }
            set
            {
                if (mutableSource == value)
                    return;
                mutableSource = value;
                OnDisabledSourceChanged();
            }
        }
        public object Key
        {
            get { return key; }
            set
            {
                if (key == value)
                    return;
                if (key != null)
                    throw new ArgumentException("Key");
                key = value;
                OnKeyChanged();
            }
        }
        internal static void EnableSource(ThemePartResourceDictionary dictionary)
        {
            dictionary.IsEnabled = true;
        }
        internal static bool EnableSource(object key)
        {
            IEnumerable<ThemePartResourceDictionary> enumerable = GetResourceDictionaries(key);
            if (!enumerable.Any())
                return false;
            foreach (ThemePartResourceDictionary resourceDictionary in enumerable)
            {
                if (resourceDictionary.AllowExnernalSetIsEnabled) resourceDictionary.IsEnabled = true;
            }
            return true;
        }
        static List<ThemePartResourceDictionary> GetResourceDictionaries(object key)
        {
            return ResourceDictionaries.Where(resourceDictionary => key.Equals(resourceDictionary.Key)).ToList();
        }
        protected virtual void OnDisabledSourceChanged() { }
        void OnIsEnabledChanged()
        {
            if (!IsEnabled)
                return;
            this.MergedDictionaries.Add(new ResourceDictionary() { Source = DisabledSource });
            ResourceDictionaries.Remove(this);
        }
        void OnKeyChanged()
        {
            ThemePartKeyExtension themePartKey = Key as ThemePartKeyExtension;
            if (DisabledSource == null && themePartKey != null)
            {
                DisabledSource = themePartKey.Uri;
            }
            ResourceDictionaries.Add(this);
        }
    }
}
