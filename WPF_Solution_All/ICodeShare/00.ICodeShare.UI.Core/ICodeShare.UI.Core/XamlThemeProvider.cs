﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace  Extended.WPF.Core
{
    public class XamlThemeProvider : INotifyPropertyChanged
    {
        public string ThemeName { get; private set; }
        public XamlThemeProvider()
        {
            ThemeName = ThemeManager.ActualApplicationThemeName;
            ThemeManager.AddThemeChangedHandler(Application.Current.MainWindow, ThemeManager_ThemeChanged);
        }
        void ThemeManager_ThemeChanged(DependencyObject sender, ThemeChangedRoutedEventArgs e)
        {
            ThemeName = ThemeManager.ActualApplicationThemeName;
            RaisePropertyChanged("ThemeName");
        }
        void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
