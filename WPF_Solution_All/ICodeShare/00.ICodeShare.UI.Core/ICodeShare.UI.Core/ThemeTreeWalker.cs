﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace  Extended.WPF.Core
{
    public class ThemeTreeWalker
    {
        readonly WeakReference wRef;
        readonly InplaceResourceProvider inplaceResourceProvider;
        protected internal ThemeTreeWalker(string themeName, bool isTouch, DependencyObject owner)
        {
            this.wRef = new WeakReference(owner);
            ThemeName = themeName;
            IsTouch = isTouch;
            inplaceResourceProvider = new InplaceResourceProvider(ThemeHelper.GetTreewalkerThemeName(this, false));
        }
        public DependencyObject Owner { get { return wRef.Target as DependencyObject; } }
        public string ThemeName { get; protected set; }
        public string FullThemeName { get { return ThemeName + (IsTouch ? ThemeManager.TouchDelimiter + ThemeManager.TouchDefinition : string.Empty); } }
        public InplaceResourceProvider InplaceResourceProvider { get { return inplaceResourceProvider; } }
        public override string ToString()
        {
            return ThemeName;
        }
        public bool IsTouch { get; private set; }
        public bool IsDefault { get { return Theme.IsDefaultTheme(ThemeName); } }
        public bool IsRegistered { get { return Theme.FindTheme(ThemeName) != null; } }
    }
}
