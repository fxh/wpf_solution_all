﻿using Extended.WPF.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Extented.UI.Core
{
    public static class DefaultStyleKeyHelper
    {
        public static Type GetControlDefaultStyleKey(Control control)
        {
            if (control == null)
                return null;
            return (Type)DefaultStyleKeyControlHelper.GetDefaultStyleKey(control);
        }

        public static void SetDefaultStyleKey(this FrameworkElement element, object value)
        {
            DefaultStyleKeyFrameworkElementHelper.SetDefaultStyleKey(element, value);
        }
        public static void SetDefaultStyleKey(this FrameworkContentElement element, object value)
        {
            DefaultStyleKeyFrameworkContentElementHelper.SetDefaultStyleKey(element, value);
        }
        public static object GetDefaultStyleKey(this FrameworkElement element)
        {
            return DefaultStyleKeyFrameworkElementHelper.GetDefaultStyleKey(element);
        }
        public static object GetDefaultStyleKey(this FrameworkContentElement element)
        {
            return DefaultStyleKeyFrameworkContentElementHelper.GetDefaultStyleKey(element);
        }
        public static object ClearDefaultStyleKey(this FrameworkElement element)
        {
            return DefaultStyleKeyFrameworkElementHelper.GetDefaultStyleKey(element);
        }
        public static object ClearDefaultStyleKey(this FrameworkContentElement element)
        {
            return DefaultStyleKeyFrameworkContentElementHelper.GetDefaultStyleKey(element);
        }
    }
}
