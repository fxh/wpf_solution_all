﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extended.WPF.Core
{
    public static class DependencyPropertyManager
    {
        public static DependencyProperty Register(string name, Type propertyType, Type ownerType)
        {
            return DependencyProperty.Register(name, propertyType, ownerType);
        }
        public static DependencyProperty Register(string name, Type propertyType, Type ownerType, PropertyMetadata typeMetadata)
        {
            return DependencyProperty.Register(name, propertyType, ownerType, typeMetadata);
        }
        public static DependencyProperty Register(string name, Type propertyType, Type ownerType, PropertyMetadata typeMetadata, ValidateValueCallback validateValueCallback)
        {
            return DependencyProperty.Register(name, propertyType, ownerType, typeMetadata, validateValueCallback);
        }
        public static DependencyProperty RegisterAttached(string name, Type propertyType, Type ownerType)
        {
            return DependencyProperty.RegisterAttached(name, propertyType, ownerType);
        }
        public static DependencyProperty RegisterAttached(string name, Type propertyType, Type ownerType, PropertyMetadata defaultMetadata)
        {
            return DependencyProperty.RegisterAttached(name, propertyType, ownerType, defaultMetadata);
        }
        public static DependencyProperty RegisterAttached(string name, Type propertyType, Type ownerType, PropertyMetadata defaultMetadata, ValidateValueCallback validateValueCallback)
        {
            return DependencyProperty.RegisterAttached(name, propertyType, ownerType, defaultMetadata, validateValueCallback);
        }
        public static DependencyPropertyKey RegisterAttachedReadOnly(string name, Type propertyType, Type ownerType, PropertyMetadata defaultMetadata)
        {
            return DependencyProperty.RegisterAttachedReadOnly(name, propertyType, ownerType, defaultMetadata);
        }
        public static DependencyPropertyKey RegisterReadOnly(string name, Type propertyType, Type ownerType, PropertyMetadata typeMetadata)
        {
            return DependencyProperty.RegisterReadOnly(name, propertyType, ownerType, typeMetadata);
        }
    }
}
