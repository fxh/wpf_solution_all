﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace  Extended.WPF.Core
{
    public class ThemeTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var themeName = value as string;
            if (themeName == null)
                return base.ConvertFrom(context, culture, value);
            Theme theme = Theme.FindTheme(themeName);
            if (theme == null)
                throw new ArgumentOutOfRangeException("theme");
            return theme;
        }
    }
}
