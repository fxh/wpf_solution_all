﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extended.WPF.Core.Helpers
{
    public static class PointHelper
    {
        public static bool IsEmpty(Point p)
        {
            return p == Empty;
        }
        public static Point Abs(Point p)
        {
            return new Point(Math.Abs(p.X), Math.Abs(p.Y));
        }
        public static Point Add(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }
        public static Point Average(Point p1, Point p2)
        {
            return new Point((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2);
        }
        public static Point Min(Point p1, Point p2)
        {
            return new Point(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y));
        }
        public static Point Max(Point p1, Point p2)
        {
            return new Point(Math.Max(p1.X, p2.X), Math.Max(p1.Y, p2.Y));
        }
        public static Point Multiply(Point p, double by)
        {
            return new Point(p.X * by, p.Y * by);
        }
        public static Point Multiply(Point p1, Point p2)
        {
            return new Point(p1.X * p2.X, p1.Y * p2.Y);
        }
        public static void Offset(ref Point p, double x, double y)
        {
            p.X += x;
            p.Y += y;
        }
        public static Point Sign(Point p)
        {
            return new Point(Math.Sign(p.X), Math.Sign(p.Y));
        }
        public static Point Subtract(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }
        public static Point Empty 
        { 
            get { return Rect.Empty.TopLeft(); } 
        }
        public static bool AreClose(Point point1, Point point2)
        {
            return (point1.X.AreClose(point2.X) && point1.Y.AreClose(point2.Y));
        }
    }
}
