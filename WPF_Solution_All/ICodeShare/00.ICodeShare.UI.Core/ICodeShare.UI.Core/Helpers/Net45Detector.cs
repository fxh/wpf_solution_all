﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Extented.UI.Core.Helpers
{
    public static class Net45Detector
    {
        static bool propertyExists;
        static Net45Detector()
        {
            propertyExists = typeof(VirtualizingPanel).GetProperty("CanHierarchicallyScrollAndVirtualize") != null;
        }
        public static bool IsNet45()
        {
            return propertyExists;
        }
    }
}
