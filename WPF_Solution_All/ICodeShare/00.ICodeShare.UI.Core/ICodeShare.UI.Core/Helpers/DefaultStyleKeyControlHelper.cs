﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Extended.WPF.Core.Helpers
{
    public class DefaultStyleKeyControlHelper : Control
    {
        public static object GetDefaultStyleKey(Control element)
        {
            return element.GetValue(Control.DefaultStyleKeyProperty);
        }
    }
}
