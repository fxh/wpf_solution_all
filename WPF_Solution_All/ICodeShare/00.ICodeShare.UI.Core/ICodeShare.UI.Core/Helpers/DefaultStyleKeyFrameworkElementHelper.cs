﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extended.WPF.Core.Helpers
{
    public class DefaultStyleKeyFrameworkElementHelper : FrameworkElement
    {
        public static void SetDefaultStyleKey(FrameworkElement element, object value)
        {
            element.SetValue(FrameworkElement.DefaultStyleKeyProperty, value);
        }
        public static object GetDefaultStyleKey(FrameworkElement element)
        {
            return element.GetValue(FrameworkElement.DefaultStyleKeyProperty);
        }
    }
}
