﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using Extented.UI.Core.Utils;
using Extented.UI.Core.Native;

namespace Extented.UI.Core.Helpers
{
    public class PSNativeMethods
    {
#if SL
		static public bool AspIsRunning {
			get { return false; }
		}
#else
        public interface IAspDetector
        {
            bool AspIsRunning { get; }
        }
        class AspDetector : IAspDetector
        {
            public bool AspIsRunning
            {
                get
                {
#if DEBUGTEST
					return HttpContextAccessor.Current != null;
#else
                    return !Environment.UserInteractive || HttpContextAccessor.Current != null;
#endif
                }
            }
        }
        static IAspDetector aspDetector = new AspDetector();
        public static void SetAspDetector(IAspDetector detector)
        {
            aspDetector = detector;
        }
        static public bool AspIsRunning
        {
            get
            {
                return aspDetector.AspIsRunning;
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804")]
        public static void ForceCreateHandle(Control control)
        {
            IntPtr ignore = control.Handle;
        }
#endif
        public static Color ValidateBackgrColor(Color color)
        {
            if (color == DXSystemColors.Window || DXColor.IsTransparentOrEmpty(color))
                return DXColor.White;
            else
                return color;
        }
        static public PointF TranslatePointF(PointF val, float dx, float dy)
        {
            return new PointF(val.X + dx, val.Y + dy);
        }
        static public PointF TranslatePointF(PointF val, PointF pos)
        {
            return new PointF(val.X + pos.X, val.Y + pos.Y);
        }
        static public Array CombineCollections(ICollection items1, ICollection items2, Type type)
        {
            Array items = Array.CreateInstance(type, items1.Count + items2.Count);
            items1.CopyTo(items, 0);
            items2.CopyTo(items, items1.Count);
            return items;
        }
        public static bool IsNaN(float value)
        {
            try
            {
                return Single.IsNaN(value);
            }
            catch
            {
                return true;
            }
        }
        public static Size GetResolutionImageSize(Image img)
        {
            return GetResolutionImageSize(img, GraphicsDpi.Pixel);
        }
        public static Size GetResolutionImageSize(Image img, float dpi)
        {
            if (img == null)
                throw new ArgumentNullException("img");
            SizeF size;
            lock (img)
            {
                size = new SizeF(img.Size.Width / img.HorizontalResolution, img.Size.Height / img.VerticalResolution);
            }
            return Size.Round(GraphicsUnitConverter.Convert(size, GraphicsDpi.Inch, dpi));
        }
        public static bool IsFloatType(Type type)
        {
            return (typeof(System.Decimal).Equals(type) ||
                    typeof(System.Single).Equals(type) ||
                    typeof(System.Double).Equals(type));
        }
        public static bool IsNumericalType(Type type)
        {
            return (typeof(System.Decimal).Equals(type) ||
                    typeof(System.Single).Equals(type) ||
                    typeof(System.Double).Equals(type) ||
                    typeof(System.Int16).Equals(type) ||
                    typeof(System.Int32).Equals(type) ||
                    typeof(System.Int64).Equals(type) ||
                    typeof(System.UInt16).Equals(type) ||
                    typeof(System.UInt32).Equals(type) ||
                    typeof(System.UInt64).Equals(type) ||
                    typeof(System.Byte).Equals(type) ||
                    typeof(System.SByte).Equals(type));
        }
        public static bool IsNullableNumericalType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) ? IsNumericalType(Nullable.GetUnderlyingType(type)) : false;
        }
        public static bool ValueInsideBounds(float value, float lowBound, float highBound)
        {
            return !FloatsComparer.Default.FirstLessSecond(value, lowBound) && FloatsComparer.Default.FirstLessSecond(value, highBound);
        }
    }
}
