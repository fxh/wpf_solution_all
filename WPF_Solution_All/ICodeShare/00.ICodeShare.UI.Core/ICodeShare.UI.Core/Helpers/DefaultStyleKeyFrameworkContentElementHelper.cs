﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extended.WPF.Core.Helpers
{
    public class DefaultStyleKeyFrameworkContentElementHelper : FrameworkContentElement
    {
        public static void SetDefaultStyleKey(FrameworkContentElement element, object value)
        {
            element.SetValue(FrameworkContentElement.DefaultStyleKeyProperty, value);
        }
        public static object GetDefaultStyleKey(FrameworkContentElement element)
        {
            return element.GetValue(FrameworkContentElement.DefaultStyleKeyProperty);
        }
    }
}
