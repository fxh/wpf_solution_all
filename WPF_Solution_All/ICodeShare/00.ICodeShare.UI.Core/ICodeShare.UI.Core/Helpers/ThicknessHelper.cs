﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extended.WPF.Core.Helpers
{
    public static class ThicknessHelper
    {
        internal const char NumericListSeparator = ',';

        public static void Inc(ref Thickness value, Thickness by)
        {
            value.Left += by.Left;
            value.Top += by.Top;
            value.Right += by.Right;
            value.Bottom += by.Bottom;
        }
        public static Thickness Parse(string s)
        {
            var result = new Thickness();
            var numbers = s.Split(NumericListSeparator);
            if (numbers != null)
                switch (numbers.Length)
                {
                    case 1:
                        result.Left = result.Top = result.Right = result.Bottom = double.Parse(numbers[0], CultureInfo.InvariantCulture);
                        break;
                    case 2:
                        result.Left = result.Right = double.Parse(numbers[0], CultureInfo.InvariantCulture);
                        result.Top = result.Bottom = double.Parse(numbers[1], CultureInfo.InvariantCulture);
                        break;
                    case 4:
                        result.Left = double.Parse(numbers[0], CultureInfo.InvariantCulture);
                        result.Top = double.Parse(numbers[1], CultureInfo.InvariantCulture);
                        result.Right = double.Parse(numbers[2], CultureInfo.InvariantCulture);
                        result.Bottom = double.Parse(numbers[3], CultureInfo.InvariantCulture);
                        break;
                }
            return result;
        }
        public static string ToString(Thickness thickness)
        {
            if (thickness.Left == thickness.Right && thickness.Top == thickness.Bottom)
                if (thickness.Left == thickness.Top)
                    return thickness.Left.ToString(CultureInfo.InvariantCulture);
                else
                    return thickness.Left.ToString(CultureInfo.InvariantCulture) + NumericListSeparator +
                        thickness.Top.ToString(CultureInfo.InvariantCulture);
            else
                return thickness.Left.ToString(CultureInfo.InvariantCulture) + NumericListSeparator +
                    thickness.Top.ToString(CultureInfo.InvariantCulture) + NumericListSeparator +
                    thickness.Right.ToString(CultureInfo.InvariantCulture) + NumericListSeparator +
                    thickness.Bottom.ToString(CultureInfo.InvariantCulture);
        }
    }
}
