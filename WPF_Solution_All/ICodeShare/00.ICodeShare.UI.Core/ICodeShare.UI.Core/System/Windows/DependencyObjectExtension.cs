﻿using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;

namespace System.Windows
{
    public static class DependencyObjectExtension
    {
        public static readonly DependencyProperty DataContextProperty =
            DependencyProperty.RegisterAttached("DataContext", typeof(object), typeof(DependencyObjectExtension), null);

        public static object GetDataContext(DependencyObject obj)
        {
            return obj.GetValue(DataContextProperty);
        }

        public static void SetDataContext(DependencyObject obj, object value)
        {
            obj.SetValue(DataContextProperty, value);
        }

        public static bool HasDefaultValue(this DependencyObject o, DependencyProperty property)
        {
            object value = o.GetValue(property);
            object defaultValue = property.GetMetadata(o.GetType()).DefaultValue;
            return object.Equals(value, defaultValue) || defaultValue == DependencyProperty.UnsetValue && value == null;
        }

        public static bool IsPropertyAssigned(this DependencyObject o, DependencyProperty property)
        {
            return o.ReadLocalValue(property) != DependencyProperty.UnsetValue;
        }

        public static bool IsPropertySet(this DependencyObject o, DependencyProperty property)
        {
            return o.IsPropertyAssigned(property) || !o.HasDefaultValue(property);
        }

        public static void ReadPropertyFromXML(this DependencyObject o, XmlReader xml, DependencyProperty property, string propertyName, Type propertyType)
        {
            object localValue = o.ReadLocalValue(property);
            if (localValue is BindingExpression)
            {
                BindingMode mode = ((BindingExpression)localValue).ParentBinding.Mode;
                if (mode == BindingMode.Default)
                {
                    var metadata = property.GetMetadata(o) as FrameworkPropertyMetadata;
                    mode = metadata != null && metadata.BindsTwoWayByDefault ? BindingMode.TwoWay : BindingMode.OneWay;
                }
                if (!(mode == BindingMode.TwoWay || mode == BindingMode.OneWayToSource))
                    return;
            }
            if (xml[propertyName] != null)
            {
                string s = xml[propertyName];
                object value;
                if (propertyType.IsEnum)
                    value = Enum.Parse(propertyType, s, true);
                else
                    if (typeof(FrameworkElement).IsAssignableFrom(propertyType))
                        value = o is FrameworkElement ? ((FrameworkElement)o).FindName(s) : null;
                    else
                    {
                        value = null;
                        var typeConverterAttributes = propertyType.GetCustomAttributes(typeof(TypeConverterAttribute), true);
                        if (typeConverterAttributes.Length > 0)
                        {
                            var converterType = Type.GetType(((TypeConverterAttribute)typeConverterAttributes[0]).ConverterTypeName);
                            if (converterType != null)
                            {
                                var converter = (TypeConverter)converterType.GetConstructor(Type.EmptyTypes).Invoke(null);
                                if (converter.CanConvertFrom(typeof(string)))
                                    if (propertyType == typeof(Thickness))
                                        value = converter.ConvertFromInvariantString(s);
                                    else
                                        value = converter.ConvertFromString(s);
                            }
                        }
                        if (value == null)
                        {
                            value = ((IConvertible)s).ToType(propertyType, CultureInfo.InvariantCulture);
                            if (value is string && localValue != null && localValue != DependencyProperty.UnsetValue &&
                                !(localValue is string) && !(localValue is BindingExpression))
                                return;
                        }
                    }
                o.SetValue(property, value);
            }
            else
                o.ClearValue(property);
        }

        public static void WritePropertyToXML(this DependencyObject o, XmlWriter xml, DependencyProperty property, string propertyName)
        {
            if (!o.IsPropertyAssigned(property))
                return;
            object value = o.GetValue(property);
            string s;
            if (value == null)
                s = null;
            else
                if (value is double)
                    s = ((double)value).ToString(CultureInfo.InvariantCulture);
                else
                    if (value is FrameworkElement)
                        s = ((FrameworkElement)value).Name;
                    else
                        s = value.ToString();
            xml.WriteAttributeString(propertyName, s);
        }

        public static object GetCoerceOldValue(this DependencyObject o, DependencyProperty dp)
        {
            return o.GetValue(dp);
        }

        public static void SetBinding(this FrameworkElement o, DependencyObject bindingSource, string bindingPath, DependencyProperty property)
        {
            Binding b = new Binding(bindingPath);
            b.Source = bindingSource;
            o.SetBinding(property, b);
        }

        public static void SetCurrentValueIfDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            if (!o.IsPropertyAssigned(property))
                o.SetCurrentValue(property, value);
        }

        public static void SetValueIfDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            if (!o.IsPropertyAssigned(property))
                o.SetValue(property, value);
        }

        public static void SetValueIfNotDefault(this DependencyObject o, DependencyProperty property, object value)
        {
            object defaultValue = property.GetMetadata(o.GetType()).DefaultValue;
            if (value == defaultValue || value == null && defaultValue == DependencyProperty.UnsetValue)
            {
                if (o.IsPropertyAssigned(property))
                    o.ClearValue(property);
            }
            else
                o.SetValue(property, value);
        }

        public static object StorePropertyValue(this DependencyObject o, DependencyProperty property)
        {
            return new DependencyPropertyValueInfo(o, property);
        }

        public static object StoreAndAssignPropertyValue(this DependencyObject o, DependencyProperty property)
        {
            var result = o.StorePropertyValue(property);
            o.SetValue(property, o.GetValue(property));
            return result;
        }

        public static void RestorePropertyValue(this DependencyObject o, DependencyProperty property, object storedInfo)
        {
            ((DependencyPropertyValueInfo)storedInfo).RestorePropertyValue(o, property);
        }

        public static UIElement GetElementByName(this DependencyObject owner, string elementName)
        {
            if (VisualTreeHelper.GetChildrenCount(owner) == 0)
                return null;
            FrameworkElement element = (FrameworkElement)VisualTreeHelper.GetChild(owner, 0);
            return element.FindName(elementName) as UIElement;
        }

        public static T FindElementByTypeInParents<T>(this DependencyObject element, DependencyObject stopElement) where T : FrameworkElement
        {
            if (element == null || element == stopElement)
                return null;
            if (element is T)
                return element as T;
            return VisualTreeHelper.GetParent(element).FindElementByTypeInParents<T>(stopElement);
        }

        public static bool FindIsInParents(this DependencyObject child, DependencyObject parent)
        {
            if (child == null) return false;
            if (parent == null) return false;
            if (VisualTreeHelper.GetParent(child) == null) return false;
            if (child == parent) return true;
            return FindIsInParents(VisualTreeHelper.GetParent(child), parent);
        }

        public static bool IsInDesignTool(this DependencyObject o)
        {
            return DesignerProperties.GetIsInDesignMode(o);
        }

        private struct DependencyPropertyValueInfo
        {
            private object _LocalValue;
            private object _Value;

            public DependencyPropertyValueInfo(DependencyObject o, DependencyProperty property)
            {
                _LocalValue = o.ReadLocalValue(property);
                _Value = o.GetValue(property);
            }

            public override bool Equals(object obj)
            {
                if (obj is DependencyPropertyValueInfo)
                {
                    var info = (DependencyPropertyValueInfo)obj;
                    return IsAssigned == info.IsAssigned &&
                        (_Value == null && info._Value == null || _Value != null && _Value.Equals(info._Value));
                }
                else
                    return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public void RestorePropertyValue(DependencyObject o, DependencyProperty property)
            {
                if (IsAssigned)
                    if (_LocalValue is BindingExpressionBase)
                        ((FrameworkElement)o).SetBinding(property, ((BindingExpressionBase)_LocalValue).ParentBindingBase);
                    else
                        o.SetValue(property, _LocalValue);
                else
                {
                    o.ClearValue(property);
                    o.InvalidateProperty(property);
                }
            }

            private bool IsAssigned { get { return _LocalValue != DependencyProperty.UnsetValue; } }
        }
    }
}