﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Media
{
    public static class TranslateTransformExtension
    {
        public static Point GetTranslationPoint(this TranslateTransform transform)
        {
            return new Point(transform.X, transform.Y);
        }
    }
}
