﻿namespace System.Windows
{
    public static class WinFormExtension
    {
        public static System.Drawing.Size ToWinFormsSize(this System.Windows.Size size)
        {
            return new System.Drawing.Size((int)size.Width, (int)size.Height);
        }

        public static System.Windows.Size FromWinForms(this System.Drawing.Size size)
        {
            return new System.Windows.Size(size.Width, size.Height);
        }

        public static System.Windows.Point FromWinForms(this System.Drawing.Point point)
        {
            return new System.Windows.Point(point.X, point.Y);
        }

        public static System.Drawing.Rectangle ToWinFormsRectangle(this Rect rect)
        {
            return new System.Drawing.Rectangle(rect.Location.ToWinFormsPoint(), rect.Size.ToWinFormsSize());
        }

        public static Rect FromWinForms(this System.Drawing.Rectangle rect)
        {
            return new System.Windows.Rect(rect.Location.FromWinForms(), rect.Size.FromWinForms());
        }

        public static System.Drawing.Point ToWinFormsPoint(this System.Windows.Point point)
        {
            return new System.Drawing.Point((int)point.X, (int)point.Y);
        }

        public static System.Drawing.Color ToWinFormsColor(this System.Windows.Media.Color color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
}