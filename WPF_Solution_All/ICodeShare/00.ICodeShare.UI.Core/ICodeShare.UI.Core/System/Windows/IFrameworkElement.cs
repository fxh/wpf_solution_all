﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows
{
    public interface IFrameworkElement
    {
        Size Measure(Size availableSize);
        void InvalidateMeasureEx();
    }
}
