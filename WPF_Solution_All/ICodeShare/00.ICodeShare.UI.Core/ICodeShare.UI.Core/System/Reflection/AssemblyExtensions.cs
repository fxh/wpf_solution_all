﻿using System.IO;
using System.Windows;
using System.Windows.Markup;

namespace System.Reflection
{
    public static class AssemblyExtensions
    {
        public static ResourceDictionary GetResourceDictionary(this Assembly assembly, string resourceName)
        {
            Stream stream = assembly.GetEmbeddedResource(resourceName);
            return XamlReader.Load(stream) as ResourceDictionary;
        }
    }
}