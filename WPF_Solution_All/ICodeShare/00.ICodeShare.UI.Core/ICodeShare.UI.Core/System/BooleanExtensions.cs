﻿using System.Windows;

namespace System
{
    public static class BooleanExtensions
    {
        public static Visibility ToVisibility(this bool value, bool isUseHidden = false)
        {
            return value ? Visibility.Visible : (isUseHidden ? Visibility.Hidden : Visibility.Collapsed);
        }

        public static Visibility ToInverseVisibility(this bool value, bool isUseHidden = false)
        {
            return !value ? Visibility.Visible : (isUseHidden ? Visibility.Hidden : Visibility.Collapsed);
        }
    }
}