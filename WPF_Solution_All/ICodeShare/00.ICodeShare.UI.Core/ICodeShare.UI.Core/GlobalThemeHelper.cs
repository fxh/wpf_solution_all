﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace  Extended.WPF.Core
{
    public interface IThemeManager
    {
        void SetThemeName(DependencyObject d, string value);
        void ClearThemeName(DependencyObject d);
        string GetThemeName(DependencyObject d);
        BindingExpression GetThemeNameBindingExpression(DependencyObject d);
    }
}
