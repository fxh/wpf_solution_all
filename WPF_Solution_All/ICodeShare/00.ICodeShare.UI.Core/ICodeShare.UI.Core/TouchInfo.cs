﻿using  Extended.WPF.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace  Extended.WPF.Core
{
    public class TouchInfo : DependencyObject
    {
        public static readonly DependencyProperty MarginProperty;
        public static readonly DependencyProperty PaddingProperty;
        public Thickness Value { get; set; }
        public Thickness TouchValue { get; set; }
        public double? Scale { get; set; }
        [IgnoreDependencyPropertiesConsistencyChecker]
        private DependencyProperty targetProperty;
        public DependencyProperty TargetProperty
        {
            get { return targetProperty; }
            set { targetProperty = value; }
        }
        public static void SetMargin(DependencyObject d, TouchInfo value)
        {
            d.SetValue(MarginProperty, value);
        }
        public static TouchInfo GetMargin(DependencyObject d)
        {
            return (TouchInfo)d.GetValue(MarginProperty);
        }
        public static void SetPadding(DependencyObject d, TouchInfo value)
        {
            d.SetValue(PaddingProperty, value);
        }
        public static TouchInfo GetPadding(DependencyObject d)
        {
            return (TouchInfo)d.GetValue(PaddingProperty);
        }
        static TouchInfo()
        {
            Type ownerType = typeof(TouchInfo);
            MarginProperty = DependencyPropertyManager.RegisterAttached("Margin", typeof(TouchInfo), ownerType, new PropertyMetadata(null, OnTouchPropertyChanged));
            PaddingProperty = DependencyPropertyManager.RegisterAttached("Padding", typeof(TouchInfo), ownerType, new PropertyMetadata(null, OnTouchPropertyChanged));
        }
        public static void OnTouchPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
                ThemeManager.RemoveTouchInfo(d, e.OldValue as TouchInfo);
            if (e.NewValue != null)
                ThemeManager.AddTouchInfo(d, e.NewValue as TouchInfo);
        }
    }
}
