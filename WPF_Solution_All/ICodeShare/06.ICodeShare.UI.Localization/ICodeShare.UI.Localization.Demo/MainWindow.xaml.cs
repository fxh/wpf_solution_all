﻿using ICodeShare.UI.Localization.Engine;
using Infrastructure.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Localization.Demo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LocalSettings.CurrentLanguage = (cmbLanguage.SelectedItem as ComboBoxItem).Content.ToString();
            LocalizeDictionary.ChangeLanguage(LocalSettings.CurrentLanguage);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TestWindow tw = new TestWindow();
            tw.ShowDialog();
        }

      
    }
}
