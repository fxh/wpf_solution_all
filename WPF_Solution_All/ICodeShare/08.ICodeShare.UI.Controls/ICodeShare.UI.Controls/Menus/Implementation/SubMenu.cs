﻿using ICodeShare.UI.Controls.Automation.Peers;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace ICodeShare.UI.Controls
{
    [Localizability(LocalizationCategory.Menu)]
    public class SubMenu : MenuBase
    {
        static SubMenu()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(typeof(SubMenu)));
            IsTabStopProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(false));
            KeyboardNavigation.TabNavigationProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(KeyboardNavigationMode.Cycle));
            KeyboardNavigation.ControlTabNavigationProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(KeyboardNavigationMode.Contained));
            KeyboardNavigation.DirectionalNavigationProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(KeyboardNavigationMode.Cycle));
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new SubMenuAutomationPeer(this);
        }
    }
}