﻿using System.Diagnostics.Contracts;
using System.Windows.Automation.Peers;

namespace ICodeShare.UI.Controls.Automation.Peers
{
    public class SubMenuAutomationPeer : FrameworkElementAutomationPeer
    {
        public SubMenuAutomationPeer(SubMenu owner)
            : base(owner)
        {
        }

        [System.Diagnostics.Contracts.Pure]
        protected override string GetClassNameCore()
        {
            Contract.Ensures(Contract.Result<string>() == "SubMenu");
            return "SubMenu";
        }

        [System.Diagnostics.Contracts.Pure]
        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            Contract.Ensures(Contract.Result<AutomationControlType>() == AutomationControlType.Menu);
            return AutomationControlType.Menu;
        }
    }
}