﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 进度条状态
    /// </summary>
    public enum ProgressState
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal,

        /// <summary>
        /// 动态
        /// </summary>
        Indeterminate,

        /// <summary>
        /// 忙碌
        /// </summary>
        Busy
    }
}