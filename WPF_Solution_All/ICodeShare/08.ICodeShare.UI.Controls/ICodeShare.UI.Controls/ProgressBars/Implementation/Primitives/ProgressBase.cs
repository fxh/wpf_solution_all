﻿using ICodeShare.UI.Controls.Automation;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;

namespace ICodeShare.UI.Controls.Primitives
{
    [TemplatePart(Name = TrackName, Type = typeof(FrameworkElement))]
    public abstract class ProgressBase : RangeBase
    {
        #region Members

        private const string TrackName = "PART_Track";
        internal FrameworkElement Track;

        #endregion Members

        #region Constructors

        static ProgressBase()
        {
            MaximumProperty.OverrideMetadata(typeof(ProgressBase), new FrameworkPropertyMetadata(100d));
        }

        #endregion Constructors

        #region Properties

        #region Percent 百分比

        private static readonly DependencyPropertyKey PercentPropertyKey = DependencyProperty.RegisterReadOnly(
            "Percent",
            typeof(double), typeof(ProgressBase),
                                                new FrameworkPropertyMetadata(0d, FrameworkPropertyMetadataOptions.None, OnPercentChanged));

        public static readonly DependencyProperty PercentProperty = PercentPropertyKey.DependencyProperty;

        [Browsable(false)]
        public double Percent
        {
            get { return (double)GetValue(PercentProperty); }
            private set { SetValue(PercentPropertyKey, value); }
        }

        private static void OnPercentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var instance = (ProgressBase)obj;
            instance.OnPercentChanged((double)e.OldValue, (double)e.NewValue);
        }

        // ReSharper disable VirtualMemberNeverOverriden.Global
        protected virtual void OnPercentChanged(double oldPercent, double newPercent)
        // ReSharper restore VirtualMemberNeverOverriden.Global
        {
        }

        #endregion Percent 百分比

        #region Value 值

        protected override void OnValueChanged(double oldValue, double newValue)
        {
            base.OnValueChanged(oldValue, newValue);
            Percent = State != ProgressState.Normal || Maximum <= Minimum ? double.NaN : (Value - Minimum) / (Maximum - Minimum);
        }

        #endregion Value 值

        #region State 状态

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(ProgressState), typeof(ProgressBase),
                                        new FrameworkPropertyMetadata(ProgressState.Normal, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                                                                      OnStateChanged));

        [Bindable(true)]
        [Category("Behavior")]
        [Description("Determines the state of control.")]
        public ProgressState State
        {
            get { return (ProgressState)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        private static void OnStateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var progressBar = (ProgressBase)obj;
            progressBar.OnStateChanged((ProgressState)e.OldValue, (ProgressState)e.NewValue);
        }

        // ReSharper disable VirtualMemberNeverOverriden.Global
        protected virtual void OnStateChanged(ProgressState oldState, ProgressState newState)
        // ReSharper restore VirtualMemberNeverOverriden.Global
        {
            var peer = UIElementAutomationPeer.FromElement(this) as ProgressAutomationPeer;
            if (peer != null)
            {
                peer.InvalidatePeer();
            }

            if (IsEnabled)
            {
                switch (newState)
                {
                    case ProgressState.Busy:
                        VisualStateManager.GoToState(this, "Busy", true);
                        if (IndeterminateAnimation != null && IsIndeterminateAnimationRunning)
                        {
                            IsIndeterminateAnimationRunning = false;
                            IndeterminateAnimation.Stop(this);
                        }
                        if (BusyAnimation != null)
                        {
                            BusyAnimation.Begin(this, Template, true);
                            IsBusyAnimationRunning = true;
                        }
                        break;

                    case ProgressState.Indeterminate:
                        VisualStateManager.GoToState(this, "Indeterminate", true);
                        if (BusyAnimation != null && IsBusyAnimationRunning)
                        {
                            IsBusyAnimationRunning = false;
                            BusyAnimation.Stop(this);
                        }
                        if (IndeterminateAnimation != null)
                        {
                            IndeterminateAnimation.Begin(this, Template, true);
                            IsIndeterminateAnimationRunning = true;
                        }
                        break;

                    case ProgressState.Normal:
                        VisualStateManager.GoToState(this, "Normal", true);
                        if (IndeterminateAnimation != null && IsIndeterminateAnimationRunning)
                        {
                            IsIndeterminateAnimationRunning = false;
                            IndeterminateAnimation.Stop(this);
                        }
                        if (BusyAnimation != null && IsBusyAnimationRunning)
                        {
                            IsBusyAnimationRunning = false;
                            BusyAnimation.Stop(this);
                        }
                        break;
                }
            }
        }

        #endregion State 状态

        #region IndeterminateAnimation 动态动画

        internal const string DefaultIndeterminateAnimationName = "CF98B9E7AB2F4CBD9EA654552441CD6A";

        public static readonly DependencyProperty IndeterminateAnimationProperty =
            DependencyProperty.Register("IndeterminateAnimation", typeof(Storyboard), typeof(ProgressBase),
                                        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        [Category("Appearance")]
        [Description("Determines the animation that playing in Indeterminate state.")]
        public Storyboard IndeterminateAnimation
        {
            get { return (Storyboard)GetValue(IndeterminateAnimationProperty); }
            set { SetValue(IndeterminateAnimationProperty, value); }
        }

        #endregion IndeterminateAnimation 动态动画

        #region IsIndeterminateAnimationRunning 动态动画是否正在执行

        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "DependencyPropertyKey is immutable type")]
        protected static readonly DependencyPropertyKey IsIndeterminateAnimationRunningPropertyKey =
            DependencyProperty.RegisterReadOnly("IsIndeterminateAnimationRunning", typeof(bool), typeof(ProgressBase),
                                                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.None));

        public static readonly DependencyProperty IsIndeterminateAnimationRunningProperty = IsIndeterminateAnimationRunningPropertyKey.DependencyProperty;

        [Browsable(false)]
        public bool IsIndeterminateAnimationRunning
        {
            get { return (bool)GetValue(IsIndeterminateAnimationRunningProperty); }
            protected set { SetValue(IsIndeterminateAnimationRunningPropertyKey, value); }
        }

        #endregion IsIndeterminateAnimationRunning 动态动画是否正在执行

        #region BusyAnimation 忙碌动画

        internal const string DefaultBusyAnimationName = "B45C62BF28AC49FDB8F172249BF56E5B";

        public static readonly DependencyProperty BusyAnimationProperty =
            DependencyProperty.Register("BusyAnimation", typeof(Storyboard), typeof(ProgressBase),
                                        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        [Category("Appearance")]
        [Description("Determines the animation that playing in Busy state.")]
        public Storyboard BusyAnimation
        {
            get { return (Storyboard)GetValue(BusyAnimationProperty); }
            set { SetValue(BusyAnimationProperty, value); }
        }

        #endregion BusyAnimation 忙碌动画

        #region IsBusyAnimationRunning 忙碌动画是否正在执行

        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "DependencyPropertyKey is immutable type")]
        protected static readonly DependencyPropertyKey IsBusyAnimationRunningPropertyKey =
            DependencyProperty.RegisterReadOnly("IsBusyAnimationRunning", typeof(bool), typeof(ProgressBase),
                                                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.None));

        public static readonly DependencyProperty IsBusyAnimationRunningProperty = IsBusyAnimationRunningPropertyKey.DependencyProperty;

        [Browsable(false)]
        public bool IsBusyAnimationRunning
        {
            get { return (bool)GetValue(IsBusyAnimationRunningProperty); }
            protected set { SetValue(IsBusyAnimationRunningPropertyKey, value); }
        }

        public static readonly RoutedEvent AnimationsUpdatingEvent = EventManager.RegisterRoutedEvent("AnimationsUpdating", RoutingStrategy.Tunnel,
                                                                                                      typeof(RoutedEventHandler), typeof(ProgressBase));

        #endregion IsBusyAnimationRunning 忙碌动画是否正在执行

        #region 动画更新中事件

        [Category("Appearance")]
        [Description("Occurs when a state's animations updating.")]
        public event RoutedEventHandler AnimationsUpdating
        {
            add { AddHandler(AnimationsUpdatingEvent, value); }
            remove { RemoveHandler(AnimationsUpdatingEvent, value); }
        }

        // ReSharper disable VirtualMemberNeverOverriden.Global
        protected virtual void OnAnimationsUpdating(RoutedEventArgs e)
        // ReSharper restore VirtualMemberNeverOverriden.Global
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent AnimationsUpdatedEvent = EventManager.RegisterRoutedEvent("AnimationsUpdated", RoutingStrategy.Bubble,
                                                                                   typeof(RoutedEventHandler), typeof(ProgressBase));

        #endregion 动画更新中事件

        #region AnimationsUpdated 动画已更新事件

        [Category("Appearance")]
        [Description("Occurs when a state's animations updated.")]
        public event RoutedEventHandler AnimationsUpdated
        {
            add { AddHandler(AnimationsUpdatedEvent, value); }
            remove { RemoveHandler(AnimationsUpdatedEvent, value); }
        }

        // ReSharper disable VirtualMemberNeverOverriden.Global
        protected virtual void OnAnimationsUpdated(RoutedEventArgs e)
        // ReSharper restore VirtualMemberNeverOverriden.Global
        {
            RaiseEvent(e);
        }

        #endregion AnimationsUpdated 动画已更新事件

        #endregion Properties

        #region Fuctions

        [SecuritySafeCritical]
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            OnApplyTemplateInternal();
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new ProgressAutomationPeer(this);
        }

        [SecurityCritical]
        internal virtual void OnApplyTemplateInternal()
        {
            if (Template != null)
            {
                if (Track != null)
                {
                    Track.SizeChanged -= OnAnimationUpdate;
                }
                Track = Template.FindName(TrackName, this) as FrameworkElement;
                if (Track == null)
                {
                    Trace.TraceWarning(TrackName + " not found.");
                }
                else
                {
                    Track.SizeChanged += OnAnimationUpdate;
                }
            }
        }

        private void OnAnimationUpdate(object sender, SizeChangedEventArgs e)
        {
            if (!double.IsNaN(e.NewSize.Width) && !double.IsNaN(e.NewSize.Height))
            {
                OnAnimationsUpdating(new RoutedEventArgs(AnimationsUpdatingEvent));
                OnAnimationsUpdated(new RoutedEventArgs(AnimationsUpdatedEvent));
            }
        }

        #endregion Fuctions
    }
}