﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public class BoxButton : Button
    {
        static BoxButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BoxButton), new FrameworkPropertyMetadata(typeof(BoxButton)));
        }

        #region Label标签属性

        #region LabelTemplateProperty Label数据模板

        /// <summary>
        /// Label数据模板
        /// </summary>
        public DataTemplate LabelTemplate
        {
            get { return (DataTemplate)GetValue(LabelTemplateProperty); }
            set { SetValue(LabelTemplateProperty, value); }
        }

        /// <summary>
        /// Label数据模板
        /// </summary>
        public static readonly DependencyProperty LabelTemplateProperty = DependencyProperty.RegisterAttached(
            "LabelTemplate", typeof(DataTemplate), typeof(BoxButton), new FrameworkPropertyMetadata(null));

        #endregion LabelTemplateProperty Label数据模板

        #region LabelText 标签内容

        /// <summary>
        /// 标签标签文字
        /// </summary>
        public object LabelText
        {
            get { return (object)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        /// <summary>
        /// Label标签内容
        /// </summary>
        public static readonly DependencyProperty LabelTextProperty =
            DependencyProperty.Register("LabelText", typeof(object), typeof(BoxButton), new PropertyMetadata(null));

        #endregion LabelText 标签内容

        #region LabelMargin 标签Margin属性

        /// <summary>
        /// 标签Margin属性
        /// </summary>
        public Thickness LabelMargin
        {
            get { return (Thickness)GetValue(LabelMarginProperty); }
            set { SetValue(LabelMarginProperty, value); }
        }

        public static readonly DependencyProperty LabelMarginProperty =
            DependencyProperty.Register("LabelMargin", typeof(Thickness), typeof(BoxButton), new PropertyMetadata(new Thickness(0)));

        #endregion LabelMargin 标签Margin属性

        #region LabelHorizontalAlignment  标签HorizontalAlignment属性

        /// <summary>
        /// 标签HorizontalAlignment属性
        /// </summary>
        public HorizontalAlignment LabelHorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(LabelHorizontalAlignmentProperty); }
            set { SetValue(LabelHorizontalAlignmentProperty, value); }
        }

        /// <summary>
        /// Label 的 HorizontalAlignment 属性
        /// </summary>
        public static readonly DependencyProperty LabelHorizontalAlignmentProperty =
            DependencyProperty.Register("LabelHorizontalAlignment", typeof(HorizontalAlignment), typeof(BoxButton), new PropertyMetadata(HorizontalAlignment.Center));

        #endregion LabelHorizontalAlignment  标签HorizontalAlignment属性

        #endregion Label标签属性
    }
}