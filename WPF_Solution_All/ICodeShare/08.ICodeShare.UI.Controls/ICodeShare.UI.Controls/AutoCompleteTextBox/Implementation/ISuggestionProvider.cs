﻿using System.Collections;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 筛选器
    /// </summary>
    public interface ISuggestionProvider
    {
        #region Public Methods

        /// <summary>
        /// 获取筛选集合
        /// </summary>
        /// <param name="filter">筛选字符串</param>
        /// <returns></returns>
        IEnumerable GetSuggestions(string filter);

        #endregion Public Methods
    }
}