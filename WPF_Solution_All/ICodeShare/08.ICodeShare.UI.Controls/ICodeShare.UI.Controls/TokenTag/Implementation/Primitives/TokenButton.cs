﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;


namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class TokenButton : Button
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        internal TokenButton(object content)
            : base()
        {
            Content = content;
        }

        /// <summary>
        /// 
        /// </summary>
        public TokenButton()
            : base()
        {
        }
    }
}
