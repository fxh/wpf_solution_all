﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public sealed class BorderFix : ContentControl
    {
        #region Constructors

        /// <summary>
        /// 取代Border作为控件样式中内容控件的容器
        /// Initialises a new instance of the <see cref="BorderFix"/> class.
        /// </summary>
        static BorderFix()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BorderFix), new FrameworkPropertyMetadata(typeof(BorderFix)));
        }

        #endregion Constructors
    }
}