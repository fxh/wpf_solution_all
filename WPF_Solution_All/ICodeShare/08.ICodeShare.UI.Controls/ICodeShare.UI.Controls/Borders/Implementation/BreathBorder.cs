﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 呼吸等待效果
    /// </summary>
    public class BreathBorder:ContentControl
    {
        #region Members
        
        #endregion

        #region Constructors
        static BreathBorder()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BreathBorder), new FrameworkPropertyMetadata(typeof(BreathBorder)));

          }
        #endregion

        #region Properties

        #region CornerRadius 边框圆角

        /// <summary>
        /// The corner radius property.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            "CornerRadius",
            typeof(CornerRadius),
            typeof(BreathBorder));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        #endregion

        #region ClipContent 是否裁剪内容

        /// <summary>
        /// Gets or sets a value indicating whether the content is clipped.
        /// </summary>
        [System.ComponentModel.Category("Appearance"), System.ComponentModel.Description("Sets whether the content is clipped or not.")]
        public bool ClipContent
        {
            get { return (bool)this.GetValue(ClipContentProperty); }
            set { this.SetValue(ClipContentProperty, value); }
        }

        /// <summary>
        /// The clip content property.
        /// </summary>
        public static readonly DependencyProperty ClipContentProperty =
            DependencyProperty.Register("ClipContent", typeof(bool), typeof(BreathBorder), new PropertyMetadata(false));

        #endregion

        #region BreathColor 呼吸灯颜色

        /// <summary>
        /// Gets or sets a value indicating whether the content is clipped.
        /// </summary>
        [System.ComponentModel.Category("Appearance"), System.ComponentModel.Description("Sets the Flash color.")]
        public Color BreathColor
        {
            get { return (Color)this.GetValue(BreathColorProperty); }
            set { this.SetValue(BreathColorProperty, value); }
        }

          /// <summary>
        /// The inner glow colour.
        /// </summary>
        public static readonly DependencyProperty BreathColorProperty =
            DependencyProperty.Register("BreathColor", typeof(Color), typeof(BreathBorder), new PropertyMetadata(Colors.LightGray));

        #endregion

        #region RippleDisabled 是否不可用

        /// <summary>
        /// Set to <c>True</c> to disable ripple effect
        /// </summary>
        public static readonly DependencyProperty IsDisabledProperty = DependencyProperty.RegisterAttached(
            "RippleDisabled", typeof(bool), typeof(BreathBorder), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));

        /// <summary>
        ///   Determine if <c>True</c> to disable ripple effect
        /// </summary>
        public bool RippleDisabled
        {
            get { return (bool)GetValue(IsDisabledProperty); }
            set { SetValue(IsDisabledProperty, value); }
        }

        #endregion

        #endregion
    }
}
