﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public class CalloutToolTip : ToolTip
    {
        #region 依赖属性定义

        #region AnglePlacement

        /// <summary>
        /// 鼠标按下时按钮的背景色
        /// </summary>
        public TailPlacementMode AnglePlacement
        {
            get { return (TailPlacementMode)GetValue(AnglePlacementProperty); }
            set { SetValue(AnglePlacementProperty, value); }
        }

        public static readonly DependencyProperty AnglePlacementProperty =
            DependencyProperty.Register("AnglePlacement", typeof(TailPlacementMode), typeof(CalloutToolTip), new PropertyMetadata(TailPlacementMode.TopLeft));

        #endregion AnglePlacement

        #region IsShowShadow

        /// <summary>
        /// 是否显示阴影
        /// </summary>
        public bool IsShowShadow
        {
            get { return (bool)GetValue(IsShowShadowProperty); }
            set { SetValue(IsShowShadowProperty, value); }
        }

        public static readonly DependencyProperty IsShowShadowProperty =
            DependencyProperty.Register("IsShowShadow", typeof(bool), typeof(CalloutToolTip), new PropertyMetadata(true));

        #endregion IsShowShadow

        #endregion 依赖属性定义

        #region Constructors

        static CalloutToolTip()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalloutToolTip), new FrameworkPropertyMetadata(typeof(CalloutToolTip)));
        }

        #endregion Constructors
    }
}