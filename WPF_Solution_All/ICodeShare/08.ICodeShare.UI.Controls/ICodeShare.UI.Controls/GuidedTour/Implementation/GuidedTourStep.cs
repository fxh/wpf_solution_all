﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public enum GuidePlacementMode
    {
        /// <summary>
        /// Indicates left placement.
        /// </summary>
        Left,

        /// <summary>
        /// Indicates top placement.
        /// </summary>
        Top,

        /// <summary>
        /// Indicates right placement.
        /// </summary>
        Right,

        /// <summary>
        /// Indicates bottom placement.
        /// </summary>
        Bottom
    }

    public class GuidedTourStep : ContentControl
    {
        /// <summary>
        /// Static constructor for GuidedTourItem class.
        /// </summary>
        static GuidedTourStep()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(GuidedTourStep), new FrameworkPropertyMetadata(typeof(GuidedTourStep)));
        }

        #region Properties

        #region Title

        /// <summary>
        /// Gets or sets title.
        /// </summary>
        public object Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        /// <summary>
        /// Identifies TitleProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(GuidedTourStep));

        #endregion Title

        #region TargetElement 目标对象

        /// <summary>
        /// Gets or sets target element.
        /// </summary>
        public FrameworkElement TargetElement { get; set; }

        #endregion TargetElement 目标对象

        #region AlternateElements 备用对象

        /// <summary>
        /// Gets or sets alternate target elements.
        /// </summary>
        public FrameworkElement[] AlternateElements { get; set; }

        #endregion AlternateElements 备用对象

        #region Placement

        /// <summary>
        /// Gets or sets placement.
        /// </summary>
        public GuidePlacementMode Placement
        {
            get { return (GuidePlacementMode)GetValue(PlacementProperty); }
            set { SetValue(PlacementProperty, value); }
        }

        /// <summary>
        /// Identifies PlacementProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty PlacementProperty = DependencyProperty.Register("Placement", typeof(GuidePlacementMode), typeof(GuidedTourStep));

        #endregion Placement

        #region Position

        /// <summary>
        /// Gets or sets item tip position.
        /// </summary>
        public Point Position
        {
            get { return (Point)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        /// <summary>
        /// Identifies PositionProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty PositionProperty = DependencyProperty.Register("Position", typeof(Point), typeof(GuidedTourStep));

        #endregion Position

        #region AutoFocus

        /// <summary>
        /// Gets or sets a value indicating whether item should automatically focus target element.
        /// </summary>
        public bool AutoFocus
        {
            get { return (bool)GetValue(AutoFocusProperty); }
            set { SetValue(AutoFocusProperty, value); }
        }

        /// <summary>
        /// Identifies AutoFocusProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty AutoFocusProperty = DependencyProperty.Register("AutoFocus", typeof(bool), typeof(GuidedTourStep));

        #endregion AutoFocus

        #endregion Properties

        /// <summary>
        /// Hides this item.
        /// </summary>
        public void Hide()
        {
            Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Shows this item.
        /// </summary>
        public void Show()
        {
            Visibility = Visibility.Visible;
        }
    }
}