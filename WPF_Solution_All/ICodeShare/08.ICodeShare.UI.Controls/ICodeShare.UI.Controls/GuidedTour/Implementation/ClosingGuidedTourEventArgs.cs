﻿using System.Windows;

namespace ICodeShare.UI.Controls
{
    public delegate void ClosingGuidedTourEventHandler(object sender, ClosingGuidedTourEventArgs e);

    public class ClosingGuidedTourEventArgs : RoutedEventArgs
    {
        /// <summary>
        /// Initializes new instance of ClosingGuidedTourEventArgs with specified routed event.
        /// </summary>
        /// <param name="routedEvent">Routed event.</param>
        public ClosingGuidedTourEventArgs(RoutedEvent routedEvent)
            : base(routedEvent)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether closing action was canceled.
        /// </summary>
        public bool Cancel { get; set; }
    }
}