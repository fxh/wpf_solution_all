﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// A control that displays text, with a drop shadow.
    /// </summary>
    public class RaisedTextBlock : Control
    {
        /// <summary>
        /// The drop shadow color property.
        /// </summary>
        public static readonly DependencyProperty DropShadowColorProperty = DependencyProperty.Register("DropShadowColor", typeof(Color),
                                                                                                                                          typeof(RaisedTextBlock),
                                                                                                                                          new PropertyMetadata(Colors.Transparent));

        /// <summary>
        /// The drop shadow opacity property.
        /// </summary>
        public static readonly DependencyProperty DropShadowOpacityProperty = DependencyProperty.Register("DropShadowOpacity", typeof(double),
                                                                                                                                            typeof(RaisedTextBlock),
                                                                                                                                            new PropertyMetadata(0.0));

        /// <summary>
        /// The text property.
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(RaisedTextBlock), null);

        /// <summary>
        /// The text decorations property.
        /// </summary>
        public static readonly DependencyProperty TextDecorationsProperty = DependencyProperty.Register("TextDecorations", typeof(TextDecorationCollection),
                                                                                                                                          typeof(RaisedTextBlock), null);

        /// <summary>
        /// The text wrapping property.
        /// </summary>
        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register("TextWrapping", typeof(TextWrapping),
                                                                                                                                      typeof(RaisedTextBlock), null);

        /// <summary>
        /// The drop shadow distance property.
        /// </summary>
        public static readonly DependencyProperty DropShadowDistanceProperty = DependencyProperty.Register("DropShadowDistance", typeof(double),
                                                                                                                                              typeof(RaisedTextBlock),
                                                                                                                                              new PropertyMetadata(DropShadowDistanceChanged));

        /// <summary>
        /// The drop shadow angle property.
        /// </summary>
        public static readonly DependencyProperty DropShadowAngleProperty = DependencyProperty.Register("DropShadowAngle", typeof(double),
                                                                                                                                          typeof(RaisedTextBlock),
                                                                                                                                          new PropertyMetadata(DropShadowAngleChanged));


        /// <summary>
        /// Stores the drop shadow translate transform.
        /// </summary>
        private TranslateTransform _dropShadowTranslate;

        /// <summary>
        /// RaisedTextBlock constructor.
        /// </summary>
        public RaisedTextBlock()
        {
            DefaultStyleKey = typeof(RaisedTextBlock);
        }

        /// <summary>
        /// Gets or sets the drop shadow color.
        /// </summary>
        [Category("Appearance"), Description("The drop shadow color.")]
        public Color DropShadowColor
        {
            get
            {
                return (Color)GetValue(DropShadowColorProperty);
            }
            set
            {
                SetValue(DropShadowColorProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the drop shadow opacity.
        /// </summary>
        [Category("Appearance"), Description("The drop shadow opacity.")]
        public double DropShadowOpacity
        {
            get
            {
                return (double)GetValue(DropShadowOpacityProperty);
            }
            set
            {
                SetValue(DropShadowOpacityProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the link text.
        /// </summary>
        [Category("Common Properties"), Description("The text content.")]
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the text decorations.
        /// </summary>
        [Category("Common Properties"), Description("The text decorations.")]
        public TextDecorationCollection TextDecorations
        {
            get
            {
                return (TextDecorationCollection)GetValue(TextDecorationsProperty);
            }
            set
            {
                SetValue(TextDecorationsProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the text wrapping.
        /// </summary>
        [Category("Common Properties"), Description("Whether the text wraps.")]
        public TextWrapping TextWrapping
        {
            get
            {
                return (TextWrapping)GetValue(TextWrappingProperty);
            }
            set
            {
                SetValue(TextWrappingProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the drop shadow distance.
        /// </summary>
        [Category("Appearance"), Description("The drop shadow distance.")]
        public double DropShadowDistance
        {
            get
            {
                return (double)GetValue(DropShadowDistanceProperty);
            }
            set
            {
                SetValue(DropShadowDistanceProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the drop shadow angle.
        /// </summary>
        [Category("Appearance"), Description("The drop shadow angle.")]
        public double DropShadowAngle
        {
            get
            {
                return (double)GetValue(DropShadowAngleProperty);
            }
            set
            {
                SetValue(DropShadowAngleProperty, value);
            }
        }

        /// <summary>
        /// Gets the UI elements out of the template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _dropShadowTranslate = GetTemplateChild("PART_DropShadowTranslate") as TranslateTransform;
            UpdateDropShadowPosition();
        }

        /// <summary>
        /// Converts degrees into radians.
        /// </summary>
        /// <param name="degrees">The degree value.</param>
        /// <returns>The degrees as radians.</returns>
        private static double DegreesToRadians(double degrees)
        {
            return degrees * (Math.PI / 180);
        }

        /// <summary>
        /// Gets a point offset by a distance and angle (in degrees).
        /// </summary>
        /// <param name="angle">The angle in degrees.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>The offset point.</returns>
        private static Point GetOffset(double angle, double distance)
        {
            double x = Math.Cos(DegreesToRadians(angle)) * distance;
            double y = Math.Tan(DegreesToRadians(angle)) * x;
            return new Point(x, y);
        }

        /// <summary>
        /// Updates the drop shadow.
        /// </summary>
        internal void UpdateDropShadowPosition()
        {
            if (_dropShadowTranslate != null)
            {
                Point offset = GetOffset(DropShadowAngle, DropShadowDistance);

                _dropShadowTranslate.X = offset.X;
                _dropShadowTranslate.Y = offset.Y;
            }
        }


        /// <summary>
        /// Updates the drop shadow.
        /// </summary>
        /// <param name="dependencyObject">The drop shadow text block.</param>
        /// <param name="eventArgs">Dependency Property Changed Event Args</param>
        private static void DropShadowDistanceChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var dropShadowTextBlock = (RaisedTextBlock)dependencyObject;
            dropShadowTextBlock.UpdateDropShadowPosition();
        }

        /// <summary>
        /// Updates the drop shadow.
        /// </summary>
        /// <param name="dependencyObject">The drop shadow text block.</param>
        /// <param name="eventArgs">Dependency Property Changed Event Args</param>
        private static void DropShadowAngleChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var dropShadowTextBlock = (RaisedTextBlock)dependencyObject;
            dropShadowTextBlock.UpdateDropShadowPosition();
        }
    }
}