﻿自定义控件设置默认样式文件的两种方法：
1.控件中添加Static构造函数重写DefaultStyleKeyProperty元数据
   static SwitchButton()
   {
            //设置用于引用此控件的样式，方式一
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchButton), new FrameworkPropertyMetadata(typeof(SwitchButton)));
    }
2.在无参数构造函数中为DefaultStyleKey赋值
     public SwitchButton()
     {
            //设置用于引用此控件的样式，方式二
            DefaultStyleKey = typeof(SwitchButton);
      }