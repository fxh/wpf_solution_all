﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls
{
    // When adding to this enum, please update the OnFilterModePropertyChanged
    // in the AutoCompleteBox class that is used for validation.

    /// <summary>
    /// Specifies how text in the text box portion of the
    /// <see cref="T:System.Windows.Controls.AutoCompleteBox" /> control is used
    /// to filter items specified by the
    /// <see cref="P:System.Windows.Controls.AutoCompleteBox.ItemsSource" />
    /// property for display in the drop-down.
    /// </summary>
    /// <QualityBand>Stable</QualityBand>
    public enum AutoCompleteFilterMode
    {
        /// <summary>
        /// Specifies that no filter is used. All items are returned.
        /// </summary>
        None = 0,

        /// <summary>
        /// 以...开头
        /// Specifies a culture-sensitive, case-insensitive filter where the
        /// returned items start with the specified text. The filter uses the
        /// <see cref="M:System.String.StartsWith(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.CurrentCultureIgnoreCase" /> as
        /// the string comparison criteria.
        /// </summary>
        StartsWith = 1,

        /// <summary>
        /// 以...开头，使用区域敏感排序规则和当前区域比较字符串。
        /// Specifies a culture-sensitive, case-sensitive filter where the
        /// returned items start with the specified text. The filter uses the
        /// <see cref="M:System.String.StartsWith(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.CurrentCulture" /> as the string
        /// comparison criteria.
        /// </summary>
        StartsWithCaseSensitive = 2,

        /// <summary>
        /// 以...开头，忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-insensitive filter where the returned
        /// items start with the specified text. The filter uses the
        /// <see cref="M:System.String.StartsWith(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.OrdinalIgnoreCase" /> as the
        /// string comparison criteria.
        /// </summary>
        StartsWithOrdinal = 3,

        /// <summary>
        /// 以...开头，使用区域敏感排序规则、当前区域来比较字符串，同时忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-sensitive filter where the returned items
        /// start with the specified text. The filter uses the
        /// <see cref="M:System.String.StartsWith(System.String,System.StringComparison)" />
        /// method, specifying <see cref="P:System.StringComparer.Ordinal" /> as
        /// the string comparison criteria.
        /// </summary>
        StartsWithOrdinalCaseSensitive = 4,

        /// <summary>
        /// 包含
        /// Specifies a culture-sensitive, case-insensitive filter where the
        /// returned items contain the specified text.
        /// </summary>
        Contains = 5,

        /// <summary>
        /// 包含，使用区域敏感排序规则和当前区域比较字符串。
        /// Specifies a culture-sensitive, case-sensitive filter where the
        /// returned items contain the specified text.
        /// </summary>
        ContainsCaseSensitive = 6,

        /// <summary>
        /// 包含，忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-insensitive filter where the returned
        /// items contain the specified text.
        /// </summary>
        ContainsOrdinal = 7,

        /// <summary>
        /// 包含，使用区域敏感排序规则、当前区域来比较字符串，同时忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-sensitive filter where the returned items
        /// contain the specified text.
        /// </summary>
        ContainsOrdinalCaseSensitive = 8,

        /// <summary>
        /// 相等
        /// Specifies a culture-sensitive, case-insensitive filter where the
        /// returned items equal the specified text. The filter uses the
        /// <see cref="M:System.String.Equals(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.CurrentCultureIgnoreCase" /> as
        /// the search comparison criteria.
        /// </summary>
        Equals = 9,

        /// <summary>
        /// 相等，使用区域敏感排序规则和当前区域比较字符串。
        /// Specifies a culture-sensitive, case-sensitive filter where the
        /// returned items equal the specified text. The filter uses the
        /// <see cref="M:System.String.Equals(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.CurrentCulture" /> as the string
        /// comparison criteria.
        /// </summary>
        EqualsCaseSensitive = 10,

        /// <summary>
        /// 相等，忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-insensitive filter where the returned
        /// items equal the specified text. The filter uses the
        /// <see cref="M:System.String.Equals(System.String,System.StringComparison)" />
        /// method, specifying
        /// <see cref="P:System.StringComparer.OrdinalIgnoreCase" /> as the
        /// string comparison criteria.
        /// </summary>
        EqualsOrdinal = 11,

        /// <summary>
        /// 相等，使用区域敏感排序规则、当前区域来比较字符串，同时忽略被比较字符串的大小写。
        /// Specifies an ordinal, case-sensitive filter where the returned items
        /// equal the specified text. The filter uses the
        /// <see cref="M:System.String.Equals(System.String,System.StringComparison)" />
        /// method, specifying <see cref="P:System.StringComparer.Ordinal" /> as
        /// the string comparison criteria.
        /// </summary>
        EqualsOrdinalCaseSensitive = 12,

        /// <summary>
        /// 自定义
        /// Specifies that a custom filter is used. This mode is used when the
        /// <see cref="P:System.Windows.Controls.AutoCompleteBox.TextFilter" />
        /// or
        /// <see cref="P:System.Windows.Controls.AutoCompleteBox.ItemFilter" />
        /// properties are set.
        /// </summary>
        Custom = 13,
    }
}
