﻿using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;

namespace ICodeShare.UI.Controls.Automation
{
    public class ToggleCommandButtonAutomationPeer : CommandButtonBaseAutomationPeer, IToggleProvider
    {
        public ToggleCommandButtonAutomationPeer(ToggleCommandButton owner)
            : base(owner)
        {
        }

        [System.Diagnostics.Contracts.Pure]
        protected override string GetClassNameCore()
        {
            Contract.Ensures(Contract.Result<string>() == "ToggleCommandButton");
            return "ToggleCommandButton";
        }

        public override object GetPattern(PatternInterface patternInterface)
        {
            return patternInterface == PatternInterface.Toggle ? this : base.GetPattern(patternInterface);
        }

        public void Toggle()
        {
            if (!IsEnabled())
            {
                throw new ElementNotEnabledException();
            }

            var owner = (ToggleCommandButton)Owner;
            owner.OnToggle();
        }

        public ToggleState ToggleState
        {
            get
            {
                var owner = (ToggleCommandButton)Owner;
                return ConvertToToggleState(owner.IsChecked);
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        internal void RaiseToggleStatePropertyChangedEvent(bool? oldValue, bool? newValue)
        {
            if (oldValue != newValue)
            {
                RaisePropertyChangedEvent(TogglePatternIdentifiers.ToggleStateProperty, ConvertToToggleState(oldValue), ConvertToToggleState(newValue));
            }
        }

        private static ToggleState ConvertToToggleState(bool? value)
        {
            switch (value)
            {
                case null:
                    return ToggleState.Indeterminate;

                case true:
                    return ToggleState.On;

                default:
                    return ToggleState.Off;
            }
        }
    }
}