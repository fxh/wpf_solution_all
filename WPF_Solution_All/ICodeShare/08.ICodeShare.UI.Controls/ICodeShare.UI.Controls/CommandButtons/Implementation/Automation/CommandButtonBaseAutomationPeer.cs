﻿using ICodeShare.UI.Controls.Primitives;
using System.Diagnostics.Contracts;
using System.Windows.Automation.Peers;

namespace ICodeShare.UI.Controls.Automation
{
    public abstract class CommandButtonBaseAutomationPeer : ButtonBaseAutomationPeer
    {
        protected CommandButtonBaseAutomationPeer(CommandButtonBase owner)
            : base(owner)
        {
        }

        public new CommandButtonBase Owner
        {
            get
            {
                Contract.Ensures(Contract.Result<CommandButtonBase>() != null);

                var result = (CommandButtonBase)base.Owner;

                // NOTE: Lack of contracts: Owner must ensure non-null value
                Contract.Assume(result != null);
                return result;
            }
        }

        [System.Diagnostics.Contracts.Pure]
        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            Contract.Ensures(Contract.Result<AutomationControlType>() == AutomationControlType.Button);
            return AutomationControlType.Button;
        }
    }
}