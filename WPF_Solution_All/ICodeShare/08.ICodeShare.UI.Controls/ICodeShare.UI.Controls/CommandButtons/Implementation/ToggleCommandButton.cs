﻿using ICodeShare.UI.Controls.Automation;
using ICodeShare.UI.Controls.Primitives;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Automation.Peers;

namespace ICodeShare.UI.Controls
{
    [DefaultEvent("Checked")]
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public class ToggleCommandButton : CommandButtonBase
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "We need to use static constructor for custom actions during dependency properties initialization")]
        static ToggleCommandButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ToggleCommandButton), new FrameworkPropertyMetadata(typeof(ToggleCommandButton)));
        }

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool?), typeof(ToggleCommandButton),
                                        new FrameworkPropertyMetadata(false,
                                                                      FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnIsCheckedChanged));

        [Bindable(true)]
        [Category("Appearance")]
        [Description("Indicates whether the button is checked.")]
        [TypeConverter(typeof(NullableBoolConverter))]
        [Localizability(LocalizationCategory.None, Readability = Readability.Unreadable)]
        public bool? IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        private static void OnIsCheckedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var instance = (ToggleCommandButton)obj;
            instance.OnIsCheckedChanged((bool?)e.OldValue, (bool?)e.NewValue);
        }

        protected virtual void OnIsCheckedChanged(bool? oldIsChecked, bool? newIsChecked)
        {
            if (oldIsChecked != newIsChecked)
            {
                var peer = UIElementAutomationPeer.FromElement(this) as ToggleCommandButtonAutomationPeer;
                if (peer != null)
                {
                    peer.RaiseToggleStatePropertyChangedEvent(oldIsChecked, newIsChecked);
                }

                switch (newIsChecked)
                {
                    case true:
                        OnChecked(new RoutedEventArgs(CheckedEvent));
                        VisualStateManager.GoToState(this, "Checked", true);
                        break;

                    case false:
                        OnUnchecked(new RoutedEventArgs(UncheckedEvent));
                        VisualStateManager.GoToState(this, "Normal", true);
                        break;

                    case null:
                        OnIndeterminate(new RoutedEventArgs(IndeterminateEvent));
                        VisualStateManager.GoToState(this, "Indeterminate", true);
                        break;
                }
            }
        }

        public static readonly RoutedEvent CheckedEvent = EventManager.RegisterRoutedEvent("Checked", RoutingStrategy.Bubble,
                                                                                           typeof(RoutedEventHandler), typeof(ToggleCommandButton));

        [Category("Behavior")]
        [Description("Occurs when a button is checked.")]
        public event RoutedEventHandler Checked
        {
            add { AddHandler(CheckedEvent, value); }
            remove { RemoveHandler(CheckedEvent, value); }
        }

        protected virtual void OnChecked(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent UncheckedEvent = EventManager.RegisterRoutedEvent("Unchecked", RoutingStrategy.Bubble,
                                                                                             typeof(RoutedEventHandler), typeof(ToggleCommandButton));

        [Category("Behavior")]
        [Description("Occurs when a button is unchecked.")]
        public event RoutedEventHandler Unchecked
        {
            add { AddHandler(UncheckedEvent, value); }
            remove { RemoveHandler(UncheckedEvent, value); }
        }

        protected virtual void OnUnchecked(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent IndeterminateEvent = EventManager.RegisterRoutedEvent("Indeterminate", RoutingStrategy.Bubble,
                                                                                                 typeof(RoutedEventHandler), typeof(ToggleCommandButton));

        [Category("Behavior")]
        [Description("Occurs when a button state is indeterminate.")]
        public event RoutedEventHandler Indeterminate
        {
            add { AddHandler(IndeterminateEvent, value); }
            remove { RemoveHandler(IndeterminateEvent, value); }
        }

        protected virtual void OnIndeterminate(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly DependencyProperty IsThreeStateProperty =
            DependencyProperty.Register("IsThreeState", typeof(bool), typeof(ToggleCommandButton),
                                        new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.None));

        [Bindable(true)]
        [Category("Behavior")]
        [Description("Determines whether the control supports two or three states.")]
        public bool IsThreeState
        {
            get { return (bool)GetValue(IsThreeStateProperty); }
            set { SetValue(IsThreeStateProperty, value); }
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new ToggleCommandButtonAutomationPeer(this);
        }

        protected override void OnClick()
        {
            base.OnClick();
            OnToggle();
        }

        protected internal virtual void OnToggle()
        {
            bool? isChecked;
            if (IsChecked == true)
            {
                isChecked = IsThreeState ? (bool?)null : false;
            }
            else
            {
                isChecked = IsChecked.HasValue;
            }
            IsChecked = isChecked;
        }
    }
}