﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public enum WaterfallMode
    {
        /// <summary>
        /// 资源色宽度
        /// </summary>
        ItemWidth,

        /// <summary>
        /// 列数
        /// </summary>
        Columns
    }

    /// <summary>
    /// 瀑布流容器
    /// </summary>
    public class WaterfallCanvas : Canvas
    {
        private const double DEFAULITEMWIDTH = 180;

        #region Constructors

        static WaterfallCanvas()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WaterfallCanvas), new FrameworkPropertyMetadata(typeof(WaterfallCanvas)));
        }

        public WaterfallCanvas()
        {
            Loaded += delegate
            {
                if (Mode == WaterfallMode.ItemWidth)
                {
                    SetColumnsByItemWidth(ItemWidth);
                }
                else
                {
                    SetItemWidthByColumns(Columns);
                }
                Margin = new Thickness(Margin.Left);
            };
            SizeChanged += delegate
            {
                if (Mode == WaterfallMode.ItemWidth)
                {
                    SetColumnsByItemWidth(ItemWidth);
                }
                else
                {
                    SetItemWidthByColumns(Columns);
                }
            };
        }

        #endregion Constructors

        #region Properties

        #region Mode 布局模式

        /// <summary>
        /// 布局模式
        /// </summary>
        public WaterfallMode Mode
        {
            get { return (WaterfallMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ModeProperty =
               DependencyProperty.Register("Mode", typeof(WaterfallMode), typeof(WaterfallCanvas), new PropertyMetadata(WaterfallMode.Columns));

        #endregion Mode 布局模式

        #region ItemWidth 总列树

        /// <summary>
        /// 总列树
        /// </summary>
        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }

        public static readonly DependencyProperty ItemWidthProperty =
               DependencyProperty.Register("ItemWidth", typeof(double), typeof(WaterfallCanvas), new PropertyMetadata(DEFAULITEMWIDTH, OnItemWidthPropertyChanged));

        /// <summary>
        /// ItemWidthProperty property changed handler.
        /// </summary>
        /// <param name="dependencyObject">WaterfallCanvas that changed its ItemWidth.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnItemWidthPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            WaterfallCanvas source = (WaterfallCanvas)dependencyObject;
            if (source.Mode == WaterfallMode.ItemWidth)
            {
                source.SetColumnsByItemWidth((double)e.NewValue);
            }
        }

        #endregion ItemWidth 总列树

        #region Columns

        /// <summary>
        /// 列数
        /// </summary>
        public int Columns
        {
            get { return (int)this.GetValue(ColumnsProperty); }
            set { this.SetValue(ColumnsProperty, value); }
        }

        public static readonly DependencyProperty ColumnsProperty =
            DependencyProperty.Register("Columns", typeof(int), typeof(WaterfallCanvas), new PropertyMetadata(0, OnColumnsPropertyChanged));

        public static void OnColumnsPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            WaterfallCanvas source = (WaterfallCanvas)dependencyObject;
            if (source.Mode == WaterfallMode.Columns)
            {
                source.SetItemWidthByColumns((int)e.NewValue);
            }
        }

        #endregion Columns

        #endregion Properties

        #region Methods

        /// <summary>
        /// 根据列宽度获取列树
        /// </summary>
        private void SetColumnsByItemWidth(double itemWidth)
        {
            Columns = (int)(ActualWidth / itemWidth);
            if (Columns <= 0)
            {
                Columns = 1;
            }
            Refresh();
        }

        private void SetItemWidthByColumns(int columns)
        {
            ItemWidth = (ActualWidth / columns);
            if (ItemWidth <= 0)
            {
                ItemWidth = DEFAULITEMWIDTH;
            }
            Refresh();
        }

        public void Add(FrameworkElement element)
        {
            if (element.Visibility != Visibility.Collapsed)
            {
                element.Width = ItemWidth;
                if (element is Grid)
                {
                    if ((element as Grid).Children.Count > 0)
                    {
                        ((element as Grid).Children[0] as FrameworkElement).Margin = new Thickness(Margin.Left);
                    }
                }

                Children.Add(element);
                Refresh();
            }
        }

        private void Refresh()
        {
            // 初始化参数
            var maxHeight = 0.0;
            var list = new Dictionary<int, Point>();
            var nlist = new Dictionary<int, Dictionary<int, Point>>();
            for (int i = 0; i < Children.Count; i++)
            {
                (Children[i] as FrameworkElement).UpdateLayout();
                list.Add(i, new Point(i, (Children[i] as FrameworkElement).ActualHeight, 0.0));
            }
            for (int i = 0; i < Columns; i++)
            {
                nlist.Add(i, new Dictionary<int, Point>());
            }

            // 智能排序到 nlist
            for (int i = 0; i < list.Count; i++)
            {
                if (i < Columns)
                {
                    list[i].Buttom = list[i].Height;
                    nlist[i].Add(nlist[i].Count, list[i]);
                }
                else
                {
                    var b = 0.0;
                    var l = 0;
                    for (int j = 0; j < Columns; j++)
                    {
                        var jh = nlist[j][nlist[j].Count - 1].Buttom + list[i].Height;
                        if (b == 0.0 || jh < b)
                        {
                            b = jh;
                            l = j;
                        }
                    }
                    list[i].Buttom = b;
                    nlist[l].Add(nlist[l].Count, list[i]);
                }
            }

            // 开始布局
            for (int i = 0; i < nlist.Count; i++)
            {
                for (int j = 0; j < nlist[i].Count; j++)
                {
                    Children[nlist[i][j].Index].SetValue(LeftProperty, i * ActualWidth / Columns);
                    Children[nlist[i][j].Index].SetValue(TopProperty, nlist[i][j].Buttom - nlist[i][j].Height);
                    Children[nlist[i][j].Index].SetValue(WidthProperty, ActualWidth / Columns);

                    if (Children[nlist[i][j].Index] is Grid)
                    {
                        ((Children[nlist[i][j].Index] as Grid).Children[0] as FrameworkElement).Margin = Margin;
                    }
                }

                // 不知道为什么如果不写这么一句会出错
                if (nlist.ContainsKey(i))
                {
                    if (nlist[i].ContainsKey(nlist[i].Count - 1))
                    {
                        var mh = nlist[i][nlist[i].Count - 1].Buttom;
                        maxHeight = mh > maxHeight ? mh : maxHeight;
                    }
                }
            }
            Height = maxHeight;
            list.Clear();
            nlist.Clear();
        }

        public void Remove(UIElement element)
        {
            Children.Remove(element);
            Refresh();
        }

        #endregion Methods

        public class Point
        {
            public int Index { get; set; }

            public double Buttom { get; set; }

            public double Height { get; set; }

            public Point(int index, double height, double buttom)
            {
                Index = index; Height = height; Buttom = buttom;
            }
        }
    }
}