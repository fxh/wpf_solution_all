﻿namespace ICodeShare.UI.Controls
{
    public enum DateTimeFormat
    {
        Custom,
        FullDateTime,
        LongDate,
        LongTime,
        MonthDay,
        RFC1123,
        ShortDate,
        ShortTime,
        SortableDateTime,
        UniversalSortableDateTime,
        YearMonth
    }
}