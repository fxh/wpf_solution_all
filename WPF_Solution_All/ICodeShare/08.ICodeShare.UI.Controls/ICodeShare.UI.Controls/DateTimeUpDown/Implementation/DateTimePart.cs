﻿namespace ICodeShare.UI.Controls
{
    public enum DateTimePart
    {
        Day,
        DayName,
        AmPmDesignator,
        Millisecond,
        Hour12,
        Hour24,
        Minute,
        Month,
        MonthName,
        Other,
        Period,
        TimeZone,
        Second,
        Year
    }
}