﻿using System;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 缩略图数据源源类型
    /// </summary>
    public enum EnumThumbnail
    {
        Image,
        Vedio,
        WebImage,
        Auto,
        FileX,
    }
}