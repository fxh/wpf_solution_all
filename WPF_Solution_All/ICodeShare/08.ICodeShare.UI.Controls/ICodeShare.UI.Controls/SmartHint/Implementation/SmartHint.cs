﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// This interface is the adapter from UiControl (like <see cref="TextBox"/>, <see cref="ComboBox"/> and others) to <see cref="SmartHint"/>
    /// <para/>
    /// You should implement this interface in order to use SmartHint for your own control.
    /// </summary>
    public interface IHintProxy : IDisposable
    {
        /// <summary>
        /// Checks to see if the targetted control can be deemed as logically
        /// empty, even if not null, affecting the current hint display.
        /// </summary>
        /// <returns></returns>
        bool IsEmpty();

        [Obsolete]
        object Content { get; }

        bool IsLoaded { get; }

        bool IsVisible { get; }

        event EventHandler ContentChanged;

        event EventHandler IsVisibleChanged;

        event EventHandler Loaded;
    }

    /// <summary>
    /// A control that implement placeholder behavior. Can work as a simple placeholder either as a floating hint, see <see cref="UseFloating"/> property.
    /// <para/>
    /// To set a target control you should set the HintProxy property. Use the <see cref="HintProxyFabricConverter.Instance"/> converter which converts a control into the IHintProxy interface.
    /// </summary>
    [TemplateVisualState(GroupName = ContentStatesGroupName, Name = ContentEmptyName)]
    [TemplateVisualState(GroupName = ContentStatesGroupName, Name = ContentNotEmptyName)]
    public class SmartHint : Control
    {
        #region Members

        public const string ContentStatesGroupName = "ContentStates";
        public const string ContentEmptyName = "ContentEmpty";
        public const string ContentNotEmptyName = "ContentNotEmpty";
        private ContentControl _floatingHintPart = null;

        #endregion

        #region Constructors

        static SmartHint()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SmartHint), new FrameworkPropertyMetadata(typeof(SmartHint)));
        }

        public SmartHint()
        {
            IsHitTestVisible = false;
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
        }

        #endregion

        #region Properties 属性

        #region HintProxy 附加提示的对象


        public IHintProxy HintProxy 
        {
            get { return (IHintProxy)GetValue(HintProxyProperty); }
            set { SetValue(HintProxyProperty, value); }
        }

        public static readonly DependencyProperty HintProxyProperty = DependencyProperty.Register(
            "HintProxy", typeof(IHintProxy), typeof(SmartHint), new PropertyMetadata(default(IHintProxy), HintProxyPropertyChangedCallback));

        private static void HintProxyPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var smartHint = dependencyObject as SmartHint;
            if (smartHint == null) return;

            var hintProxy = dependencyPropertyChangedEventArgs.OldValue as IHintProxy;

            if (hintProxy != null)
            {
                hintProxy.IsVisibleChanged -= smartHint.OnHintProxyIsVisibleChanged;
                hintProxy.ContentChanged -= smartHint.OnHintProxyContentChanged;
                hintProxy.Loaded -= smartHint.OnHintProxyContentChanged;
                hintProxy.Dispose();
            }

            hintProxy = dependencyPropertyChangedEventArgs.NewValue as IHintProxy;
            if (hintProxy == null) return;

            hintProxy.IsVisibleChanged += smartHint.OnHintProxyIsVisibleChanged;
            hintProxy.ContentChanged += smartHint.OnHintProxyContentChanged;
            hintProxy.Loaded += smartHint.OnHintProxyContentChanged;
            smartHint.RefreshState(false);
        }

        #endregion HintProxy 附加提示的对象

        #region Hint 提示内容

        public static readonly DependencyProperty HintProperty = DependencyProperty.Register(
            "Hint", typeof(object), typeof(SmartHint), new PropertyMetadata(null));

        public object Hint
        {
            get { return GetValue(HintProperty); }
            set { SetValue(HintProperty, value); }
        }

        #endregion Hint

        #region IsContentNullOrEmpty 

        private static readonly DependencyPropertyKey IsContentNullOrEmptyPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "IsContentNullOrEmpty", typeof(bool), typeof(SmartHint),
                new PropertyMetadata(default(bool)));

        public static readonly DependencyProperty IsContentNullOrEmptyProperty =
            IsContentNullOrEmptyPropertyKey.DependencyProperty;

        public bool IsContentNullOrEmpty
        {
            get { return (bool)GetValue(IsContentNullOrEmptyProperty); }
            private set { SetValue(IsContentNullOrEmptyPropertyKey, value); }
        }

        #endregion IsContentNullOrEmpty

        #region UseFloating

        public static readonly DependencyProperty UseFloatingProperty = DependencyProperty.Register(
            "UseFloating", typeof(bool), typeof(SmartHint), new PropertyMetadata(false));

        public bool UseFloating
        {
            get { return (bool)GetValue(UseFloatingProperty); }
            set { SetValue(UseFloatingProperty, value); }
        }

        #endregion UseFloating

        #region FloatingScale & FloatingOffset

        public static readonly DependencyProperty FloatingScaleProperty = DependencyProperty.Register(
            "FloatingScale", typeof(double), typeof(SmartHint), new PropertyMetadata(.74));

        public double FloatingScale
        {
            get { return (double)GetValue(FloatingScaleProperty); }
            set { SetValue(FloatingScaleProperty, value); }
        }

        public static readonly DependencyProperty FloatingOffsetProperty = DependencyProperty.Register(
            "FloatingOffset", typeof(Point), typeof(SmartHint), new PropertyMetadata(new Point(1, -16)));

        public Point FloatingOffset
        {
            get { return (Point)GetValue(FloatingOffsetProperty); }
            set { SetValue(FloatingOffsetProperty, value); }
        }

        #endregion FloatingScale & FloatingOffset

        #region HintOpacity 提示透明度

        public static readonly DependencyProperty HintOpacityProperty = DependencyProperty.Register(
            "HintOpacity", typeof(double), typeof(SmartHint), new PropertyMetadata(.46));

        public double HintOpacity
        {
            get { return (double)GetValue(HintOpacityProperty); }
            set { SetValue(HintOpacityProperty, value); }
        }

        #endregion HintOpacity

        #endregion

        

        

        protected virtual void OnHintProxyContentChanged(object sender, EventArgs e)
        {
            IsContentNullOrEmpty = HintProxy.IsEmpty();

            if (HintProxy.IsLoaded)
            {
                RefreshState(true);
            }
            else
            {
                HintProxy.Loaded += HintProxySetStateOnLoaded;
            }
        }

        private void HintProxySetStateOnLoaded(object sender, EventArgs e)
        {
            RefreshState(false);
            HintProxy.Loaded -= HintProxySetStateOnLoaded;
        }

        protected virtual void OnHintProxyIsVisibleChanged(object sender, EventArgs e)
        {
            RefreshState(false);
        }

        private void RefreshState(bool useTransitions)
        {
            var proxy = HintProxy;

            if (proxy == null) return;
            if (!proxy.IsVisible) return;

            var action = new Action(() =>
            {
                var isEmpty = proxy.IsEmpty();

                var state = isEmpty
                    ? ContentEmptyName
                    : ContentNotEmptyName;

                VisualStateManager.GoToState(this, state, useTransitions);
            });

            if (DesignerProperties.GetIsInDesignMode(this))
            {
                action();
            }
            else
            {
                Dispatcher.BeginInvoke(action);
            }
        }
    }
}