﻿namespace ICodeShare.UI.Controls
{
    public enum ApplicationBarTransitionMode
    {
        None,

        Fade,

        Slide
    }
}