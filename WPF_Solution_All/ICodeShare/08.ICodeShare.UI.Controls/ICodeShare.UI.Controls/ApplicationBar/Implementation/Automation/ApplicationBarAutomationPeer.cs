﻿using System.Diagnostics.Contracts;
using System.Windows.Automation.Peers;

namespace ICodeShare.UI.Controls.Automation
{
    public class ApplicationBarAutomationPeer : FrameworkElementAutomationPeer
    {
        public ApplicationBarAutomationPeer(ApplicationBar owner)
            : base(owner)
        {
        }

        [System.Diagnostics.Contracts.Pure]
        protected override string GetClassNameCore()
        {
            Contract.Ensures(Contract.Result<string>() == "ApplicationBar");
            return "ApplicationBar";
        }

        [System.Diagnostics.Contracts.Pure]
        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            Contract.Ensures(Contract.Result<AutomationControlType>() == AutomationControlType.Menu);
            return AutomationControlType.Menu;
        }
    }
}