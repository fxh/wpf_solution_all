﻿using ICodeShare.UI.Controls.Automation;
using ICodeShare.UI.Controls.Utilities;
using ICodeShare.UI.Controls.Primitives;
using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace ICodeShare.UI.Controls
{
    [DefaultEvent("Opened")]
    [Localizability(LocalizationCategory.Menu)]
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(CommandButton))]
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public class ApplicationBar : ItemsControl
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        static ApplicationBar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(typeof(ApplicationBar)));

            var itemsPanelTemplate = new ItemsPanelTemplate(new FrameworkElementFactory(typeof(ApplicationBarPanel)));
            itemsPanelTemplate.Seal();
            ItemsPanelProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(itemsPanelTemplate));

            IsTabStopProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(false));
            FocusableProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(false));
            FocusManager.IsFocusScopeProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(true));
            KeyboardNavigation.DirectionalNavigationProperty.OverrideMetadata(
                typeof(ApplicationBar), new FrameworkPropertyMetadata(KeyboardNavigationMode.Cycle));
            KeyboardNavigation.TabNavigationProperty.OverrideMetadata(
                typeof(ApplicationBar), new FrameworkPropertyMetadata(KeyboardNavigationMode.Cycle));
            KeyboardNavigation.ControlTabNavigationProperty.OverrideMetadata(
                typeof(ApplicationBar), new FrameworkPropertyMetadata(KeyboardNavigationMode.Once));

            HorizontalAlignmentProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(HorizontalAlignment.Stretch));
            VerticalAlignmentProperty.OverrideMetadata(typeof(ApplicationBar), new FrameworkPropertyMetadata(VerticalAlignment.Bottom));

            EventManager.RegisterClassHandler(typeof(ApplicationBar), Mouse.LostMouseCaptureEvent,
                                              new MouseEventHandler(OnLostMouseCapture));
            EventManager.RegisterClassHandler(typeof(ApplicationBar), Mouse.PreviewMouseUpOutsideCapturedElementEvent,
                                              new MouseButtonEventHandler(OnPreviewMouseButtonOutsideCapturedElement));
        }

        public static readonly DependencyProperty DockProperty =
            DependencyProperty.RegisterAttached("Dock", typeof(ApplicationBarDock), typeof(ApplicationBar),
                                                new FrameworkPropertyMetadata(ApplicationBarDock.Left, FrameworkPropertyMetadataOptions.AffectsArrange));

        [System.Diagnostics.Contracts.Pure]
        [AttachedPropertyBrowsableForChildren]
        public static ApplicationBarDock GetDock(UIElement obj)
        {
            return (ApplicationBarDock)(obj.GetValue(DockProperty));
        }

        public static void SetDock(UIElement obj, ApplicationBarDock value)
        {
            if (obj == null) return;
            obj.SetValue(DockProperty, value);
        }

        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(ApplicationBar),
                                        new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                                                                      OnIsOpenChanged));

        [Bindable(true)]
        [Category("Behavior")]
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        private bool _isOpen;

        private static void OnIsOpenChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var instance = (ApplicationBar)obj;
            instance.OnIsOpenChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        internal bool IsOpening;
        internal bool IsClosing;

        protected virtual void OnIsOpenChanged(bool oldIsOpen, bool newIsOpen)
        {
            if (newIsOpen && !oldIsOpen)
            {
                IsOpening = true;

                OnOpening(new RoutedEventArgs(OpeningEvent, this));

                if (TransitionMode == ApplicationBarTransitionMode.None)
                {
                    Mouse.Capture(this, CaptureMode.SubTree);

                    _isOpen = true;
                    InvalidateArrange();

                    OnOpened(new RoutedEventArgs(OpenedEvent, this));

                    IsOpening = false;
                }
                else
                {
                    var storyboard = new Storyboard
                        {
                            FillBehavior = FillBehavior.Stop
                        };

                    Timeline animation;
                    switch (TransitionMode)
                    {
                        case ApplicationBarTransitionMode.Fade:
                            animation = new DoubleAnimation(0d, 1d, new Duration(TimeSpan.FromSeconds(0.2d)));

                            Storyboard.SetTarget(animation, this);
                            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));

                            // NOTE: Lack of contracts
                            Contract.Assume(storyboard.Children != null);
                            storyboard.Children.Add(animation);
                            break;

                        case ApplicationBarTransitionMode.Slide:
                            animation = new DoubleAnimation(0d, DesiredSize.Height, new Duration(TimeSpan.FromSeconds(0.2d)));

                            Storyboard.SetTarget(animation, this);
                            Storyboard.SetTargetProperty(animation, new PropertyPath("Height"));

                            // NOTE: Lack of contracts
                            Contract.Assume(storyboard.Children != null);
                            storyboard.Children.Add(animation);
                            break;
                    }

                    storyboard.Completed += (sender, e) =>
                    {
                        storyboard.Stop(this);
                        storyboard.Remove(this);

                        OnOpened(new RoutedEventArgs(OpenedEvent, this));

                        IsOpening = false;
                    };

                    if (!StaysOpen)
                    {
                        Mouse.Capture(this, CaptureMode.SubTree);
                    }

                    if (!storyboard.IsFrozen && storyboard.CanFreeze)
                    {
                        storyboard.Freeze();
                    }
                    storyboard.Begin(this, true);

                    _isOpen = true;
                    InvalidateArrange();
                }
            }
            if (!newIsOpen && oldIsOpen)
            {
                IsClosing = true;

                OnClosing(new RoutedEventArgs(ClosingEvent, this));

                if (TransitionMode == ApplicationBarTransitionMode.None)
                {
                    Mouse.Capture(null);

                    _isOpen = false;
                    InvalidateArrange();

                    OnClosed(new RoutedEventArgs(ClosedEvent, this));

                    IsClosing = false;
                }
                else
                {
                    var storyboard = new Storyboard
                        {
                            FillBehavior = FillBehavior.Stop
                        };

                    Timeline animation;
                    switch (TransitionMode)
                    {
                        case ApplicationBarTransitionMode.Fade:
                            animation = new DoubleAnimation(1d, 0d, new Duration(TimeSpan.FromSeconds(0.2d)));

                            Storyboard.SetTarget(animation, this);
                            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));

                            // NOTE: Lack of contracts
                            Contract.Assume(storyboard.Children != null);
                            storyboard.Children.Add(animation);
                            break;

                        case ApplicationBarTransitionMode.Slide:
                            animation = new DoubleAnimation(DesiredSize.Height, 0d, new Duration(TimeSpan.FromSeconds(0.2d)));

                            Storyboard.SetTarget(animation, this);
                            Storyboard.SetTargetProperty(animation, new PropertyPath("Height"));

                            // NOTE: Lack of contracts
                            Contract.Assume(storyboard.Children != null);
                            storyboard.Children.Add(animation);
                            break;
                    }

                    storyboard.Completed += (sender, e) =>
                    {
                        storyboard.Stop(this);
                        storyboard.Remove(this);

                        if (!StaysOpen)
                        {
                            Mouse.Capture(null);
                        }

                        _isOpen = false;
                        InvalidateArrange();

                        OnClosed(new RoutedEventArgs(ClosedEvent, this));

                        IsClosing = false;
                    };

                    if (!storyboard.IsFrozen && storyboard.CanFreeze)
                    {
                        storyboard.Freeze();
                    }
                    storyboard.Begin(this, true);
                }
            }
        }

        public static readonly RoutedEvent OpeningEvent =
            EventManager.RegisterRoutedEvent("Opening", RoutingStrategy.Tunnel, typeof(RoutedEventHandler), typeof(ApplicationBar));

        [Category("Behavior")]
        [Description("Occurs before opening ApplicationBar instance.")]
        public event RoutedEventHandler Opening
        {
            add { AddHandler(OpeningEvent, value); }
            remove { RemoveHandler(OpeningEvent, value); }
        }

        protected virtual void OnOpening(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent OpenedEvent =
            EventManager.RegisterRoutedEvent("Opened", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ApplicationBar));

        [Category("Behavior")]
        [Description("Occurs after ApplicationBar instance is opened.")]
        public event RoutedEventHandler Opened
        {
            add { AddHandler(OpenedEvent, value); }
            remove { RemoveHandler(OpenedEvent, value); }
        }

        protected virtual void OnOpened(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent ClosingEvent =
            EventManager.RegisterRoutedEvent("Closing", RoutingStrategy.Tunnel, typeof(RoutedEventHandler), typeof(ApplicationBar));

        [Category("Behavior")]
        [Description("Occurs before closing ApplicationBar instance.")]
        public event RoutedEventHandler Closing
        {
            add { AddHandler(ClosingEvent, value); }
            remove { RemoveHandler(ClosingEvent, value); }
        }

        protected virtual void OnClosing(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly RoutedEvent ClosedEvent =
            EventManager.RegisterRoutedEvent("Closed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ApplicationBar));

        [Category("Behavior")]
        public event RoutedEventHandler Closed
        {
            add { AddHandler(ClosedEvent, value); }
            remove { RemoveHandler(ClosedEvent, value); }
        }

        protected virtual void OnClosed(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        public static readonly DependencyProperty StaysOpenProperty =
            DependencyProperty.Register("StaysOpen", typeof(bool), typeof(ApplicationBar),
                                        new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.None, OnStaysOpenChanged));

        [Category("Behavior")]
        public bool StaysOpen
        {
            get { return (bool)GetValue(StaysOpenProperty); }
            set { SetValue(StaysOpenProperty, value); }
        }

        private static void OnStaysOpenChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var instance = (ApplicationBar)obj;
            instance.OnStaysOpenChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnStaysOpenChanged(bool oldStaysOpen, bool newStaysOpen)
        {
        }

        public static readonly DependencyProperty PreventsOpenProperty =
            DependencyProperty.RegisterAttached("PreventsOpen", typeof(bool), typeof(ApplicationBar),
                                                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));

        [System.Diagnostics.Contracts.Pure]
        public static bool GetPreventsOpen(UIElement obj)
        {
            if (obj == null) return false;
            return (bool)obj.GetValue(PreventsOpenProperty);
        }

        public static void SetPreventsOpen(UIElement obj, bool value)
        {
            if (obj == null) return;
            obj.SetValue(PreventsOpenProperty, value);
        }

        public static readonly DependencyProperty TransitionModeProperty =
            DependencyProperty.Register("TransitionMode", typeof(ApplicationBarTransitionMode), typeof(ApplicationBar),
                                        new FrameworkPropertyMetadata(ApplicationBarTransitionMode.Slide, FrameworkPropertyMetadataOptions.None,
                                                                      OnTransitionModeChanged));

        [Category("Appearance")]
        [Description("Animation for the opening and closing of a ApplicationBar.")]
        public ApplicationBarTransitionMode TransitionMode
        {
            get { return (ApplicationBarTransitionMode)GetValue(TransitionModeProperty); }
            set { SetValue(TransitionModeProperty, value); }
        }

        private static void OnTransitionModeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj == null) return;
            var instance = (ApplicationBar)obj;
            instance.OnTransitionModeChanged((ApplicationBarTransitionMode)e.OldValue,
                                             (ApplicationBarTransitionMode)e.NewValue);
        }

        protected virtual void OnTransitionModeChanged(ApplicationBarTransitionMode oldTransitionMode, ApplicationBarTransitionMode newTransitionMode)
        {
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is CommandButtonBase;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new CommandButton();
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new ApplicationBarAutomationPeer(this);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            return _isOpen ? base.ArrangeOverride(arrangeBounds) : new Size(0, 0);
        }

        private static void OnLostMouseCapture(object sender, MouseEventArgs e)
        {
            var instance = sender as ApplicationBar;
            var source = e.OriginalSource as DependencyObject;
            if (instance != null && source != null && !instance.StaysOpen)
            {
                var visualParent = TreeHelper.FindParent<ApplicationBar>(source);
                var logicalParent = TreeHelper.FindParent<ApplicationBar>(source);
                if (Equals(instance, visualParent) || Equals(instance, logicalParent) || (Mouse.Captured == null && instance.IsOpen))
                {
                    Mouse.Capture(instance, CaptureMode.SubTree);
                    e.Handled = true;
                }
            }
        }

        private static void OnPreviewMouseButtonOutsideCapturedElement(object sender, MouseButtonEventArgs e)
        {
            var instance = sender as ApplicationBar;
            var source = e.OriginalSource as DependencyObject;
            if (instance != null && source != null && !instance.StaysOpen)
            {
                var visualParent = TreeHelper.FindParent<ApplicationBar>(source);
                var logicalParent = TreeHelper.FindParent<ApplicationBar>(source);
                if (!(Equals(instance, visualParent) || Equals(instance, logicalParent)))
                {
                    instance.IsOpen = false;
                }
            }
        }
    }
}