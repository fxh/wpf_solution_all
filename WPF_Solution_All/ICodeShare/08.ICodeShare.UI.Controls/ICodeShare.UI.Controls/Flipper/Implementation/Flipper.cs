﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ICodeShare.UI.Controls
{
    [TemplateVisualState(GroupName = TemplateFlipGroupName, Name = TemplateFlippedStateName)]
    [TemplateVisualState(GroupName = TemplateFlipGroupName, Name = TemplateUnflippedStateName)]
    public class Flipper : Control
    {
        #region Members 

        public static RoutedCommand FlipCommand = new RoutedCommand();
        public const string TemplateFlipGroupName = "FlipStates";
        public const string TemplateFlippedStateName = "Flipped";
        public const string TemplateUnflippedStateName = "Unflipped";

        #endregion

        #region Constructors

        static Flipper()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Flipper), new FrameworkPropertyMetadata(typeof(Flipper)));
        }

        public Flipper()
        {
            CommandBindings.Add(new CommandBinding(FlipCommand, FlipHandler));
        }

        #endregion

        #region Properties

        #region CornerRadius 边框圆角

        /// <summary>
        /// 圆角大小,左上，右上，右下，左下
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
               DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(Flipper), new PropertyMetadata(new CornerRadius(0)));

        #endregion CornerRadius 边框圆角

        #region Orientation 翻转方向

        /// <summary>
        /// 翻转方向
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Orientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OrientationProperty =
               DependencyProperty.Register("Orientation", typeof(Orientation), typeof(Flipper), new PropertyMetadata(Orientation.Horizontal));

        #endregion Orientation 翻转方向

        #region FrontContent 正面内容

        public static readonly DependencyProperty FrontContentProperty = DependencyProperty.Register(
            "FrontContent", typeof(object), typeof(Flipper), new PropertyMetadata(default(object)));

        public object FrontContent
        {
            get { return (object)GetValue(FrontContentProperty); }
            set { SetValue(FrontContentProperty, value); }
        }

        #endregion

        #region FrontContentTemplate 正面样式

        public static readonly DependencyProperty FrontContentTemplateProperty = DependencyProperty.Register(
            "FrontContentTemplate", typeof(DataTemplate), typeof(Flipper), new PropertyMetadata(default(DataTemplate)));

        public DataTemplate FrontContentTemplate
        {
            get { return (DataTemplate)GetValue(FrontContentTemplateProperty); }
            set { SetValue(FrontContentTemplateProperty, value); }
        }

        #endregion

        #region FrontContentTemplateSelector 正面样式选择器

        public static readonly DependencyProperty FrontContentTemplateSelectorProperty = DependencyProperty.Register(
            "FrontContentTemplateSelector", typeof(DataTemplateSelector), typeof(Flipper), new PropertyMetadata(default(DataTemplateSelector)));

        public DataTemplateSelector FrontContentTemplateSelector
        {
            get { return (DataTemplateSelector)GetValue(FrontContentTemplateSelectorProperty); }
            set { SetValue(FrontContentTemplateSelectorProperty, value); }
        }

        #endregion

        #region FrontContentStringFormat 正面内容字符串格式

        public static readonly DependencyProperty FrontContentStringFormatProperty = DependencyProperty.Register(
            "FrontContentStringFormat", typeof(string), typeof(Flipper), new PropertyMetadata(default(string)));

        public string FrontContentStringFormat
        {
            get { return (string)GetValue(FrontContentStringFormatProperty); }
            set { SetValue(FrontContentStringFormatProperty, value); }
        }

        #endregion

        #region BackContent 背面内容

        public static readonly DependencyProperty BackContentProperty = DependencyProperty.Register(
            "BackContent", typeof(object), typeof(Flipper), new PropertyMetadata(default(object)));

        public object BackContent
        {
            get { return (object)GetValue(BackContentProperty); }
            set { SetValue(BackContentProperty, value); }
        }

        #endregion

        #region BackContentTemplate 背面样式

        public static readonly DependencyProperty BackContentTemplateProperty = DependencyProperty.Register(
            "BackContentTemplate", typeof(DataTemplate), typeof(Flipper), new PropertyMetadata(default(DataTemplate)));

        public DataTemplate BackContentTemplate
        {
            get { return (DataTemplate)GetValue(BackContentTemplateProperty); }
            set { SetValue(BackContentTemplateProperty, value); }
        }

        #endregion

        #region BackContentTemplateSelector 背面样式选择器

        public static readonly DependencyProperty BackContentTemplateSelectorProperty = DependencyProperty.Register(
            "BackContentTemplateSelector", typeof(DataTemplateSelector), typeof(Flipper), new PropertyMetadata(default(DataTemplateSelector)));

        public DataTemplateSelector BackContentTemplateSelector
        {
            get { return (DataTemplateSelector)GetValue(BackContentTemplateSelectorProperty); }
            set { SetValue(BackContentTemplateSelectorProperty, value); }
        }

        #endregion

        #region 背面内容字符串格式

        public static readonly DependencyProperty BackContentStringFormatProperty = DependencyProperty.Register(
            "BackContentStringFormat", typeof(string), typeof(Flipper), new PropertyMetadata(default(string)));

        public string BackContentStringFormat
        {
            get { return (string)GetValue(BackContentStringFormatProperty); }
            set { SetValue(BackContentStringFormatProperty, value); }
        }

        #endregion

        #region IsFlipped 是否翻转

        public static readonly DependencyProperty IsFlippedProperty = DependencyProperty.Register(
            "IsFlipped", typeof(bool), typeof(Flipper), new PropertyMetadata(default(bool), FlippedPropertyChangedCallback));

        public bool IsFlipped
        {
            get { return (bool)GetValue(IsFlippedProperty); }
            set { SetValue(IsFlippedProperty, value); }
        }

        private static void FlippedPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((Flipper)dependencyObject).UpdateVisualStates(true);
        }
        #endregion

        #endregion

        #region Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            UpdateVisualStates(false);
        }

        private void UpdateVisualStates(bool useTransitions)
        {
            VisualStateManager.GoToState(this, IsFlipped ? TemplateFlippedStateName : TemplateUnflippedStateName,
                useTransitions);
        }

        private void FlipHandler(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            SetCurrentValue(IsFlippedProperty, !IsFlipped);
        }

        #endregion
    }

}