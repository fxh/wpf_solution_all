﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 组合图标控件
    /// </summary>
    public class FastOverlayIcon : Control
    {
        #region Constructors

        /// <summary>
        /// Initialises static members of the <see cref="FastOverlayIcon"/> class.
        /// </summary>
        static FastOverlayIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata( typeof(FastOverlayIcon), new FrameworkPropertyMetadata(typeof(FastOverlayIcon)));
        }

        #endregion Constructors

        #region Dependency Properties

        #region IconFont 图标

        /// <summary>
        /// IconFont 图标
        /// </summary>
        public string IconFont
        {
            get { return (string)GetValue(IconFontProperty); }
            set { SetValue(IconFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFontProperty =
            DependencyProperty.Register("IconFont", typeof(string), typeof(FastOverlayIcon), new PropertyMetadata(null));

        #endregion IconFont 图标

        #region FontAwesome 图标

        /// <summary>
        /// FontAwesome 图标
        /// </summary>
        public string FontAwesome
        {
            get { return (string)GetValue(FontAwesomeProperty); }
            set { SetValue(FontAwesomeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontAwesome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontAwesomeProperty =
            DependencyProperty.Register("FontAwesome", typeof(string), typeof(FastOverlayIcon), new PropertyMetadata(null));

        #endregion FontAwesome 图标

        #region IconGeometry Path路径图标

        /// <summary>
        /// xaml路径图标
        /// </summary>
        public Geometry IconGeometry
        {
            get { return (Geometry)GetValue(IconGeometryProperty); }
            set { SetValue(IconGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconGeometry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconGeometryProperty =
            DependencyProperty.Register("IconGeometry", typeof(Geometry), typeof(FastOverlayIcon), new PropertyMetadata(null));

        #endregion IconGeometry Path路径图标

        #region IconImage Image图标

        /// <summary>
        /// Image图标
        /// </summary>
        public ImageSource IconImage
        {
            get { return (ImageSource)GetValue(IconImageProperty); }
            set { SetValue(IconImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconImageProperty =
            DependencyProperty.Register("IconImage", typeof(ImageSource), typeof(FastOverlayIcon), new PropertyMetadata(null));

        #endregion IconImage Image图标

        #region IconText Text文字

        /// <summary>
        /// IconText文字
        /// </summary>
        public string IconText
        {
            get { return (string)GetValue(IconTextProperty); }
            set { SetValue(IconTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconTextProperty =
            DependencyProperty.Register("IconText", typeof(string), typeof(FastOverlayIcon), new PropertyMetadata(null));

        #endregion IconText Text文字

        #region IconWeight 图标型号

        public IconWeight IconWeight
        {
            get { return (IconWeight)this.GetValue(IconWeightProperty); }
            set { this.SetValue(IconWeightProperty, value); }
        }

        public static readonly DependencyProperty IconWeightProperty =
            DependencyProperty.Register("IconWeight", typeof(IconWeight), typeof(FastOverlayIcon), new PropertyMetadata(IconWeight.Custom, OnIconWeightPropertyChanged));

        #endregion IconWeight 图标型号

        #region IconSize 图标大小

        /// <summary>
        /// 图标大小
        /// </summary>
        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(FastOverlayIcon), new PropertyMetadata(24.0));

        #endregion IconSize 图标大小

        #region CornerRadius 外侧圆角

        /// <summary>
        /// 外侧圆角
        /// </summary>
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(FastOverlayIcon), new PropertyMetadata(new CornerRadius()));

        #endregion CornerRadius 外侧圆角

        #region ShowOverlay 是否显示附加层

        /// <summary>
        /// 是否显示附加层
        /// </summary>
        public bool ShowOverlay
        {
            get { return (bool)GetValue(ShowOverlayProperty); }
            set { SetValue(ShowOverlayProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowOverlay.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowOverlayProperty =
            DependencyProperty.Register("ShowOverlay", typeof(bool), typeof(FastOverlayIcon), new PropertyMetadata(true));

        #endregion ShowOverlay 是否显示附加层

        #endregion Dependency Properties

        #region Private Static Methods

        private static void OnIconWeightPropertyChanged(
            DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs e)
        {
            FastOverlayIcon icon = (FastOverlayIcon)dependencyObject;
            icon.UpdateIsOverlayVisible();
        }

        #endregion Private Static Methods

        #region Private Methods

        private void UpdateIsOverlayVisible()
        {
            if (this.IconWeight == IconWeight.Small)
            {
                this.ShowOverlay = false;
            }
        }

        #endregion Private Methods
    }
}