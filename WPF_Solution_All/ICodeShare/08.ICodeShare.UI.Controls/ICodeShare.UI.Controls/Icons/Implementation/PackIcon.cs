﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// Icon from the Material Design Icons project, <see cref="https://materialdesignicons.com/"/>.
    /// </summary>
    //public class PackIcon : PackIconBase<PackIconKind>
    //{
    //    static PackIcon()
    //    {
    //        DefaultStyleKeyProperty.OverrideMetadata(typeof(PackIcon), new FrameworkPropertyMetadata(typeof(PackIcon)));
    //    }

    //    public PackIcon()
    //        : base(PackIconDataFactory.Create)
    //    {
    //    }
    //}

    public class PackIcon : Control
    {
        static PackIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PackIcon), new FrameworkPropertyMetadata(typeof(PackIcon)));
        }


         private static Lazy<IDictionary<PackIconKind, string>> _dataIndex;

        /// <param name="dataIndexFactory">
        /// Inheritors should provide a factory for setting up the path data index (per icon kind).
        /// The factory will only be utilised once, across all closed instances (first instantiation wins).
        /// </param>
         public PackIcon()
        {
            if (_dataIndex == null)
                _dataIndex = new Lazy<IDictionary<PackIconKind, string>>(PackIconDataFactory.Create);
        }

        public static readonly DependencyProperty KindProperty
            = DependencyProperty.Register("Kind", typeof(PackIconKind), typeof(PackIcon), new PropertyMetadata(default(PackIconKind), KindPropertyChangedCallback));

        private static void KindPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((PackIcon)dependencyObject).UpdateData();
        }

        /// <summary>
        /// Gets or sets the icon to display.
        /// </summary>
        public PackIconKind Kind
        {
            get { return (PackIconKind)GetValue(KindProperty); }
            set { SetValue(KindProperty, value); }
        }

#if NETFX_CORE
        private static readonly DependencyProperty DataProperty
            = DependencyProperty.Register("Data), typeof(string), typeof(PackIcon), new PropertyMetadata(""));

        /// <summary>
        /// Gets the icon path data for the current <see cref="Kind"/>.
        /// </summary>
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            private set { SetValue(DataProperty, value); }
        }
#else

        private static readonly DependencyPropertyKey DataPropertyKey
            = DependencyProperty.RegisterReadOnly("Data", typeof(string), typeof(PackIcon), new PropertyMetadata(""));

        // ReSharper disable once StaticMemberInGenericType
        public static readonly DependencyProperty DataProperty = DataPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the icon path data for the current <see cref="Kind"/>.
        /// </summary>
        [TypeConverter(typeof(GeometryConverter))]
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            private set { SetValue(DataPropertyKey, value); }
        }

#endif

#if NETFX_CORE
        protected override void OnApplyTemplate()
#else

        public override void OnApplyTemplate()
#endif
        {
            base.OnApplyTemplate();

            UpdateData();
        }

        internal  void UpdateData()
        {
            string data = null;
            if (_dataIndex.Value != null)
                _dataIndex.Value.TryGetValue(Kind, out data);
            Data = data;
        }
    }
}