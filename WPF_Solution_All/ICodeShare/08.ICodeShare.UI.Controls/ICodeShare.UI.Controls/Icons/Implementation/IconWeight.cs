﻿namespace ICodeShare.UI.Controls
{
    public enum IconWeight
    {
        Small,
        Medium,
        Large,
        VeryLarge,
        Custom
    }
}