﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    public class PackLightIcon : Control
    {
        static PackLightIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PackLightIcon), new FrameworkPropertyMetadata(typeof(PackLightIcon)));
        }


        private static Lazy<IDictionary<PackLightIconKind, string>> _dataIndex;

        /// <param name="dataIndexFactory">
        /// Inheritors should provide a factory for setting up the path data index (per icon kind).
        /// The factory will only be utilised once, across all closed instances (first instantiation wins).
        /// </param>
        public PackLightIcon()
        {
            if (_dataIndex == null)
                _dataIndex = new Lazy<IDictionary<PackLightIconKind, string>>(PackLightIconDataFactory.Create);
        }

        public static readonly DependencyProperty KindProperty
            = DependencyProperty.Register("Kind", typeof(PackLightIconKind), typeof(PackLightIcon), new PropertyMetadata(default(PackLightIconKind), KindPropertyChangedCallback));

        private static void KindPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((PackLightIcon)dependencyObject).UpdateData();
        }

        /// <summary>
        /// Gets or sets the icon to display.
        /// </summary>
        public PackLightIconKind Kind
        {
            get { return (PackLightIconKind)GetValue(KindProperty); }
            set { SetValue(KindProperty, value); }
        }

#if NETFX_CORE
        private static readonly DependencyProperty DataProperty
            = DependencyProperty.Register("Data), typeof(string), typeof(PackIconLight), new PropertyMetadata(""));

        /// <summary>
        /// Gets the icon path data for the current <see cref="Kind"/>.
        /// </summary>
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            private set { SetValue(DataProperty, value); }
        }
#else

        private static readonly DependencyPropertyKey DataPropertyKey
            = DependencyProperty.RegisterReadOnly("Data", typeof(string), typeof(PackLightIcon), new PropertyMetadata(""));

        // ReSharper disable once StaticMemberInGenericType
        public static readonly DependencyProperty DataProperty = DataPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the icon path data for the current <see cref="Kind"/>.
        /// </summary>
        [TypeConverter(typeof(GeometryConverter))]
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            private set { SetValue(DataPropertyKey, value); }
        }

#endif

#if NETFX_CORE
        protected override void OnApplyTemplate()
#else

        public override void OnApplyTemplate()
#endif
        {
            base.OnApplyTemplate();

            UpdateData();
        }

        internal void UpdateData()
        {
            string data = null;
            if (_dataIndex.Value != null)
                _dataIndex.Value.TryGetValue(Kind, out data);
            Data = data;
        }
    }
}
