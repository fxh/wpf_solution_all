﻿using System.ComponentModel;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// Names for each component bit
    /// </summary>
    internal enum SixteenSegment
    {
        /// <summary>
        /// Top Horizontal left
        /// 上水平左
        /// </summary>
        [Description("上水平左")]
        THL,

        /// <summary>
        /// Top Horizontal Right
        /// 上水平右
        /// </summary>
        [Description("上水平右")]
        THR,

        /// <summary>
        /// Top Vertical Left
        /// 上竖直左
        /// </summary>
        [Description("上竖直左")]
        TVL,

        /// <summary>
        /// Top Vertical Middle
        /// 上竖直中
        /// </summary>
        [Description("上竖直中")]
        TVM,

        /// <summary>
        /// Top Vertical Right
        /// 上竖直右
        /// </summary>
        [Description("上竖直右")]
        TVR,

        /// <summary>
        /// Middle horizontal left
        /// 中水平左
        /// </summary>
        [Description("中水平左")]
        MHL,

        /// <summary>
        /// Middle horizontal right
        /// 中水平右
        /// </summary>
        [Description("中水平右")]
        MHR,

        /// <summary>
        /// Bottom vertical left
        /// 下竖直左
        /// </summary>
        [Description("下竖直左")]
        BVL,

        /// <summary>
        /// Bottom vertical middle
        /// 下竖直中
        /// </summary>
        [Description("下竖直中")]
        BVM,

        /// <summary>
        /// Bottom vertical right
        /// 下竖直右
        /// </summary>
        [Description("下竖直右")]
        BVR,

        /// <summary>
        /// Bottom horizontal left
        /// 下水平左
        /// </summary>
        [Description("下水平左")]
        BHL,

        /// <summary>
        /// Bottom horizontal right
        /// 下水平右
        /// </summary>
        [Description("下水平右")]
        BHR,

        /// <summary>
        /// Top Diagonal left
        /// 上倾斜左
        /// </summary>
        [Description("上倾斜左")]
        TDL,

        /// <summary>
        /// Top Diagonal Right
        /// 上倾斜右
        /// </summary>
        [Description("上倾斜右")]
        TDR,

        /// <summary>
        /// Bottom Diagonal left
        /// 下倾斜左
        /// </summary>
        [Description("下倾斜左")]
        BDL,

        /// <summary>
        /// Bottom Diagonal Right
        /// 下倾斜右
        /// </summary>
        [Description("下倾斜右")]
        BDR
    }
}