﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 角标类型
    /// </summary>
    public enum BadgeMode
    {
        /// <summary>
        /// 无限制
        /// </summary>
        UnLimited,

        /// <summary>
        /// 数字
        /// </summary>
        Number,

        /// <summary>
        /// 受限数字
        /// </summary>
        LimitedNumber,

        /// <summary>
        /// 显示成一个圆点
        /// </summary>
        Dot
    }
}
