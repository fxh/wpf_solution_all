﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public class Badge : Control
    {
        #region 构造函数

        static Badge()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Badge), new FrameworkPropertyMetadata(typeof(Badge)));
        }

        #endregion 构造函数

        #region Number数字

        /// <summary>
        /// 数值
        /// </summary>
        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        // Using a DependencyProperty as the Number store for Badge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(Badge), new PropertyMetadata(0));

        #endregion Number数字

        #region IsDot是否点模式

        /// <summary>
        /// 是否点模式
        /// </summary>
        public bool IsDot
        {
            get { return (bool)GetValue(IsDotProperty); }
            set { SetValue(IsDotProperty, value); }
        }

        // Using a DependencyProperty as the IsDot store for Badge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDotProperty =
            DependencyProperty.Register("IsDot", typeof(bool), typeof(Badge), new PropertyMetadata(false));

        #endregion IsDot是否点模式
    }
}