﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{


    public class FastBadge : Control
    {
        #region 构造函数

        static FastBadge()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FastBadge), new FrameworkPropertyMetadata(typeof(FastBadge)));
        }


        #endregion 构造函数

        #region Mode 角标类型

        public static readonly DependencyProperty ModeProperty = DependencyProperty.Register(
            "Mode", typeof(BadgeMode), typeof(FastBadge), new PropertyMetadata(BadgeMode.UnLimited, OnModeChanged));

        public BadgeMode Mode
        {
            get { return (BadgeMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        private static void OnModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var fastBadge = d as FastBadge;
            BadgeMode mode = (BadgeMode)e.NewValue;
            if (fastBadge != null)
            {
                int num = fastBadge.Number;
                if (fastBadge.Mode == BadgeMode.Number)
                {
                    fastBadge.Badge = num.ToString();
                }
                else if (fastBadge.Mode == BadgeMode.LimitedNumber)
                {
                    if (num > fastBadge.MaxValue)
                    {
                        fastBadge.Badge = fastBadge.MaxValue.ToString() + "+";
                    }
                    else if (num < fastBadge.MinValue)
                    {
                        fastBadge.Badge = fastBadge.MinValue.ToString();
                    }
                    else
                    {
                        fastBadge.Badge = num.ToString();
                    }
                }
            }
        }
        #endregion

        #region IsBadgeSet 是否已设置角标

        private static readonly DependencyPropertyKey IsBadgeSetPropertyKey = DependencyProperty.RegisterReadOnly(
            "IsBadgeSet",
            typeof(bool),
            typeof(FastBadge),
            new PropertyMetadata(default(bool)));

        public static readonly DependencyProperty IsBadgeSetProperty =
            IsBadgeSetPropertyKey.DependencyProperty;

        /// <summary>
        /// 是否已设置角标
        /// </summary>
        public bool IsBadgeSet
        {
            get { return (bool)GetValue(IsBadgeSetProperty); }
            private set { SetValue(IsBadgeSetPropertyKey, value); }
        }

        #endregion

        #region Badge 角标内容

        public static readonly DependencyProperty BadgeProperty = DependencyProperty.Register(
             "Badge", typeof(object), typeof(FastBadge), new FrameworkPropertyMetadata(default(object), FrameworkPropertyMetadataOptions.AffectsArrange, OnBadgeChanged));

        public object Badge
        {
            get { return (object)GetValue(BadgeProperty); }
            set { SetValue(BadgeProperty, value); }
        }

        private static void OnBadgeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (FastBadge)d;

            instance.IsBadgeSet = !string.IsNullOrWhiteSpace(e.NewValue as string) || (e.NewValue != null && !(e.NewValue is string));

            var args = new RoutedPropertyChangedEventArgs<object>(e.OldValue,e.NewValue) { RoutedEvent = BadgeChangedEvent };
            instance.RaiseEvent(args);
        }
        #endregion

        #region Number数字

        /// <summary>
        /// 数值
        /// </summary>
        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        // Using a DependencyProperty as the Number store for FastBadge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(FastBadge), new PropertyMetadata(0, OnNumberChanged));

        private static void OnNumberChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var fastBadge = d as FastBadge;
            int num = (int)e.NewValue;
            if (fastBadge != null)
            {
                if (fastBadge.Mode == BadgeMode.Number)
                {
                    fastBadge.Badge = num.ToString();
                }
                else if (fastBadge.Mode == BadgeMode.LimitedNumber)
                {
                    if (num > fastBadge.MaxValue)
                    {
                        fastBadge.Badge = fastBadge.MaxValue.ToString() + "+";
                    }
                    else if (num < fastBadge.MinValue)
                    {
                        fastBadge.Badge = fastBadge.MinValue.ToString();
                    }
                    else
                    {
                        fastBadge.Badge = num.ToString();
                    }
                }
            }
        }
        #endregion Number数字

        #region MinValue 最小值

        /// <summary>
        /// 最小值
        /// </summary>
        public int MinValue
        {
            get { return (int)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        // Using a DependencyProperty as the MinValue store for FastBadge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(int), typeof(FastBadge), new PropertyMetadata(0));

        #endregion

        #region MaxValue 最大值

        /// <summary>
        /// 最小值
        /// </summary>
        public int MaxValue
        {
            get { return (int)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        // Using a DependencyProperty as the MaxValue store for FastBadge.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(int), typeof(FastBadge), new PropertyMetadata(99));

        #endregion

        #region BadgeChanged 角标改变事件

        public static readonly RoutedEvent BadgeChangedEvent =
            EventManager.RegisterRoutedEvent(
                "BadgeChanged",
                RoutingStrategy.Bubble,
                typeof(RoutedPropertyChangedEventHandler<object>),
                typeof(FastBadge));

        public event RoutedPropertyChangedEventHandler<object> BadgeChanged
        {
            add { AddHandler(BadgeChangedEvent, value); }
            remove { RemoveHandler(BadgeChangedEvent, value); }
        }

        #endregion

    }
}
