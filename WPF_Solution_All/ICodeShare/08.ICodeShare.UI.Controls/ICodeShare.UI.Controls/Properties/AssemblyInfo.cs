﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("ICodeShare.UI.Controls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ICodeShare.UI.Controls")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]

//若要开始生成可本地化的应用程序，请在
//<PropertyGroup> 中的 .csproj 文件中
//设置 <UICulture>CultureYouAreCodingWith</UICulture>。  例如，如果您在源文件中
//使用的是美国英语，请将 <UICulture> 设置为 en-US。  然后取消
//对以下 NeutralResourceLanguage 特性的注释。  更新
//以下行中的“en-US”以匹配项目文件中的 UICulture 设置。

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]
[assembly: ThemeInfo(
    //ResourceDictionaryLocation.None, //主题特定资源词典所处位置
    ResourceDictionaryLocation.SourceAssembly, //主题特定资源词典所处位置
    //(在页面或应用程序资源词典中
    // 未找到某个资源的情况下使用)
    ResourceDictionaryLocation.SourceAssembly //常规资源词典所处位置
    //(在页面、应用程序或任何主题特定资源词典中
    // 未找到某个资源的情况下使用)
)]

// 程序集的版本信息由下面四个值组成:
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”:
// [assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyVersion("1.0.0.0")]
//[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Common")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Core")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Core.Converters")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Core.Input")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Primitives")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/params", "ICodeShare.UI.Controls.Parameters")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/validations", "ICodeShare.UI.Controls.ValidationRules")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/assists", "ICodeShare.UI.Controls.Assists")]
[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/behaviors", "ICodeShare.UI.Controls.Behaviors")]

//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Core.Media")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.PropertyGrid")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.PropertyGrid.Attributes")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.PropertyGrid.Commands")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.PropertyGrid.Converters")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.PropertyGrid.Editors")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Zoombox")]
//[assembly: XmlnsDefinition("http://schemas.extended.wpf.com/controls", "ICodeShare.UI.Controls.Panels")]
#pragma warning disable 1699
[assembly: AssemblyDelaySign(false)]
#pragma warning restore 1699