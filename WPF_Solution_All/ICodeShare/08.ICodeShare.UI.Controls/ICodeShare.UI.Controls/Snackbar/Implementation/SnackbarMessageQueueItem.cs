﻿using System;

namespace ICodeShare.UI.Controls
{
    internal class SnackbarMessageQueueItem
    {
        public SnackbarMessageQueueItem(object content, object actionContent = null, object actionHandler = null, object actionArgument = null, Type argumentType = null, bool isPromoted = false)
        {
            Content = content;
            ActionContent = actionContent;
            ActionHandler = actionHandler;
            ActionArgument = actionArgument;
            ArgumentType = argumentType;
            IsPromoted = isPromoted;
        }

        public object Content { get; private set; }

        public object ActionContent { get; private set; }

        public object ActionHandler { get; private set; }

        public object ActionArgument { get; private set; }

        public Type ArgumentType { get; private set; }

        public bool IsPromoted { get; private set; }
    }
}