﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ICodeShare.UI.Controls
{
    public class NavigateMenu : ListBox
    {
        #region Private属性

        private CollectionViewSource viewSource = new CollectionViewSource();

        #endregion Private属性

        #region 依赖属性set get

        /// <summary>
        ///
        /// </summary>
        public string GroupDescriptions
        {
            get { return (string)GetValue(GroupDescriptionsProperty); }
            set { SetValue(GroupDescriptionsProperty, value); }
        }

        public static readonly DependencyProperty GroupDescriptionsProperty = DependencyProperty.Register(
            "GroupDescriptions", 
            typeof(string),
            typeof(NavigateMenu),
            new PropertyMetadata(""));

        public IEnumerable GroupItemsSource
        {
            get { return (IEnumerable)GetValue(GroupItemsSourceProperty); }
            set { SetValue(GroupItemsSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GroupItemsSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupItemsSourceProperty =DependencyProperty.Register(
            "GroupItemsSource", 
            typeof(IEnumerable), 
            typeof(NavigateMenu), 
            new PropertyMetadata(default(IEnumerable)));


        public GroupStyle MyGroupStyle
        {
            get { return (GroupStyle)GetValue(MyGroupStyleProperty); }
            set { SetValue(MyGroupStyleProperty, value); }
        }

        public static readonly DependencyProperty MyGroupStyleProperty =DependencyProperty.Register(
            "MyGroupStyle",
            typeof(GroupStyle), 
            typeof(NavigateMenu), 
            new PropertyMetadata(default(GroupStyle)));

        public bool ShowGroup
        {
            get { return (bool)GetValue(ShowGroupProperty); }
            set { SetValue(ShowGroupProperty, value); }
        }

        public static readonly DependencyProperty ShowGroupProperty = DependencyProperty.Register(
           "ShowGroup",
           typeof(bool),
           typeof(NavigateMenu),
           new PropertyMetadata(false));
        #endregion 依赖属性set get

        #region Constructors

        static NavigateMenu()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigateMenu), new FrameworkPropertyMetadata(typeof(NavigateMenu)));
        }

        public NavigateMenu()
        {
            this.Loaded += NavigateMenu_Loaded;
        }

        void NavigateMenu_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= NavigateMenu_Loaded;
            //ResourceDictionary rd = new ResourceDictionary();
            //rd.Source = new Uri("/WPF.UI;component/MyControls/NavigateMenu/Themes/Generic.xaml", UriKind.Relative);
            //this.Resources.MergedDictionaries.Add(rd);

            //Style style = this.Resources.MergedDictionaries[0]["NavigateMenuGroupStyle"] as Style;
            //this.GroupStyle.Clear();
            //this.GroupStyle.Add(new System.Windows.Controls.GroupStyle() { ContainerStyle = style });

            if (!string.IsNullOrEmpty(this.GroupDescriptions))
            {
                string[] list = this.GroupDescriptions.Split(',');
                foreach (string desc in list)
                {
                    viewSource.GroupDescriptions.Add(new PropertyGroupDescription(desc));
                }
            }
            viewSource.Source = this.GroupItemsSource;

            Binding binding = new Binding();
            binding.Source = viewSource;

            BindingOperations.SetBinding(this, NavigateMenu.ItemsSourceProperty, binding);
        }
        #endregion Constructors

        #region Override方法

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new NavigateMenuItem();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

         
        }

        #endregion Override方法
    }
}