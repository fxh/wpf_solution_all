﻿using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public class NavigateMenuItem : ListBoxItem
    {
        #region Constructors

        static NavigateMenuItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigateMenuItem), new FrameworkPropertyMetadata(typeof(NavigateMenuItem)));
        }

        #endregion Constructors
    }
}