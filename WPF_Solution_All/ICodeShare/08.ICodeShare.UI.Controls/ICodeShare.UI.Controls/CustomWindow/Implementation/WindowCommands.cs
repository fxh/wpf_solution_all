﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ICodeShare.UI.Controls
{
    public static class WindowCommands
    {

        private static RoutedUICommand _minimize;
        /// <summary>
        /// 最小化
        /// </summary>
        public static RoutedUICommand Minimize
        {
            get
            {
                return _minimize ?? (_minimize = new RoutedUICommand("Minimize", "Minimize", typeof(BaseWindow)));
            }
        }



        private static RoutedUICommand _maximize;
        /// <summary>
        /// 最大化
        /// </summary>
        public static RoutedUICommand Maximize
        {
            get
            {
                return _maximize ?? (_maximize = new RoutedUICommand("Maximize", "Miaximize", typeof(BaseWindow)));
            }
        }

        private static RoutedUICommand _restore;
        /// <summary>
        /// 还原
        /// </summary>
        public static RoutedUICommand Restore
        {
            get
            {
                return _restore ?? (_restore = new RoutedUICommand("Restore", "Restore", typeof(BaseWindow)));
            }
        }


        private static RoutedUICommand _close;
        /// <summary>
        /// 关闭
        /// </summary>
        public static RoutedUICommand Close
        {
            get
            {
                return _close ?? (_close = new RoutedUICommand("Close", "Close", typeof(BaseWindow)));
            }
        }


    }
}
