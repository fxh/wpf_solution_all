﻿using ICodeShare.UI.Controls.Assists;
using ICodeShare.UI.Controls.Native;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;

namespace ICodeShare.UI.Controls
{
    public abstract class CustomWindowBase : BaseWindow
    {
        #region Constructors

        public CustomWindowBase()
        {
            DependencyPropertyDescriptor.FromProperty(ApplicationBarProperty, typeof(BaseWindow)).AddValueChanged(this, OnApplicationBarChanged);
         }

        #endregion

        #region Dependency Properties

        #region BackgroundContent

        /// <summary>
        /// Gets or sets the content of the background.
        /// </summary>
        /// <value>
        /// The content of the background.
        /// </value>
        public object BackgroundContent
        {
            get { return this.GetValue(BackgroundContentProperty); }
            set { this.SetValue(BackgroundContentProperty, value); }
        }

        public static readonly DependencyProperty BackgroundContentProperty = DependencyProperty.Register(
           "BackgroundContent",
           typeof(object),
           typeof(CustomWindowBase));

        #endregion

        #region BackgroundContentTemplate

        /// <summary>
        /// Gets or sets the background content template.
        /// </summary>
        /// <value>
        /// The background content template.
        /// </value>
        public DataTemplate BackgroundContentTemplate
        {
            get { return (DataTemplate)this.GetValue(BackgroundContentTemplateProperty); }
            set { this.SetValue(BackgroundContentTemplateProperty, value); }
        }

        public static readonly DependencyProperty BackgroundContentTemplateProperty = DependencyProperty.Register(
            "BackgroundContentTemplate",
            typeof(DataTemplate),
            typeof(CustomWindowBase));

        #endregion

        #region BackgroundContentTemplateSelector

        /// <summary>
        /// Gets or sets the background content template selector.
        /// </summary>
        /// <value>
        /// The background content template selector.
        /// </value>
        public DataTemplateSelector BackgroundContentTemplateSelector
        {
            get { return (DataTemplateSelector)this.GetValue(BackgroundContentTemplateSelectorProperty); }
            set { this.SetValue(BackgroundContentTemplateSelectorProperty, value); }
        }

        public static readonly DependencyProperty BackgroundContentTemplateSelectorProperty = DependencyProperty.Register(
            "BackgroundContentTemplateSelector",
            typeof(DataTemplateSelector),
            typeof(CustomWindowBase));

        #endregion

        #region ForegroundContent

        /// <summary>
        /// Gets or sets the content of the foreground.
        /// </summary>
        /// <value>
        /// The content of the foreground.
        /// </value>
        public object ForegroundContent
        {
            get { return this.GetValue(ForegroundContentProperty); }
            set { this.SetValue(ForegroundContentProperty, value); }
        }

        public static readonly DependencyProperty ForegroundContentProperty = DependencyProperty.Register(
            "ForegroundContent",
            typeof(object),
            typeof(CustomWindowBase));

        #endregion

        #region ForegroundContentTemplate

        /// <summary>
        /// Gets or sets the foreground content template.
        /// </summary>
        /// <value>
        /// The foreground content template.
        /// </value>
        public DataTemplate ForegroundContentTemplate
        {
            get { return (DataTemplate)this.GetValue(ForegroundContentTemplateProperty); }
            set { this.SetValue(ForegroundContentTemplateProperty, value); }
        }

        public static readonly DependencyProperty ForegroundContentTemplateProperty = DependencyProperty.Register(
            "ForegroundContentTemplate",
            typeof(DataTemplate),
            typeof(CustomWindowBase));

        #endregion

        #region ForegroundContentTemplateSelector

        /// <summary>
        /// Gets or sets the foreground content template selector.
        /// </summary>
        /// <value>
        /// The foreground content template selector.
        /// </value>
        public DataTemplateSelector ForegroundContentTemplateSelector
        {
            get { return (DataTemplateSelector)this.GetValue(ForegroundContentTemplateSelectorProperty); }
            set { this.SetValue(ForegroundContentTemplateSelectorProperty, value); }
        }

        public static readonly DependencyProperty ForegroundContentTemplateSelectorProperty = DependencyProperty.Register(
            "ForegroundContentTemplateSelector",
            typeof(DataTemplateSelector),
            typeof(CustomWindowBase));

        #endregion

        #region WindowPlacement

        /// <summary>
        /// Gets or sets the window placement XML. Can be used to save and restore the windoe placement.
        /// </summary>
        /// <value>
        /// The window placement.
        /// </value>
        public string WindowPlacement
        {
            get { return (string)this.GetValue(WindowPlacementProperty); }
            set { this.SetValue(WindowPlacementProperty, value); }
        }

        public static readonly DependencyProperty WindowPlacementProperty = DependencyProperty.Register(
            "WindowPlacement",
            typeof(string),
            typeof(CustomWindowBase),
            new PropertyMetadata(null));

        #endregion

        #endregion

        #region Methods

        protected override void Dispose(bool disposing)
        {
            DependencyPropertyDescriptor
                .FromProperty(ApplicationBarProperty, typeof(BaseWindow))
                .RemoveValueChanged(this, OnApplicationBarChanged);
            base.Dispose(disposing);
        }

        private static void OnApplicationBarChanged(object sender, EventArgs e)
        {
            CustomWindowBase window = sender as CustomWindowBase;
            if (window != null)
            {
                window.SetApplicationBarInternal(window);
            }
        }

        private void SetApplicationBarInternal(CustomWindowBase window)
        {
            Decorator decorator = (Decorator)window.GetTemplateChild("ApplicationBarHost");
            if (decorator != null)
            {
                ApplicationBar applicationBar = GetApplicationBar(window);
                decorator.Child = applicationBar;
            }
        }
        #endregion
    }
}
