﻿using System.Windows.Automation.Peers;

namespace ICodeShare.UI.Controls.Automation.Peers
{
    /// <summary>
    /// Wraps an <see cref="T:System.Windows.Controls.AccordionItem" />.
    /// </summary>
    public class AccordionItemWrapperAutomationPeer : FrameworkElementAutomationPeer
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="AccordionItemWrapperAutomationPeer"/> class.
        /// </summary>
        /// <param name="item">The <see cref="T:System.Windows.Controls.AccordionItem" /> to wrap.</param>
        public AccordionItemWrapperAutomationPeer(AccordionItem item)
            : base(item)
        {
        }
    }
}