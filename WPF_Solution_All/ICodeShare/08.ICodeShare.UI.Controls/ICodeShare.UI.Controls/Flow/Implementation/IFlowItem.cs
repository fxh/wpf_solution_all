﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 所有的FlowItem对象继承IFlowItem接口
    /// </summary>
    public interface IFlowItem
    {
        int Id { get; }

        string Title { get; }

        double OffsetRate { get; }
    }
}