﻿namespace ICodeShare.UI.Controls
{
    public enum ColorSortingMode
    {
        Alphabetical,
        HueSaturationBrightness
    }
}