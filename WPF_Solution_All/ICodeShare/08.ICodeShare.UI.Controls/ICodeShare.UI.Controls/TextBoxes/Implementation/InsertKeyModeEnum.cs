﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 输入模式
    /// </summary>
    public enum InsertKeyMode
    {
        /// <summary>
        /// 默认
        /// </summary>
        Default = 0,
        /// <summary>
        /// 插入
        /// </summary>
        Insert = 1,
        /// <summary>
        /// 覆写
        /// </summary>
        Overwrite = 2
    }
}