﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 格式文本复制到剪切板的方式
    /// </summary>
    public enum MaskFormat
    {
        /// <summary>
        /// 不包含提示和文字
        /// </summary>
        ExcludePromptAndLiterals,
        /// <summary>
        /// 包含文字
        /// </summary>
        IncludeLiterals,
        /// <summary>
        /// 包含提示字符
        /// </summary>
        IncludePrompt,
        /// <summary>
        /// 包含提示和文字
        /// </summary>
        IncludePromptAndLiterals
    }
}