﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls
{
    public delegate void SearchSelectionEventHandler(object sender, SearchSelectionEventArgs e);

    public class SearchSelectionEventArgs : EventArgs
    {
        public SearchSelectionEventArgs(object selectedItem)
        {
            this.SelectedItem = selectedItem;
        }
        public object SelectedItem { get; private set; }
    }
}
