﻿namespace ICodeShare.UI.Controls
{
    public enum ComboBoxPopupPlacement
    {
        Undefined,
        Down,
        Up,
        Classic
    }
}