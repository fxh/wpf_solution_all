﻿namespace ICodeShare.UI.Controls
{
    public enum TimeFormat
    {
        Custom,
        ShortTime,
        LongTime
    }
}