﻿using System;
using System.Windows;

namespace ICodeShare.UI.Controls
{
    public delegate void DialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs);

    public class DialogOpenedEventArgs : RoutedEventArgs
    {
        public DialogOpenedEventArgs(DialogSession session, RoutedEvent routedEvent)
        {
            if (session == null) throw new ArgumentNullException("session");

            Session = session;
            RoutedEvent = routedEvent;
        }

        /// <summary>
        /// Allows interation with the current dialog session.
        /// </summary>
        public DialogSession Session { get; private set; }
    }
}