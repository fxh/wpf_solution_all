namespace ICodeShare.UI.Controls
{
    public enum TransitionEffectKind
    {
        None,
        ExpandIn,
        FadeIn,
        SlideInFromLeft,
        SlideInFromTop,
        SlideInFromRight,
        SlideInFromBottom
    }
}