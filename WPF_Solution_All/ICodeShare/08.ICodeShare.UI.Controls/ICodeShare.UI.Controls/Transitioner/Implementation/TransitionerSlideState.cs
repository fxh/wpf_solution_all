namespace ICodeShare.UI.Controls
{
    public enum TransitionerSlideState
    {
        None,
        Current,
        Previous,
    }
}