﻿using System.Windows;

namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// Content control to host the content of an individual page within a <see cref="Transitioner"/>.
    /// </summary>
    public class TransitionerSlide : TransitioningContentBase
    {
        static TransitionerSlide()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TransitionerSlide), new FrameworkPropertyMetadata(typeof(TransitionerSlide)));
        }

        #region TransitionOrigin 动画中心点

        /// <summary>
        /// 变换中心点
        /// </summary>
        public Point TransitionOrigin
        {
            get { return (Point)GetValue(TransitionOriginProperty); }
            set { SetValue(TransitionOriginProperty, value); }
        }


        public static readonly DependencyProperty TransitionOriginProperty = DependencyProperty.Register(
            "TransitionOrigin", 
            typeof(Point), 
            typeof(Transitioner), 
            new PropertyMetadata(new Point(0.5, 0.5)));

        #endregion

        #region InTransitionFinished

        public static RoutedEvent InTransitionFinished =EventManager.RegisterRoutedEvent(
            "InTransitionFinished",
            RoutingStrategy.Bubble,
            typeof(RoutedEventHandler),
            typeof(TransitionerSlide));

        protected void OnInTransitionFinished(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        #endregion

        #region State 状态
        
        /// <summary>
        /// 状态
        /// </summary>
        public TransitionerSlideState State
        {
            get { return (TransitionerSlideState)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public static readonly DependencyProperty StateProperty = DependencyProperty.Register(
            "State", 
            typeof(TransitionerSlideState), 
            typeof(TransitionerSlide), 
            new PropertyMetadata(default(TransitionerSlideState), new PropertyChangedCallback(StatePropertyChangedCallback)));

        private static void StatePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((TransitionerSlide)d).AnimateToState();
        }

        #endregion

        #region ForwardWipe 向前动画

        /// <summary>
        /// 向前动画
        /// </summary>
        public ITransitionWipe ForwardWipe
        {
            get { return (ITransitionWipe)GetValue(ForwardWipeProperty); }
            set { SetValue(ForwardWipeProperty, value); }
        }

        public static readonly DependencyProperty ForwardWipeProperty = DependencyProperty.Register(
            "ForwardWipe", 
            typeof(ITransitionWipe), 
            typeof(TransitionerSlide), 
            new PropertyMetadata(new CircleWipe()));

        #endregion

        #region BackwardWipe 向后动画

        /// <summary>
        ///向后动画
        /// </summary>
        public ITransitionWipe BackwardWipe
        {
            get { return (ITransitionWipe)GetValue(BackwardWipeProperty); }
            set { SetValue(BackwardWipeProperty, value); }
        }

        public static readonly DependencyProperty BackwardWipeProperty = DependencyProperty.Register(
            "BackwardWipe", 
            typeof(ITransitionWipe), 
            typeof(TransitionerSlide),
            new PropertyMetadata(new SlideOutWipe()));

        #endregion


        private void AnimateToState()
        {
            if (State != TransitionerSlideState.Current) return;

            RunOpeningEffects();
        }
    }
}