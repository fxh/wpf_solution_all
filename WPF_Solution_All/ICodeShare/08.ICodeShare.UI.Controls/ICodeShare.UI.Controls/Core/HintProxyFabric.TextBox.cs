﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.Core
{
    public static partial class HintProxyFabric
    {
        private sealed class TextBoxHintProxy : IHintProxy
        {
            private readonly TextBox _textBox;

            public object Content
            {
                get { return _textBox.Text; }
            }

            public bool IsLoaded
            {
                get { return _textBox.IsLoaded; }
            }

            public bool IsVisible
            {
                get { return _textBox.IsVisible; }
            }

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(_textBox.Text);
            }

            public event EventHandler ContentChanged;

            public event EventHandler IsVisibleChanged;

            public event EventHandler Loaded;

            public TextBoxHintProxy(TextBox textBox)
            {
                if (textBox == null) throw new ArgumentNullException("textBox");

                _textBox = textBox;
                _textBox.TextChanged += TextBoxTextChanged;
                _textBox.Loaded += TextBoxLoaded;
                _textBox.IsVisibleChanged += TextBoxIsVisibleChanged;
            }

            private void TextBoxIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
            {
                if (IsVisibleChanged != null)
                {
                    IsVisibleChanged.Invoke(sender, EventArgs.Empty);
                }
            }

            private void TextBoxLoaded(object sender, RoutedEventArgs e)
            {
                if (Loaded != null)
                {
                    Loaded.Invoke(sender, EventArgs.Empty);
                }
            }

            private void TextBoxTextChanged(object sender, TextChangedEventArgs e)
            {
                if (ContentChanged != null)
                {
                    ContentChanged.Invoke(sender, EventArgs.Empty);
                }
            }

            public void Dispose()
            {
                _textBox.TextChanged -= TextBoxTextChanged;
                _textBox.Loaded -= TextBoxLoaded;
                _textBox.IsVisibleChanged -= TextBoxIsVisibleChanged;
            }
        }
    }
}