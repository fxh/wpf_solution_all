﻿using System.Windows;

namespace ICodeShare.UI.Controls.Themes
{
    public static class ResourceColors
    {
        #region Brush Keys

        public static readonly ComponentResourceKey ControlNormalBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ControlNormalBackgroundKey");
        public static readonly ComponentResourceKey ControlDisabledBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ControlDisabledBackgroundKey");
        public static readonly ComponentResourceKey ControlNormalBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ControlNormalBorderKey");
        public static readonly ComponentResourceKey ControlMouseOverBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ControlMouseOverBorderKey");
        public static readonly ComponentResourceKey ControlSelectedBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ControlSelectedBorderKey");
        public static readonly ComponentResourceKey ControlFocusedBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ControlFocusedBorderKey");

        public static readonly ComponentResourceKey ButtonNormalOuterBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonNormalOuterBorderKey");
        public static readonly ComponentResourceKey ButtonNormalInnerBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonNormalInnerBorderKey");
        public static readonly ComponentResourceKey ButtonNormalBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonNormalBackgroundKey");

        public static readonly ComponentResourceKey ButtonMouseOverBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonMouseOverBackgroundKey");
        public static readonly ComponentResourceKey ButtonMouseOverOuterBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonMouseOverOuterBorderKey");
        public static readonly ComponentResourceKey ButtonMouseOverInnerBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonMouseOverInnerBorderKey");

        public static readonly ComponentResourceKey ButtonPressedOuterBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonPressedOuterBorderKey");
        public static readonly ComponentResourceKey ButtonPressedInnerBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonPressedInnerBorderKey");
        public static readonly ComponentResourceKey ButtonPressedBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonPressedBackgroundKey");

        public static readonly ComponentResourceKey ButtonFocusedOuterBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonFocusedOuterBorderKey");
        public static readonly ComponentResourceKey ButtonFocusedInnerBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonFocusedInnerBorderKey");
        public static readonly ComponentResourceKey ButtonFocusedBackgroundKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonFocusedBackgroundKey");

        public static readonly ComponentResourceKey ButtonDisabledOuterBorderKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonDisabledOuterBorderKey");
        public static readonly ComponentResourceKey ButtonInnerBorderDisabledKey = new ComponentResourceKey(typeof(ResourceColors), "ButtonInnerBorderDisabledKey");

        #endregion Brush Keys

        public static readonly ComponentResourceKey GlyphNormalForegroundKey = new ComponentResourceKey(typeof(ResourceColors), "GlyphNormalForegroundKey");
        public static readonly ComponentResourceKey GlyphDisabledForegroundKey = new ComponentResourceKey(typeof(ResourceColors), "GlyphDisabledForegroundKey");

        public static readonly ComponentResourceKey SpinButtonCornerRadiusKey = new ComponentResourceKey(typeof(ResourceColors), "SpinButtonCornerRadiusKey");

        public static readonly ComponentResourceKey SpinnerButtonStyleKey = new ComponentResourceKey(typeof(ResourceColors), "SpinnerButtonStyleKey");
    }
}