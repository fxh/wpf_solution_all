﻿using System;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.Core
{
    public static partial class HintProxyFabric
    {
        private sealed class PasswordBoxHintProxy : IHintProxy
        {
            private readonly PasswordBox _passwordBox;

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(_passwordBox.Password);
            }

            public object Content
            {
                get { return _passwordBox.Password; }
            }

            public bool IsLoaded
            {
                get { return _passwordBox.IsLoaded; }
            }

            public bool IsVisible
            {
                get { return _passwordBox.IsVisible; }
            }

            public event EventHandler ContentChanged;

            public event EventHandler IsVisibleChanged;

            public event EventHandler Loaded;

            public PasswordBoxHintProxy(PasswordBox passwordBox)
            {
                if (passwordBox == null) throw new ArgumentNullException("passwordBox");

                _passwordBox = passwordBox;
                _passwordBox.PasswordChanged += PasswordBoxPasswordChanged;
                _passwordBox.Loaded += PasswordBoxLoaded;
                _passwordBox.IsVisibleChanged += PasswordBoxIsVisibleChanged;
            }

            private void PasswordBoxIsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
            {
                if (IsVisibleChanged != null)
                {
                    IsVisibleChanged.Invoke(this, EventArgs.Empty);
                }
            }

            private void PasswordBoxLoaded(object sender, System.Windows.RoutedEventArgs e)
            {
                if (Loaded != null)
                {
                    Loaded.Invoke(this, EventArgs.Empty);
                }
            }

            private void PasswordBoxPasswordChanged(object sender, System.Windows.RoutedEventArgs e)
            {
                if (ContentChanged != null)
                {
                    ContentChanged.Invoke(this, EventArgs.Empty);
                }
            }

            public void Dispose()
            {
                _passwordBox.PasswordChanged -= PasswordBoxPasswordChanged;
                _passwordBox.Loaded -= PasswordBoxLoaded;
                _passwordBox.IsVisibleChanged -= PasswordBoxIsVisibleChanged;
            }
        }
    }
}