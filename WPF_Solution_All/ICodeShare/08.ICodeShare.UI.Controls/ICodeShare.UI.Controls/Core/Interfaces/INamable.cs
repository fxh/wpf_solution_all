﻿namespace ICodeShare.UI.Controls.Interfaces
{
    public interface INamable
    {
        string Name { get; set; }
    }
}