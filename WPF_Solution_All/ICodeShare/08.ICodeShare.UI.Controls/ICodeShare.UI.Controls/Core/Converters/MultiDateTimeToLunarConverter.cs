﻿using ICodeShare.UI.Controls.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ICodeShare.UI.Controls.Core.Converters
{
    #region IValueConverter
    /// <summary>
    /// DateTime转换成阴历时间
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///   xmlns:cvts="http://schemas.extended.wpf.com/converters"
    /// 2.在xaml文件添加Resources。
    ///   <Window.Resources>
    ///   <cvts:MultiDateTimeToLunarConverter x:Key="cvtsMultiDateTimeToLunar"/>
    ///   </Window.Resources>
    /// 3.在xaml文件中指定Binding值的Converter
    ///    <TextBox Text="{Binding Time,Converter={StaticResource cvtsMultiDateTimeToLunar}}"></TextBox>
    /// </example>
    public sealed class MultiDateTimeToLunarConverter : IMultiValueConverter
    {
        #region Members

        #endregion

        #region Construction
        /// <summary>
        /// 
        /// </summary>
        public MultiDateTimeToLunarConverter()
        {

        }
        #endregion

        #region Properties

        #endregion

        #region  Base Class Overrides 基类方法重写
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length != 3) throw new ArgumentException("Unexpected", "values");
            if (!(values[0] is DateTime)) throw new ArgumentException("Cannot convert from type " + values[0].GetType().Name, "value");

            
            DateTime time = System.Convert.ToDateTime(values[0], CultureInfo.InvariantCulture);
            int day = time.Day;
            if (values[1] != null)
            {
                int.TryParse(values[1].ToString(), out day);
            }
            bool isInactive = false;
            bool.TryParse(values[2].ToString(), out isInactive);
            if (!isInactive)
            {
                int distanceDays = day - time.Day;
                time = time.AddDays(distanceDays);
            }
            else
            {
                if (day < 15)
                {
                    int monthDays = DateTime.DaysInMonth(time.Year, time.Month);
                    int distanceDays = monthDays - time.Day;
                    distanceDays += day;
                    time = time.AddDays(distanceDays);
                }
                else
                {
                    DateTime lastMonthToday = time.AddMonths(-1);
                    int lastMonthDays = DateTime.DaysInMonth(lastMonthToday.Year, lastMonthToday.Month);
                    int distanceDays = lastMonthDays - day;
                    distanceDays += time.Day;
                    time = time.AddDays(-distanceDays);      
                }
            }
            bool isHoliday = false;
            string strLunar = LunarShow(time, out isHoliday);

            return strLunar;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion

        /// <summary>
        /// 阴历显示什么
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string LunarShow(DateTime dt, out bool isHoliday)
        {
            ChineseCalendar chineseCalendar = new ChineseCalendar(dt);
            isHoliday = true;
            string res = "";
            if (chineseCalendar.ChineseTwentyFourDay != "")
            {
                res = chineseCalendar.ChineseTwentyFourDay;
            }
            else if (chineseCalendar.ChineseCalendarHoliday != "")
            {
                res = chineseCalendar.ChineseCalendarHoliday;
            }
            else if (chineseCalendar.DateHoliday != "")
            {
                res = chineseCalendar.DateHoliday;
            }
            else
            {
                isHoliday = false;
                res = chineseCalendar.ChineseShortDateString;
            }
            return res;
        }
    }
    #endregion
}
