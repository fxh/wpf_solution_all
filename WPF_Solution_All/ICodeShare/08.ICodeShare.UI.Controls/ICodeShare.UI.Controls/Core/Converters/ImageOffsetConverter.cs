﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Core.Converters
{
    public class ImageOffsetConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double d = (double)value;
            TranslateTransform tt = new TranslateTransform();
            tt.Y = d;
            return tt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}