﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ICodeShare.UI.Controls.Core.Converters
{
    public class GlassEffectWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double dbl = (double)value;
            return (dbl * 2) * 0.94;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}