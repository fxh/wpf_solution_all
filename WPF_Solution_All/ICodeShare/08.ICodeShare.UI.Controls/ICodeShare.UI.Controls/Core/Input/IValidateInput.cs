﻿namespace ICodeShare.UI.Controls.Core.Input
{
    public interface IValidateInput
    {
        event InputValidationErrorEventHandler InputValidationError;

        bool CommitInput();
    }
}