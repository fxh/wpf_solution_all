﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    public static class DatePickerAssist
    {

        #region DropDownContent 下拉按钮内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetDropDownContent(DependencyObject element, object value)
        {
            element.SetValue(DropDownContentProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static object GetDropDownContent(DependencyObject element)
        {
            return (object)element.GetValue(DropDownContentProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty DropDownContentProperty = DependencyProperty.RegisterAttached(
            "DropDownContent", typeof(object), typeof(DatePickerAssist), new PropertyMetadata(default(object)));

        #endregion
    }
}
