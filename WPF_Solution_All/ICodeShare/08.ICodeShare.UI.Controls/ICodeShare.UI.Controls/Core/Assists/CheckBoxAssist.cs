﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.Assists
{
    public static class CheckBoxAssist
    {
        #region CheckSize 选择框大小

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static double GetCheckSize(DependencyObject element)
        {
            return (double)element.GetValue(CheckSizeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetCheckSize(DependencyObject element, double value)
        {
            element.SetValue(CheckSizeProperty, value);
        }

        // Using a DependencyProperty as the backing store for CheckSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckSizeProperty =
            DependencyProperty.RegisterAttached(
            "CheckSize",
            typeof(double),
            typeof(CheckBoxAssist),
            new FrameworkPropertyMetadata(16.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckSize 选择框大小
    }
}
