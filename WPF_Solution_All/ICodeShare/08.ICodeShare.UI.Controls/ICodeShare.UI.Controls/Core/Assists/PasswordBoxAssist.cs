﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 密码框附加属性
    /// </summary>
    public static class PasswordBoxAssist
    {
        #region IsUpdating 是否正在更新绑定值

        private static readonly DependencyPropertyKey IsUpdatingPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "IsUpdating",
                typeof(bool),
                typeof(PasswordBoxAssist),
                new PropertyMetadata(false));

       public static readonly DependencyProperty IsUpdatingProperty = IsUpdatingPropertyKey.DependencyProperty;

        private static void SetIsUpdating(DependencyObject element, bool value)
        {
            element.SetValue(IsUpdatingPropertyKey, value);
        }

        /// <summary>
        /// Framework use only.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static bool GetIsUpdating(DependencyObject element)
        {
            return (bool)element.GetValue(IsUpdatingProperty);
        }

        #endregion  

        #region IsMonitorPassword 是否监控字数

        public static void SetIsMonitorPassword(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonitorPasswordProperty, value);
        }

        public static bool GetIsMonitorPassword(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonitorPasswordProperty);
        }

        public static readonly DependencyProperty IsMonitorPasswordProperty = DependencyProperty.RegisterAttached(
            "IsMonitorPassword",
            typeof(bool),
            typeof(PasswordBoxAssist),

            new UIPropertyMetadata(false, OnIsMonitorPasswordChanged));
        static void OnIsMonitorPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var passBox = d as PasswordBox;
            if (passBox != null)
            {
                if ((bool)e.NewValue)
                {
                    passBox.Loaded -= PasswordBoxLoaded;
                    passBox.Loaded += PasswordBoxLoaded;
                    if (passBox.IsLoaded)
                    {
                        PasswordBoxLoaded(passBox, new RoutedEventArgs());
                    }
                    passBox.PasswordChanged += PasswordChanged;

                    // Also fixes 1343, also triggers the show of the floating watermark if necessary
                    passBox.Dispatcher.BeginInvoke((Action)(() =>
                        PasswordChanged(passBox, new RoutedEventArgs(PasswordBox.PasswordChangedEvent, passBox))));
                }
                else
                {
                    passBox.PasswordChanged -= PasswordChanged;
                }
            }
        }

        #endregion

        #region CanPlainText 是否监控字数

        public static void SetCanPlainText(DependencyObject obj, bool value)
        {
            obj.SetValue(CanPlainTextProperty, value);
        }

        public static bool GetCanPlainText(DependencyObject obj)
        {
            return (bool)obj.GetValue(CanPlainTextProperty);
        }

        public static readonly DependencyProperty CanPlainTextProperty = DependencyProperty.RegisterAttached(
            "CanPlainText",
            typeof(bool),
            typeof(PasswordBoxAssist),
            new UIPropertyMetadata(false));
        #endregion

        #region PlainButtonContent 附加按钮内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetPlainButtonContent(DependencyObject element, object value)
        {
            element.SetValue(PlainButtonContentProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static object GetPlainButtonContent(DependencyObject element)
        {
            return (object)element.GetValue(PlainButtonContentProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty PlainButtonContentProperty = DependencyProperty.RegisterAttached(
            "PlainButtonContent", typeof(object), typeof(PasswordBoxAssist), new PropertyMetadata(default(object)));

        #endregion

        #region HasPassword 是否有密码输入

        /// <summary>
        /// Gets if the attached TextBox has text.
        /// </summary>
        public static bool GetHasPassword(DependencyObject obj)
        {
            return (bool)obj.GetValue(HasPasswordProperty);
        }

        public static void SetHasPassword(DependencyObject obj, bool value)
        {
            obj.SetValue(HasPasswordProperty, value);
        }

        private static readonly DependencyProperty HasPasswordProperty = DependencyProperty.RegisterAttached(
            "HasPassword",
            typeof(bool),
            typeof(PasswordBoxAssist),
            new FrameworkPropertyMetadata(false));

        #endregion

        #region PasswordLength 密码长度
        private static void SetPasswordLength(DependencyObject obj, int value)
        {
            obj.SetValue(PasswordLengthProperty, value);
            obj.SetValue(HasPasswordProperty, value >= 1);
        }

        public static readonly DependencyProperty PasswordLengthProperty = DependencyProperty.RegisterAttached(
            "PasswordLength",
            typeof(int),
            typeof(PasswordBoxAssist),
            new UIPropertyMetadata(0));
        #endregion

        #region Password 明文密码

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="PasswordBox"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetPassword(DependencyObject element, string value)
        {
            element.SetValue(PasswordProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="PasswordBox"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        public static string GetPassword(DependencyObject element)
        {
            return (string)element.GetValue(PasswordProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="PasswordBox"/> styles.
        /// </summary>
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.RegisterAttached(
            "Password",
            typeof(string),
            typeof(PasswordBoxAssist),
            new PropertyMetadata(string.Empty, OnPasswordPropertyChanged));

        private static void OnPasswordPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {

            PasswordBox targetPasswordBox = dependencyObject as PasswordBox;
            if (targetPasswordBox != null)
            {
                if (!GetIsUpdating(targetPasswordBox))
                {
                    targetPasswordBox.PasswordChanged -= PasswordChanged;
                    targetPasswordBox.Password = (string)e.NewValue;
                    SetPasswordLength(targetPasswordBox, targetPasswordBox.Password.Length);
                    targetPasswordBox.PasswordChanged += PasswordChanged;
                }
            }
        }
        #endregion

        static void PasswordBoxLoaded(object sender, RoutedEventArgs e)
        {
            var passbox = sender as PasswordBox;
            if (passbox != null)
            {
                SetPasswordLength(passbox, passbox.Password.Length);
            }
        }

        /// <summary>
        /// 密码改变事件
        /// </summary>
        static void PasswordChanged(object sender, RoutedEventArgs e)
        {
            var passBox = sender as PasswordBox;
            if (passBox == null)
                return;
            SetPasswordLength(passBox, passBox.Password.Length);
            SetIsUpdating(passBox, true);
            SetPassword(passBox, passBox.Password);
            SetIsUpdating(passBox, false);
        }
    }
}
