﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 附加水印
    /// </summary>
    public static class WatermarkAssist
    {
        #region HasWatermark 是否有水印

        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(RichTextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(AutoCompleteTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePickerTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePicker))]
        public static bool GetHasWatermark(DependencyObject element)
        {
            return (bool)element.GetValue(HasWatermarkProperty);
        }

        private static void SetHasWatermark(DependencyObject element, object value)
        {
            element.SetValue(HasWatermarkPropertyKey, value);
        }

        private static readonly DependencyPropertyKey HasWatermarkPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "HasWatermark",
                typeof(bool),
                typeof(WatermarkAssist),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));

        public static readonly DependencyProperty HasWatermarkProperty = HasWatermarkPropertyKey.DependencyProperty;

        #endregion

        #region Watermark 水印内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(RichTextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(AutoCompleteTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePickerTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePicker))]
        public static object GetWatermark(DependencyObject element)
        {
            return (object)element.GetValue(WatermarkProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetWatermark(DependencyObject element, object value)
        {
            element.SetValue(WatermarkProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.RegisterAttached(
            "Watermark",
            typeof(object),
            typeof(WatermarkAssist),
            new FrameworkPropertyMetadata(default(object), FrameworkPropertyMetadataOptions.Inherits, WatermarkPropertyChangedCallback));

        private static void WatermarkPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            bool result = false;
            if (dependencyPropertyChangedEventArgs.NewValue != null)
            {
                result = true;
            }
            if (GetWatermarkTemplate(dependencyObject) != null)
            {
                result = true;
            }
            SetHasWatermark(dependencyObject, result);
        }

        #endregion

        #region WatermarkTemplate 水印样式

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(RichTextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(AutoCompleteTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePickerTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePicker))]
        public static DataTemplate GetWatermarkTemplate(DependencyObject element)
        {
            return (DataTemplate)element.GetValue(WatermarkTemplateProperty);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static void SetWatermarkTemplate(DependencyObject element, DataTemplate value)
        {
            element.SetValue(WatermarkTemplateProperty, value);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty WatermarkTemplateProperty = DependencyProperty.RegisterAttached(
            "WatermarkTemplate",
            typeof(DataTemplate),
            typeof(WatermarkAssist),
            new FrameworkPropertyMetadata(default(DataTemplate), FrameworkPropertyMetadataOptions.Inherits, WatermarkPropertyChangedCallback));

        #endregion

        #region WatermarkOpacity 水印透明度

        /// <summary>
        /// Gets the text box view margin.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>
        /// The <see cref="Thickness" />.
        /// </returns>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(RichTextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(AutoCompleteTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePickerTextBox))]
        [AttachedPropertyBrowsableForType(typeof(DatePicker))]
        public static double GetWatermarkOpacity(DependencyObject element)
        {
            return (double)element.GetValue(WatermarkOpacityProperty);
        }

        /// <summary>
        /// Sets the hint opacity.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetWatermarkOpacity(DependencyObject element, double value)
        {
            element.SetValue(WatermarkOpacityProperty, value);
        }

        /// <summary>
        /// The hint opacity property
        /// </summary>
        public static readonly DependencyProperty WatermarkOpacityProperty = DependencyProperty.RegisterAttached(
            "WatermarkOpacity",
            typeof(double),
            typeof(WatermarkAssist),
            new FrameworkPropertyMetadata(.56, FrameworkPropertyMetadataOptions.Inherits));

        #endregion WatermarkOpacity
    }
}
