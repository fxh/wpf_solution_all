﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    public static class ButtonAssist
    {

        #region DefaultForeground 默认字体颜色 用于替代Foreground属性，界面xaml设置控件Foreground属性后，触发器修改字体颜色无效。

        /// <summary>
        /// Gets the brush used to draw the font color.
        /// </summary>
       [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        public static Brush GetDefaultForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(DefaultForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the font color.
        /// </summary>
        public static void SetDefaultForeground(DependencyObject element, Brush value)
        {
            element.SetValue(DefaultForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="DefaultForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty DefaultForegroundProperty =
            DependencyProperty.RegisterAttached("DefaultForeground", typeof(Brush), typeof(ButtonAssist),
            new FrameworkPropertyMetadata(Brushes.Black,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion DefaultForeground 默认字体颜色 用于替代Foreground属性，界面xaml设置控件Foreground属性后，触发器修改字体颜色无效。

        #region IsMouseOver=true 鼠标经过时

        #region MouseOverOpacityMask 鼠标进入透明遮罩颜色

        /// <summary>
        /// Gets the brush used to draw the mouse over border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        public static Brush GetMouseOverOpacityMask(DependencyObject element)
        {
            return (Brush)element.GetValue(MouseOverOpacityMaskProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse over border.
        /// </summary>
        public static void SetMouseOverOpacityMask(DependencyObject element, Brush value)
        {
            element.SetValue(MouseOverOpacityMaskProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="MouseOverOpacityMask" /> property.
        /// </summary>
        public static readonly DependencyProperty MouseOverOpacityMaskProperty = DependencyProperty.RegisterAttached(
            "MouseOverOpacityMask",
            typeof(Brush),
            typeof(ButtonAssist),
            new FrameworkPropertyMetadata(Brushes.White,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion MouseOverOpacityMask 鼠标进入透明遮罩颜色

        #endregion IsMouseOver=true 鼠标经过时

        #region IsPressed=true 鼠标按下时

        #region PressedOpacityMask 鼠标按下时透明遮罩颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        public static Brush GetPressedOpacityMask(DependencyObject element)
        {
            return (Brush)element.GetValue(PressedOpacityMaskProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetPressedOpacityMask(DependencyObject element, Brush value)
        {
            element.SetValue(PressedOpacityMaskProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="PressedOpacityMask" /> property.
        /// </summary>
        public static readonly DependencyProperty PressedOpacityMaskProperty = DependencyProperty.RegisterAttached(
            "PressedOpacityMask",
            typeof(Brush),
            typeof(ButtonAssist),
            new FrameworkPropertyMetadata(Brushes.White,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion PressedOpacityMask 鼠标按下时边框颜色

        #endregion IsPressed=true 鼠标按下时
    }
}