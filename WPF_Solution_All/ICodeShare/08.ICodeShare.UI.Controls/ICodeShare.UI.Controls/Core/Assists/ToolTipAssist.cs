﻿using System.Windows;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    public static class ToolTipAssist
    {
        public static CustomPopupPlacementCallback CustomPopupPlacementCallback
        {
            get { return CustomPopupPlacementCallbackImpl; }
        }

        public static CustomPopupPlacement[] CustomPopupPlacementCallbackImpl(Size popupSize, Size targetSize, Point offset)
        {
            return new[]
            {
                new CustomPopupPlacement(new Point(targetSize.Width/2 - popupSize.Width/2, targetSize.Height + 14), PopupPrimaryAxis.Horizontal)
            };
        }
    }
}