﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    public static class FastIconAssist
    { 
        #region IconFont 图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        public static string GetIconFont(DependencyObject element)
        {
            return (string)element.GetValue(IconFontProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconFont(DependencyObject element, string value)
        {
            element.SetValue(IconFontProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFontProperty =DependencyProperty.RegisterAttached(
            "IconFont",
            typeof(string), 
            typeof(FastIconAssist), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconFont 图标

        #region FontAwesome 图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        public static string GetFontAwesome(DependencyObject element)
        {
            return (string)element.GetValue(FontAwesomeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetFontAwesome(DependencyObject element, string value)
        {
            element.SetValue(FontAwesomeProperty, value);
        }

        // Using a DependencyProperty as the backing store for FontAwesome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontAwesomeProperty =DependencyProperty.RegisterAttached(
            "FontAwesome", 
            typeof(string), 
            typeof(FastIconAssist), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion FontAwesome 图标

        #region IconGeometry Path路径图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        public static Geometry GetIconGeometry(DependencyObject element)
        {
            return (Geometry)element.GetValue(IconGeometryProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconGeometry(DependencyObject element, Geometry value)
        {
            element.SetValue(IconGeometryProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconGeometry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconGeometryProperty =DependencyProperty.RegisterAttached(
            "IconGeometry", 
            typeof(Geometry), 
            typeof(FastIconAssist), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconGeometry Path路径图标

        #region IconImage Image图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        public static ImageSource GetIconImage(DependencyObject element)
        {
            return (ImageSource)element.GetValue(IconImageProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconImage(DependencyObject element, ImageSource value)
        {
            element.SetValue(IconImageProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconImageProperty =DependencyProperty.RegisterAttached(
            "IconImage", 
            typeof(ImageSource), 
            typeof(FastIconAssist), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconImage Image图标

        #region IconText Text文字

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        public static string GetIconText(DependencyObject element)
        {
            return (string)element.GetValue(IconTextProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconText(DependencyObject element, string value)
        {
            element.SetValue(IconTextProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconTextProperty = DependencyProperty.RegisterAttached(
            "IconText",
            typeof(string), 
            typeof(FastIconAssist), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconText Text文字

        #region CornerRadius 图标边框圆角

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        public static CornerRadius GetCornerRadius(DependencyObject element)
        {
            return (CornerRadius)element.GetValue(CornerRadiusProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetCornerRadius(DependencyObject element, CornerRadius value)
        {
            element.SetValue(CornerRadiusProperty, value);
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.RegisterAttached(
            "CornerRadius",
            typeof(CornerRadius),
            typeof(FastIconAssist),
            new FrameworkPropertyMetadata(new CornerRadius(), FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CornerRadius 图标边框圆角

        #region BorderBrush 图标边框颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        public static Brush GetBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(BorderBrushProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(BorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for BorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.RegisterAttached(
            "BorderBrush",
            typeof(Brush),
            typeof(FastIconAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion BorderBrush 图标边框颜色

        #region BorderThickness 图标边框粗细

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        public static Thickness GetBorderThickness(DependencyObject element)
        {
            return (Thickness)element.GetValue(BorderThicknessProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBorderThickness(DependencyObject element, Thickness value)
        {
            element.SetValue(BorderThicknessProperty, value);
        }

        public static readonly DependencyProperty BorderThicknessProperty =
            DependencyProperty.RegisterAttached(
            "BorderThickness",
            typeof(Thickness),
            typeof(FastIconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion BorderThickness 图标边框粗细

        #region Background 图标背景颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        public static Brush GetBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(BackgroundProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBackground(DependencyObject element, Brush value)
        {
            element.SetValue(BackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for Background.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached(
            "Background",
            typeof(Brush),
            typeof(FastIconAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Background 图标背景颜色
    }
}