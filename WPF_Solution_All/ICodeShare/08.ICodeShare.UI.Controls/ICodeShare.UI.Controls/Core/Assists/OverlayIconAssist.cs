﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 附加图标属性
    /// </summary>
    public static class OverlayIconAssist
    {

        #region IconFont 图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static string GetIconFont(DependencyObject element)
        {
            return (string)element.GetValue(IconFontProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconFont(DependencyObject element, string value)
        {
            element.SetValue(IconFontProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFontProperty =
            DependencyProperty.RegisterAttached("IconFont", typeof(string), typeof(OverlayIconAssist), new PropertyMetadata(null));

        #endregion IconFont 图标

        #region FontAwesome 图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static string GetFontAwesome(DependencyObject element)
        {
            return (string)element.GetValue(FontAwesomeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetFontAwesome(DependencyObject element, string value)
        {
            element.SetValue(FontAwesomeProperty, value);
        }

        // Using a DependencyProperty as the backing store for FontAwesome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontAwesomeProperty =
            DependencyProperty.RegisterAttached("FontAwesome", typeof(string), typeof(OverlayIconAssist), new PropertyMetadata(null));

        #endregion FontAwesome 图标

        #region IconGeometry Path路径图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Geometry GetIconGeometry(DependencyObject element)
        {
            return (Geometry)element.GetValue(IconGeometryProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconGeometry(DependencyObject element, Geometry value)
        {
            element.SetValue(IconGeometryProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconGeometry.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconGeometryProperty =
            DependencyProperty.RegisterAttached("IconGeometry", typeof(Geometry), typeof(OverlayIconAssist), new PropertyMetadata(null));

        #endregion IconGeometry Path路径图标

        #region IconImage Image图标

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static ImageSource GetIconImage(DependencyObject element)
        {
            return (ImageSource)element.GetValue(IconImageProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconImage(DependencyObject element, ImageSource value)
        {
            element.SetValue(IconImageProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconImageProperty =
            DependencyProperty.RegisterAttached("IconImage", typeof(ImageSource), typeof(OverlayIconAssist), new PropertyMetadata(null));

        #endregion IconImage Image图标

        #region IconText Text文字

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static string GetIconText(DependencyObject element)
        {
            return (string)element.GetValue(IconTextProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconText(DependencyObject element, string value)
        {
            element.SetValue(IconTextProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconTextProperty =
            DependencyProperty.RegisterAttached("IconText", typeof(string), typeof(OverlayIconAssist), new PropertyMetadata(null));

        #endregion IconText Text文字

        #region IconDock 图标位置

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Dock GetIconDock(DependencyObject element)
        {
            return (Dock)element.GetValue(IconDockProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconDock(DependencyObject element, Dock value)
        {
            element.SetValue(IconDockProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconDock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconDockProperty = DependencyProperty.RegisterAttached(
            "IconDock",
            typeof(Dock),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(Dock.Left, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconDock 图标位置

        #region IconSize 图标大小

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static double GetIconSize(DependencyObject element)
        {
            return (double)element.GetValue(IconSizeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconSize(DependencyObject element, double value)
        {
            element.SetValue(IconSizeProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.RegisterAttached(
            "IconSize",
            typeof(double),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(6.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconSize 图标大小

        #region IconBrush 图标颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Brush GetIconBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(IconBrushProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconBrush(DependencyObject element, Brush value)
        {
            element.SetValue(IconBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconBrushProperty =
            DependencyProperty.RegisterAttached(
            "IconBrush",
            typeof(Brush),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconBrush 图标颜色

        #region HorizontalAlignment 横向对齐方式

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static HorizontalAlignment GetHorizontalAlignment(DependencyObject element)
        {
            return (HorizontalAlignment)element.GetValue(HorizontalAlignmentProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetHorizontalAlignment(DependencyObject element, HorizontalAlignment value)
        {
            element.SetValue(HorizontalAlignmentProperty, value);
        }

        // Using a DependencyProperty as the backing store for HorizontalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HorizontalAlignmentProperty =
            DependencyProperty.RegisterAttached(
            "HorizontalAlignment",
            typeof(HorizontalAlignment),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(HorizontalAlignment.Center, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion HorizontalAlignment 横向对齐方式

        #region VerticalAlignment 纵向对齐方式

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static VerticalAlignment GetVerticalAlignment(DependencyObject element)
        {
            return (VerticalAlignment)element.GetValue(VerticalAlignmentProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetVerticalAlignment(DependencyObject element, VerticalAlignment value)
        {
            element.SetValue(VerticalAlignmentProperty, value);
        }

        // Using a DependencyProperty as the backing store for VerticalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VerticalAlignmentProperty =
            DependencyProperty.RegisterAttached(
            "VerticalAlignment",
            typeof(VerticalAlignment),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(VerticalAlignment.Center, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion VerticalAlignment 纵向对齐方式

        #region CornerRadius 图标边框圆角

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static CornerRadius GetCornerRadius(DependencyObject element)
        {
            return (CornerRadius)element.GetValue(CornerRadiusProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetCornerRadius(DependencyObject element, CornerRadius value)
        {
            element.SetValue(CornerRadiusProperty, value);
        }

        // Using a DependencyProperty as the backing store for CornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.RegisterAttached(
            "CornerRadius",
            typeof(CornerRadius),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(new CornerRadius(), FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion CornerRadius 图标边框圆角

        #region BorderBrush 图标边框颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Brush GetBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(BorderBrushProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(BorderBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for BorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.RegisterAttached(
            "BorderBrush",
            typeof(Brush),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion BorderBrush 图标边框颜色

        #region BorderThickness 图标边框粗细

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Thickness GetBorderThickness(DependencyObject element)
        {
            return (Thickness)element.GetValue(BorderThicknessProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBorderThickness(DependencyObject element, Thickness value)
        {
            element.SetValue(BorderThicknessProperty, value);
        }

        public static readonly DependencyProperty BorderThicknessProperty =
            DependencyProperty.RegisterAttached(
            "BorderThickness",
            typeof(Thickness),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion BorderThickness 图标边框粗细

        #region Background 图标背景颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Brush GetBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(BackgroundProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBackground(DependencyObject element, Brush value)
        {
            element.SetValue(BackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for Background.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached(
            "Background",
            typeof(Brush),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Background 图标背景颜色

        #region Padding 图标内缩进

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Thickness GetPadding(DependencyObject element)
        {
            return (Thickness)element.GetValue(PaddingProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetPadding(DependencyObject element, Thickness value)
        {
            element.SetValue(PaddingProperty, value);
        }

        public static readonly DependencyProperty PaddingProperty =
            DependencyProperty.RegisterAttached(
            "Padding", typeof(Thickness),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Padding 图标内缩进

        #region Margin  图标的Margin属性

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(FastOverlayIcon))]
        public static Thickness GetMargin(DependencyObject element)
        {
            return (Thickness)element.GetValue(MarginProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetMargin(DependencyObject element, Thickness value)
        {
            element.SetValue(MarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for Margin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MarginProperty =
            DependencyProperty.RegisterAttached(
            "Margin",
            typeof(Thickness),
            typeof(OverlayIconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Margin  图标的Margin属性
    }
}
