﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.Assists
{
    public static class UnderlineAssist
    {
        #region LineSize 线宽

        /// <summary>
        ///
        /// </summary>
        public static double GetLineSize(DependencyObject element)
        {
            return (double)element.GetValue(LineSizeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetLineSize(DependencyObject element, double value)
        {
            element.SetValue(LineSizeProperty, value);
        }

        // Using a DependencyProperty as the backing store for LineSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineSizeProperty =
            DependencyProperty.RegisterAttached(
            "LineSize",
            typeof(double),
            typeof(UnderlineAssist),
            new FrameworkPropertyMetadata(2.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion LineSize 线宽

        #region Orientation 线方向

        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        public static Orientation GetOrientation(DependencyObject element)
        {
            return (Orientation)element.GetValue(OrientationProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetOrientation(DependencyObject element, Orientation value)
        {
            element.SetValue(OrientationProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="Orientation" /> property.
        /// </summary>
        public static readonly DependencyProperty OrientationProperty =DependencyProperty.RegisterAttached(
            "Orientation", 
            typeof(Orientation), 
            typeof(UnderlineAssist),
            new UIPropertyMetadata(Orientation.Horizontal));

        #endregion Orientation 线方向

        #region Visibility 可见性

        /// <summary>
        /// Controls the visibility of the underline decoration.
        /// </summary>
        public static void SetVisibility(DependencyObject element, Visibility value)
        {
            element.SetValue(VisibilityProperty, value);
        }

        /// <summary>
        /// Controls the visibility of the underline decoration.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static Visibility GetVisibility(DependencyObject element)
        {
            return (Visibility)element.GetValue(VisibilityProperty);
        }

        /// <summary>
        /// Controls the visibility of the underline decoration.
        /// </summary>
        public static readonly DependencyProperty VisibilityProperty = DependencyProperty.RegisterAttached(
            "Visibility", typeof(Visibility), typeof(UnderlineAssist), new PropertyMetadata(default(Visibility)));

        #endregion
    }
}
