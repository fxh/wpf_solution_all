﻿using ICodeShare.UI.Controls.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ICodeShare.UI.Controls.Assists
{
    public static class WindowAssist
    {
        #region Thickness 边框线

        [AttachedPropertyBrowsableForType(typeof(Window))]
        public static Thickness GetResizeBorderThickness(DependencyObject element)
        {
            return (Thickness)element.GetValue(ResizeBorderThicknessProperty);
        }

        public static void SetResizeBorderThickness(DependencyObject element, Thickness value)
        {
            element.SetValue(ResizeBorderThicknessProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty ResizeBorderThicknessProperty = DependencyProperty.RegisterAttached(
            "ResizeBorderThickness",
            typeof(Thickness),
            typeof(WindowAssist),
            new FrameworkPropertyMetadata(new Thickness(3d),
                FrameworkPropertyMetadataOptions.AffectsArrange, null, ThicknessUtil.CoerceNonNegative));

        #endregion Thickness 边框线

        #region MinimizeButtonToolTip  最小化按钮提示

        [AttachedPropertyBrowsableForType(typeof(Window))]
        public static object GetMinimizeButtonToolTip(DependencyObject element)
        {
            return (object)element.GetValue(MinimizeButtonToolTipProperty);
        }

        public static void SetMinimizeButtonToolTip(DependencyObject element, object value)
        {
            element.SetValue(MinimizeButtonToolTipProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty MinimizeButtonToolTipProperty = DependencyProperty.RegisterAttached(
            "MinimizeButtonToolTip",
            typeof(object),
            typeof(WindowAssist),
           new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        #endregion MinimizeButtonToolTip 最小化按钮提示

        #region MaximizeButtonToolTip  最大化按钮提示

        [AttachedPropertyBrowsableForType(typeof(Window))]
        public static object GetMaximizeButtonToolTip(DependencyObject element)
        {
            return (object)element.GetValue(MaximizeButtonToolTipProperty);
        }

        public static void SetMaximizeButtonToolTip(DependencyObject element, object value)
        {
            element.SetValue(MaximizeButtonToolTipProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty MaximizeButtonToolTipProperty = DependencyProperty.RegisterAttached(
            "MaximizeButtonToolTip",
            typeof(object),
            typeof(WindowAssist),
           new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        #endregion MaximizeButtonToolTip 最大化按钮提示

        #region RestoreButtonToolTip  还原按钮提示

        [AttachedPropertyBrowsableForType(typeof(Window))]
        public static object GetRestoreButtonToolTip(DependencyObject element)
        {
            return (object)element.GetValue(RestoreButtonToolTipProperty);
        }

        public static void SetRestoreButtonToolTip(DependencyObject element, object value)
        {
            element.SetValue(RestoreButtonToolTipProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty RestoreButtonToolTipProperty = DependencyProperty.RegisterAttached(
            "RestoreButtonToolTip",
            typeof(object),
            typeof(WindowAssist),
           new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        #endregion RestoreButtonToolTip 还原按钮提示

        #region CloseButtonToolTip  关闭按钮提示

        [AttachedPropertyBrowsableForType(typeof(Window))]
        public static object GetCloseButtonToolTip(DependencyObject element)
        {
            return (object)element.GetValue(CloseButtonToolTipProperty);
        }

        public static void SetCloseButtonToolTip(DependencyObject element, object value)
        {
            element.SetValue(CloseButtonToolTipProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty CloseButtonToolTipProperty = DependencyProperty.RegisterAttached(
            "CloseButtonToolTip",
            typeof(object),
            typeof(WindowAssist),
           new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        #endregion CloseButtonToolTip 关闭按钮提示
    }
}
