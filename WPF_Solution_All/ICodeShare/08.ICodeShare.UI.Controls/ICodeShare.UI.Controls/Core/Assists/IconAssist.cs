﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    public static class IconAssist
    {
        #region HasIcon 是否有图标
        /// <summary>
        /// Framework use only.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetHasIcon(DependencyObject element)
        {
            return (bool)element.GetValue(HasIconProperty);
        }

        private static void SetHasIcon(DependencyObject element, bool value)
        {
            element.SetValue(HasIconPropertyKey, value);
        }

        private static readonly DependencyPropertyKey HasIconPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "HasIcon", 
                typeof(bool), 
                typeof(IconAssist),
                new PropertyMetadata(false));

        public static readonly DependencyProperty HasIconProperty = HasIconPropertyKey.DependencyProperty;

        #endregion

        #region Icon 图标内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static object GetIcon(DependencyObject element)
        {
            return (object)element.GetValue(IconProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetIcon(DependencyObject element, object value)
        {
            element.SetValue(IconProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty IconProperty = DependencyProperty.RegisterAttached(
            "Icon", typeof(object), typeof(IconAssist), new PropertyMetadata(default(object), IconPropertyChangedCallback));

        private static void IconPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            bool result = false;
            if (dependencyPropertyChangedEventArgs.NewValue != null)
            {
                result = true;
            }
            if (GetIconTemplate(dependencyObject) != null)
            {
                result = true;
            }
            SetHasIcon(dependencyObject, result);
        }

        #endregion

        #region IconTemplate 图标内容样式

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static DataTemplate GetIconTemplate(DependencyObject element)
        {
            return (DataTemplate)element.GetValue(IconTemplateProperty);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static void SetIconTemplate(DependencyObject element, DataTemplate value)
        {
            element.SetValue(IconTemplateProperty, value);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty IconTemplateProperty = DependencyProperty.RegisterAttached(
            "IconTemplate", typeof(DataTemplate), typeof(IconAssist), new PropertyMetadata(default(DataTemplate), IconPropertyChangedCallback));
        #endregion

        #region IconDock 图标位置

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        public static Dock GetIconDock(DependencyObject element)
        {
            return (Dock)element.GetValue(IconDockProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconDock(DependencyObject element, Dock value)
        {
            element.SetValue(IconDockProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconDock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconDockProperty = DependencyProperty.RegisterAttached(
            "IconDock",
            typeof(Dock),
            typeof(IconAssist),
            new FrameworkPropertyMetadata(Dock.Left, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconDock 图标位置

        #region AllowAnimation 是否有旋转动画

        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        public static bool GetAllowAnimation(DependencyObject element)
        {
            return (bool)element.GetValue(AllowAnimationProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetAllowAnimation(DependencyObject element, bool value)
        {
            element.SetValue(AllowAnimationProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="AllowAnimation" /> property.
        /// </summary>
        public static readonly DependencyProperty AllowAnimationProperty =
            DependencyProperty.RegisterAttached("AllowAnimation", typeof(bool), typeof(IconAssist), new UIPropertyMetadata(false));

        #endregion AllowAnimation 是否有旋转动画

        #region IconSize 图标大小

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        public static double GetIconSize(DependencyObject element)
        {
            return (double)element.GetValue(IconSizeProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconSize(DependencyObject element, double value)
        {
            element.SetValue(IconSizeProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.RegisterAttached(
            "IconSize",
            typeof(double),
            typeof(IconAssist),
            new FrameworkPropertyMetadata(12.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconSize 图标大小

        #region IconBrush 图标颜色

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        public static Brush GetIconBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(IconBrushProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetIconBrush(DependencyObject element, Brush value)
        {
            element.SetValue(IconBrushProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconBrushProperty =
            DependencyProperty.RegisterAttached(
            "IconBrush",
            typeof(Brush),
            typeof(IconAssist),
            new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion IconBrush 图标颜色

        #region Padding 图标内缩进

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        public static Thickness GetPadding(DependencyObject element)
        {
            return (Thickness)element.GetValue(PaddingProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetPadding(DependencyObject element, Thickness value)
        {
            element.SetValue(PaddingProperty, value);
        }

        public static readonly DependencyProperty PaddingProperty =
            DependencyProperty.RegisterAttached(
            "Padding", 
            typeof(Thickness),
            typeof(IconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Padding 图标内缩进

        #region Margin  图标的Margin属性

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(FastIcon))]
        [AttachedPropertyBrowsableForType(typeof(PackIcon))]
        [AttachedPropertyBrowsableForType(typeof(StyleIcon))]
        public static Thickness GetMargin(DependencyObject element)
        {
            return (Thickness)element.GetValue(MarginProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetMargin(DependencyObject element, Thickness value)
        {
            element.SetValue(MarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for Margin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MarginProperty =
            DependencyProperty.RegisterAttached(
            "Margin",
            typeof(Thickness),
            typeof(IconAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Margin  图标的Margin属性
    }
}
