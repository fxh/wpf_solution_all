﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 附加前缀
    /// </summary>
    public static class PrefixAssist
    {
        #region HasPrefix 是否有前缀

        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static bool GetHasPrefix(DependencyObject element)
        {
            return (bool)element.GetValue(HasPrefixProperty);
        }

        private static void SetHasPrefix(DependencyObject element, object value)
        {
            element.SetValue(HasPrefixPropertyKey, value);
        }

        private static readonly DependencyPropertyKey HasPrefixPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "HasPrefix",
                typeof(bool),
                typeof(PrefixAssist),
                new PropertyMetadata(false));

        public static readonly DependencyProperty HasPrefixProperty = HasPrefixPropertyKey.DependencyProperty;

        #endregion

        #region Prefix 前缀内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static object GetPrefix(DependencyObject element)
        {
            return (object)element.GetValue(PrefixProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetPrefix(DependencyObject element, object value)
        {
            element.SetValue(PrefixProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty PrefixProperty = DependencyProperty.RegisterAttached(
            "Prefix",
            typeof(object),
            typeof(PrefixAssist),
            new PropertyMetadata(default(object), PrefixPropertyChangedCallback));

        private static void PrefixPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            bool result=false;
            if (GetPrefix(dependencyObject)!=null)
            {
                result = true;
            }
            if (GetPrefixTemplate(dependencyObject) != null)
            {
                result = true;
            }
            SetHasPrefix(dependencyObject, result);
        }

        #endregion

        #region PrefixTemplate 前缀样式

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static DataTemplate GetPrefixTemplate(DependencyObject element)
        {
            return (DataTemplate)element.GetValue(PrefixTemplateProperty);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static void SetPrefixTemplate(DependencyObject element, DataTemplate value)
        {
            element.SetValue(PrefixTemplateProperty, value);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty PrefixTemplateProperty = DependencyProperty.RegisterAttached(
            "PrefixTemplate",
            typeof(DataTemplate),
            typeof(PrefixAssist),
            new PropertyMetadata(default(DataTemplate), PrefixPropertyChangedCallback));

        #endregion
    }
}
