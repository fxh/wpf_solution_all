﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 通用控件附加属性
    /// </summary>
    public static class ControlAssist
    {
        #region CornerRadius 边框圆角

        /// <summary>
        /// The CornerRadius property allows users to control the roundness of the button corners independently by
        /// setting a radius value for each corner. Radius values that are too large are scaled so that they
        /// smoothly blend from corner to corner. (Can be used e.g. at MetroButton style)
        /// Description taken from original Microsoft description :-D
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ProgressBar))]
        public static CornerRadius GetCornerRadius(DependencyObject element)
        {
            return (CornerRadius)element.GetValue(CornerRadiusProperty);
        }

        public static void SetCornerRadius(DependencyObject element, CornerRadius value)
        {
            element.SetValue(CornerRadiusProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.RegisterAttached(
            "CornerRadius",
            typeof(CornerRadius),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(new CornerRadius(0),
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion CornerRadius 边框圆角

        #region IsMouseOver=true 鼠标指针悬停在控件上

        #region MouseOverBackground 鼠标指针悬停在控件上时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse over background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetMouseOverBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(MouseOverBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse over background.
        /// </summary>
        public static void SetMouseOverBackground(DependencyObject element, Brush value)
        {
            element.SetValue(MouseOverBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="MouseOverBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty MouseOverBackgroundProperty = DependencyProperty.RegisterAttached(
            "MouseOverBackground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion MouseOverBackground 鼠标进入背景颜色

        #region MouseOverBorderBrush 鼠标指针悬停在控件上时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse over border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetMouseOverBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(MouseOverBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse over border.
        /// </summary>
        public static void SetMouseOverBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(MouseOverBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="MouseOverBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty MouseOverBorderBrushProperty = DependencyProperty.RegisterAttached(
            "MouseOverBorderBrush",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion MouseOverBorderBrush 鼠标进入边框框颜色

        #region MouseOverForeground 鼠标指针悬停在控件上时，字体颜色

        /// <summary>
        /// Gets the brush used to draw the mouse over font color.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetMouseOverForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(MouseOverForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse over font color.
        /// </summary>
        public static void SetMouseOverForeground(DependencyObject element, Brush value)
        {
            element.SetValue(MouseOverForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="MouseOverForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty MouseOverForegroundProperty = DependencyProperty.RegisterAttached(
            "MouseOverForeground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion MouseOverForeground 鼠标进入字体颜色

        #endregion IsMouseOver=true 鼠标指针悬停在控件上

        #region IsPressed=true 控件已按下

        #region PressedBackground 控件已按下时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetPressedBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(PressedBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed background.
        /// </summary>
        public static void SetPressedBackground(DependencyObject element, Brush value)
        {
            element.SetValue(PressedBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="PressedBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty PressedBackgroundProperty = DependencyProperty.RegisterAttached(
            "PressedBackground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion PressedBackground 鼠标按下时背景颜色

        #region PressedBorderBrush 控件已按下时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetPressedBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(PressedBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetPressedBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(PressedBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="PressedBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty PressedBorderBrushProperty = DependencyProperty.RegisterAttached(
            "PressedBorderBrush",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion PressedBorderBrush 鼠标按下时边框颜色

        #region PressedForeground 控件已按下时，字体颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed font color.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetPressedForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(PressedForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed font color.
        /// </summary>
        public static void SetPressedForeground(DependencyObject element, Brush value)
        {
            element.SetValue(PressedForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="PressedForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty PressedForegroundProperty = DependencyProperty.RegisterAttached(
            "PressedForeground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion PressedForeground 鼠标按下时字体颜色

        #endregion IsPressed=true 控件已按下

        #region IsChecked=true 控件被勾选 ToggleButton

        #region CheckedBackground 控件被选中时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetCheckedBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(CheckedBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed background.
        /// </summary>
        public static void SetCheckedBackground(DependencyObject element, Brush value)
        {
            element.SetValue(CheckedBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CheckedBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty CheckedBackgroundProperty = DependencyProperty.RegisterAttached(
            "CheckedBackground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckedBackground 鼠标按下时背景颜色

        #region CheckedBorderBrush 控件被选中时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetCheckedBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(CheckedBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetCheckedBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(CheckedBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CheckedBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty CheckedBorderBrushProperty = DependencyProperty.RegisterAttached(
            "CheckedBorderBrush",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckedBorderBrush 鼠标按下时边框颜色

        #region CheckedForeground 控件被选中时，字体颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed font color.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetCheckedForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(CheckedForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed font color.
        /// </summary>
        public static void SetCheckedForeground(DependencyObject element, Brush value)
        {
            element.SetValue(CheckedForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CheckedForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty CheckedForegroundProperty = DependencyProperty.RegisterAttached(
            "CheckedForeground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckedForeground 选中时字体颜色

        #endregion

        #region IsFocused=true 控件具有焦点

        #region FocusedBackground 控件具有焦点时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetFocusedBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(FocusedBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed background.
        /// </summary>
        public static void SetFocusedBackground(DependencyObject element, Brush value)
        {
            element.SetValue(FocusedBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="FocusedBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty FocusedBackgroundProperty = DependencyProperty.RegisterAttached(
            "FocusedBackground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion FocusedBackground 鼠标按下时背景颜色

        #region FocusedBorderBrush 控件具有焦点时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the focus border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetFocusedBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(FocusedBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the focus border.
        /// </summary>
        public static void SetFocusedBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(FocusedBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="FocusedBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty FocusedBorderBrushProperty = DependencyProperty.RegisterAttached(
            "FocusedBorderBrush",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion FocusedBorderBrush 获取焦点后边框颜色

        #region FocusedForeground 控件具有焦点时，字体颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed font color.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(RepeatButton))]
        [AttachedPropertyBrowsableForType(typeof(Button))]
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBox))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetFocusedForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(FocusedForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed font color.
        /// </summary>
        public static void SetFocusedForeground(DependencyObject element, Brush value)
        {
            element.SetValue(FocusedForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="FocusedForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty FocusedForegroundProperty = DependencyProperty.RegisterAttached(
            "FocusedForeground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion FocusedForeground 选中时字体颜色

        #endregion IsFocused 获取焦点时

        #region IsSelected=true 控件被选中

        #region SelectedBackground 控件被选中时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetSelectedBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(SelectedBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed background.
        /// </summary>
        public static void SetSelectedBackground(DependencyObject element, Brush value)
        {
            element.SetValue(SelectedBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="SelectedBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty SelectedBackgroundProperty = DependencyProperty.RegisterAttached(
            "SelectedBackground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion SelectedBackground 鼠标按下时背景颜色

        #region SelectedBorderBrush 控件被选中时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetSelectedBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(SelectedBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetSelectedBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(SelectedBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="SelectedBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty SelectedBorderBrushProperty = DependencyProperty.RegisterAttached(
            "SelectedBorderBrush",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion SelectedBorderBrush 鼠标按下时边框颜色

        #region SelectedForeground 控件被选中时，字体颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed font color.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(TreeView))]
        public static Brush GetSelectedForeground(DependencyObject element)
        {
            return (Brush)element.GetValue(SelectedForegroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed font color.
        /// </summary>
        public static void SetSelectedForeground(DependencyObject element, Brush value)
        {
            element.SetValue(SelectedForegroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="SelectedForeground" /> property.
        /// </summary>
        public static readonly DependencyProperty SelectedForegroundProperty = DependencyProperty.RegisterAttached(
            "SelectedForeground",
            typeof(Brush),
            typeof(ControlAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion SelectedForeground 选中时字体颜色

        #endregion
    }
}
