﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 伸缩控件附加属性
    /// </summary>
    public static class ExpanderAssist
    {
        #region HasHeaderIcon 是否有选中内容

        private static readonly DependencyPropertyKey HasHeaderIconPropertyKey = DependencyProperty.RegisterAttachedReadOnly(
                "HasHeaderIcon",
                typeof(bool),
                typeof(ExpanderAssist),
                new PropertyMetadata(false));

        public static readonly DependencyProperty HasHeaderIconProperty = HasHeaderIconPropertyKey.DependencyProperty;

        private static void SetHasHeaderIcon(DependencyObject element, object value)
        {
            element.SetValue(HasHeaderIconPropertyKey, value);
        }

        /// <summary>
        /// Framework use only.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetHasHeaderIcon(DependencyObject element)
        {
            return (bool)element.GetValue(HasHeaderIconProperty);
        }

        #endregion

        #region HeaderIcon 选中内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty HeaderIconProperty = DependencyProperty.RegisterAttached(
            "HeaderIcon", 
            typeof(object),
            typeof(ExpanderAssist), 
            new PropertyMetadata(default(object), HeaderIconPropertyChangedCallback));

        private static void HeaderIconPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            SetHasHeaderIcon(dependencyObject, dependencyPropertyChangedEventArgs.NewValue != null);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetHeaderIcon(DependencyObject element, object value)
        {
            element.SetValue(HeaderIconProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static object GetHeaderIcon(DependencyObject element)
        {
            return (object)element.GetValue(HeaderIconProperty);
        }

        #endregion
    }
}
