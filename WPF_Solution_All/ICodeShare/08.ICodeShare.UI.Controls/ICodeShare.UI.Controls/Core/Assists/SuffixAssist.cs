﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 附加后缀
    /// </summary>
    public static class SuffixAssist
    {
        #region HasSuffix 是否有后缀


        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static bool GetHasSuffix(DependencyObject element)
        {
            return (bool)element.GetValue(HasSuffixProperty);
        }

        private static void SetHasSuffix(DependencyObject element, object value)
        {
            element.SetValue(HasSuffixPropertyKey, value);
        }

        private static readonly DependencyPropertyKey HasSuffixPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "HasSuffix",
                typeof(bool),
                typeof(SuffixAssist),
                new PropertyMetadata(false));

        public static readonly DependencyProperty HasSuffixProperty = HasSuffixPropertyKey.DependencyProperty;

        #endregion

        #region Suffix 后缀内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static object GetSuffix(DependencyObject element)
        {
            return (object)element.GetValue(SuffixProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetSuffix(DependencyObject element, object value)
        {
            element.SetValue(SuffixProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty SuffixProperty = DependencyProperty.RegisterAttached(
            "Suffix",
            typeof(object),
            typeof(SuffixAssist),
            new PropertyMetadata(default(object), SuffixPropertyChangedCallback));

        private static void SuffixPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            bool result = false;
            if (GetSuffix(dependencyObject) != null)
            {
                result = true;
            }
            if (GetSuffixTemplate(dependencyObject) != null)
            {
                result = true;
            }
            SetHasSuffix(dependencyObject, result);
        }

        #endregion

        #region SuffixTemplate 后缀样式

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static DataTemplate GetSuffixTemplate(DependencyObject element)
        {
            return (DataTemplate)element.GetValue(SuffixTemplateProperty);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static void SetSuffixTemplate(DependencyObject element, DataTemplate value)
        {
            element.SetValue(SuffixTemplateProperty, value);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty SuffixTemplateProperty = DependencyProperty.RegisterAttached(
            "SuffixTemplate",
            typeof(DataTemplate),
            typeof(SuffixAssist),
            new PropertyMetadata(default(DataTemplate),SuffixPropertyChangedCallback));
        #endregion
    }
}
