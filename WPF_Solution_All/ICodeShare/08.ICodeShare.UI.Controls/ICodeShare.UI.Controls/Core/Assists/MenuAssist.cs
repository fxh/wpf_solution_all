﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 菜单子项附加属性
    /// </summary>
    public static class MenuAssist
    {
        #region Orientation 菜单方向
        [AttachedPropertyBrowsableForType(typeof(MenuBase))]
        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        public static Orientation GetOrientation(DependencyObject element)
        {
            return (Orientation)element.GetValue(OrientationProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetOrientation(DependencyObject element, Orientation value)
        {
            element.SetValue(OrientationProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="Orientation" /> property.
        /// </summary>
        public static readonly DependencyProperty OrientationProperty =DependencyProperty.RegisterAttached(
            "Orientation", 
            typeof(Orientation),
            typeof(MenuAssist),
            new UIPropertyMetadata(Orientation.Horizontal));

        #endregion Orientation 菜单方向

        #region TopLevelPlacement 顶部菜单弹窗位置
        [AttachedPropertyBrowsableForType(typeof(MenuBase))]
        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        public static PlacementMode GetTopLevelPlacement(DependencyObject element)
        {
            return (PlacementMode)element.GetValue(TopLevelPlacementProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetTopLevelPlacement(DependencyObject element, PlacementMode value)
        {
            element.SetValue(TopLevelPlacementProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="TopLevelPlacement" /> property.
        /// </summary>
        public static readonly DependencyProperty TopLevelPlacementProperty = DependencyProperty.RegisterAttached(
            "TopLevelPlacement",
            typeof(PlacementMode),
            typeof(MenuAssist),
            new UIPropertyMetadata(PlacementMode.Bottom));

        #endregion TopLevelPlacement 顶部菜单弹窗位置

        #region SubmenuPlacement 二级菜单弹窗位置

        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary> 
        [AttachedPropertyBrowsableForType(typeof(MenuBase))]
        public static PlacementMode GetSubmenuPlacement(DependencyObject element)
        {
            return (PlacementMode)element.GetValue(SubmenuPlacementProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetSubmenuPlacement(DependencyObject element, PlacementMode value)
        {
            element.SetValue(SubmenuPlacementProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="SubmenuPlacement" /> property.
        /// </summary>
        public static readonly DependencyProperty SubmenuPlacementProperty =
            DependencyProperty.RegisterAttached(
            "SubmenuPlacement", 
            typeof(PlacementMode), 
            typeof(MenuAssist), 
            new UIPropertyMetadata(PlacementMode.Right));

        #endregion SubmenuPlacement 二级菜单弹窗位置
    }
}
