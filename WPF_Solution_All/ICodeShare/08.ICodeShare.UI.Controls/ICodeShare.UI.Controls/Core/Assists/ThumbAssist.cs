﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    public static class ThumbAssist
    {
        #region CornerRadius 边框圆角

        /// <summary>
        /// The CornerRadius property allows users to control the roundness of the button corners independently by
        /// setting a radius value for each corner. Radius values that are too large are scaled so that they
        /// smoothly blend from corner to corner. (Can be used e.g. at MetroButton style)
        /// Description taken from original Microsoft description :-D
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static CornerRadius GetCornerRadius(DependencyObject element)
        {
            return (CornerRadius)element.GetValue(CornerRadiusProperty);
        }

        public static void SetCornerRadius(DependencyObject element, CornerRadius value)
        {
            element.SetValue(CornerRadiusProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CornerRadius" /> property.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.RegisterAttached(
            "CornerRadius",
            typeof(CornerRadius),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(new CornerRadius(),
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion CornerRadius 边框圆角

        #region Background 滑块背景

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(BackgroundProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetBackground(DependencyObject element, Brush value)
        {
            element.SetValue(BackgroundProperty, value);
        }

        // Using a DependencyProperty as the backing store for Background.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached(
            "Background",
            typeof(Brush),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Background 滑块背景

        #region BorderBrush 滑块边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(BorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(BorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="BorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty BorderBrushProperty = DependencyProperty.RegisterAttached(
            "BorderBrush",
            typeof(Brush),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion BorderBrush 滑块边框颜色

        #region BorderThickness 边框线粗细

        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Thickness GetBorderThickness(DependencyObject element)
        {
            return (Thickness)element.GetValue(BorderThicknessProperty);
        }

        public static void SetBorderThickness(DependencyObject element, Thickness value)
        {
            element.SetValue(BorderThicknessProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="BorderThickness" /> property.
        /// </summary>
        public static readonly DependencyProperty BorderThicknessProperty = DependencyProperty.RegisterAttached(
            "BorderThickness",
            typeof(Thickness),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(new Thickness(0.0),
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion BorderThickness 边框线粗细

        #region IsChecked=true 控件被勾选 ToggleButton

        #region CheckedBackground 控件被选中时，背景颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetCheckedBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(CheckedBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed background.
        /// </summary>
        public static void SetCheckedBackground(DependencyObject element, Brush value)
        {
            element.SetValue(CheckedBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CheckedBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty CheckedBackgroundProperty = DependencyProperty.RegisterAttached(
            "CheckedBackground",
            typeof(Brush),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckedBackground 鼠标按下时背景颜色

        #region CheckedBorderBrush 控件被选中时，边框颜色

        /// <summary>
        /// Gets the brush used to draw the mouse pressed border.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static Brush GetCheckedBorderBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(CheckedBorderBrushProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse pressed border.
        /// </summary>
        public static void SetCheckedBorderBrush(DependencyObject element, Brush value)
        {
            element.SetValue(CheckedBorderBrushProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="CheckedBorderBrush" /> property.
        /// </summary>
        public static readonly DependencyProperty CheckedBorderBrushProperty = DependencyProperty.RegisterAttached(
            "CheckedBorderBrush",
            typeof(Brush),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckedBorderBrush 选中时边框颜色

        #endregion

        #region Width 边框圆角

        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static double GetWidth(DependencyObject element)
        {
            return (double)element.GetValue(WidthProperty);
        }

        public static void SetWidth(DependencyObject element, double value)
        {
            element.SetValue(WidthProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="Width" /> property.
        /// </summary>
        public static readonly DependencyProperty WidthProperty = DependencyProperty.RegisterAttached(
            "Width",
            typeof(double),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(25.0,
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion Width 边框圆角

        #region Height 边框圆角

        /// <summary>
        /// The Height property allows users to control the roundness of the button corners independently by
        /// setting a radius value for each corner. Radius values that are too large are scaled so that they
        /// smoothly blend from corner to corner. (Can be used e.g. at MetroButton style)
        /// Description taken from original Microsoft description :-D
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ToggleButton))]
        [AttachedPropertyBrowsableForType(typeof(RadioButton))]
        [AttachedPropertyBrowsableForType(typeof(CheckBox))]
        public static double GetHeight(DependencyObject element)
        {
            return (double)element.GetValue(HeightProperty);
        }

        public static void SetHeight(DependencyObject element, double value)
        {
            element.SetValue(HeightProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="Height" /> property.
        /// </summary>
        public static readonly DependencyProperty HeightProperty = DependencyProperty.RegisterAttached(
            "Height",
            typeof(double),
            typeof(ThumbAssist),
            new FrameworkPropertyMetadata(25.0,FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion Height 边框圆角
    }
}
