﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 标签附加属性
    /// </summary>
    public static class LabelAssist
    {
        #region HasLabel 是否有标签

        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static bool GetHasLabel(DependencyObject element)
        {
            return (bool)element.GetValue(HasLabelProperty);
        }

        private static void SetHasLabel(DependencyObject element, object value)
        {
            element.SetValue(HasLabelPropertyKey, value);
        }

        private static readonly DependencyPropertyKey HasLabelPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly(
                "HasLabel",
                typeof(bool),
                typeof(LabelAssist),
                new PropertyMetadata(false));

        public static readonly DependencyProperty HasLabelProperty = HasLabelPropertyKey.DependencyProperty;

        #endregion

        #region Label 标签内容

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static object GetLabel(DependencyObject element)
        {
            return (object)element.GetValue(LabelProperty);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetLabel(DependencyObject element, object value)
        {
            element.SetValue(LabelProperty, value);
        }

        /// <summary>
        /// Allows on (IsChecked) content to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty LabelProperty = DependencyProperty.RegisterAttached(
            "Label",
            typeof(object),
            typeof(LabelAssist),
            new PropertyMetadata(default(object), LabelPropertyChangedCallback));

        private static void LabelPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            bool result = false;
            if (GetLabel(dependencyObject) != null)
            {
                result = true;
            }
            if (GetLabelTemplate(dependencyObject) != null)
            {
                result = true;
            }
            SetHasLabel(dependencyObject, result);
        }

        #endregion

        #region LabelTemplate 标签样式

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static DataTemplate GetLabelTemplate(DependencyObject element)
        {
            return (DataTemplate)element.GetValue(LabelTemplateProperty);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static void SetLabelTemplate(DependencyObject element, DataTemplate value)
        {
            element.SetValue(LabelTemplateProperty, value);
        }

        /// <summary>
        /// Allows an on (IsChecked) template to be provided on supporting <see cref="ToggleButton"/> styles.
        /// </summary>
        public static readonly DependencyProperty LabelTemplateProperty = DependencyProperty.RegisterAttached(
            "LabelTemplate",
            typeof(DataTemplate),
            typeof(LabelAssist),
            new PropertyMetadata(default(DataTemplate), LabelPropertyChangedCallback));

        #endregion

        #region MinWidth 标签最小宽度

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static double GetMinWidth(DependencyObject element)
        {
            return (double)element.GetValue(MinWidthProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetMinWidth(DependencyObject element, double value)
        {
            element.SetValue(MinWidthProperty, value);
        }

        // Using a DependencyProperty as the backing store for MinWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinWidthProperty =
            DependencyProperty.RegisterAttached(
            "MinWidth",
            typeof(double),
            typeof(LabelAssist),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion MinWidth 标签最小宽度

        #region Margin  标签的与边缘距离

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static Thickness GetMargin(DependencyObject element)
        {
            return (Thickness)element.GetValue(MarginProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetMargin(DependencyObject element, Thickness value)
        {
            element.SetValue(MarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for Margin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MarginProperty =
            DependencyProperty.RegisterAttached(
            "Margin",
            typeof(Thickness),
            typeof(LabelAssist),
            new FrameworkPropertyMetadata(new Thickness(0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion Margin  标签的与边缘距离

        #region HorizontalAlignment  标签水平对齐方式

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        public static HorizontalAlignment GetHorizontalAlignment(DependencyObject element)
        {
            return (HorizontalAlignment)element.GetValue(HorizontalAlignmentProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetHorizontalAlignment(DependencyObject element, HorizontalAlignment value)
        {
            element.SetValue(HorizontalAlignmentProperty, value);
        }

        // Using a DependencyProperty as the backing store for HorizontalAlignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HorizontalAlignmentProperty =
            DependencyProperty.RegisterAttached(
            "HorizontalAlignment",
            typeof(HorizontalAlignment),
            typeof(LabelAssist),
            new FrameworkPropertyMetadata(HorizontalAlignment.Right, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion HorizontalAlignment  标签水平对齐方式
    }
}
