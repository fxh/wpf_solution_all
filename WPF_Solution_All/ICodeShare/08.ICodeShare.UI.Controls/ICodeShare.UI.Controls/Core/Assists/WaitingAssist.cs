﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ICodeShare.UI.Controls.Assists
{
    public static class WaitingAssist
    {
        #region IsWaitingFor 等待效果

        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        public static bool GetIsWaitingFor(DependencyObject element)
        {
            return (bool)element.GetValue(IsWaitingForProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetIsWaitingFor(DependencyObject element, bool value)
        {
            element.SetValue(IsWaitingForProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="IsWaitingFor" /> property.
        /// </summary>
        public static readonly DependencyProperty IsWaitingForProperty =
            DependencyProperty.RegisterAttached("IsWaitingFor", typeof(bool), typeof(WaitingAssist), new UIPropertyMetadata(false));

        #endregion IsWaitingFor 等待效果

        #region WaitingColor

        public static readonly DependencyProperty WaitingColorProperty = DependencyProperty.RegisterAttached(
            "WaitingColor", typeof(Color),
            typeof(WaitingAssist),
            new FrameworkPropertyMetadata(Colors.White, FrameworkPropertyMetadataOptions.Inherits | FrameworkPropertyMetadataOptions.AffectsRender));

        public static void SetWaitingColor(DependencyObject element, Color value)
        {
            element.SetValue(WaitingColorProperty, value);
        }

        [AttachedPropertyBrowsableForType(typeof(ButtonBase))]
        [AttachedPropertyBrowsableForType(typeof(TextBoxBase))]
        [AttachedPropertyBrowsableForType(typeof(PasswordBox))]
        public static Color GetWaitingColor(DependencyObject element)
        {
            return (Color)element.GetValue(WaitingColorProperty);
        }

        #endregion WaitingColor
    }
}
