﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 滚动条
    /// </summary>
    public static class ScrollViewerAssist
    {
        #region HorizontalOffset 横轴偏移

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ScrollViewer))]
        public static double GetHorizontalOffset(DependencyObject element)
        {
            return (double)element.GetValue(HorizontalOffsetProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetHorizontalOffset(DependencyObject element, double value)
        {
            element.SetValue(HorizontalOffsetProperty, value);
        }

        // Using a DependencyProperty as the backing store for HorizontalOffset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.RegisterAttached(
            "HorizontalOffset",
            typeof(double),
            typeof(ScrollViewerAssist),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits, HorizontalOffsetPropertyChangedCallback));


        private static void HorizontalOffsetPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            ScrollViewer scrollViewer = dependencyObject as ScrollViewer;
            if (scrollViewer != null)
            {
                scrollViewer.ScrollToHorizontalOffset((double)e.NewValue);
            }
        }
        #endregion HorizontalOffset 横轴偏移

        #region VerticalOffset 竖轴偏移

        /// <summary>
        ///
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ScrollViewer))]
        public static double GetVerticalOffset(DependencyObject element)
        {
            return (double)element.GetValue(VerticalOffsetProperty);
        }

        /// <summary>
        ///
        /// </summary>
        public static void SetVerticalOffset(DependencyObject element, double value)
        {
            element.SetValue(VerticalOffsetProperty, value);
        }

        // Using a DependencyProperty as the backing store for VerticalOffset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VerticalOffsetProperty =
            DependencyProperty.RegisterAttached(
            "VerticalOffset",
            typeof(double),
            typeof(ScrollViewerAssist),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits, VerticalOffsetPropertyChangedCallback));


        private static void VerticalOffsetPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            ScrollViewer scrollViewer = dependencyObject as ScrollViewer;
            if (scrollViewer != null)
            {
                scrollViewer.ScrollToVerticalOffset((double)e.NewValue);
            }
        }
        #endregion VerticalOffset 竖轴偏移

        #region VerticalScrollBarOnLeftSide 竖轴是否靠左
        [AttachedPropertyBrowsableForType(typeof(ScrollViewer))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(HamburgerMenu))]
        public static bool GetVerticalScrollBarOnLeftSide(DependencyObject element)
        {
            return (bool)element.GetValue(VerticalScrollBarOnLeftSideProperty);
        }

        public static void SetVerticalScrollBarOnLeftSide(DependencyObject element, bool value)
        {
            element.SetValue(VerticalScrollBarOnLeftSideProperty, value);
        }

        public static readonly DependencyProperty VerticalScrollBarOnLeftSideProperty = DependencyProperty.RegisterAttached(
            "VerticalScrollBarOnLeftSide",
            typeof(bool),
            typeof(ScrollViewerAssist),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.Inherits));

        #endregion

        #region IsHorizontalWheel 是否横向滚轮

        [AttachedPropertyBrowsableForType(typeof(ScrollViewer))]
        public static bool GetIsHorizontalWheel(DependencyObject element)
        {
            return (bool)element.GetValue(IsHorizontalWheelProperty);
        }

        public static void SetIsHorizontalWheel(DependencyObject element, bool value)
        {
            element.SetValue(IsHorizontalWheelProperty, value);
        }

        public static readonly DependencyProperty IsHorizontalWheelProperty = DependencyProperty.RegisterAttached(
            "IsHorizontalWheel",
            typeof(bool),
            typeof(ScrollViewerAssist),
            new PropertyMetadata(false, OnIsHorizontalWheelPropertyChangedCallback));

        private static void OnIsHorizontalWheelPropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var scrollViewer = o as ScrollViewer;
            if (scrollViewer != null && e.NewValue != e.OldValue && e.NewValue is bool)
            {
                scrollViewer.PreviewMouseWheel -= ScrollViewerOnPreviewMouseWheel;
                if ((bool)e.NewValue)
                {
                    scrollViewer.PreviewMouseWheel += ScrollViewerOnPreviewMouseWheel;
                }
            }
        }

        private static void ScrollViewerOnPreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer != null && scrollViewer.HorizontalScrollBarVisibility != ScrollBarVisibility.Disabled)
            {
                if (e.Delta > 0)
                {
                    scrollViewer.LineLeft();
                }
                else
                {
                    scrollViewer.LineRight();
                }
                e.Handled = true;
            }
        }

        #endregion
    }
}
