﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
namespace ICodeShare.UI.Controls.Assists
{
    /// <summary>
    /// 选择器附加属性
    /// </summary>
    public static class SelectorAssist
    {
        #region CheckMarkOpacity 水印透明度

        /// <summary>
        /// Gets the text box view margin.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>
        /// The <see cref="Thickness" />.
        /// </returns>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(ListViewItem))]
        public static double GetCheckMarkOpacity(DependencyObject element)
        {
            return (double)element.GetValue(CheckMarkOpacityProperty);
        }

        /// <summary>
        /// Sets the hint opacity.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetCheckMarkOpacity(DependencyObject element, double value)
        {
            element.SetValue(CheckMarkOpacityProperty, value);
        }

        /// <summary>
        /// The hint opacity property
        /// </summary>
        public static readonly DependencyProperty CheckMarkOpacityProperty = DependencyProperty.RegisterAttached(
            "CheckMarkOpacity",
            typeof(double),
            typeof(SelectorAssist),
            new FrameworkPropertyMetadata(0.1, FrameworkPropertyMetadataOptions.Inherits));

        #endregion CheckMarkOpacity

        #region ShowCheckMark 是否显示勾选标记

        /// <summary>
        /// Get where show the shadow for waiting data
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(ListViewItem))]
        public static bool GetShowCheckMark(DependencyObject element)
        {
            return (bool)element.GetValue(ShowCheckMarkProperty);
        }

        /// <summary>
        /// Set where show the shadow for waiting data
        /// </summary>
        public static void SetShowCheckMark(DependencyObject element, bool value)
        {
            element.SetValue(ShowCheckMarkProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="ShowCheckMark" /> property.
        /// </summary>
        public static readonly DependencyProperty ShowCheckMarkProperty = DependencyProperty.RegisterAttached(
            "ShowCheckMark",
            typeof(bool),
            typeof(SelectorAssist),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));

        #endregion ShowCheckMark 是否显示勾选标记

        #region AlternationBackground 隔行变色时，交替颜色

        /// <summary>
        /// Gets the brush used to draw the mouse over background.
        /// </summary>
        [AttachedPropertyBrowsableForType(typeof(ComboBox))]
        [AttachedPropertyBrowsableForType(typeof(ComboBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListBox))]
        [AttachedPropertyBrowsableForType(typeof(ListBoxItem))]
        [AttachedPropertyBrowsableForType(typeof(ListView))]
        [AttachedPropertyBrowsableForType(typeof(ListViewItem))]
        public static Brush GetAlternationBackground(DependencyObject element)
        {
            return (Brush)element.GetValue(AlternationBackgroundProperty);
        }

        /// <summary>
        /// Sets the brush used to draw the mouse over background.
        /// </summary>
        public static void SetAlternationBackground(DependencyObject element, Brush value)
        {
            element.SetValue(AlternationBackgroundProperty, value);
        }

        /// <summary>
        /// DependencyProperty for <see cref="AlternationBackground" /> property.
        /// </summary>
        public static readonly DependencyProperty AlternationBackgroundProperty = DependencyProperty.RegisterAttached(
            "AlternationBackground",
            typeof(Brush),
            typeof(SelectorAssist),
            new FrameworkPropertyMetadata(Brushes.Transparent,FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Inherits));

        #endregion AlternationBackground 鼠标进入背景颜色
    }
}
