﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.ValidationRules
{
    /// <summary>
    /// 绑定时间验证规则，大于现在验证
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:vals="http://schemas.extended.wpf.com/validations"
    /// 2.在xaml文件中指定Binding值的Converter
    ///  <DatePicker.SelectedDate>
    ///      <Binding Path="FutureValidatingDate" UpdateSourceTrigger="PropertyChanged">
    ///          <Binding.ValidationRules>
    ///              <vals:FutureDateValidationRule ValidatesOnTargetUpdated="True"/>
    ///           </Binding.ValidationRules>
    ///      </Binding>
    ///   </DatePicker.SelectedDate>
    /// </example>
    public class FutureDateValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            DateTime tempTime;

            if (!DateTime.TryParse((value ?? "").ToString(), CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out tempTime))
            {
                return new ValidationResult(false, "Invalid date");
            }

            if (tempTime.Date <= DateTime.Now.Date)
            {
                return new ValidationResult(false, "Future date required");
            }

            return ValidationResult.ValidResult;
        }
    }
}