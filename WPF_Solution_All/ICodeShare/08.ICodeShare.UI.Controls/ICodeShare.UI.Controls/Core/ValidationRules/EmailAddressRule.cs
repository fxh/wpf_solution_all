﻿using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.ValidationRules
{
    /// <summary>
    /// 绑定数据验证规则，Email验证
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:vals="http://schemas.extended.wpf.com/validations"
    /// 2.在xaml文件中指定Binding值的Converter
    ///  <TextBox.Text>
    ///      <Binding Path="Email" Mode="TwoWay" UpdateSourceTrigger="PropertyChanged">
    ///          <Binding.ValidationRules>
    ///              <vals:EmailAddressRule/>
    ///           </Binding.ValidationRules>
    ///      </Binding>
    ///   </TextBox.Text>
    /// </example>
    public class EmailAddressRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string emailAddress = value as string;

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                string eamilAddressFormartRegex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

                // 检查输入的字符串是否符合邮箱地址格式
                if (!Regex.IsMatch(emailAddress, eamilAddressFormartRegex))
                {
                    return new ValidationResult(false, "邮箱地址格式不正确");
                }
            }
            return new ValidationResult(true, null);
        }
    }
}