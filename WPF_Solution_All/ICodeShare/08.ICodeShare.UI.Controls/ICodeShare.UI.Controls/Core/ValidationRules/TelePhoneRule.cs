﻿using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls.ValidationRules
{
    /// <summary>
    /// 绑定数据验证规则，固定电话验证
    /// </summary>
    /// <example>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:vals="http://schemas.extended.wpf.com/validations"
    /// 2.在xaml文件中指定Binding值的Converter
    ///  <TextBox.Text>
    ///      <Binding Path="TelePhone" Mode="TwoWay" UpdateSourceTrigger="PropertyChanged">
    ///          <Binding.ValidationRules>
    ///              <vals:IPAddressRule/>
    ///           </Binding.ValidationRules>
    ///      </Binding>
    ///   </TextBox.Text>
    /// </example>
    public class TelePhoneRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string telePhone = value as string;

            if (!string.IsNullOrWhiteSpace(telePhone))
            {
                string telePhoneFormartRegex = @"^(\d{3,4}-)?\d{6,8}$";

                // 检查输入的字符串是否符合固话号码格式
                if (!Regex.IsMatch(telePhone, telePhoneFormartRegex))
                {
                    return new ValidationResult(false, "固话号码格式不正确");
                }
            }
            return new ValidationResult(true, null);
        }
    }
}