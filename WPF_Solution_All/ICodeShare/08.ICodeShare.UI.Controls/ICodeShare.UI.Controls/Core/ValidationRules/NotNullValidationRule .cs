﻿using System.Windows.Controls;

namespace ICodeShare.UI.Controls.ValidationRules
{
    /// <summary>
    /// 绑定数据验证规则，非Null非空验证
    /// </summary>
    /// 1.在xmal文件引用DateConverter类所在命名空间。
    ///    xmlns:vals="http://schemas.extended.wpf.com/validations"
    /// 2.在xaml文件中指定Binding值的Converter
    /// <example>
    ///  <TextBox.Text>
    ///      <Binding Path="DataBaseIp" Mode="TwoWay" UpdateSourceTrigger="PropertyChanged">
    ///          <Binding.ValidationRules>
    ///              <vals:NotNullValidationRule/>
    ///           </Binding.ValidationRules>
    ///      </Binding>
    ///   </TextBox.Text>
    /// </example>
    public class NotNullValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(value as string) || string.IsNullOrWhiteSpace(value as string))
            {
                return new ValidationResult(false, "不能为空！");
            }
            return new ValidationResult(true, null);
        }
    }
}