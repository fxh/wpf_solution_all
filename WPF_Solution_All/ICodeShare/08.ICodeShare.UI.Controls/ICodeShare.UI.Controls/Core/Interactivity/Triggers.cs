﻿using System.Collections.Generic;
using System.Windows.Interactivity;

namespace ICodeShare.UI.Controls.Interactivity
{
    /// <summary>
    /// The triggers.
    /// </summary>
    public sealed class Triggers : List<TriggerBase>
    {
    }
}