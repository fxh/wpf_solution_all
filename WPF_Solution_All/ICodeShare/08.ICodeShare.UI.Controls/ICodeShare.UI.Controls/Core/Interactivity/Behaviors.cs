﻿using System.Collections.Generic;
using System.Windows.Interactivity;

namespace ICodeShare.UI.Controls.Interactivity
{
    /// <summary>
    /// The behaviours.
    /// </summary>
    public sealed class Behaviors : List<Behavior>
    {
    }
}