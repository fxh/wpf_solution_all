﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Core.Utilities
{
  internal class ListUtilities
  {
    internal static Type GetListItemType( Type listType )
    {
      Type iListOfT = listType.GetInterfaces().FirstOrDefault(
        ( i ) => i.IsGenericType && i.GetGenericTypeDefinition() == typeof( IList<> ) );

      return ( iListOfT != null )
        ? iListOfT.GetGenericArguments()[ 0 ]
        : null;
    }
  }
}
