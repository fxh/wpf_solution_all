﻿using System;
using System.Globalization;
using System.Linq;

namespace ICodeShare.UI.Controls.Utilities
{
    internal static class DateTimeExtensions
    {
        internal static DateTimeFormatInfo GetDateFormat(this CultureInfo culture)
        {
            if (culture == null) throw new ArgumentNullException("culture");

            if (culture.Calendar is GregorianCalendar)
            {
                return culture.DateTimeFormat;
            }

            GregorianCalendar foundCal = null;
            DateTimeFormatInfo dtfi = null;
            foreach (var cal in culture.OptionalCalendars.OfType<GregorianCalendar>())
            {
                // Return the first Gregorian calendar with CalendarType == Localized
                // Otherwise return the first Gregorian calendar
                if (foundCal == null)
                {
                    foundCal = cal;
                }

                if (cal.CalendarType != GregorianCalendarTypes.Localized) continue;

                foundCal = cal;
                break;
            }

            if (foundCal == null)
            {
                // if there are no GregorianCalendars in the OptionalCalendars list, use the invariant dtfi
                dtfi = ((CultureInfo)CultureInfo.InvariantCulture.Clone()).DateTimeFormat;
                dtfi.Calendar = new GregorianCalendar();
            }
            else
            {
                dtfi = ((CultureInfo)culture.Clone()).DateTimeFormat;
                dtfi.Calendar = foundCal;
            }

            return dtfi;
        }

        /// <summary>
        /// 阴历显示什么
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string LunarShow(DateTime dt, out bool isHoliday)
        {
            ChineseCalendar chineseCalendar = new ChineseCalendar(dt);
            isHoliday = true;
            string res = "";
            if (chineseCalendar.ChineseTwentyFourDay != "")
            {
                res = chineseCalendar.ChineseTwentyFourDay;
            }
            else if (chineseCalendar.ChineseCalendarHoliday != "")
            {
                res = chineseCalendar.ChineseCalendarHoliday;
            }
            else if (chineseCalendar.DateHoliday != "")
            {
                res = chineseCalendar.DateHoliday;
            }
            else
            {
                isHoliday = false;
                res = chineseCalendar.ChineseShortDateString;
            }
            return res;
        }

        public static DateTime GetContextNow(DateTimeKind kind)
        {
            if (kind == DateTimeKind.Unspecified)
                return DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);

            return (kind == DateTimeKind.Utc)
              ? DateTime.UtcNow
              : DateTime.Now;
        }

        public static bool IsSameDate(DateTime? date1, DateTime? date2)
        {
            if (date1 == null || date2 == null)
                return false;

            return (date1.Value.Date == date2.Value.Date);
        }
    }
}