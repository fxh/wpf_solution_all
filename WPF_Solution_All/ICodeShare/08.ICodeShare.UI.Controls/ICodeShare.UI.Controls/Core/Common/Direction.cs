﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Core.Common
{
    /// <summary>
    /// An Enum representing different directions, such as Left or Right.
    /// </summary>
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }
}
