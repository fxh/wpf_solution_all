﻿namespace ICodeShare.UI.Controls.Common
{
    public enum ShadowDepth
    {
        Depth0,
        Depth1,
        Depth2,
        Depth3,
        Depth4,
        Depth5
    }
}