﻿namespace ICodeShare.UI.Controls.Core.Common
{
    /// <summary>
    /// An Enum representing different positions, such as Left or Right.
    /// </summary>
    public enum Position
    {
        Left,
        Right,
        Top,
        Bottom
    }
}