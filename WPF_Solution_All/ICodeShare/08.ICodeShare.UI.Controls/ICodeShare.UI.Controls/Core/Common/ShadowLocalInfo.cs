﻿namespace ICodeShare.UI.Controls.Common
{
    internal class ShadowLocalInfo
    {
        public ShadowLocalInfo(double standardOpacity)
        {
            StandardOpacity = standardOpacity;
        }

        public double StandardOpacity { get; private set; }
    }
}