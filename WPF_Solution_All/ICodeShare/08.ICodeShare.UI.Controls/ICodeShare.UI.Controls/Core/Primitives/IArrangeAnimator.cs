﻿using System.Windows;

namespace ICodeShare.UI.Controls.Primitives
{
    public interface IArrangeAnimator
    {
        Rect Arrange(double elapsedTime, Point desiredPosition, Size desiredSize, Point currentPosition, Size currentSize);
    }
}
