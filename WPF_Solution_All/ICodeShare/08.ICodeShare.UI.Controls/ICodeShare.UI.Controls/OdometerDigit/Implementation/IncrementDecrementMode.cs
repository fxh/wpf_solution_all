﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// The type of update required
    /// </summary>
    public enum IncrementDecrementMode
    {
        /// <summary>
        /// Preventative enumerand for possible serialization issues is this is ever serialized
        /// </summary>
        Error = 0,

        /// <summary>
        /// Start from InitialValue and add one every Interval Seconds
        /// </summary>
        AutoIncrement,

        /// <summary>
        /// Start from InitialValue and subtract one every Interval Seconds
        /// </summary>
        AutoDecrement,

        /// <summary>
        /// Start from initial value and increment or decrement until we reach FinalValue then stop
        /// </summary>
        InitialToFinal,

        /// <summary>
        /// Initialize all digits to a staic value and leave well alone. In future we might
        /// have a initial value animation to make this less boring.
        /// </summary>
        Static
    }
}