﻿namespace ICodeShare.UI.Controls
{
    /// <summary>
    /// 计数单位枚举
    /// </summary>
    public enum CountingUnit
    {
        /// <summary>
        /// Included to show obvious serialization proplems later
        /// </summary>
        Error = 0,

        /// <summary>
        /// Increment the value by 1, the control will automatically roll over if necessary
        /// </summary>
        Units = 1,

        /// <summary>
        /// Increment the value by 10, the control will automatically roll over if necessary
        /// </summary>
        Tens = 2,

        /// <summary>
        /// Increment the value by 100, the control will automatically roll over if necessary
        /// </summary>
        Hundreds = 3,

        /// <summary>
        /// Increment the value by 1000, the control will automatically roll over if necessary
        /// </summary>
        Thousands = 4,

        /// <summary>
        /// Increment the value by 10000, the control will automatically roll over if necessary
        /// </summary>
        TensOfThousands = 5,

        /// <summary>
        /// Increment the value by 100000, the control will automatically roll over if necessary
        /// </summary>
        HundredsOfThousands = 6,

        /// <summary>
        /// Increment the value by 1000000, the control will automatically roll over if necessary
        /// </summary>
        Millions = 7,

        /// <summary>
        /// Increment the value by 10000000, the control will automatically roll over if necessary
        /// </summary>
        TensOfMillions = 8,

        /// <summary>
        /// Increment the value by 100000000, the control will automatically roll over if necessary
        /// </summary>
        HundredsOfMillions = 9,

        /// <summary>
        /// Increment the value by 10000000000, the control will automatically roll over
        /// if necessary. Pleas note: We are based on an Int32 at the moment so the tenth
        /// digit can  only be 0,1 or 2
        /// </summary>
        Billions = 10
    }
}