﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ICodeShare.UI.Controls
{
    public class CircleClock:Control
    {
        static CircleClock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CircleClock), new FrameworkPropertyMetadata(typeof(CircleClock)));
        }

        #region Properties

        #region Time 当前时间

        public DateTime Time
        {
            get { return (DateTime)GetValue(TimeProperty); }
            private set { SetValue(TimePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey TimePropertyKey = DependencyProperty.RegisterReadOnly(
                "Time",
                typeof(DateTime),
                typeof(CircleClock),
                new FrameworkPropertyMetadata(default(DateTime), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, TimePropertyChangedCallback));

        public static readonly DependencyProperty TimeProperty =TimePropertyKey.DependencyProperty;

        private static void TimePropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var clock = (CircleClock)dependencyObject;
        }

        #endregion

        #endregion
    }
}
