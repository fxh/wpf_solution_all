﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Input
{
    /// <summary>
    /// 带标题的命令，带命令参数
    /// </summary>
    /// <typeparam name="T">参数类型</typeparam>
    public sealed class CaptionCommand<T> : Command<T>, ICaption, INotifyPropertyChanged
    {
        #region Fields

        private readonly Action<T> execute;
        private readonly Func<T, bool> canExecute;

        #endregion

        #region Properties
        private string _caption;
        /// <summary>
        /// 标题
        /// </summary>
        public string Caption
        {
            get { return _caption; }
            set 
            { 
                _caption = value; 
                OnPropertyChanged("Caption");
            }
        }
        #endregion //Properties

           #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CaptionCommand{T}"/> class.
        /// </summary>
        /// <param name="caption">the command title</param>
        /// <param name="execute">The execute.</param>
        public CaptionCommand(string caption, Action<T> execute)
            : this(caption, execute, null)
        {
            Caption = caption;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CaptionCommand{T}"/> class.
        /// </summary>
        /// <param name="caption">the command title</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public CaptionCommand(string caption, Action<T> execute, Func<T, bool> canExecute)
        {
            Caption = caption;
            this.execute = execute;
            this.canExecute = canExecute;
        }

        #endregion

        #region Command Members

        /// <summary>
        /// Determines whether this instance can execute.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance can execute; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanExecute(T parameter)
        {
            if (this.canExecute != null)
            {
                return this.canExecute(parameter);
            }

            return true;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        public override void Execute(T parameter)
        {
            if (this.execute != null)
            {
                this.execute(parameter);
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
