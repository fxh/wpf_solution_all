﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Input
{
    /// <summary>
    /// 带标题命令，不带命令参数
    /// </summary>
    public sealed class CaptionCommand : Command, ICaption, INotifyPropertyChanged
    {
        #region Fields

        private readonly Action execute;
        private readonly Func<bool> canExecute;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="caption">the command title</param>
        /// <param name="execute">The execute.</param>
        public CaptionCommand(string caption, Action execute)
            : this(caption,execute, null)
        {
            Caption = caption;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="caption">the command title</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public CaptionCommand(string caption, Action execute, Func<bool> canExecute)
        {
            Caption = caption;
            this.execute = execute;
            this.canExecute = canExecute;
        }

        #endregion

        #region Properties
        private string _caption;
        /// <summary>
        /// 标题
        /// </summary>
        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                OnPropertyChanged("Caption");
            }
        }
        #endregion //Properties

        #region Command Members

        /// <summary>
        /// Determines whether this instance can execute.
        /// </summary>
        /// <returns>
        /// <c>true</c> if this instance can execute; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanExecute()
        {
            if (this.canExecute != null)
            {
                return this.canExecute();
            }

            return true;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        public override void Execute()
        {
            if (this.execute != null)
            {
                this.execute();
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
