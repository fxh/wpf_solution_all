﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Input
{
    /// <summary>
    /// 带标题的接口
    /// </summary>
    public interface ICaption
    {
        /// <summary>
        /// 标题
        /// </summary>
        string Caption { get; set; }
    }
}
