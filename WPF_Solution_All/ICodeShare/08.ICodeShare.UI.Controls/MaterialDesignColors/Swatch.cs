﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace MaterialDesignColors
{
    /// <summary>
    /// Defines a single colour swatch.
    /// </summary>
    public class Swatch
    {
        public Swatch(string name, IEnumerable<Hue> primaryHues, IEnumerable<Hue> accentHues)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (primaryHues == null) throw new ArgumentNullException("primaryHues");
            if (accentHues == null) throw new ArgumentNullException("accentHues");

            var primaryHuesList = primaryHues.ToList();            
            if (primaryHuesList.Count == 0) throw new ArgumentException("Non primary hues provided.", "primaryHues");

            Name = name;
            PrimaryHues = primaryHuesList;
            var accentHuesList = accentHues.ToList();
            AccentHues = accentHuesList;
            ExemplarHue = primaryHuesList[Math.Min(5, primaryHuesList.Count-1)];
            if (IsAccented)
                AccentExemplarHue = accentHuesList[Math.Min(2, accentHuesList.Count - 1)];
        }

        public string Name { get; private set; }

        public Hue ExemplarHue { get; private set; }

        public Hue AccentExemplarHue { get; private set; }

        public IEnumerable<Hue> PrimaryHues { get; private set; }

        public IEnumerable<Hue> AccentHues { get; private set; }

        public bool IsAccented 
        {
            get{return AccentHues.Any();}
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
