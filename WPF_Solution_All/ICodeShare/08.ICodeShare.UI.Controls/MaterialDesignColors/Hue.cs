﻿using System;
using System.Windows.Media;

namespace MaterialDesignColors
{
    public class Hue
    {
        public Hue(string name, Color color, Color foreground)
        {
            if (name == null) throw new ArgumentNullException("name");

            Name = name;
            Color = color;
            Foreground = foreground;
        }

        public string Name { get; private set; }

        public Color Color { get; private set; }

        public Color Foreground { get; private set; }

        public override string ToString()
        {
            return Name;
        }
    }
}