﻿using ICodeShare.UI.Controls.Demo.Common;
using ICodeShare.UI.Controls.Demo.ExampleViews;
using ICodeShare.UI.Controls.Demo.ExampleWindows;
using ICodeShare.UI.Controls.Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.ExampleDatas
{
    public class MenuFactory
    {
        private static MenuFactory instance;

        public static MenuFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MenuFactory();
                }
                return instance;
            }
        }

        private MenuFactory()
        {
            if (MenuList == null)
            {
                MenuList = new List<MenuInfo>();

                //#region Hightlight

                // MenuList.Add(new MenuInfo()
                //{
                //    Name = "Calendar日历控件",
                //    GroupName = ExampleCategory.Hightlight.ToString(),
                //    ViewType = typeof(CalendarView),
                //});

                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "DatePicker日期选择",
                //    GroupName = ExampleCategory.Hightlight.ToString(),
                //    ViewType = typeof(DatePickerView),
                //});

                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "TimePicker时间选择器",
                //    GroupName = ExampleCategory.Hightlight.ToString(),
                //    ViewType = typeof(TimePickerView),
                //});

                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "DateTimePicker时间选择器",
                //    GroupName = ExampleCategory.Hightlight.ToString(),
                //    ViewType = typeof(DateTimePickerView),
                //});

                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "TimeSpanUpDown时间间隔选择器",
                //    GroupName = ExampleCategory.Hightlight.ToString(),
                //    ViewType = typeof(TimeSpanUpDownView),
                //});

                //#endregion

                #region Base
                MenuList.Add(new MenuInfo()
                {
                    Name = "Theme主题",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(PaletteSelectorView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Color色彩",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(ColorView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "CMYK色轮",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(ColorCMYK),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Brush画刷",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(BrushView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Border边框",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(BorderView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Shape图形",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(CustomShapeView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Behavior行为",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(BehaviorView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Thumb滑块",
                    GroupName = ExampleCategory.Basic.ToString(),
                    ViewType = typeof(ThumbView),
                });
                #endregion

                #region Icon
                MenuList.Add(new MenuInfo()
                {
                    Name = "FastIcon图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(FastIconView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Style样式图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(StyleIconView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Pack枚举图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(PackIconView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "PackLight枚举图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(PackLightIconView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Text字符图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(TextIconView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Image图片图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(ImageIconView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "IconFont字体图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(IconFontView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "FontAwesome字体图标",
                    GroupName = ExampleCategory.Icon.ToString(),
                    ViewType = typeof(FontAwesomeView),
                });
                #endregion

                #region Form
                MenuList.Add(new MenuInfo()
                {
                    Name = "Button按钮",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ButtonView),
                });
              
                MenuList.Add(new MenuInfo()
                {
                    Name = "ToggleButton状态开关",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ToggleButtonView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "CheckBox多选框",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(CheckBoxView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Radio单选框",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(RadioButtonView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ComboBox选择器",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ComboBoxView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Calendar日历控件",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(CalendarView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "DatePicker日期选择",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(DatePickerView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Expander展开控件",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ExpanderView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "GroupBox分组",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(GroupBoxView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Panel容器",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(AnimatedPanelView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Label纯文本",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(LabelView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ItemsControl集合控件",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ItemsControlView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ListBox列表",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ListBoxView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ListView列表",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ListViewView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "DataGrid列表",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(DataGridView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Menu菜单",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(MenuView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "PasswordBox密码",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(PasswordBoxView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Progress进度条",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ProgressBarView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "RepeatButton触发按钮",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(RepeatButtonView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Slider滑块",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(SliderView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ScrollViewer滚动条",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ScrollViewerView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "TextBox输入框",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(TextBoxView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "RichTextBox富文本框",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(RichTextBoxView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "TextBlock文本",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(TextBlockView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ToolBar工具栏",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ToolBarView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "TabControl页签",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(TabControlView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ToolTip提示",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ToolTipView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Tree树形控件",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(TreeViewView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Image图片",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ImageView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Popup悬浮窗",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(PopupView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ContextMenu右键菜单",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ContextMenuView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Validate数据校验",
                    GroupName = ExampleCategory.Form.ToString(),
                    ViewType = typeof(ValidateView),
                });

                #endregion

                #region UpDown
                MenuList.Add(new MenuInfo()
                {
                    Name = "DoubleUpDown双精度选择器",
                    GroupName = ExampleCategory.UpDown.ToString(),
                    ViewType = typeof(DoubleUpDownView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "TimeSpanUpDown时间间隔选择器",
                    GroupName = ExampleCategory.UpDown.ToString(),
                    ViewType = typeof(TimeSpanUpDownView),
                });
                #endregion

                #region View

                MenuList.Add(new MenuInfo()
                {
                    Name = "Gauge表盘",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(GaugeView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Badge徽标数",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(BadgeView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Notice通知提醒",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Carousel走马灯",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(GalleryView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "MessageBox提示框",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Page分页",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Cascader级联选择",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "StepBar步骤条",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(StepView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Loading加载中",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(LoadingView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "BusyIndicator遮罩层",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(BusyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "分组面板",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "DropDown下拉",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
               
                MenuList.Add(new MenuInfo()
                {
                    Name = "Flayout悬浮面板",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Editor富文本编辑器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Rate评分",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "TimePicker时间选择器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(TimePickerView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "DateTimePicker时间选择器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(DateTimePickerView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "锚点定位",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "颜色选择器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(ColorPickerView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "定制主题",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "悬浮按钮",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Tag标签",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Timeline时间轴",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Waterfall瀑布流",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(WaterfallView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "SwitchMenu菜单",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Accordion手风琴",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(AccordionView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "音视频播放器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "内容导航",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ComboTree下拉树",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Clock时钟",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Poptip气泡提示",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "NaviationPanel导航容器",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ItemsControl动画",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(AnimationView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "AppBar菜单",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(AppBarView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Digit数码管",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(DigitLEDView),
                });

                


                MenuList.Add(new MenuInfo()
                {
                    Name = "Upload上传",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(EmptyView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ToggleSwitch开关控件",
                    GroupName = ExampleCategory.View.ToString(),
                    ViewType = typeof(ToggleSwitchView),
                });
                #endregion

                #region Custom

                MenuList.Add(new MenuInfo()
                {
                    Name = "Spinner加减",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(SpinnerView),
                });



                MenuList.Add(new MenuInfo()
                {
                    Name = "Flip滚动数字",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(FlipView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Card信息卡片",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(CardView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Radar蛛网",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(RadarView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "ColorZone颜色块",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(ColorZoneView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Shadow阴影效果",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(ShadowView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Snackbar下边弹窗",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(SnackbarView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "DrawerHost抽屉容器",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(DrawerHostView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Flyout弹出式视窗",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(FlyoutView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "DialogHost对话框",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(DialogHostView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Chip便签",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(ChipView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "RatingBar评分组件",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(RatingBarView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "Transitioner过度动画",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(TransitionerView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "GuideTour使用导航",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(GuideTourView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "SplitView分割框",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(SplitViewView),
                });
                MenuList.Add(new MenuInfo()
                {
                    Name = "ParallaxView视差",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(ParallaxViewView),
                });

                MenuList.Add(new MenuInfo()
                {
                    Name = "Window自定义窗体",
                    GroupName = ExampleCategory.Custom.ToString(),
                    ViewType = typeof(WindowView),
                });
                #endregion

                #region Window
                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "Clip不规则窗体",
                //    GroupName = ExampleCategory.Window.ToString(),
                //    ViewType = typeof(ClipWindow),
                //});
                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "Elysium自定义窗口",
                //    GroupName = ExampleCategory.Window.ToString(),
                //    ViewType = typeof(CustomWindow),
                //});
                //MenuList.Add(new MenuInfo()
                //{
                //    Name = "Extra扩展自定义窗口",
                //    GroupName = ExampleCategory.Window.ToString(),
                //    ViewType = typeof(NormalWindow),
                //});
                #endregion
            }

        }

        public List<MenuInfo> MenuList { get; private set; }
    }
}
