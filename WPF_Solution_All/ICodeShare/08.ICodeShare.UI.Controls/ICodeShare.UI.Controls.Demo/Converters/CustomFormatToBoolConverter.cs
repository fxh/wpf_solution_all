﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace ICodeShare.UI.Controls.Demo.Converters
{
  internal class CustomFormatToBoolConverter : IValueConverter
  {
    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      if( value is DateTimeFormat )
      {
        return ( DateTimeFormat )value == DateTimeFormat.Custom;
      }
      else if( value is TimeFormat )
      {
        return ( TimeFormat )value == TimeFormat.Custom;
      }
      return false;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      throw new NotImplementedException();
    }
  }
}
