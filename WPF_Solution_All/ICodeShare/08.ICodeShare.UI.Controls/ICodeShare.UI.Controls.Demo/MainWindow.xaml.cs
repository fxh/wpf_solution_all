﻿using ICodeShare.UI.Controls.Demo.ExampleViews;
using ICodeShare.UI.Controls.Demo.ExampleWindows;
using ICodeShare.UI.Controls.Demo.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICodeShare.UI.Controls.Demo.Common;
using ICodeShare.UI.Controls.Demo.ViewModels;
using System.Reflection;

namespace ICodeShare.UI.Controls.Demo
{
    /// <summar>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel _viewModel = null;
        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MainWindowViewModel();
            this.DataContext = _viewModel;
            this.Loaded += MainWindow_Loaded;

            //this.mnuNavigate.GroupItemsSource = ExampleData.Instance.MenuList;
            //this.mnuNavigate.GroupDescriptions = "GroupName";
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainWindow_Loaded;
         
        }

        //private void menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (this.mnuNavigate.SelectedItem != null)
        //    {
        //        MenuInfo info = this.mnuNavigate.SelectedItem as MenuInfo;
        //        this.ControlPanel.Content = (UserControl)info.ViewType.Assembly.CreateInstance(info.ViewType.FullName);
        //    }
        //}

        private void SearchTextBox_SearchSelection(object sender, SearchSelectionEventArgs e)
        {
            string str = e.SelectedItem.ToString().Replace("Telerik.Windows.Examples.", "");
           
        }

        private void SearchTextBox_Filter(object sender, SearchFilterEventArgs e)
        {
            IExampleInfo example = e.Item as IExampleInfo;

            string lowerSearchText = e.SearchText.ToLowerInvariant();

            if (example != null)
            {
                string exampleName = example.ExampleGroup.Control.Name.ToLowerInvariant();
                string text = example.Text.ToLowerInvariant();
                string keywords = example.Keywords.ToLowerInvariant();

                bool isExampleName = exampleName.Contains(lowerSearchText);
                bool isText = text.Contains(lowerSearchText);
                bool isKeywords = keywords.Contains(lowerSearchText);

                e.Accepted = isExampleName || isText || isKeywords;
            }
        }

        private void lstMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lstMenu = sender as ListBox;

            if (lstMenu!=null && lstMenu.SelectedItem != null)
            {
                MenuInfo info = lstMenu.SelectedItem as MenuInfo;
                if (info.GroupName != ExampleCategory.Window.ToString())
                {
                    this.ControlPanel.Content = (UserControl)info.ViewType.Assembly.CreateInstance(info.ViewType.FullName);
                }
                else
                {
                    Window win = (Window)info.ViewType.Assembly.CreateInstance(info.ViewType.FullName);
                    win.Show();  
                }
            }
        }
    }
}
