﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// AnimationView.xaml 的交互逻辑
    /// </summary>
    public partial class AnimationView : UserControl
    {
        public AnimationView()
        {
            InitializeComponent();
            this.DataContext = new AnimationViewModel();
        }

        /// <summary>
        /// The on animate in.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        private void OnAnimateIn(object sender, RoutedEventArgs e)
        {
            this.Behavior.AnimateIn();
        }

        /// <summary>
        /// The on animate out.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        private void OnAnimateOut(object sender, RoutedEventArgs e)
        {
            this.Behavior.AnimateOut();
        }

        /// <summary>
        /// The on show.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        private void OnShow(object sender, RoutedEventArgs e)
        {
            this.AnimatingItem.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// The on collapse.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        private void OnCollapse(object sender, RoutedEventArgs e)
        {
            this.FadeBehavior.FadeOut();
            this.SlideBehavior.SlideOut();
        }

        private void OnFadeOutCompleted(object sender, EventArgs e)
        {
            this.AnimatingItem.Visibility = Visibility.Collapsed;
        }
    }
}
