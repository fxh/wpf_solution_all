﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// DateTimeView.xaml 的交互逻辑
    /// </summary>
    public partial class TimePickerView : UserControl
    {
        public TimePickerView()
        {
            InitializeComponent();
            this.Loaded += TimePickerView_Loaded;
        }

        void TimePickerView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= TimePickerView_Loaded;
            this.DataContext = new TimePickerViewModel();
        }



       

        public void ClockDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {
            clclSelected.Time = ((TimePickerViewModel)DataContext).Time;
        }

        public void ClockDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (Equals(eventArgs.Parameter, "1"))
                ((TimePickerViewModel)DataContext).Time = clclSelected.Time;
        }
    }
}
