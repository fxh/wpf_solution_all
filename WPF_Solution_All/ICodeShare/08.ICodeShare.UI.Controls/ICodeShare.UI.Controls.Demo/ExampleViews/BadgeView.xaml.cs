﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// BadgeView.xaml 的交互逻辑
    /// </summary>
    public partial class BadgeView : UserControl
    {
        public BadgeView()
        {
            InitializeComponent();
        }

        private void btnChangeAdornerType_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDot_Click(object sender, RoutedEventArgs e)
        {
            BadgeType badgeType = (BadgeType)this.btnDot.GetValue(BadgeAdorner.BadgeTypeProperty);
            if (badgeType == BadgeType.Dot)
            {
                this.btnDot.SetValue(BadgeAdorner.BadgeTypeProperty, BadgeType.Normal);
            }
            else
            {
                this.btnDot.SetValue(BadgeAdorner.BadgeTypeProperty, BadgeType.Dot);
            }
        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            int number = (int)this.btnNumber.GetValue(BadgeAdorner.NumberProperty);
            if (number < 103)
            {
                this.btnNumber.SetValue(BadgeAdorner.NumberProperty, ++number);
            }
            else
            {
                this.btnNumber.SetValue(BadgeAdorner.NumberProperty, 0);
 
            }
          
        }

        private void CountingButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (CountingBadge.Badge == null || Equals(CountingBadge.Badge, ""))
                CountingBadge.Badge = 0;

            var next = int.Parse(CountingBadge.Badge.ToString()) + 1;

            CountingBadge.Badge = next < 21 ? (object)next : null;

        }

        private void btnNumber1_Click(object sender, RoutedEventArgs e)
        {
            int number = (int)this.btnNumber1.GetValue(FastBadgeAdorner.NumberProperty);
            if (number < 106)
            {
                this.btnNumber1.SetValue(FastBadgeAdorner.NumberProperty, ++number);
            }
            else
            {
                this.btnNumber1.SetValue(FastBadgeAdorner.NumberProperty, 90);

            }
        }

        private void btnDot1_Click(object sender, RoutedEventArgs e)
        {
            BadgeMode mode = (BadgeMode)this.btnDot1.GetValue(FastBadgeAdorner.ModeProperty);
            if (mode == BadgeMode.Dot)
            {
                this.btnDot1.SetValue(FastBadgeAdorner.ModeProperty, BadgeMode.LimitedNumber);
            }
            else
            {
                this.btnDot1.SetValue(FastBadgeAdorner.ModeProperty, BadgeMode.Dot);
            }
        }
    }
}
