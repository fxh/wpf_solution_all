﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICodeShare.UI.Controls.Demo.ViewModels;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// PaletteSelectorView.xaml 的交互逻辑
    /// </summary>
    public partial class PaletteSelectorView : UserControl
    {
        public PaletteSelectorView()
        {
            InitializeComponent();
            this.Loaded += PaletteSelectorView_Loaded;
        }

        void PaletteSelectorView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PaletteSelectorView_Loaded;
            this.DataContext = new PaletteSelectorViewModel();
        }
    }
}
