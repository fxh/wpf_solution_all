﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// ValidateView.xaml 的交互逻辑
    /// </summary>
    public partial class ValidateView : UserControl
    {
        public ValidateView()
        {
            InitializeComponent();
            this.Loaded += ValidateView_Loaded;
        }

        void ValidateView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ValidateView_Loaded;
            this.DataContext = new ValidateViewModel();
        }
    }
}
