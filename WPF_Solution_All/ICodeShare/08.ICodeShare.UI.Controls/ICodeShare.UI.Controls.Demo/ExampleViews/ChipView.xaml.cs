﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// ChipView.xaml 的交互逻辑
    /// </summary>
    public partial class ChipView : UserControl
    {
        public ChipView()
        {
            InitializeComponent();
        }

        private void ButtonsDemoChip_OnClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Chip clicked.");
        }

        private void ButtonsDemoChip_OnDeleteClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Chip delete clicked.");
        }
    }
}
