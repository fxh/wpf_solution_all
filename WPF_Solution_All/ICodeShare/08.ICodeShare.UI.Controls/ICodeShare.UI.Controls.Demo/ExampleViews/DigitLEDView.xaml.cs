﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// DigitLEDView.xaml 的交互逻辑
    /// </summary>
    public partial class DigitLEDView : UserControl
    {
        private List<string> Characters = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        public DigitLEDView()
        {
            InitializeComponent();
        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            Random rd = new Random();
            stLedNumber.DisplayCharacter = rd.Next(9).ToString();
        
        }

        private void btnCharacter_Click(object sender, RoutedEventArgs e)
        {
            Random rd = new Random();
            stLedCharacter.DisplayCharacter = Characters[rd.Next(26)].ToString();
        }
    }
}
