﻿using ICodeShare.UI.Controls.Demo.ExampleWindows;
using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// WindowView.xaml 的交互逻辑
    /// </summary>
    public partial class WindowView : UserControl
    {
        private readonly WindowViewModel _viewModel;
        public WindowView()
        {
            InitializeComponent();
            this.Loaded += WindowView_Loaded;
            _viewModel = new WindowViewModel();
            this.DataContext = _viewModel;
        }

        void WindowView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= WindowView_Loaded;
          
        }

        private void btnOverlayWindow_Click(object sender, RoutedEventArgs e)
        {
            OverlayWindowExample window = new OverlayWindowExample();
            window.Owner = Window.GetWindow(this);
            window.Show();
        }
    }
}
