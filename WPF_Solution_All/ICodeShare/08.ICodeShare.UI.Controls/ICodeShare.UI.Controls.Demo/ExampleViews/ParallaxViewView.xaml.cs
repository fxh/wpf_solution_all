﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// ParallaxViewView.xaml 的交互逻辑
    /// </summary>
    public partial class ParallaxViewView : UserControl
    {
        public ParallaxViewView()
        {
            InitializeComponent();
            this.Loaded += ParallaxViewView_Loaded;
        }

        void ParallaxViewView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ParallaxViewView_Loaded;
            this.DataContext = new ParallaxViewViewModel();
        }
    }
}
