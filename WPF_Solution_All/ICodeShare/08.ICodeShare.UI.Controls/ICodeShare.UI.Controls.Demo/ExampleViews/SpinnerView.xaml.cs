﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// SpinnerView.xaml 的交互逻辑
    /// </summary>
    public partial class SpinnerView : UserControl
    {
        public SpinnerView()
        {
            InitializeComponent();
        }

        private void ButtonSpinner_Spin(object sender, SpinEventArgs e)
        {
            String[] names = (String[])this.Resources["names"];

            ButtonSpinner spinner = (ButtonSpinner)sender;
            TextBox txtBox = (TextBox)spinner.Content;

            int value = Array.IndexOf(names, txtBox.Text);
            if (e.Direction == SpinDirection.Increase)
                value++;
            else
                value--;

            if (value < 0)
                value = names.Length - 1;
            else if (value >= names.Length)
                value = 0;

            txtBox.Text = names[value];
        }
    }
}
