﻿using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// ButtonView.xaml 的交互逻辑
    /// </summary>
    public partial class ButtonView : UserControl
    {
        private readonly ButtonViewModel _viewModel;

         public ButtonView()
        {
            InitializeComponent();
            _viewModel = new ButtonViewModel();
            this.DataContext = _viewModel;
        }


        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Just checking we haven't suppressed the button.");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CountingButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (CountingBadge.Badge == null || Equals(CountingBadge.Badge, ""))
                CountingBadge.Badge = 0;

            var next = int.Parse(CountingBadge.Badge.ToString()) + 1;

            CountingBadge.Badge = next < 21 ? (object)next : "1";

        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            int number = (int)this.btnNumber.GetValue(BadgeAdorner.NumberProperty);
            if (number < 103)
            {
                this.btnNumber.SetValue(BadgeAdorner.NumberProperty, ++number);
            }
            else
            {
                this.btnNumber.SetValue(BadgeAdorner.NumberProperty, 0);

            }
        }

        private void btnDot_Click(object sender, RoutedEventArgs e)
        {
            BadgeType badgeType = (BadgeType)this.btnDot.GetValue(BadgeAdorner.BadgeTypeProperty);
            if (badgeType == BadgeType.Dot)
            {
                this.btnDot.SetValue(BadgeAdorner.BadgeTypeProperty, BadgeType.Normal);
            }
            else
            {
                this.btnDot.SetValue(BadgeAdorner.BadgeTypeProperty, BadgeType.Dot);
            }
        }
    }
}
