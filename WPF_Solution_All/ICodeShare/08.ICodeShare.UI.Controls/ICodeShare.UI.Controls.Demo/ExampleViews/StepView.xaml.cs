﻿using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// FlowView.xaml 的交互逻辑
    /// </summary>
    public partial class StepView : UserControl
    {
        private readonly StepViewModel _viewModel;

        public StepView()
        {
            InitializeComponent();
            _viewModel = new StepViewModel();
            this.DataContext = _viewModel;
        }

    }
}
