﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// PackIconView.xaml 的交互逻辑
    /// </summary>
    public partial class PackIconView : UserControl
    {
        public PackIconView()
        {
            InitializeComponent();
            this.Loaded += PackIconView_Loaded;
        }

        void PackIconView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PackIconView_Loaded;
            this.DataContext = new PackIconViewModel();
        }

        private void TextBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            textBox.Dispatcher.BeginInvoke(new Action(textBox.SelectAll));
        }

        private void Search_OnKeyDown(object sender, KeyEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (e.Key == Key.Enter)
                SearchButton.Command.Execute(textBox.Text);
        }
    }
}
