﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// CalendarView.xaml 的交互逻辑
    /// </summary>
    public partial class CalendarView : UserControl
    {
        public CalendarView()
        {
            InitializeComponent();
            this.Loaded += CalendarView_Loaded;
        }

        void CalendarView_Loaded(object sender, RoutedEventArgs e)
        {
            LoadLocales();
            //cmbLanguage.SelectionChanged += LocaleCombo_SelectionChanged;
            //cmbLanguage.SelectedItem = "zh-CN";
        }

        private void LoadLocales()
        {
            foreach (var ci in CultureInfo.GetCultures(CultureTypes.AllCultures)
                .Where(ci => ci.Calendar is GregorianCalendar)
                .OrderBy(ci => ci.Name))
            {
                cmbLanguage.Items.Add(ci.Name);
            }
        }

        private void LocaleCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lang = System.Windows.Markup.XmlLanguage.GetLanguage((string)cmbLanguage.SelectedItem);


            //calFast.Language = lang;

            calMaterialDesign.Language = lang;

        }
    }
}
