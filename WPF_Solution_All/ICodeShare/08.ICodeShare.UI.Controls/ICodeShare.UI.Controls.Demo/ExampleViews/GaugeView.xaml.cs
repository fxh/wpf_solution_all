﻿using ICodeShare.UI.Controls.Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// GaugeView.xaml 的交互逻辑
    /// </summary>
    public partial class GaugeView : UserControl
    {
        DispatcherTimer timer;
        Game game1;
        Game game2;
        Game game3;
        Game game4;
        Game game5;

        public GaugeView()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(GaugeView_Loaded);
        }

        void GaugeView_Loaded(object sender, RoutedEventArgs e)
        {
            game1 = new Game(0);
            this.myGauge1.DataContext = game1;

            game2 = new Game(0);
            this.myGauge2.DataContext = game2;

            game3 = new Game(0);
            this.myGauge3.DataContext = game3;

            //game4 = new Game(0);
            //this.myGauge4.DataContext = game4;

            //game5 = new Game(0);
            //this.myGauge5.DataContext = game5;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(2500);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Random r = new Random();
            game1.Score = r.Next(0, 100);
            
            game2.Score = r.Next(0, 50);
            double val = r.Next(1, 10);
            game3.Score = val*0.1;
            //game4.Score = r.Next(0, 100);
            //game5.Score = r.Next(0, 100);
        }
    }
}
