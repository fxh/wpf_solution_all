﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// ToolTipView.xaml 的交互逻辑
    /// </summary>
    public partial class ToolTipView : UserControl
    {
        public ToolTipView()
        {
            InitializeComponent();
            this.ToolTipText = "哈哈";
            this.text.DataContext = ToolTipText;
        }

        private string _ToolTipText;

        public string ToolTipText
        {
            get { return _ToolTipText; }
            set { _ToolTipText = value; }
        }
    }
}
