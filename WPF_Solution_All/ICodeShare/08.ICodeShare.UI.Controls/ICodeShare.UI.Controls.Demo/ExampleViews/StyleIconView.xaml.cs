﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICodeShare.UI.Controls.Demo.ViewModels;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// GeometryView.xaml 的交互逻辑
    /// </summary>
    public partial class StyleIconView : UserControl
    {
        public StyleIconView()
        {
            InitializeComponent();
            this.Loaded += UserControl_Loaded;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= UserControl_Loaded;
            this.DataContext = new StyleIconViewModel();
        }
    }
}
