﻿using ICodeShare.UI.Controls.Assists;
using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// TextBoxView.xaml 的交互逻辑
    /// </summary>
    public partial class TextBoxView : UserControl
    {
        public TextBoxView()
        {
            InitializeComponent();
            this.Loaded += TextBoxView_Loaded;
        }

        void TextBoxView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= TextBoxView_Loaded;
            this.DataContext = new TextBoxViewModel();
            acbCustom.ItemFilter = (search, item) =>
            {
                Airport airport = item as Airport;
                if (airport != null)
                {
                    // Interested in: Name, City, FAA code
                    string filter = search.ToUpper(CultureInfo.InvariantCulture);
                    return (airport.CodeFaa.ToUpper(CultureInfo.InvariantCulture).Contains(filter)
                        || airport.City.ToUpper(CultureInfo.InvariantCulture).Contains(filter)
                        || airport.Name.ToUpper(CultureInfo.InvariantCulture).Contains(filter));
                }

                return false;
            };
            acbNone.ItemFilter = (search, item) =>
            {
                Airport airport = item as Airport;
                if (airport != null)
                {
                    // Interested in: Name, City, FAA code
                    string filter = search.ToUpper(CultureInfo.InvariantCulture);
                    return airport.City.ToUpper(CultureInfo.InvariantCulture).Contains(filter);
                }

                return false;
            };

            acbNowAuto.Populating += OnPopulatingSynchronous;
            acbLaterAuto.Populating += OnPopulatingAsynchronous;
        }



        /// <summary>
        /// The Populating event handler.
        /// </summary>
        /// <param name="sender">The source object.</param>
        /// <param name="e">The event data.</param>
        private void OnPopulatingSynchronous(object sender, PopulatingEventArgs e)
        {
            AutoCompleteBox source = (AutoCompleteBox)sender;

            source.ItemsSource = new string[]
            {
                e.Parameter + "1",
                e.Parameter + "2",
                e.Parameter + "3",
            };
        }

        /// <summary>
        /// The populating handler.
        /// </summary>
        /// <param name="sender">The source object.</param>
        /// <param name="e">The event data.</param>
        private void OnPopulatingAsynchronous(object sender, PopulatingEventArgs e)
        {
            AutoCompleteBox source = (AutoCompleteBox)sender;

            // Cancel the populating value: this will allow us to call 
            // PopulateComplete as necessary.
            e.Cancel = true;

            // Use the dispatcher to simulate an asynchronous callback when 
            // data becomes available
            Dispatcher.BeginInvoke(
                new Action(delegate()
                {
                    source.ItemsSource = new string[]
                    {
                        e.Parameter + "1",
                        e.Parameter + "2",
                        e.Parameter + "3",
                    };

                    // Population is complete
                    source.PopulateComplete();
                }));
        }

        private void AutoCompleteBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AutoCompleteBox acb = (AutoCompleteBox)sender;

            // In these sample scenarios, the Tag is the name of the content 
            // presenter to use to display the value.
            string contentPresenterName = (string)acb.Tag;
            ContentPresenter cp = FindName(contentPresenterName) as ContentPresenter;
            if (cp != null)
            {
                cp.Content = acb.SelectedItem;
            }
        }

        private void StudentNameChangedHandler(object sender, RoutedEventArgs e)
        {
            string str = "";
        }
    }
}
