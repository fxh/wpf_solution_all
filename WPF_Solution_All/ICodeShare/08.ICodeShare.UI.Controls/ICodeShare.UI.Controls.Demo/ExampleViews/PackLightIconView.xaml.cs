﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// PackIconLightView.xaml 的交互逻辑
    /// </summary>
    public partial class PackLightIconView : UserControl
    {
        public PackLightIconView()
        {
            InitializeComponent();
            this.Loaded += PackLightIconView_Loaded;
        }

        void PackLightIconView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PackLightIconView_Loaded;
            this.DataContext = new PackLightIconViewModel();
        }

        private void TextBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            textBox.Dispatcher.BeginInvoke(new Action(textBox.SelectAll));
        }

        private void Search_OnKeyDown(object sender, KeyEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (e.Key == Key.Enter)
                SearchButton.Command.Execute(textBox.Text);
        }
    }
}
