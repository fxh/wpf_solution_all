﻿using ICodeShare.UI.Controls.Demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// PasswordBoxView.xaml 的交互逻辑
    /// </summary>
    public partial class PasswordBoxView : UserControl
    {
        private readonly PasswordBoxViewModel _viewModel = null;
        public PasswordBoxView()
        {
            InitializeComponent();
            this.Loaded += PasswordBoxView_Loaded;
            _viewModel = new PasswordBoxViewModel();
            this.DataContext = _viewModel;
        }

        void PasswordBoxView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PasswordBoxView_Loaded;
            _viewModel.Password = "123456";
        }
    }
}
