﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// GuideTourView.xaml 的交互逻辑
    /// </summary>
    public partial class GuideTourView : UserControl
    {
        public GuideTourView()
        {
            InitializeComponent();
            CreateGuidedTour();
        }

          private void CreateGuidedTour()
        {
            guide.Items = new[]
            {
                new GuidedTourStep() {TargetElement = GuidedTour, Content = "Click to see guided tour in action", Placement = GuidePlacementMode.Top, Title = "Start guided tour" },
                new GuidedTourStep() {TargetElement = guideElement1, AlternateElements = new[] { altGuideElement1 }, Content = "Click the button to move to next guide...", Placement = GuidePlacementMode.Right , Title = "Click first item"},
                new GuidedTourStep() {TargetElement = guideElement2, Content = "Write some text to this text box to move to next guide...", Title="Text box guide", Placement = GuidePlacementMode.Left},
                new GuidedTourStep() {TargetElement = guideElement3, Content = "Click the button to move to next guide...", Title = "Guide item title", Placement = GuidePlacementMode.Right },
                new GuidedTourStep() {TargetElement = guideElement4, Content = "Click the button to complete the tour", Title = "Last element", Placement = GuidePlacementMode.Top}
            };
        }

        private void OnResetGuide(object sender, RoutedEventArgs e)
        {
            guide.Reset();
        }

        private void OnGuidedTourClosed(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Guided tour closed!");
        }

        private void OnGuidedTourFinished(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Guided tour finished!");
        }

        private void OnClosingGuidedTour(object sender, RoutedEventArgs e)
        {
            ((ClosingGuidedTourEventArgs)e).Cancel = MessageBox.Show("Are your sure you wish to dismiss this tour?", "Closing tour", MessageBoxButton.YesNoCancel) != MessageBoxResult.Yes;
        }
    }
}
