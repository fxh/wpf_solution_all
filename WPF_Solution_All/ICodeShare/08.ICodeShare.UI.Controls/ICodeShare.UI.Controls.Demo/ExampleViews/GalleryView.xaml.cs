﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleViews
{
    /// <summary>
    /// GalleryView.xaml 的交互逻辑
    /// </summary>
    public partial class GalleryView : UserControl
    {
        public GalleryView()
        {
            InitializeComponent();
            this.Loaded += GalleryView_Loaded;
        }

        void GalleryView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= GalleryView_Loaded;
            Image image1 = new Image();
            BitmapImage bitmap = new BitmapImage(new Uri("http://trinities.org/blog/wp-content/uploads/red-ball.jpg"));
            image1.Source = bitmap;
            image1.Stretch = Stretch.Fill;

            Image image2 = new Image();
            BitmapImage bitmap2 = new BitmapImage(new Uri("http://savingwithsisters.files.wordpress.com/2012/05/ball.gif"));
            image2.Source = bitmap2;
            image2.Stretch = Stretch.Fill;
            cslTwo.ItemsSource = new Image[] {image1,image2}; 
        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((Carousel)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "Cupcakes!";
                    break;
                case 1:
                    flipview.BannerText = "Xbox!";
                    break;
                case 2:
                    flipview.BannerText = "Chess!";
                    break;
            }
        }
    }
}
