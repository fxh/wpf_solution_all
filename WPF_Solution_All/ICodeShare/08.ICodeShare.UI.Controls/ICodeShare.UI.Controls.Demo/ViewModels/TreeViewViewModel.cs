﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class TreeViewViewModel : NotificationObject
    {

        #region Properties
        private ObservableCollection<double> _listPerson = new ObservableCollection<double>();
        public ObservableCollection<double> ListPerson
        {
            get 
            { 
                return _listPerson;
            }
            set 
            {
                _listPerson = value; 
                RaisePropertyChanged(()=>ListPerson); 
            }
        }
        #endregion
    }
}
