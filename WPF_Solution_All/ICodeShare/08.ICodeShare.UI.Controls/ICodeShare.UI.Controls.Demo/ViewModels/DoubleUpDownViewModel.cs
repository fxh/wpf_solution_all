﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Globalization;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class DoubleUpDownViewModel : NotificationObject
    {

        public DoubleUpDownViewModel()
        {
            this.Cultures = new ObservableCollection<CultureInfo>() { new CultureInfo( "en-US" ),
                                                new CultureInfo("en-GB"),
                                                new CultureInfo("fr-FR"),
                                                new CultureInfo("ar-DZ"),
                                                new CultureInfo("zh-CN"),
                                                new CultureInfo("cs-CZ") };
        }

        private ObservableCollection<CultureInfo> _cultures = new ObservableCollection<CultureInfo>();
        public ObservableCollection<CultureInfo> Cultures
        {
            get
            {
                return _cultures;
            }
            set
            {
                if (_cultures != value)
                {
                    _cultures = value;
                    RaisePropertyChanged("Cultures");
                }
            }
        }
    }
}
