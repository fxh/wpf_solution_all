﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class ToggleSwitchViewModel : NotificationObject
    {
         private readonly DispatcherTimer _restartEventTimer = new DispatcherTimer();
         public ToggleSwitchViewModel()
		{
            _restartEventTimer.Interval = TimeSpan.FromSeconds(1.5);
			_restartEventTimer.Tick += delegate {
                _restartEventTimer.Stop(); 
                RestartToggleChecked = false;
            };
		}

         private bool _restartToggleChecked = false;
         public bool RestartToggleChecked
         {
             get
             {
                 return _restartToggleChecked;
             }
             set
             {
                 if (_restartToggleChecked != value)
                 {
                     _restartToggleChecked = value;
                     RaisePropertyChanged("RestartToggleChecked");
                     if (_restartToggleChecked)
                     {
                         _restartEventTimer.Start();
                     }
                 }
             }
         }
    }
}
