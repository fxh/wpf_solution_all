﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using ICodeShare.UI.Controls.Demo.ExampleDialogs;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class DialogHostViewModel : NotificationObject
    {
        public DialogHostViewModel()
        {
            RunDialogCommand = new DelegateCommand<object>(ExecuteRunDialog);
            RunExtendedDialogCommand = new DelegateCommand<object>(ExecuteRunExtendedDialog);

            //Sample 4
            OpenSample4DialogCommand = new DelegateCommand<object>(OpenSample4Dialog);
            AcceptSample4DialogCommand = new DelegateCommand<object>(AcceptSample4Dialog);
            CancelSample4DialogCommand = new DelegateCommand<object>(CancelSample4Dialog);
        }

        #region SAMPLE 3

        public ICommand RunDialogCommand { get; private set; }

        public ICommand RunExtendedDialogCommand { get; private set; }

        private async void ExecuteRunDialog(object o)
        {
            //let's set up a little MVVM, cos that's what the cool kids are doing:
            var view = new SampleDialog
            {
                DataContext = new SampleDialogViewModel()
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog", ClosingEventHandler);

            //check the result...
            Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
        }

        private void ClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("You can intercept the closing event, and cancel here.");
        }

        private async void ExecuteRunExtendedDialog(object o)
        {
            //let's set up a little MVVM, cos that's what the cool kids are doing:
            var view = new SampleDialog
            {
                DataContext = new SampleDialogViewModel()
            };

            //show the dialog
            var result = await DialogHost.Show(view, "RootDialog", ExtendedOpenedEventHandler, ExtendedClosingEventHandler);

            //check the result...
            Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
        }

        private void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs)
        {
            Console.WriteLine("You could intercept the open and affect the dialog using eventArgs.Session.");
        }

        private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if ((bool)eventArgs.Parameter == false) return;

            //OK, lets cancel the close...
            eventArgs.Cancel();

            //...now, lets update the "session" with some new content!
            eventArgs.Session.UpdateContent(new SampleProgressDialog());
            //note, you can also grab the session when the dialog opens via the DialogOpenedEventHandler

            //lets run a fake operation for 3 seconds then close this baby.
            Task.Factory.StartNew(() => Thread.Sleep(3000))
                .ContinueWith(t => eventArgs.Session.Close(false), TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion

        #region SAMPLE 4

        //pretty much ignore all the stuff provided, and manage everything via custom commands and a binding for .IsOpen
        public ICommand OpenSample4DialogCommand { get; private set; }
        public ICommand AcceptSample4DialogCommand { get; private set; }
        public ICommand CancelSample4DialogCommand { get; private set; }

        private bool _isSample4DialogOpen;
        private object _sample4Content;

        public bool IsSample4DialogOpen
        {
            get { return _isSample4DialogOpen; }
            set
            {
                if (_isSample4DialogOpen == value) return;
                _isSample4DialogOpen = value;
                RaisePropertyChanged(() => IsSample4DialogOpen);
            }
        }

        public object Sample4Content
        {
            get { return _sample4Content; }
            set
            {
                if (_sample4Content == value) return;
                _sample4Content = value;
                RaisePropertyChanged(() => Sample4Content);
            }
        }

        private void OpenSample4Dialog(object obj)
        {
            Sample4Content = new Sample4Dialog();
            IsSample4DialogOpen = true;
        }

        private void CancelSample4Dialog(object obj)
        {
            IsSample4DialogOpen = false;
        }

        private void AcceptSample4Dialog(object obj)
        {
            //pretend to do something for 3 seconds, then close
            Sample4Content = new SampleProgressDialog();
            Task.Factory.StartNew(() => Thread.Sleep(3000))
                .ContinueWith(t => IsSample4DialogOpen = false,
                    TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion
    }
}
