﻿using ICodeShare.UI.Controls.Demo.ExampleDatas;
using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public sealed class ListBoxViewModel
    {
        private readonly FundCollection funds;
        private readonly AsyncDelegateCommand pinCommand;
        private readonly IList<Airport> _listOfAirports;

        public ListBoxViewModel()
        {
            this.funds = new FundCollection();
            _listOfAirports = AirportFactory.CreateAirportList();
            //this.pinCommand = new AsyncDelegateCommand(this.Pin);
        }

        public FundCollection Funds
        {
            get { return this.funds; }
        }

        public AsyncDelegateCommand PinCommand
        {
            get { return this.pinCommand; }
        }

        public IList<Airport> ListOfAirports
        {
            get { return _listOfAirports; }
        }
        //private asny  Task Pin()
        //{

        //    //await MessageDialog.ShowAsync("PinCommand", "PinCommand Fired.");
        //}
    }
}
