﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ExampleWindows;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class WindowViewModel : NotificationObject
    {
        #region Constructors
        public WindowViewModel()
		{
            FullOverlayWindowCommand = new DelegateCommand(OnFullOverlayWindow);
            OverlayWindowCommand = new DelegateCommand(OnOverlayWindow);
            NormalWindowCommand = new DelegateCommand(OnNormalWindow);
            BaseWindowCommand = new DelegateCommand(OnBaseWindow);
            ExtraWindowCommand = new DelegateCommand(OnExtraWindow);
         }
        #endregion

        #region Properties
        private bool _allowTransparency = false;
        public bool AllowTransparency
        {
            get { return _allowTransparency; }
            set
            {
                if (_allowTransparency != value)
                {
                    _allowTransparency = value;
                    RaisePropertyChanged(() => AllowTransparency);
                    if (value)
                    {
                        CanCustomWindowStyle = false;
                        CustomWindowStyle = WindowStyle.None;
                    }
                    else
                    {
                        CanCustomWindowStyle = true;
                    }
                }
            }
        }

        private bool _canCustomWindowStyle = true;
        public bool CanCustomWindowStyle
        {
            get { return _canCustomWindowStyle; }
            set
            {
                if (_canCustomWindowStyle != value)
                {
                    _canCustomWindowStyle = value;
                    RaisePropertyChanged(() => CanCustomWindowStyle);
                }
            }
        }

        private WindowStyle _customWindowStyle = WindowStyle.SingleBorderWindow;
        public WindowStyle CustomWindowStyle
        {
            get { return _customWindowStyle; }
            set
            {
                if (_customWindowStyle != value)
                {
                    _customWindowStyle = value;
                    RaisePropertyChanged(() => CustomWindowStyle);
                }
            }
        }
        #endregion

        #region Commands
        /// <summary>
        /// 全屏覆盖窗体
        /// </summary>
        public ICommand FullOverlayWindowCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 窗口覆盖
        /// </summary>
        public ICommand OverlayWindowCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 基础窗口
        /// </summary>
        public ICommand NormalWindowCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 基础窗口
        /// </summary>
        public ICommand BaseWindowCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 扩展窗口
        /// </summary>
        public ICommand ExtraWindowCommand
        {
            get;
            private set;
        }
        #endregion

        #region Methods

        public void OnFullOverlayWindow()
        {
            OverlayWindowExample window = new OverlayWindowExample();
            window.Show();
        }

        public void OnOverlayWindow()
        {
            
        }

        public void OnNormalWindow()
        {
            Window window = new Window();
            window.AllowsTransparency = AllowTransparency;
            window.WindowStyle = CustomWindowStyle;
            window.Show();
        }

        public void OnBaseWindow()
        {
            BaseWindowExample window = new BaseWindowExample();
            window.AllowsTransparency = AllowTransparency;
            window.WindowStyle = CustomWindowStyle;
            window.Show();
        }

        public void OnExtraWindow()
        {
            ExtraWindowExample window = new ExtraWindowExample();
            window.AllowsTransparency = AllowTransparency;
            window.WindowStyle = CustomWindowStyle;
            window.Show();
        }
        #endregion
    }
}
