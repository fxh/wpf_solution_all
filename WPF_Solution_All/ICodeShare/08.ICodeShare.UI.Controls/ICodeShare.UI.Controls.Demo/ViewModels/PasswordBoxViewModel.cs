﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class PasswordBoxViewModel : NotificationObject
    {
        public PasswordBoxViewModel()
		{
            EnterCommand = new DelegateCommand<object>(OnEnter);
         }

        #region Properties
        private string _password = string.Empty;
        /// <summary>
        /// 密码
        /// </summary>
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    RaisePropertyChanged(() => Password);
                }
            }
        }
        #endregion

        #region Commands
        /// <summary>
        /// 选中命令
        /// </summary>
        public ICommand EnterCommand
        {
            get;
            private set;
        }
        #endregion

        protected void OnEnter(object args)
        {
            string strArgs = args as string;
            if (strArgs != null)
            {
                MessageBox.Show(string.Format("查询中....{0}", strArgs));
            }

        }
    }
}
