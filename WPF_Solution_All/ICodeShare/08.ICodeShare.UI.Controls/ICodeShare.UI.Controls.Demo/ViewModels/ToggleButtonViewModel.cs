﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;


namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class ToggleButtonViewModel : NotificationObject
    {
        public ToggleButtonViewModel()
		{
            CheckChangedCommand = new DelegateCommand(OnCheckChanged);
            CheckedCommand = new DelegateCommand(OnChecked);
            UnCheckedCommand = new DelegateCommand(OnUnChecked);
		}


        #region Properties

      


        
        #endregion

        #region Commands
        /// <summary>
        /// 选中命令
        /// </summary>
        public ICommand CheckChangedCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 选中命令
        /// </summary>
        public ICommand CheckedCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 未选中命令
        /// </summary>
        public ICommand UnCheckedCommand
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        protected void OnCheckChanged()
        {
            MessageBox.Show("选中状态变更事件");

        }

        protected void OnChecked()
        {
            MessageBox.Show("选中事件");
        }

        protected void OnUnChecked()
        {
            MessageBox.Show("未选中事件");
        }
        #endregion
    }
}
