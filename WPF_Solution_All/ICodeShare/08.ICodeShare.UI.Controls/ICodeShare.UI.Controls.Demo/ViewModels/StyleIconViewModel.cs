﻿using ICodeShare.UI.Controls.Demo.Models;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;


namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class StyleIconViewModel : NotificationObject
    {
        private readonly List<StyleViewModel> styles;
        private readonly ICollectionView stylesView;

        private string filterText;
        private Style overlayStyle;
        private string overlayStyleName;

        public StyleIconViewModel()
        {
            ResourceDictionary resourceDictionary = new ResourceDictionary()
            {
                Source = new Uri("pack://application:,,,/ICodeShare.UI.Controls;component/Icons/Themes/GeometryIcon.xaml", UriKind.RelativeOrAbsolute)
            };

            this.styles = new List<StyleViewModel>();
            foreach (string key in resourceDictionary.Keys)
            {
                this.styles.Add(new StyleViewModel(key, (Style)resourceDictionary[key]));
            }

            this.stylesView = CollectionViewSource.GetDefaultView(this.styles);
            this.stylesView.SortDescriptions.Add(new SortDescription("Key", ListSortDirection.Ascending));
            this.stylesView.Filter =
                x =>
                {
                    StyleViewModel styleViewModel = (StyleViewModel)x;
                    return string.IsNullOrWhiteSpace(this.FilterText) ||
                        styleViewModel.Name.ToLower().Contains(this.FilterText.ToLower()) ||
                        (styleViewModel.Tags != null && styleViewModel.Tags.Any(y => y.ToLower().Contains(this.FilterText.ToLower())));
                };
        }

        public string FilterText
        {
            get
            {
                return this.filterText;
            }

            set
            {
                filterText = value;
                base.RaisePropertyChanged<string>(() => FilterText);
                this.StylesView.Refresh();
            }
        }

        public Style OverlayStyle
        {
            get { return this.overlayStyle; }
            set
            {
                overlayStyle = value;
                base.RaisePropertyChanged<Style>(() => OverlayStyle);
            }
        }

        public string OverlayStyleName
        {
            get
            {
                return this.overlayStyleName;
            }

            set
            {
                overlayStyleName = value;
                base.RaisePropertyChanged<string>(() => OverlayStyleName);
                if (!string.IsNullOrWhiteSpace(this.OverlayStyleName))
                {
                    this.OverlayStyle = (Style)Application.Current.TryFindResource(this.OverlayStyleName);
                }
            }
        }

        public ICollectionView StylesView
        {
            get { return this.stylesView; }
        }
    }
}
