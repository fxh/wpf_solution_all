﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;


namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class ValidateViewModel : NotificationObject
    {
        private string _dataBaseIp = string.Empty;
        public string DataBaseIp
        {
            get
            {
                return _dataBaseIp;
            }
            set
            {
                if (_dataBaseIp != value)
                {
                    _dataBaseIp = value;
                    RaisePropertyChanged("DataBaseIp");
                }
            }
        }

        private string _dataBaseAccount = string.Empty;
        public string DataBaseAccount
        {
            get
            {
                return _dataBaseAccount;
            }
            set
            {
                if (_dataBaseAccount != value)
                {
                    _dataBaseAccount = value;
                    RaisePropertyChanged("DataBaseAccount");
                }
            }
        }

        private string _email = string.Empty;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    RaisePropertyChanged("Email");
                }
            }
        }

        private string _mobilePhone = string.Empty;
        public string MobilePhone
        {
            get
            {
                return _mobilePhone;
            }
            set
            {
                if (_mobilePhone != value)
                {
                    _mobilePhone = value;
                    RaisePropertyChanged("MobilePhone");
                }
            }
        }

        private string _telePhone = string.Empty;
        public string TelePhone
        {
            get
            {
                return _telePhone;
            }
            set
            {
                if (_telePhone != value)
                {
                    _telePhone = value;
                    RaisePropertyChanged("TelePhone");
                }
            }
        }
    }
}
