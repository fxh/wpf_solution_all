﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ExampleDatas;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class TextBoxViewModel : NotificationObject
    {
        private readonly IList<State> _listOfStates;
        private readonly IList<Employee> _listOfEmployees;
        private readonly IList<Airport> _listOfAirports;
        public TextBoxViewModel()
		{
            _listOfStates = StateFactory.CreateStateList();
            _listOfEmployees = EmployeeFactory.CreateEmployeeList();
            _listOfAirports = AirportFactory.CreateAirportList();
            QueryCommand = new DelegateCommand<object>(OnQuery);
            NextCommand = new DelegateCommand<object>(OnNext);
         }


        #region Properties
        private string _queryText = string.Empty;
        public string QueryText
        {
            get { return _queryText; }
            set 
            {
                if (_queryText != value)
                {
                    _queryText = value;
                    RaisePropertyChanged(() => QueryText);
                }
            }
        }

        private string _nextText = string.Empty;
        public string NextText
        {
            get { return _nextText; }
            set
            {
                if (_nextText != value)
                {
                    _nextText = value;
                    RaisePropertyChanged(() => NextText);
                }
            }
        }

        public IList<State> ListOfStates
        {
            get { return _listOfStates; }
        }

        public IList<Employee> ListOfEmployees
        {
            get { return _listOfEmployees; }
        }

        public IList<Airport> ListOfAirports
        {
            get { return _listOfAirports; }
        }
        #endregion

        #region Commands
        /// <summary>
        /// 选中命令
        /// </summary>
        public ICommand QueryCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// 选中命令
        /// </summary>
        public ICommand NextCommand
        {
            get;
            private set;
        }
        #endregion

        #region Methods
        
        #endregion

        protected void OnQuery(object args)
        {
            string strArgs = args as string;
            if (strArgs != null)
            {
                MessageBox.Show(string.Format("查询中....{0}", strArgs));
            }

        }

        protected void OnNext(object args)
        {
            string strArgs = args as string;
            if (strArgs != null)
            {
                MessageBox.Show(string.Format("跳转中....{0}", strArgs));
            }

        }
    }
}
