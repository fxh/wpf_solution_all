﻿using ICodeShare.UI.Controls.Demo.ExampleDatas;
using ICodeShare.UI.Controls.Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public sealed class ParallaxViewViewModel
    {
        private readonly IList<Airport> _listOfAirports;

        public ParallaxViewViewModel()
        {
            _listOfAirports = AirportFactory.CreateAirportList();
            //this.pinCommand = new AsyncDelegateCommand(this.Pin);
        }

        public IList<Airport> ListOfAirports
        {
            get { return _listOfAirports; }
        }
    }
}
