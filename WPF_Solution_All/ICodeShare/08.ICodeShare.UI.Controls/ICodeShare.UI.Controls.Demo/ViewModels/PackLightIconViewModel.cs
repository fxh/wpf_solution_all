﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Diagnostics;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{

    public class PackLightIconViewModel : NotificationObject
    {
        private readonly Lazy<IEnumerable<PackLightIconKind>> _packIconKinds;

        public PackLightIconViewModel()
        {
            OpenDotComCommand = new DelegateCommand<object>(OpenDotCom);
            SearchCommand = new DelegateCommand<object>(Search);
            CopyToClipboardCommand = new DelegateCommand<object>(CopyToClipboard);
            _packIconKinds = new Lazy<IEnumerable<PackLightIconKind>>(() =>
                Enum.GetValues(typeof(PackLightIconKind)).OfType<PackLightIconKind>()
                    .OrderBy(k => k.ToString(), StringComparer.InvariantCultureIgnoreCase).ToList()
                );

        }

        public ICommand OpenDotComCommand { get; private set; }
        public ICommand SearchCommand { get; private set; }
        public ICommand CopyToClipboardCommand { get; private set; }

        private IEnumerable<PackLightIconKind> _kinds;
        public IEnumerable<PackLightIconKind> Kinds
        {
            get { return _kinds ?? (_kinds = _packIconKinds.Value); }
            set
            {
                _kinds = value;
                RaisePropertyChanged("Kinds");
            }
        }

        private void OpenDotCom(object obj)
        {
            Process.Start("https://materialdesignicons.com/");
        }

        private void Search(object obj)
        {
            var text = obj as string;
            if (string.IsNullOrWhiteSpace(text))
                Kinds = _packIconKinds.Value;
            else
                Kinds =
                    _packIconKinds.Value.Where(
                        x => x.ToString().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0);
        }

        private void CopyToClipboard(object obj)
        {
            var kind = (PackLightIconKind?)obj;
            Clipboard.SetText("<ctrls:PackLightIcon Kind=\"" + kind + "\" />");
        }

    }
}
