﻿using ICodeShare.UI.Controls.Demo.Models;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class DataGridViewModel : NotificationObject
    {
        private readonly ObservableCollection<SelectableModel> _items1;
        private readonly ObservableCollection<SelectableModel> _items2;
        private readonly ObservableCollection<SelectableModel> _items3;

        public DataGridViewModel()
        {
            _items1 = CreateData();
            _items2 = CreateData();
            _items3 = CreateData();
        }

        

        public ObservableCollection<SelectableModel> Items1
        {
            get { return _items1; }
        }

        public ObservableCollection<SelectableModel> Items2
        {
            get { return _items2; }
        }

        public ObservableCollection<SelectableModel> Items3
        {
            get { return _items3; }
        }


        private static ObservableCollection<SelectableModel> CreateData()
        {
            return new ObservableCollection<SelectableModel>
            {
                new SelectableModel
                {
                    Code = 'M',
                    Name = "Material Design",
                    Description = "Material Design in XAML Toolkit"
                },
                new SelectableModel
                {
                    Code = 'D',
                    Name = "Dragablz",
                    Description = "Dragablz Tab Control",
                    Food = "Fries"
                },
                new SelectableModel
                {
                    Code = 'P',
                    Name = "Predator",
                    Description = "If it bleeds, we can kill it"
                }
            };
        }

        private void SelectAll(bool select, IEnumerable<SelectableModel> models)
        {
            foreach (var model in models)
            {
                model.IsSelected = select;
            }
        }
    }
}
