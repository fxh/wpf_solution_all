﻿using ICodeShare.UI.Controls.Demo.Models;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using ICodeShare.UI.Controls.Demo.ExampleDatas;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class ButtonViewModel : NotificationObject
    {

        public ButtonViewModel()
        {
            SampleData.Seed();
            Albums = new ObservableCollection<Album>(SampleData.Albums);
            FloatingActionDemoCommand = new DelegateCommand<object>(OnFloatingActionDemo);
        }

        #region Commands
        public ICommand FloatingActionDemoCommand { get; private set; }
        #endregion

        private ObservableCollection<Album> _albums;
        public ObservableCollection<Album> Albums 
        {
            get
            {
                if (_albums == null)
                    _albums = new ObservableCollection<Album>();
                return _albums;
            }
            set
            {
                _albums = value;
                base.RaisePropertyChanged<ObservableCollection<Album>>(() => Albums);
            }
        }

        protected void OnFloatingActionDemo(object o)
        {
            Console.WriteLine("Floating action button command. - " + (o ?? "NULL").ToString());
        }
    }
}
