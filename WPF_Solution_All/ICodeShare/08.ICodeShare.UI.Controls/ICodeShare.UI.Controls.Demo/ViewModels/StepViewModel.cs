﻿using ICodeShare.UI.Controls.Demo.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{

    public class StepViewModel : NotificationObject
    {

        public StepViewModel()
        {
            ViewModelList = new ObservableCollection<StepModel>(
                        new List<StepModel>(){
                            new StepModel() {Id = 1, OffsetRate = 0, Title = "接到报修"},
                            new StepModel() {Id = 2, OffsetRate = 0.125, Title = "派工完成"},
                            new StepModel() {Id = 3, OffsetRate = 0.25, Title = "维修完成"},
                            new StepModel() {Id = 3, OffsetRate = 0.375, Title = "客户确认(我是特别长的标题)"},
                             new StepModel() {Id = 4, OffsetRate = 0.5, Title = "接到报修"},
                            new StepModel() {Id = 6, OffsetRate = 0.625, Title = "派工完成"},
                            new StepModel() {Id = 7, OffsetRate = 0.75, Title = "维修完成"},
                            new StepModel() {Id = 7, OffsetRate = 0.875, Title = "维修完成"},
                            new StepModel() {Id = 8, OffsetRate = 1, Title = "客户确认(我是特别长的标题)"},
                        });
        }


        private ObservableCollection<StepModel> _viewModelList;
        /// <summary>
        /// ViewModel对象列表
        /// </summary>
        public ObservableCollection<StepModel> ViewModelList
        {
            get
            {
                if (_viewModelList == null)
                    _viewModelList = new ObservableCollection<StepModel>();
                return _viewModelList;
            }
            set
            {
                _viewModelList = value;
                base.RaisePropertyChanged<ObservableCollection<StepModel>>(() => ViewModelList);
            }
        }

        private DelegateCommand<RoutedEventArgs> _nodeClickCommand;

        /// <summary>
        /// Gets the NodeClickCommand.
        /// </summary>
        public DelegateCommand<RoutedEventArgs> NodeClickCommand
        {
            get
            {
                return _nodeClickCommand
                    ?? (_nodeClickCommand = new DelegateCommand<RoutedEventArgs>(
                                          p =>
                                          {
                                              var aa = p;
                                              MessageBox.Show(((FlowNodeControl)aa.OriginalSource).NodeTitle);
                                          }));
            }
        }
    }
}
