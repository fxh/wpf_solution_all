﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Diagnostics;
using MaterialDesignColors;
using ICodeShare.UI.Controls.Demo.Common;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class PaletteSelectorViewModel : NotificationObject
    {
        public PaletteSelectorViewModel()
        {
            Swatches = new SwatchesProvider().Swatches;
            ToggleBaseCommand = new DelegateCommand<object>(o=>ApplyBase((bool)o));
            ApplyPrimaryCommand = new DelegateCommand<object>(o => ApplyPrimary((Swatch)o));
            ApplyAccentCommand = new DelegateCommand<object>(o => ApplyAccent((Swatch)o));
        }


        public ICommand ToggleBaseCommand { get; private set; }

        private static void ApplyBase(bool isDark)
        {
            new PaletteHelper().SetLightDark(isDark);
        }

        public IEnumerable<Swatch> Swatches { get; private set; }

        public ICommand ApplyPrimaryCommand { get; private set; }

        private static void ApplyPrimary(Swatch swatch)
        {
            new PaletteHelper().ReplacePrimaryColor(swatch);
        }

        public ICommand ApplyAccentCommand { get; private set; }

        private static void ApplyAccent(Swatch swatch)
        {
            new PaletteHelper().ReplaceAccentColor(swatch);
        }
    }
}
