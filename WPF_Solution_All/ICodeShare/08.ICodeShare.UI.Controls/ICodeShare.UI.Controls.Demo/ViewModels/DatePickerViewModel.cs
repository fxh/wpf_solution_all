﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class DatePickerViewModel : NotificationObject
    {

        private DateTime _date=DateTime.Now;
        private DateTime? _futureValidatingDate;

        public DatePickerViewModel()
        {
            Date = DateTime.Now;

        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public DateTime? FutureValidatingDate
        {
            get { return _futureValidatingDate; }
            set
            {
                _futureValidatingDate = value;
                RaisePropertyChanged(() => FutureValidatingDate);
            }
        }
    }
}
