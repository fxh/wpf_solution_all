﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class TimePickerViewModel : NotificationObject
    {
        private DateTime _time;
        private string _validatingTime;

        public TimePickerViewModel()
        {
            Time = DateTime.Now;
        }


        public DateTime Time
        {
            get { return _time; }
            set
            {
                _time = value;
                RaisePropertyChanged(() => Time);
            }
        }

        public string ValidatingTime
        {
            get { return _validatingTime; }
            set
            {
                _validatingTime = value;
                RaisePropertyChanged(() => ValidatingTime);
            }
        }

    }
}
