﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using ICodeShare.UI.Controls.Demo.Common;
using ICodeShare.UI.Controls.Demo.Models;
using ICodeShare.UI.Controls.Demo.ExampleDatas;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class MainWindowViewModel : NotificationObject
    {
        public  List<MenuInfo> MenuData
        {
            get
            {
                return MenuFactory.Instance.MenuList;
            }
        }
 
        public IQuickStartData Data
        {
            get
            {
                return QuickStartData.Instance;
            }
        }

    }
}
