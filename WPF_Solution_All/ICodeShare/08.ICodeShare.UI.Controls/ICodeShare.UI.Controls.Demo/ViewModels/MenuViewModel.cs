﻿using ICodeShare.UI.Controls.Demo.Models;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;


namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class MenuViewModel : NotificationObject
    {
        private readonly FundCollection _funds;
        public MenuViewModel()
        {
            _funds = new FundCollection();
            ViewModelList = new ObservableCollection<CircleMenuItem>(
                                               new List<CircleMenuItem>()
                        {
                            new CircleMenuItem() {Id = 1, Title = "衣"},
                            new CircleMenuItem() {Id = 2, Title = "带"},
                            new CircleMenuItem() {Id = 3, Title = "渐"},
                            new CircleMenuItem() {Id = 4, Title = "宽"},
                            new CircleMenuItem() {Id = 5, Title = "终"},
                            new CircleMenuItem() {Id = 6, Title = "不"},
                            new CircleMenuItem() {Id = 7, Title = "悔"},
                            new CircleMenuItem() {Id = 8, Title = "为"},
                            new CircleMenuItem() {Id = 9, Title = "伊"},
                            new CircleMenuItem() {Id = 10, Title = "消"},
                            new CircleMenuItem() {Id = 11, Title = "得"},
                            new CircleMenuItem() {Id = 12, Title = "人"},
                            new CircleMenuItem() {Id = 13, Title = "憔"},
                            new CircleMenuItem() {Id = 14, Title = "悴"}
                        });
        }


        private ObservableCollection<CircleMenuItem> _viewModelList;
        /// <summary>
        /// ViewModel对象列表
        /// </summary>
        public ObservableCollection<CircleMenuItem> ViewModelList
        {
            get
            {
                if (_viewModelList == null)
                    _viewModelList = new ObservableCollection<CircleMenuItem>();
                return _viewModelList;
            }
            set
            {
                _viewModelList = value;
                RaisePropertyChanged(() => ViewModelList);
            }
        }

        private DelegateCommand<RoutedEventArgs> _nodeClickCommand;

        /// <summary>
        /// Gets the NodeClickCommand.
        /// </summary>
        public DelegateCommand<RoutedEventArgs> NodeClickCommand
        {
            get
            {
                return _nodeClickCommand
                    ?? (_nodeClickCommand = new DelegateCommand<RoutedEventArgs>(
                                          p =>
                                          {
                                              var aa = p;
                                              MessageBox.Show(((FlowNodeControl)aa.OriginalSource).NodeTitle);
                                          }));
            }
        }

        public FundCollection Funds
        {
            get { return _funds; }
        }

    }
}
