﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public sealed class AnimationViewModel
    {
        public List<string> Colours
        {
            get
            {
                return new List<string>()
                {
                    "Green",
                    "Red",
                    "Blue",
                    "Black",
                    "Yellow",
                    "Brown",
                    "Purple",
                    "Orange"
                };
            }
        }
    }
}
