﻿using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using ICodeShare.UI.Controls.Demo.Models;
using System.Collections.ObjectModel;
using ICodeShare.UI.Controls.Demo.ExampleDatas;

namespace ICodeShare.UI.Controls.Demo.ViewModels
{
    public class ComboBoxViewModel : NotificationObject
    {
        private readonly IList<State> _listOfStates;
        private readonly IList<Employee> _listOfEmployees;
        private readonly IList<Airport> _listOfAirports;
        private readonly IList<int> _longListToTestComboVirtualization;
        private readonly IList<Dept> _deptList;

        public ComboBoxViewModel()
        {
            _listOfStates = StateFactory.CreateStateList();
            _listOfEmployees = EmployeeFactory.CreateEmployeeList();
            _listOfAirports = AirportFactory.CreateAirportList();
            _longListToTestComboVirtualization = new List<int>(Enumerable.Range(0, 1000));

            SelectedValueOne = LongListToTestComboVirtualization.Skip(2).First();
            SelectedTextTwo = null;
            _deptList = new List<Dept>();
            for (int i = 0; i < 10; i++)
            {
                Dept dept = new Dept();
                dept.ID = i.ToString();
                dept.Name = "第一级" + i;
                if (i % 2 == 0)
                {
                    dept.Children = new ObservableCollection<Dept>();
                    for (int j = 0; j < 5; j++)
                    {
                        Dept child = new Dept();
                        child.ID = i.ToString() + j.ToString();
                        child.Name = "第二级" + i.ToString() + j.ToString();

                        if (j % 2 == 0)
                        {
                            child.Children = new ObservableCollection<Dept>();
                            for (int k = 0; k < 2; k++)
                            {
                                Dept three = new Dept();
                                three.ID = i.ToString() + j.ToString() + k.ToString();
                                three.Name = "第二级" + i.ToString() + j.ToString() + k.ToString();
                                child.Children.Add(three);
                            }
                        }

                        dept.Children.Add(child);
                    }
                }

                _deptList.Add(dept);
            }
          }


        private int? _selectedValueOne;
        public int? SelectedValueOne
        {
            get { return _selectedValueOne; }
            set
            {
                _selectedValueOne = value;
                RaisePropertyChanged(() => SelectedValueOne);
            }
        }


        private string _selectedTextTwo;
        public string SelectedTextTwo
        {
            get { return _selectedTextTwo; }
            set
            {
                _selectedTextTwo = value;
                RaisePropertyChanged(() => SelectedTextTwo);
            }
        }

        public IList<int> LongListToTestComboVirtualization
        {
            get { return _longListToTestComboVirtualization; }
        }

        public IList<Dept> DeptList
        {
            get { return _deptList; }
        }

        public IList<State> ListOfStates
        {
            get { return _listOfStates; }
        }

        public IList<Employee> ListOfEmployees
        {
            get { return _listOfEmployees; }
        }

        public IList<Airport> ListOfAirports
        {
            get { return _listOfAirports; }
        }
    }
}
