﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Common
{
    public interface IExampleFile
    {
        Uri ExampleUri { get; }
        string DisplayName { get; }
    }
}
