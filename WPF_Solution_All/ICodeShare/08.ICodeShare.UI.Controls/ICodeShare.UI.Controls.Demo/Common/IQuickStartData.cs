﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Common
{
    public interface IQuickStartData
    {
        IEnumerable<IControlInfo> HighlightedControlsNonTouch { get; }
        IEnumerable<IControlInfo> HighlightedControlsTouch { get; }

        IEnumerable<IControlInfo> Controls { get; }
        IEnumerable<IControlInfo> ControlsNonTouch { get; }
        IEnumerable<IControlInfo> ControlsTouch { get; }

        IEnumerable<IExampleInfo> Examples { get; }
        IEnumerable<IExampleInfo> ExamplesNonTouch { get; }
        IEnumerable<IExampleInfo> ExamplesTouch { get; }
    }
}
