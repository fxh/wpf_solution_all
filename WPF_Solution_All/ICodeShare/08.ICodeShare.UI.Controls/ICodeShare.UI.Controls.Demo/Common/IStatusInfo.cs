﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Common
{

    public interface IStatusInfo
    {
        StatusMode Status { get; }
    }
}
