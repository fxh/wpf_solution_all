﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Common
{
    public enum ViewMode
    {
        Example,
        Code
    }

    public enum StatusMode
    {
        Normal,
        New,
        Beta,
        Ctp,
        Updated,
        Obsolete
    }

    public enum ExampleType
    {
        Normal,
        Theming
    }

    public enum Platform
    {
        WPF,
        Silverlight
    }

    public enum Mode
    {
        Desktop,
        Touch,
        Both
    }
}
