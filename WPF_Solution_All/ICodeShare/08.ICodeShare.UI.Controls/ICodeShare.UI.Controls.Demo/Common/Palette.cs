﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MaterialDesignColors;

namespace ICodeShare.UI.Controls.Demo.Common
{
    /// <summary>
    /// Provides full information about a palette.
    /// </summary>
    public class Palette
    {
        public Palette(Swatch primarySwatch, Swatch accentSwatch, int primaryLightHueIndex, int primaryMidHueIndex, int primaryDarkHueIndex, int accentHueIndex)
        {
            PrimarySwatch = primarySwatch;
            AccentSwatch = accentSwatch;
            PrimaryLightHueIndex = primaryLightHueIndex;
            PrimaryMidHueIndex = primaryMidHueIndex;
            PrimaryDarkHueIndex = primaryDarkHueIndex;
            AccentHueIndex = accentHueIndex;
        }

        public Swatch PrimarySwatch { get; private set; }

        public Swatch AccentSwatch { get; private set; }

        public int PrimaryLightHueIndex { get; private set; }

        public int PrimaryMidHueIndex { get; private set; }

        public int PrimaryDarkHueIndex { get; private set; }

        public int AccentHueIndex { get; private set; }
    }
}
