﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Common
{
    public enum ExampleCategory
    {
        Hightlight,
        Basic,
        Form,
        View,
        UpDown,
        Icon,
        Custom,
        Window,
    }
}
