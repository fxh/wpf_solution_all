﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class FormatObject
    {
        public string Value
        {
            get;
            set;
        }

        public string DisplayValue
        {
            get;
            set;
        }
    }
}
