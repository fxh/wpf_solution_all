﻿using ICodeShare.UI.Controls.Demo.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class HighlightInfo
    {
        private XElement element;

        public HighlightInfo(XElement element)
        {
            this.element = element;
            this.Initialize();
        }

        public Platform Platform { get; private set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }

        private void Initialize()
        {
            if (element == null)
            {
                return;
            }

            this.Text = this.element.GetAttribute("text", null);
            this.Description = this.element.GetAttribute("description", null);
            this.Image = this.element.GetAttribute("image", null);

            var platform = this.element.GetAttribute("platform", null);
            this.Platform = string.IsNullOrEmpty(platform) ? Platform.WPF : (Platform)Enum.Parse(typeof(Platform), platform, true);
        }
    }
}
