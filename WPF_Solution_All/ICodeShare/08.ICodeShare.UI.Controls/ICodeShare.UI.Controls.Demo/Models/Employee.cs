﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class Employee
    {
        /// <summary>
        /// Gets or sets the first name of the employee.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the employee.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets the display name for the employee. Uses simple concatenation 
        /// currently.
        /// </summary>
        public string DisplayName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        public Employee()
        {
        }

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        /// <param name="firstName">First name of the employee.</param>
        /// <param name="lastName">Last name of the employee.</param>
        /// <param name="resourceName">
        /// Name of a resource containing a photograph of the employee.
        /// </param>
        internal Employee(string firstName, string lastName, string resourceName)
            : this(firstName, lastName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        /// <param name="firstName">First name of the employee.</param>
        /// <param name="lastName">Last name of the employee.</param>
        internal Employee(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        /// <summary>
        /// Overrides the ToString method to return the DisplayName for the 
        /// Employee.
        /// </summary>
        /// <returns>Returns the full name of the employee.</returns>
        public override string ToString()
        {
            return DisplayName;
        }
    }

    public static class EmployeeFactory
    {
        public static IList<Employee> CreateEmployeeList()
        {
            List<Employee> employees = new List<Employee>();
            employees.Add(new Employee("Walid", "Abu-Hadba"));
            employees.Add(new Employee("J", "Allard"));
            employees.Add(new Employee("Klaus", "Holse Andersen"));
            employees.Add(new Employee("Nancy", "Anderson"));
            employees.Add(new Employee("Brian", "Arbogast"));
            employees.Add(new Employee("Orlando", "Ayala"));
            employees.Add(new Employee("Robert J.", "Bach"));
            employees.Add(new Employee("Steve", "Ballmer", "SteveBallmer.jpg"));
            employees.Add(new Employee("Martha", "Béjar"));
            employees.Add(new Employee("Joe", "Belfiore"));
            employees.Add(new Employee("Sue", "Bevington"));
            employees.Add(new Employee("Frank H.", "Brod"));
            employees.Add(new Employee("Brad", "Brooks"));
            employees.Add(new Employee("Lisa", "Brummel"));
            employees.Add(new Employee("Tom", "Burt"));
            employees.Add(new Employee("Chris", "Capossela"));
            employees.Add(new Employee("Scott", "Charney"));
            employees.Add(new Employee("Debra", "Chrapaty"));
            employees.Add(new Employee("Jean-Philippe", "Courtois"));
            employees.Add(new Employee("Alain", "Crozier"));
            employees.Add(new Employee("Kurt", "DelBene"));
            employees.Add(new Employee("Michael", "Delman"));
            employees.Add(new Employee("Joe", "DeVaan"));
            employees.Add(new Employee("Gerri", "Elliott"));
            employees.Add(new Employee("Stephen", "Elop"));
            employees.Add(new Employee("Ben", "Fathi"));
            employees.Add(new Employee("Bill", "Gates", "BillGates.jpg"));
            employees.Add(new Employee("Grant", "George"));
            employees.Add(new Employee("Tom", "Gibbons"));
            employees.Add(new Employee("L. Michael", "Golden"));
            employees.Add(new Employee("Alexander", "Gounares"));
            employees.Add(new Employee("Steve", "Guggenheimer"));
            employees.Add(new Employee("Anoop", "Gupta"));
            employees.Add(new Employee("Scott", "Guthrie", "ScottGuthrie.jpg"));
            employees.Add(new Employee("Tony", "Hey"));
            employees.Add(new Employee("Yasuyuki", "Higuchi"));
            employees.Add(new Employee("Roz", "Ho"));
            employees.Add(new Employee("Kathleen", "Hogan"));
            employees.Add(new Employee("Frank", "Holland"));
            employees.Add(new Employee("Todd", "Holmdahl"));
            employees.Add(new Employee("Darren", "Huston"));
            employees.Add(new Employee("Rajesh", "Jha"));
            employees.Add(new Employee("Chris", "Jones"));
            employees.Add(new Employee("Erik", "Jorgensen"));
            employees.Add(new Employee("Rich", "Kaplan"));
            employees.Add(new Employee("Bob", "Kelly"));
            employees.Add(new Employee("Jawad", "Khaki"));
            employees.Add(new Employee("Shane", "Kim"));
            employees.Add(new Employee("Peter", "Klein"));
            employees.Add(new Employee("Mitchell L.", "Koch"));
            employees.Add(new Employee("Ted", "Kummert"));
            employees.Add(new Employee("Julie", "Larson-Green"));
            employees.Add(new Employee("Antoine", "Leblond"));
            employees.Add(new Employee("Andrew", "Lees"));
            employees.Add(new Employee("John M.", "Lervik"));
            employees.Add(new Employee("Lewis", "Levin"));
            employees.Add(new Employee("Dan'l", "Lewin"));
            employees.Add(new Employee("Moshe", "Lichtman"));
            employees.Add(new Employee("Christopher", "Liddell"));
            employees.Add(new Employee("Steve", "Liffick"));
            employees.Add(new Employee("Brian", "MacDonald"));
            employees.Add(new Employee("Ron", "Markezich"));
            employees.Add(new Employee("Maria", "Martinez"));
            employees.Add(new Employee("Mich", "Mathews"));
            employees.Add(new Employee("Don A.", "Mattrick"));
            employees.Add(new Employee("Joe", "Matz"));
            employees.Add(new Employee("Brian", "McAndrews"));
            employees.Add(new Employee("Richard", "McAniff"));
            employees.Add(new Employee("Yusuf", "Mehdi"));
            employees.Add(new Employee("Jim", "Minervino"));
            employees.Add(new Employee("William H.", "Mitchell"));
            employees.Add(new Employee("Jens Winther", "Moberg"));
            employees.Add(new Employee("Mindy", "Mount"));
            employees.Add(new Employee("Bob", "Muglia"));
            employees.Add(new Employee("Craig", "Mundie"));
            employees.Add(new Employee("Terry", "Myerson"));
            employees.Add(new Employee("Satya", "Nadella"));
            employees.Add(new Employee("Mike", "Nash"));
            employees.Add(new Employee("Peter", "Neupert"));
            employees.Add(new Employee("Ray", "Ozzie"));
            employees.Add(new Employee("Gurdeep", "Singh Pall"));
            employees.Add(new Employee("Michael", "Park"));
            employees.Add(new Employee("Umberto", "Paolucci"));
            employees.Add(new Employee("Sanjay", "Parthasarathy"));
            employees.Add(new Employee("Pamela", "Passman"));
            employees.Add(new Employee("Alain", "Peracca"));
            employees.Add(new Employee("Todd", "Peters"));
            employees.Add(new Employee("Joe", "Peterson"));
            employees.Add(new Employee("Marshall C.", "Phelps, Jr."));
            employees.Add(new Employee("Scott", "Pitasky"));
            employees.Add(new Employee("Will", "Poole"));
            employees.Add(new Employee("Rick", "Rashid"));
            employees.Add(new Employee("Tami", "Reller"));
            employees.Add(new Employee("J.", "Ritchie"));
            employees.Add(new Employee("Enrique", "Rodriguez"));
            employees.Add(new Employee("Eduardo", "Rosini"));
            employees.Add(new Employee("Jon", "Roskill"));
            employees.Add(new Employee("Eric", "Rudder"));
            employees.Add(new Employee("John", "Schappert"));
            employees.Add(new Employee("Tony", "Scott"));
            employees.Add(new Employee("Jeanne", "Sheldon"));
            employees.Add(new Employee("Harry", "Shum"));
            employees.Add(new Employee("Steven", "Sinofsky"));
            employees.Add(new Employee("Brad", "Smith"));
            employees.Add(new Employee("Mary E.", "Snapp"));
            employees.Add(new Employee("Amitabh", "Srivastava"));
            employees.Add(new Employee("Kirill", "Tatarinov"));
            employees.Add(new Employee("Jeff", "Teper"));
            employees.Add(new Employee("David", "Thompson"));
            employees.Add(new Employee("Rick", "Thompson"));
            employees.Add(new Employee("Brian", "Tobey"));
            employees.Add(new Employee("David", "Treadwell"));
            employees.Add(new Employee("B. Kevin", "Turner"));
            employees.Add(new Employee("David", "Vaskevitch"));
            employees.Add(new Employee("Bill", "Veghte"));
            employees.Add(new Employee("Henry P.", "Vigil"));
            employees.Add(new Employee("Robert", "Wahbe"));
            employees.Add(new Employee("Todd", "Warren"));
            employees.Add(new Employee("Allison", "Watson"));
            employees.Add(new Employee("Blair", "Westlake"));
            employees.Add(new Employee("Simon", "Witts"));
            employees.Add(new Employee("Robert", "Youngjohns"));
            employees.Add(new Employee("Ya-Qin", "Zhang"));
            employees.Add(new Employee("George", "Zinn"));
            return employees.ToList();
        }
    }
}
