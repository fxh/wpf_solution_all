﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class Airport
    {
        /// <summary>
        /// Gets or sets the friendly airport name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a sometimes shorter representation of the Name property.
        /// </summary>
        public string LimitedName
        {
            get
            {
                if (Name == null || Name.Length < 30)
                {
                    return Name;
                }

                return Name.Substring(0, 30) + "...";
            }
        }

        /// <summary>
        /// Gets or sets the airport city or cities name.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state, region, or territory name.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the Federal Aviation Administration code.
        /// </summary>
        public string CodeFaa { get; set; }

        /// <summary>
        /// Gets or sets the International Air Transport Association code.
        /// </summary>
        public string CodeIata { get; set; }

        /// <summary>
        /// Gets or sets the four-digit International Civil Aviation 
        /// Organization code.
        /// </summary>
        public string CodeIcao { get; set; }

        /// <summary>
        /// Initializes a new Airport class instance.
        /// </summary>
        public Airport()
        {
        }

        /// <summary>
        /// Initializes a new Airport class instance. This is a data-entry 
        /// friendly constructor.
        /// </summary>
        /// <param name="city">The city or cities name.</param>
        /// <param name="state">The state or region.</param>
        /// <param name="faa">The Federal Aviation Administration code.</param>
        /// <param name="iata">The International Air Transport Association code.</param>
        /// <param name="icao">The four-digit International Civil Aviation
        /// Organization code.</param>
        /// <param name="airport">The friendly airport name.</param>
        public Airport(string city, string state, string faa, string iata, string icao, string airport)
        {
            City = city;
            State = state;
            CodeFaa = faa;
            CodeIata = iata;
            CodeIcao = icao;
            Name = airport;
        }

        /// <summary>
        /// The code and name together.
        /// </summary>
        /// <returns>Returns a string.</returns>
        public override string ToString()
        {
            return CodeFaa;
        }
    }

    
}
