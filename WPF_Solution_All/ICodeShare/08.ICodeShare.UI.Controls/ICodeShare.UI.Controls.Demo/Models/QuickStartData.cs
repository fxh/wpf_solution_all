﻿using ICodeShare.UI.Controls.Demo.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class QuickStartData : IQuickStartData
    {
        private XElement element;
        private static QuickStartData instance;
        private IEnumerable<IControlInfo> allControls;

        public static QuickStartData Instance
        {
            get
            {
                if (instance == null)
                {
                    XElement rootElement = DownloadExamplesXml().Root;
                    instance = new QuickStartData(rootElement);
                }

                return instance;
            }
        }

        private QuickStartData(XElement element)
        {
            this.element = element;
            this.LoadData();
        }

        public HomePageInfo HomePage { get; private set; }

        public IEnumerable<IControlInfo> ControlsNonTouch { get; private set; }

        public IEnumerable<IControlInfo> ControlsTouch { get; private set; }

        public IEnumerable<IControlInfo> Controls { get; private set; }

        public IEnumerable<IControlInfo> HighlightedControlsNonTouch { get; private set; }

        public IEnumerable<IControlInfo> HighlightedControlsTouch { get; private set; }

        public IEnumerable<IExampleInfo> Examples { get; private set; }

        public IEnumerable<IExampleInfo> ExamplesNonTouch { get; private set; }

        public IEnumerable<IExampleInfo> ExamplesTouch { get; private set; }

        private static XDocument DownloadExamplesXml()
        {
            var streamResourceInfo = System.Windows.Application.GetResourceStream(new Uri("/ICodeShare.UI.Controls.Demo;component/Examples.xml", UriKind.RelativeOrAbsolute));
            if (streamResourceInfo == null)
            {
                throw new Exception("Cannot find examples.xml");
            }

            return XDocument.Load(XmlReader.Create(streamResourceInfo.Stream));
        }

        private void LoadData()
        {
            this.LoadHomePageData();
            var controlsList = this.LoadControlsData();

            this.allControls = controlsList.OrderBy(c => c.Name).ToList();
            this.HighlightedControlsNonTouch = this.LoadHighLightedControls(controlsList, false);
            this.HighlightedControlsTouch = this.LoadHighLightedControls(controlsList, true);

            this.Controls = this.allControls;
            this.ControlsNonTouch = this.allControls.Where(c => c.NonTouchExamples.Count > 0).ToList();
            this.ControlsTouch = this.allControls.Where(c => c.TouchExamples.Count > 0).ToList();

            this.Examples = this.allControls.SelectMany(c => c.Examples).ToList();
            this.ExamplesNonTouch = this.Examples.FilterNonTouchExamples().ToList();
            this.ExamplesTouch = this.Examples.FilterTouchExamples().ToList();
        }

        private List<IControlInfo> LoadControlsData()
        {
            List<IControlInfo> controlsList = new List<IControlInfo>();

            var controls = this.element.Element("Controls");
            if (controls != null)
            {
                foreach (var item in controls.Elements("Control").Select(e => new ControlInfo(e)))
                {
                    if (item.Status != StatusMode.Obsolete)
                    {
                        controlsList.Add(item);
                    }
                }
            }

            return controlsList.Where(c => c.Platform == Platform.WPF).ToList();
        }

        private IEnumerable<IControlInfo> LoadHighLightedControls(List<IControlInfo> controlsList, bool loadTouchControls)
        {
            string highlightedControlsElementName = loadTouchControls ? "HighlightedControlsTouch" : "HighlightedControls";
            var highlightedControls = this.element.Element(highlightedControlsElementName).Elements("HighlightedControl");
            return highlightedControls.Select((node) => controlsList.FirstOrDefault((ici) => node.Attribute("name").Value == ici.Name));
        }

        private void LoadHomePageData()
        {
            var homepage = this.element.Element("HomepageInfo");
            if (homepage != null)
            {
                this.HomePage = new HomePageInfo(homepage);
            }
        }
    }
}
