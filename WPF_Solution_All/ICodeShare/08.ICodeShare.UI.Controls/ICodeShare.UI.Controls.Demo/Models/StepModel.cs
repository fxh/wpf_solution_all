﻿using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICodeShare.UI.Controls.Demo.Models
{
    public class StepModel : NotificationObject,IFlowItem
    {

        private int _id = 0;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                base.RaisePropertyChanged<int>(() => Id);
            }
        }

        private string _title =string.Empty;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                base.RaisePropertyChanged<string>(() => Title);
            }
        }

        private double _offsetRate = 0.0;
        public double OffsetRate
        {
            get
            {
                return _offsetRate;
            }
            set
            {
                _offsetRate = value;
                base.RaisePropertyChanged<double>(() => OffsetRate);
            }
        }
    }
}
