﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ICodeShare.UI.Controls.Demo.ExampleWindows
{
    /// <summary>
    /// OverlayWindowExample.xaml 的交互逻辑
    /// </summary>
    public partial class OverlayWindowExample : OverlayWindow
    {
        public OverlayWindowExample()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
