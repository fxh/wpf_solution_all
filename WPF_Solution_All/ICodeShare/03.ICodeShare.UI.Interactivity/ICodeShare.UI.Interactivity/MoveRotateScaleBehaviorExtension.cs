﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ICodeShare.UI.Interactivity
{
    public static class MoveRotateScaleBehaviorExtension
    {
        public static double GetXOffset(this TranslateRotateScaleBehavior moveRotateScaleBehavior)
        {
            Window w = Window.GetWindow(moveRotateScaleBehavior.ContextedElement);
            var p1 = moveRotateScaleBehavior.ContextedElement.TransformToAncestor(w).Transform(new Point(0, 0));
            var p2 = moveRotateScaleBehavior.SpatialContext.TransformToAncestor(w).Transform(new Point(0, 0));
            var p3 = p1 - p2;
            double returnVal = ((FrameworkElement)moveRotateScaleBehavior.ContextedElement).ActualWidth / 2 + p3.X;

            return returnVal;
        }

        public static double GetYOffset(this TranslateRotateScaleBehavior moveRotateScaleBehavior)
        {
            Window w = Window.GetWindow(moveRotateScaleBehavior.ContextedElement);
            var p1 = moveRotateScaleBehavior.ContextedElement.TransformToAncestor(w).Transform(new Point(0, 0));
            var p2 = moveRotateScaleBehavior.SpatialContext.TransformToAncestor(w).Transform(new Point(0, 0));
            var p3 = p1 - p2;

            double returnVal = ((FrameworkElement)moveRotateScaleBehavior.ContextedElement).ActualHeight / 2 + p3.Y;

            return returnVal;
        }

        public static Point GetBoundedPosition(this TranslateRotateScaleBehavior moveRotateScaleBehavior, StylusEventArgs args)
        {
            Point touchLocation = args.GetPosition(moveRotateScaleBehavior.SpatialContext);
            if (touchLocation.X > moveRotateScaleBehavior.SpatialContext.ActualWidth)
                touchLocation.X = moveRotateScaleBehavior.SpatialContext.ActualWidth;
            if (touchLocation.X < 0)
                touchLocation.X = 0;
            if (touchLocation.Y > moveRotateScaleBehavior.SpatialContext.ActualWidth)
                touchLocation.Y = moveRotateScaleBehavior.SpatialContext.ActualHeight;
            if (touchLocation.Y < 0)
                touchLocation.Y = 0;
            return touchLocation;
        }

        public static Point GetPosition(this TranslateRotateScaleBehavior moveRotateScaleBehavior, StylusEventArgs args)
        {
            Point touchLocation = args.GetPosition(moveRotateScaleBehavior.SpatialContext);
            return touchLocation;
        }

        public static void BringToFront(this TranslateRotateScaleBehavior moveRotateScaleBehavior)
        {
            var topZIndex = (from UIElement child in moveRotateScaleBehavior.SpatialContext.Children
                             orderby Canvas.GetZIndex(child) descending
                             select Canvas.GetZIndex(child)).FirstOrDefault();
            topZIndex++;
            Canvas.SetZIndex(moveRotateScaleBehavior.ContextedElement, topZIndex);
        }
    }
}
