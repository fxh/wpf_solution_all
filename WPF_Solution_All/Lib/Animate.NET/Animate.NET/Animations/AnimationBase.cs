﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Animator
{
    public abstract class AnimationBase
    {
        public bool IsStoryboardComplete;
        public FrameworkElement Element;
        protected TimeSpan AnimationLength;

        internal event Action<AnimationBase> StoryboardCompleted;
        public event Action<AnimationBase> AnimationCompleted;
        public event Action<AnimationBase> AnimationStarted;

        protected AnimationBase(FrameworkElement Element, TimeSpan AnimationLength)
        {
            this.Element = Element;
            this.AnimationLength = AnimationLength;
        }

        public static AnimationBase Create(params AnimationBase[] Animations)
        {
            return new GroupAnimation(TimeSpan.Zero, Animations);
        }

        public static AnimationBase Wait(TimeSpan WaitTime, params AnimationBase[] Animations)
        {
            return new GroupAnimation(WaitTime, Animations);
        }

        protected virtual void OnAnimationStarted()
        {
            if (AnimationStarted != null)
                AnimationStarted(this);
        }

        protected virtual void OnAnimationCompleted()
        {
            if (AnimationCompleted != null)
                AnimationCompleted(this);
        }

        protected virtual void OnStoryboardCompleted()
        {
            if (StoryboardCompleted != null)
                StoryboardCompleted(this);

            OnAnimationCompleted();
        }

        protected virtual string ResourceKey
        {
            get { return GetType().FullName; }
        }

        protected abstract Storyboard CreateStoryboard(FrameworkElement element);

        protected abstract void ApplyValues(Storyboard storyboard);

        public virtual AnimationBase Begin()
        {
            var storyboard = CreateStoryboard(Element);

            if (Element != null)
            {
                //Element.Resources.Add(ResourceKey, storyboard);

                foreach (var timeline in storyboard.Children)
                {
                    Storyboard.SetTarget(timeline, Element);
                }
            }

            ApplyValues(storyboard);

            storyboard.Completed += (sender, e) =>
                {
                    IsStoryboardComplete = true;
                    OnStoryboardCompleted();
                };
            storyboard.Begin();

            OnAnimationStarted();

            return this;
        }

        public AnimationBase WhenComplete(Action<AnimationBase> Completed)
        {
            if (IsStoryboardComplete)
                OnAnimationCompleted();
            else
                AnimationCompleted += Completed;
    
            return this;
        }

        public override string ToString()
        {
            return AnimationLength.ToString();
        }
    }
}