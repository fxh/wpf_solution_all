﻿using System.Diagnostics;

namespace Infrastructure.Guard
{
    /// <summary>
    /// Determines the validity of an argument and throws Argument exceptions if invalid.
    /// </summary>
    [DebuggerStepThrough]
    public static partial class Guard
    {
    }
}
