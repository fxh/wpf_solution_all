﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Infrastructure.ExcelUtils.UnitTests
{
    public class Common
    {
        public DataTable GetDataTable()
        {
            DataTable dt = new DataTable();
            for (int i = 1; i <= 8; i++)
            {
                if (i == 4)
                {
                    dt.Columns.Add("Col" + i.ToString(), typeof(double));
                }
                else if (i == 7)
                {
                    dt.Columns.Add("Col" + i.ToString(), typeof(DateTime));
                }
                else if (i == 8)
                {
                    dt.Columns.Add("Col" + i.ToString(), typeof(bool));
                }
                else
                {
                    dt.Columns.Add("Col" + i.ToString(), typeof(string));
                }
            }

            for (int i = 1; i <= 100; i++)
            {
                dt.Rows.Add("Name" + i.ToString(), (i % 2) > 0 ? "男" : "女", "科目" + i.ToString(), i * new Random().Next(1, 5), "待定".PadRight(10, '测'), Guid.NewGuid().ToString("N"), DateTime.Now, true);
            }

            return dt;
        }

        public List<Student> GetStudentList()
        {
            List<Student> studentList = new List<Student>();
            for (int i = 1; i <= 10; i++)
            {
                studentList.Add(new Student
                {
                    Name = "Name" + i.ToString(),
                    Sex = (i % 2) > 0 ? "男" : "女",
                    KM = "科目" + i.ToString(),
                    Score = i * new Random().Next(1, 5),
                    Result = "待定"
                });
            }
            return studentList;
        }

        public DataGridView GetDataGridViewWithData()
        {
            var grid = new DataGridView();
            var dt = GetDataTable();
            foreach (DataColumn col in dt.Columns)
            {
                bool v = col.Ordinal > 4 ? false : true;
                grid.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = col.ColumnName, HeaderText = "列名" + col.ColumnName, Visible = v, ValueType = col.DataType });
            }
            foreach (DataRow row in dt.Rows)
            {
                ArrayList values = new ArrayList();
                foreach (DataColumn col in dt.Columns)
                {
                    values.Add(row[col]);
                }
                grid.Rows.Add(values.ToArray());
            }
            return grid;
        }

        public DataSet GetDataSet()
        {
            DataTable dt1 = new DataTable("SchoolLevel");
            for (int i = 1; i <= 3; i++)
            {
                if (i == 2)
                {
                    dt1.Columns.Add("Col" + i.ToString(), typeof(int));
                }
                else
                {
                    dt1.Columns.Add("Col" + i.ToString(), typeof(string));
                }
            }

            var rnd = new Random();
            for (int i = 1; i <= 9; i++)
            {
                dt1.Rows.Add(i.ToString() + "年级", (i + rnd.Next(1, 8)) * 5, "牛" + i.ToString());
            }

            DataTable dt2 = new DataTable("ClassInfo");
            for (int i = 1; i <= 4; i++)
            {
                if (i == 3)
                {
                    dt2.Columns.Add("Col" + i.ToString(), typeof(int));
                }
                else
                {
                    dt2.Columns.Add("Col" + i.ToString(), typeof(string));
                }
            }

            string[] fNames = { "张", "李", "赵", "谢" };
            for (int i = 1; i <= 9; i++)
            {
                for (int n = 1; n <= 10; n++)
                {
                    dt2.Rows.Add(i.ToString() + "年级", string.Format("{0}-{1}班", i, n), (i + n) * 5, fNames[rnd.Next(0, 3)] + "某" + i.ToString());
                }
            }

            DataTable dt3 = GetDataTable();

            var ds = new DataSet();
            ds.Tables.AddRange(new[] { dt1, dt2, dt3 });
            return ds;
        }
    }
}