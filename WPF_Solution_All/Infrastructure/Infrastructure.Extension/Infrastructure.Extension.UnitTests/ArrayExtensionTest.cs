﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Infrastructure.Extension.UnitTests
{
    [TestClass]
    public class ArrayExtensionTest
    {
        #region Slice
        [TestMethod]
        public void Slice_StringTest()
        {
            string[] array = new string[] {"123","456","789" };
            string[] arrayResult = new string[] {"456","789" };
            Assert.AreEqual(ArrayExtensions.Slice(array, 1, 2).Length, 2);
        }
        #endregion

        #region Slice

        [TestMethod]
        public void BlockCopyTest()
        {
            string[] source = new string[15];
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = "string " + i.ToString();
            }

            string[] block = source.Slice(0, 10);

            Assert.AreEqual(10, block.Length);

            for (int i = 0; i < block.Length; i++)
            {
                Assert.AreEqual(string.Format("string {0}", i), block[i]);
            }
        }

        [TestMethod]
        public void BlockCopyWithPadding()
        {
            string[] source = new string[15];
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = "string " + i.ToString();
            }

            string[] block = source.Slice(10, 10, true);

            Assert.AreEqual(10, block.Length);

            for (int i = 0; i < block.Length; i++)
            {
                if (i < 5)
                    Assert.AreEqual(string.Format("string {0}", i + 10), block[i]);
                else
                    Assert.AreEqual(null, block[i]);
            }
        }

        [TestMethod]
        public void SliceWithoutPadding()
        {
            string[] source = new string[15];
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = "string " + i.ToString();
            }

            string[] block = source.Slice(10, 10, false);

            Assert.AreEqual(5, block.Length);

            for (int i = 0; i < block.Length; i++)
            {
                if (i < 5)
                    Assert.AreEqual(string.Format("string {0}", i + 10), block[i]);
                else // should never get here
                    Assert.AreEqual(null, block[i]);
            }
        }

        #endregion

        #region IsNullOrEmpty

        [TestMethod]
        public void IsNullOrEmpty_NullTest()
        {
            string[] array = null;

            Assert.IsTrue(ArrayExtensions.IsNullOrEmpty(array));
        }

        [TestMethod]
        public void IsNullOrEmpty_EmptyTest()
        {
            string[] array = new string[0];

            Assert.IsTrue(ArrayExtensions.IsNullOrEmpty(array));
        }

        [TestMethod]
        public void IsNullOrEmpty_NotNullOrEmptyTest()
        {
            string[] array = new string[1];
           
            Assert.IsFalse(ArrayExtensions.IsNullOrEmpty(array));
        }

        #endregion
    }
}
