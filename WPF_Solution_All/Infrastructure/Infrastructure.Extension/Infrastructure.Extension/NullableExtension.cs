﻿using System.ComponentModel;

namespace System
{
    /// <summary>
    /// 可空值类型扩展方法
    /// </summary>
    [EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class NullableExtension
    {
        /// <summary>
        /// 从可空类型中获得原始值
        /// </summary>
        /// <typeparam name="T">可空值类型的数值类型</typeparam>
        /// <param name="value">包装后的对象</param>
        /// <returns>原始 <typeparamref name="T"/> 类型的值</returns>
        public static T GetValue<T>(this System.Nullable<T> value) where T : struct
        {
            return value.HasValue ? value.Value : default(T);
        }

        /// <summary>
        /// 从可空类型中获得原始值
        /// </summary>
        /// <typeparam name="T">可空值类型的数值类型</typeparam>
        /// <param name="value">包装后的对象</param>
        /// <param name="defaultValue">默认值</param>
        /// <returns>原始 <typeparamref name="T"/> 类型的值</returns>
        public static T GetValue<T>(this System.Nullable<T> value, T defaultValue) where T : struct
        {
            return value.HasValue ? value.Value : defaultValue;
        }
    }
}