﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;

namespace System
{
    /// <summary>
    /// 对数组类型的扩展方法
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
    public static class ArrayExtensions
    {
        /// <summary>
        /// 从System.Array中的源索引开始,复制指定长度的一系列元素到一个新的System.Array
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static T[] Slice<T>(this T[] source, int startIndex, int length)
        {
            T[] slice = new T[length];
             Array.Copy(source, startIndex, slice, 0, length);
            return slice;
        }

        /// <summary>
        /// Returns a block of items from an array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <param name="padToLength"></param>
        /// <returns></returns>
        /// <remarks>
        /// Test results prove that Array.Copy is many times faster than Skip/Take and LINQ
        /// Item count: 1,000,000
        /// Array.Copy:     15 ms 
        /// Skip/Take:  42,464 ms - 42.5 seconds
        /// LINQ:          881 ms
        /// 
        /// Contributed by Chris Gessler</remarks>
        public static T[] Slice<T>(this T[] array, int index, int length, bool padToLength)
        {
            if (array == null) throw new NullReferenceException();

            int n = length;
            T[] b = null;

            if (array.Length < index + length)
            {
                n = array.Length - index;
                if (padToLength)
                {
                    b = new T[length];
                }
            }

            if (b == null) b = new T[n];
            Array.Copy(array, index, b, 0, n);
            return b;
        }

        #region Boolean 判断
        /// <summary>
        /// 获得指定数组是否为空
        /// </summary>
        /// <typeparam name="T">数组类型</typeparam>
        /// <param name="array">要检测的数组</param>
        /// <returns>如果为空或长度为零的数组，则返回true</returns>
        public static bool IsNullOrEmpty(this Array array)
        {
            return array == null || array.Length == 0;
        }

        /// <summary>
        /// 测试一个元素是否在数组中
        /// </summary>
        /// <typeparam name="T">数组类型</typeparam>
        /// <param name="array">原数组</param>
        /// <param name="value">值</param>
        /// <param name="comparer">比较器</param>
        /// <returns>是否在数组中</returns>
        /// <exception cref="System.ArgumentNullException">array</exception>
        public static bool Contains<T>(this T[] array, T value, Func<T, T, bool> comparer)
        {
            if (array == null) throw new ArgumentNullException("array");
            if (comparer == null) throw new ArgumentNullException("comparer");

            foreach (T item in array)
            {
                if (comparer(value, item)) return true;
            }

            return false;
        }

        ///<summary>
        ///	检测索引是否在数组的范围之内
        ///</summary>
        ///<param name = "source">源数组</param>
        ///<param name = "index">检查的索引</param>
        ///<param name="dimension">检查的维度</param>
        ///<returns><c>true</c> 表示有效，<c>false</c> 表示索引超过范围</returns>
        public static bool WithinIndex(this Array source, int index)
        {
            return WithinIndex(source, index, 0);
        }

        ///<summary>
        ///	检测索引是否在数组的范围之内
        ///</summary>
        ///<param name = "source">源数组</param>
        ///<param name = "index">检查的索引</param>
        ///<param name="dimension">检查的维度</param>
        ///<returns><c>true</c> 表示有效，<c>false</c> 表示索引超过范围</returns>
        public static bool WithinIndex(this Array source, int index, int dimension)
        {
            return source != null && index >= source.GetLowerBound(dimension) && index <= source.GetUpperBound(dimension);
        }

        #endregion Boolean 判断

        #region Join 拼接
        /// <summary>
        /// 使用指定的分隔符将字符串数组连接起来
        /// </summary>
        /// <param name="array">字符串数组</param>
        /// <param name="seperator">分隔符</param>
        /// <returns>参见 <see cref="T:System.String"/></returns>
        public static string Join(this string[] array, string seperator)
        {
            return string.Join(seperator, array);
        }

        #endregion //Join

        #region Combine 合并
        /// <summary>
        /// Combine two arrays into one.
        /// </summary>
        /// <typeparam name="T">Type of Array</typeparam>
        /// <param name="combineWith">Base array in which arrayToCombine will add.</param>
        /// <param name="arrayToCombine">Array to combine with Base array.</param>
        /// <returns></returns>
        /// <example>
        /// 	<code>
        /// 		int[] arrayOne = new[] { 1, 2, 3, 4 };
        /// 		int[] arrayTwo = new[] { 5, 6, 7, 8 };
        /// 		Array combinedArray = arrayOne.CombineArray<int>(arrayTwo);
        /// 	</code>
        /// </example>
        /// <remarks>
        /// 	Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/
        /// </remarks>
        public static T[] CombineArray<T>(this T[] combineWith, T[] arrayToCombine)
        {
            if (combineWith != default(T[]) && arrayToCombine != default(T[]))
            {
                int initialSize = combineWith.Length;
                Array.Resize<T>(ref combineWith, initialSize + arrayToCombine.Length);
                Array.Copy(arrayToCombine, arrayToCombine.GetLowerBound(0), combineWith, initialSize, arrayToCombine.Length);
            }
            return combineWith;
        }


        #endregion //Combine

        #region Filter 筛选

        /// <summary>
        /// 对象数组进行过滤，并返回新的数组
        /// </summary>
        /// <typeparam name="T">数组类型</typeparam>
        /// <param name="array">原数组</param>
        /// <param name="filterExpression">过滤比较</param>
        /// <returns>
        /// 过滤后的数组
        /// </returns>
        /// <exception cref="System.ArgumentNullException">array</exception>
        public static T[] Filter<T>(this T[] array, Func<int, T, bool> filterExpression)
        {
            if (array == null) throw new ArgumentNullException("array");
            if (filterExpression == null) throw new ArgumentNullException("filterExpression");

            var list = new List<T>(array.Length);
            for (int i = 0; i < array.Length; i++)
            {
                if (filterExpression(i, array[i])) list.Add(array[i]);
            }

            return list.ToArray();
        }

        #endregion Filter 筛选

        #region Each 遍历

        /// <summary>
        /// 遍历数组，并对数组执行指定操作
        /// </summary>
        /// <typeparam name="T">数组类型</typeparam>
        /// <param name="array">原数组</param>
        /// <param name="action">执行的操作</param>
        /// <exception cref="System.ArgumentNullException">array is null</exception>
        public static void ForEachWithIndex<T>(this T[] array, Action<int, T> action)
        {
            if (array == null) throw new ArgumentNullException("array");
            if (action == null) throw new ArgumentNullException("action");

            for (int i = 0; i < array.Length; i++)
            {
                action(i, array[i]);
            }
        }

        #endregion Each 遍历

        #region Convert 转换

        /// <summary>
        /// 将指定的字节数组转换为Base64格式
        /// </summary>
        /// <param name="array">要转换的字节数组</param>
        /// <returns>Base64格式的字符串</returns>
        public static string ToBase64(this byte[] array)
        {
            return Convert.ToBase64String(array);
        }

        /// <summary>
        /// 将指定的字节数组转换为Image图像对象
        /// </summary>
        /// <param name="array">要转换的字节数组</param>
        /// <returns><see cref="T:System.Drawing.Image"/></returns>
        public static Image ToImage(this byte[] array)
        {
            try
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    ms.Write(array, 0, array.Length);
                    ms.Seek(0, System.IO.SeekOrigin.Begin);

                    var img = Image.FromStream(ms);
                    ms.Close();

                    return img;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static bool ToBoolean(this byte[] buffer)
        {
            return ToBoolean(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static bool ToBoolean(this byte[] buffer, int offset)
        {
            return BitConverter.ToBoolean(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static char ToChar(this byte[] buffer)
        {
            return ToChar(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static char ToChar(this byte[] buffer, int offset)
        {
            return BitConverter.ToChar(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static double ToDouble(this byte[] buffer)
        {
            return ToDouble(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static double ToDouble(this byte[] buffer, int offset)
        {
            return BitConverter.ToDouble(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static short ToShort(this byte[] buffer)
        {
            return ToShort(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static short ToShort(this byte[] buffer, int offset)
        {
            return BitConverter.ToInt16(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static int ToInt(this byte[] buffer)
        {
            return ToInt32(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static int ToInt32(this byte[] buffer, int offset)
        {
            return BitConverter.ToInt32(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static long ToInt64(this byte[] buffer)
        {
            return ToInt64(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static long ToInt64(this byte[] buffer, int offset)
        {
            return BitConverter.ToInt64(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <returns>值</returns>
        public static float ToSingle(this byte[] buffer)
        {
            return ToSingle(buffer, 0);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static float ToSingle(this byte[] buffer, int offset)
        {
            return BitConverter.ToSingle(buffer, offset);
        }

        /// <summary>
        /// 将字节数组转换为字符串形式
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <param name="length">长度</param>
        /// <param name="seperator">分隔符</param>
        /// <returns><see cref="T:System.String"/></returns>
        public static string ToHexString(this byte[] buffer, int offset = 0, int length = 0, char seperator = '\0', bool upperCase = true)
        {
            if (seperator == '-')
                return BitConverter.ToString(buffer, offset, length);

            if (length == 0) length = buffer.Length;
            var sb = new StringBuilder(length * 2 + (seperator == '\0' ? 0 : length - 1));
            var endOffset = offset + length;
            for (int i = offset; i < endOffset; i++)
            {
                sb.Append(buffer[i].ToString(upperCase ? "X2" : "x2"));
                if (seperator != '\0' && i < endOffset - 1)
                    sb.Append(seperator);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static ushort ToUInt16(this byte[] buffer, int offset)
        {
            return BitConverter.ToUInt16(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static uint ToInt32U(this byte[] buffer, int offset)
        {
            return BitConverter.ToUInt32(buffer, offset);
        }

        /// <summary>
        /// 返回指定的字节数组中以指定位置的字节转换来的值
        /// </summary>
        /// <param name="buffer">来源字节数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>值</returns>
        public static ulong ToUInt64(this byte[] buffer, int offset)
        {
            return BitConverter.ToUInt64(buffer, offset);
        }

        #endregion Convert 转换

        #region BeConvert 转换

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this bool value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this short value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this ushort value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this char value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this long value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this ulong value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this int value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this uint value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this float value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的数值转换为字节组
        /// </summary>
        /// <param name="value">要转换的数值</param>
        /// <returns>字节数组</returns>
        public static byte[] ToBytes(this double value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this int value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this int value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 4) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 4);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this uint value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this uint value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 4) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 4);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this short value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this short value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 2) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 2);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this ushort value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this ushort value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 2) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 2);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this long value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this ulong value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 8) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 8);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this float value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this float value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 4) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 4);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this double value, byte[] buffer)
        {
            CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的值复制到指定的缓冲字节组中
        /// </summary>
        /// <param name="value">要写入的值</param>
        /// <param name="buffer">目标缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <exception cref="System.ArgumentException">目标数组为空或者目标长度不足以写入值</exception>
        public static void CopyToBuffer(this double value, byte[] buffer, int offset)
        {
            if (buffer == null || buffer.Length < offset + 8) throw new ArgumentException();
            Array.Copy(BitConverter.GetBytes(value), 0, buffer, offset, 8);
        }

        /// <summary>
        /// 将指定的字符串复制到缓冲数组中
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="buffer">缓冲数组</param>
        /// <returns>写入的长度</returns>
        /// <exception cref="System.ArgumentNullException">buffer</exception>
        /// <exception cref="System.InvalidOperationException">缓冲数组长度不足</exception>
        public static int CopyToBuffer(this string value, byte[] buffer)
        {
            return CopyToBuffer(value, buffer, 0);
        }

        /// <summary>
        /// 将指定的字符串复制到缓冲数组中
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="buffer">缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>写入的长度</returns>
        /// <exception cref="System.ArgumentNullException">buffer</exception>
        /// <exception cref="System.InvalidOperationException">缓冲数组长度不足</exception>
        public static int CopyToBuffer(this string value, byte[] buffer, int offset)
        {
            return CopyToBuffer(value, null, buffer, offset);
        }

        /// <summary>
        /// 将指定的字符串复制到缓冲数组中
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="encoding">编码，默认为Unicode</param>
        /// <param name="buffer">缓冲数组</param>
        /// <param name="offset">数据偏移</param>
        /// <returns>写入的长度</returns>
        /// <exception cref="System.ArgumentNullException">buffer</exception>
        /// <exception cref="System.InvalidOperationException">缓冲数组长度不足</exception>
        public static int CopyToBuffer(this string value, System.Text.Encoding encoding, byte[] buffer, int offset)
        {
            if (buffer == null) throw new ArgumentNullException("buffer");
            if (value.IsNullOrEmpty()) return 0;

            var bytes = (encoding ?? Encoding.Unicode).GetBytes(value);
            if (buffer.Length < offset + bytes.Length) throw new InvalidOperationException("缓冲数组长度不足");
            Array.Copy(bytes, 0, buffer, offset, buffer.Length);
            return buffer.Length;
        }

        #endregion BeConvert 转换

        #region Clear 移除控件
        /// <summary>
        /// To clear the contents of the array.
        /// 清空数组中所有内容
        /// </summary>
        /// <param name="array"> The array to clear</param>
        /// <returns>Cleared array</returns>
        /// <example>
        ///     <code>
        ///         Array array = Array.CreateInstance(typeof(string), 2);
        ///         array.SetValue("One", 0); array.SetValue("Two", 1);
        ///         Array arrayToClear = array.ClearAll();
        ///     </code>
        /// </example>
        /// <remarks>
        /// 	Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/
        /// </remarks>
        public static Array ClearAll(this Array array)
        {
            if (array != null)
                Array.Clear(array, 0, array.Length);
            return array;
        }

        /// <summary>
        /// To clear the contents of the array.
        /// </summary>
        /// <typeparam name="T">The type of array</typeparam>
        /// <param name="clear"> The array to clear</param>
        /// <returns>Cleared array</returns>
        /// <example>
        ///     <code>
        ///         int[] result = new[] { 1, 2, 3, 4 }.ClearAll<int>();
        ///     </code>
        /// </example>
        /// <remarks>
        /// 	Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/
        /// </remarks>
        public static T[] ClearAll<T>(this T[] arrayToClear)
        {
            if (arrayToClear != null)
                for (int i = arrayToClear.GetLowerBound(0); i <= arrayToClear.GetUpperBound(0); ++i)
                    arrayToClear[i] = default(T);
            return arrayToClear;
        }

        /// <summary>
        /// To clear a specific item in the array.
        /// </summary>
        /// <param name="arrayToClear">The array in where to clean the item.</param>
        /// <param name="at">Which element to clear.</param>
        /// <returns></returns>
        /// <example>
        ///     <code>
        ///         Array array = Array.CreateInstance(typeof(string), 2);
        ///         array.SetValue("One", 0); array.SetValue("Two", 1);
        ///         Array result = array.ClearAt(2);
        ///     </code>
        /// </example>
        /// <remarks>
        /// 	Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/
        /// </remarks>
        public static Array ClearAt(this Array arrayToClear, int at)
        {
            if (arrayToClear != null)
            {
                int arrayIndex = at.GetArrayIndex();
                if (arrayIndex.IsIndexInArray(arrayToClear))
                    Array.Clear(arrayToClear, arrayIndex, 1);
            }
            return arrayToClear;
        }


        /// <summary>
        /// To clear a specific item in the array.
        /// </summary>
        /// <typeparam name="T">The type of array</typeparam>
        /// <param name="arrayToClear">Array to clear.</param>
        /// <param name="at">Which element to clear.</param>
        /// <returns></returns>
        /// <example>
        ///     <code>
        ///           string[] clearString = new[] { "A" }.ClearAt<string>(0);
        ///     </code>
        /// </example>
        /// <remarks>
        /// 	Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/
        /// </remarks>
        public static T[] ClearAt<T>(this T[] arrayToClear, int at)
        {
            if (arrayToClear != null)
            {
                int arrayIndex = at.GetArrayIndex();
                if (arrayIndex.IsIndexInArray(arrayToClear))
                    arrayToClear[arrayIndex] = default(T);
            }
            return arrayToClear;
        }
        #endregion 
    }
}