﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common
{
    /// <summary>
    /// 枚举类
    /// </summary>
    [Serializable]
    public class EnumContext
    {
        public EnumContext()
        {

        }

        public EnumContext(string enumValue, int enumIndex, string description)
        {
            _enumValue = enumValue;
            _enumIndex = enumIndex;
            _description = description;
        }

        private string _description;
        private string _enumValue;
        private int _enumIndex;

        /// <summary>
        /// override ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _description;
        }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// 枚举字符串值
        /// </summary>
        public string EnumValue
        {
            get { return _enumValue; }
            set { _enumValue = value; }
        }

        /// <summary>
        /// 枚举索引
        /// </summary>
        public int EnumIndex
        {
            get { return _enumIndex; }
            set { _enumIndex = value; }
        }
    }
}
