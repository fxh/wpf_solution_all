﻿using SmartAssembly.Attributes;
using System.Reflection;

namespace System.Common
{
    /// <summary>
    /// 描述项目
    /// </summary>
    [DoNotPruneType]
    public class DescriptionGeneric<T> : Description
    {
        /// <summary>
        /// 创建 <see cref="DescriptionGeneric"></see> 的新实例
        /// </summary>
        public DescriptionGeneric(string displayName, string descriptionText, object value, FieldInfo field)
            : base(displayName, descriptionText, value, field)
        {
        }

        /// <summary>
        /// 值
        /// </summary>
        public new T Value
        {
            get
            {
                return (T)base.Value;
            }
            protected set
            {
                base.Value = value;
            }
        }

        /// <summary>
        /// 获得值的表达式形式
        /// </summary>
        /// <returns></returns>
        public override string GetValueString()
        {
            return Value.ToString();
        }
    }
}