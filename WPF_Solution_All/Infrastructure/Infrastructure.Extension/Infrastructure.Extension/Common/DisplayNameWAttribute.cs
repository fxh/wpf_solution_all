﻿using System.ComponentModel;

namespace System.Common
{
    /// <summary>
    ///
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DisplayNameWAttribute : DisplayNameAttribute
    {
        public DisplayNameWAttribute()
        {
        }

        public DisplayNameWAttribute(string displayName)
            : base(displayName)
        {
        }
    }
}