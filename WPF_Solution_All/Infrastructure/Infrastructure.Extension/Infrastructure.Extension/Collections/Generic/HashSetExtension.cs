﻿namespace System.Collections.Generic
{
    /// <summary>
    /// <see cref="HashSet{T}"/> extension methods.
    /// </summary>
    public static class HashSetExtension
    {
        /// <summary>
        /// 在HashSet中添加值，并返回是否添加成功
        /// </summary>
        /// <typeparam name="T">HashSet的类型</typeparam>
        /// <param name="hashset">目标HashSet</param>
        /// <param name="obj">要添加的值</param>
        /// <returns>true 表示添加成功；false 表示已经存在</returns>
        public static bool SafeAdd<T>(this HashSet<T> hashset, T obj)
        {
            if (hashset.Contains(obj)) return false;
            hashset.Add(obj);
            return true;
        }
    }
}