﻿using System.ComponentModel;

namespace System
{
    /// <summary>
    /// 可空值类型扩展方法
    /// </summary>
    [EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class FloatExtension
    {
        #region Rangle 范围值
        /// <summary>Checks whether the value is in range</summary>
        /// <param name="value">The Value</param>
        /// <param name="minValue">The minimum value</param>
        /// <param name="maxValue">The maximum value</param>
        public static bool InRange(this float value, float minValue, float maxValue)
        {
            return (value >= minValue && value <= maxValue);
        }

        /// <summary>Checks whether the value is in range or returns the default value</summary>
        /// <param name="value">The Value</param>
        /// <param name="minValue">The minimum value</param>
        /// <param name="maxValue">The maximum value</param>
        /// <param name="defaultValue">The default value</param>
        public static float InRange(this float value, float minValue, float maxValue, float defaultValue)
        {
            return value.InRange(minValue, maxValue) ? value : defaultValue;
        }
        #endregion //Rangle 范围值

        /// <summary>
        /// 显示为字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDisplayString(this float? value)
        {
            return value == null ? "" : value.ToString();
        }

        /// <summary>
        /// 显示为字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDisplayString(this float? value, string defaultDisplayString)
        {
            return value == null ? defaultDisplayString : value.ToString();
        }
    }
}