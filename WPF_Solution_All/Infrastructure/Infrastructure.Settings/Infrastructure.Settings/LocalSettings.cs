﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Infrastructure.Settings
{
    public static class LocalSettings
    {
        #region Members 成员变量
        /// <summary>
        /// 配置对象
        /// </summary>
        private static SettingsObject _settingsObject;
        /// <summary>
        /// 支持的语言
        /// </summary>
        private static IList<string> _supportedLanguages;
        /// <summary>
        /// 特定区域性信息
        /// </summary>
        private static CultureInfo _cultureInfo;
        /// <summary>
        /// 应用程序名称
        /// </summary>
        private const string APPNAME = "Shield";
        /// <summary>
        /// 应用程序主版本号
        /// </summary>
        private const int MAINAPPVERSION = 1;
        /// <summary>
        /// 包含本地化Resource文件的DLL名称
        /// </summary>
        private const string LOCATIONASSEMBLY = "Infrastructure.Localization.DLL";
        #endregion

        #region Construction 构造函数
        static LocalSettings()
        {
            InitFilePaths();
            LoadSettings();
        }
        #endregion

        #region Properties 属性
        #region 包含本地化Resource文件的DLL路径
        public static string LocationAssembly
        {
            get
            {
                return LOCATIONASSEMBLY;
            }
        }
        #endregion

        #region 支持的语言集合
        /// <summary>
        /// 支持的语言集合
        /// </summary>
        public static IList<string> SupportedLanguages
        {
            get
            {
                //return _supportedLanguages ?? (_supportedLanguages = new[] { 
                //    "en", "tr", "it", "pt-BR", "hr", "ar", "hu", 
                //    "es", "id", "el", "zh-CN", "de", "sq", "cs", "nl",
                //    "he", "fr", "ru-RU", "da", "fa", "tk-TM" });
                return _supportedLanguages ?? (_supportedLanguages = new[] { 
                    "en", "zh-CN", "de","fr", "zh-TW" });
            }
        }
        #endregion

        #region 当前应用程序语言
        public static string CurrentLanguage
        {
            get { return _settingsObject.CurrentLanguage; }
            set
            {
                _settingsObject.CurrentLanguage = value;
                _cultureInfo = CultureInfo.GetCultureInfo(value);
                UpdateThreadLanguage();
                SaveSettings();
            }
        }
        #endregion

        #region 是否设置当前线程的区域性
        /// <summary>
        /// 是否设置当前线程的区域性
        /// </summary>
        public static bool OverrideWindowsRegionalSettings
        {
            get { return _settingsObject.OverrideWindowsRegionalSettings; }
            set { _settingsObject.OverrideWindowsRegionalSettings = value; }
        }
        #endregion

        #region 应用程序名称
        public static string AppName
        {
            get
            {
                return APPNAME;
            }
        }
        #endregion

        #region 应用名称=应用程序名称+大版本号
        public static string FullAppName
        {
            get
            {
                return AppName+ MAINAPPVERSION;
            }
        }
        #endregion

        #region 公用文档库中应用程序配置文件夹路径
        /// <summary>
        /// 公用文档库中应用程序配置文件夹路径
        /// </summary>
        public static string DocumentPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\" + LocalSettings.FullAppName;
            }
        }
        #endregion

        #region 所有用户使用的ApplicationData文件夹下应用程序配置文件夹路径
        /// <summary>
        /// 所有用户的ApplicationData文件夹下应用程序配置文件夹路径
        /// </summary>
        /// <remarks>
        ///  C:/Documents and Settings/All Users/Application Data
        /// </remarks>
        public static string DataPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\"+ AppName + "\\" + LocalSettings.FullAppName;
            }
        }
        #endregion

        #region 当前用户使用的ApplicationData文件夹下应用程序配置文件夹路径
        /// <summary>
        /// 当前用户的ApplicationData文件夹下应用程序配置文件夹路径
        /// </summary>
        /// <remarks>
        /// C:/Documents and Settings/<用户名>/Application Data
        /// </remarks>
        public static string UserPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\"+ AppName + "\\" + LocalSettings.FullAppName;
            }
        }
        #endregion

        #region 所有用户共用的ApplicationData文件夹下应用程序配置文件路径
        /// <summary>
        /// 所有用户共用的ApplicationData文件夹下应用程序配置文件路径
        /// </summary>
        public static string CommonSettingsFileName
        {
            get
            {
                return DataPath + "\\Settings.txt";
            }
        }
        #endregion

        #region 当前用户共用的ApplicationData文件夹下应用程序配置文件路径
        /// <summary>
        /// 当前用户共用的ApplicationData文件夹下应用程序配置文件路径
        /// </summary>
        public static string UserSettingsFileName
        {
            get
            {
                return UserPath + "\\Settings.txt";
            }
        }
        #endregion

        #region 实际配置文件路径
        /// <summary>
        /// 实际配置文件路径
        /// </summary>
        public static string SettingsFileName
        {
            get
            {
                return File.Exists(UserSettingsFileName) ? UserSettingsFileName : CommonSettingsFileName;
            }
        }
        #endregion

        #region
        #endregion

        #endregion

        #region Methods 方法
        /// <summary>
        /// 一些文件路径的检查和创建
        /// </summary>
        public static void InitFilePaths()
        {
            if (!Directory.Exists(DocumentPath))
                Directory.CreateDirectory(DocumentPath);
            if (!Directory.Exists(DataPath))
                Directory.CreateDirectory(DataPath);
            if (!Directory.Exists(UserPath))
                Directory.CreateDirectory(UserPath);
        }

        /// <summary>
        /// 从配置文件中读取配置信息
        /// </summary>
        public static void LoadSettings()
        {
            _settingsObject = new SettingsObject();
            string fileName = SettingsFileName;
            if (File.Exists(fileName))
            {
                var serializer = new XmlSerializer(_settingsObject.GetType());
                var reader = new XmlTextReader(fileName);
                try
                {
                    _settingsObject = serializer.Deserialize(reader) as SettingsObject;
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// 把配置信息保存到配置文件中
        /// </summary>
        public static void SaveSettings()
        {
            try
            {
                var serializer = new XmlSerializer(_settingsObject.GetType());
                var writer = new XmlTextWriter(SettingsFileName, null);
                try
                {
                    serializer.Serialize(writer, _settingsObject);
                }
                finally
                {
                    writer.Close();
                }
            }
            catch (UnauthorizedAccessException)
            {
                if (!File.Exists(UserSettingsFileName))
                {
                    File.Create(UserSettingsFileName).Close();
                    SaveSettings();
                }
            }
            catch (IOException)
            {
                return;
            }
        }

        /// <summary>
        /// 更新当前线程区域性信息
        /// </summary>
        public static void UpdateThreadLanguage()
        {
            if (_cultureInfo != null)
            {
                if (OverrideWindowsRegionalSettings)
                    Thread.CurrentThread.CurrentCulture = _cultureInfo;
                Thread.CurrentThread.CurrentUICulture = _cultureInfo;
            }
        }
        #endregion

    }
}
