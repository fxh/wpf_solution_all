﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace Infrastructure.Settings
{
    public class SettingsObject
    {
        #region 当前语言
        public string CurrentLanguage { get; set; }
        #endregion

        #region 是否设置当前线程的区域性
        public bool OverrideWindowsRegionalSettings { get; set; }
        #endregion
    }
}
