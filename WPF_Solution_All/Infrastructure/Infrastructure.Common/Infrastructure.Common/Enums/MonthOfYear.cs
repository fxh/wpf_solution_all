﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Common.Enums
{
    /// <summary>
    /// 月份枚举 
    /// </summary>
    public enum MonthOfYear
    {
        // 摘要: 
        //     表示一月。
        January=1,
        // 摘要: 
        //     表示二月。
        February=2,
        // 摘要: 
        //     表示三月。
        March=3,
        // 摘要: 
        //     表示四月。
        April=4,
        // 摘要: 
        //     表示五月。
        May=5,
        // 摘要: 
        //     表示六月。
        June=6,
        // 摘要: 
        //     表示七月。
        July=7,
        // 摘要: 
        //     表示八月。
        August=8,
        // 摘要: 
        //     表九月。
        September=9,
        // 摘要: 
        //     表示十月。
        October=10,
        // 摘要: 
        //     表示十一月。
        November=11,
        // 摘要: 
        //     表示十二月。
        December=12
    }
}
