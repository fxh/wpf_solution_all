﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Common.Enums
{
    /// <summary>
    /// 二十四节气
    /// </summary>
    public enum SolarTerms
    {
        // 摘要: 
        //     表示立春。
        SpringBegins = 01,
        // 摘要: 
        //     表示雨水。
        TheRains = 11,
        // 摘要: 
        //     表示惊蛰。
        InsectsAwaken = 23,
        // 摘要: 
        //     表示春分。
        VernalEquinox = 3,
        // 摘要: 
        //     表示清明。
        ClearAndBright = 4,
        // 摘要: 
        //     表示谷雨。
        GrainRain = 5,
        // 摘要: 
        //     表示立夏。
        SummerBegins = 6,
        // 摘要: 
        //     表示小满。
        GrainBuds = 7,
        // 摘要: 
        //     表示芒种。
        GrainInEar = 8,
        // 摘要: 
        //     表示夏至。
        SummerSolstice = 9,
        // 摘要: 
        //     表示小暑。
        SlightHeat = 10,
        // 摘要: 
        //     表示大暑。
        GreatHeat = 11,
        // 摘要: 
        //     表示立秋。
        AutumnBegins = 12,
        // 摘要: 
        //     表示处暑。
        StoppingTheHeat = 13,
        // 摘要: 
        //     表示一月。
        WhiteDews = 14,//"白露",
        // 摘要: 
        //     表示一月。
        AutumnEquinox = 15,//"秋分",
        // 摘要: 
        //     表示一月。
        ColdDews = 16,//"寒露",
        // 摘要: 
        //     表示一月。
        HoarFrostFalls = 17,// "霜降",
        // 摘要: 
        //     表示一月。
        WinterBegins = 18,//"立冬",
        // 摘要: 
        //     表示一月。
        LightSnow = 19,//"小雪",
        // 摘要: 
        //     表示一月。
        HeavySnow = 20,//"大雪",
        // 摘要: 
        //     表示一月。
        WinterSolstice = 21,//"冬至",
        // 摘要: 
        //     表示一月。
        SlightSold = 22,//"小寒",
        // 摘要: 
        //     表示一月。
         GreatCold = 23,//"大寒"
    }
}
