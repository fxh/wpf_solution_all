﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utils
{
    public interface IAssemblyLoader
    {
        Action<double> ProgressChangedCallback { get; set; }
        Action<object> CompletedCallback { get; set; }
        bool CanLoadAssembly(string name);
        void LoadAssembly(string name);
    }
}
