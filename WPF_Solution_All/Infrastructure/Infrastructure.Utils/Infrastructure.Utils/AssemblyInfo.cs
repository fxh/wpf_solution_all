﻿
public static class AssemblyInfo
{
    public const string VersionShort = "14.2";
    public const string Version = "14.2.9.0";
    public const string VSuffixWithoutSeparator = "v" + VersionShort;
    public const string VSuffix = "." + VSuffixWithoutSeparator;
    public const string ThemePrefixWithoutSeparator = "Themes";
    public const string ThemePrefix = "." + ThemePrefixWithoutSeparator + ".";
    public const string SRAssemblyToolkitPrefix = "DevExpress.Xpf";

    public const string SRAssemblyXpfMvvm = "DevExpress.Mvvm" + VSuffix;
    public const string SRAssemblyData = "DevExpress.Data" + VSuffix;
    public const string SRAssemblyDemoDataCore = "DevExpress.DemoData" + VSuffix + ".Core";
    public const string SRAssemblyXpfPrefix = "DevExpress.Xpf";
    public const string SRAssemblyXpfCore = "DevExpress.Xpf.Core.v14.2";
}
