using Infrastructure.Utils.Helpers;
using System;
using System.Diagnostics;

namespace Infrastructure.Utils.Extensions
{
	public static class EventHandlerExtensions
	{
		[DebuggerHidden]
		public static void Raise(this EventHandler handler, object sender)
		{
			EventHelper.Raise(handler, sender);
		}
		[DebuggerHidden]
		public static void Raise<T>(this EventHandler<T> handler, object sender, T e) where T : EventArgs
		{
			EventHelper.Raise<T>(handler, sender, e);
		}
		[DebuggerHidden]
		public static void Raise<T>(this EventHandler<T> handler, object sender, EventHelper.CreateEventArguments<T> createEventArguments) where T : EventArgs
		{
			EventHelper.Raise<T>(handler, sender, createEventArguments);
		}
		[DebuggerHidden, Obsolete("This API will be removed in a future version of The Helper Trinity.")]
		public static void Raise(this Delegate handler, object sender, EventArgs e)
		{
			EventHelper.Raise(handler, sender, e);
		}
		[DebuggerHidden]
		public static void BeginRaise(this EventHandler handler, object sender, AsyncCallback callback, object asyncState)
		{
			EventHelper.BeginRaise(handler, sender, callback, asyncState);
		}
		[DebuggerHidden]
		public static void BeginRaise<T>(this EventHandler<T> handler, object sender, T e, AsyncCallback callback, object asyncState) where T : EventArgs
		{
			EventHelper.BeginRaise<T>(handler, sender, e, callback, asyncState);
		}
		[DebuggerHidden]
		public static void BeginRaise<T>(this EventHandler<T> handler, object sender, EventHelper.CreateEventArguments<T> createEventArguments, AsyncCallback callback, object asyncState) where T : EventArgs
		{
			EventHelper.BeginRaise<T>(handler, sender, createEventArguments, callback, asyncState);
		}
		[DebuggerHidden, Obsolete("This API will be removed in a future version of The Helper Trinity.")]
		public static void BeginRaise(this Delegate handler, object sender, EventArgs e, AsyncCallback callback, object asyncState)
		{
			EventHelper.BeginRaise(handler, sender, e, callback, asyncState);
		}
	}
}
