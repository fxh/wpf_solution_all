﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Utils.Helpers
{
    public static class StringHelper
    {
        public static string CombineUri(params string[] parts)
        {
            if (parts.Length == 0) return string.Empty;
            string ret = parts[0];
            for (int i = 1; i < parts.Length; ++i)
            {
                ret += "/" + parts[i];
            }
            ret = ret.Replace('\\', '/');
            while (ret.Contains("//"))
                ret = ret.Replace("//", "/");
            if (ret.Length > 1 && ret[ret.Length - 1] == '/') ret = ret.Remove(ret.Length - 1);
            if ((parts[0].Length == 0 || parts[0][0] != '/') && ret.Length != 0 && ret[0] == '/') ret = ret.Substring(1);
            return ret;
        }
    }
}
