﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Infrastructure.Utils.Helpers
{
    /// <summary>
    /// ExecutingAssembly 当前执行代码程序集帮助类
    /// </summary>
    public class ExecutingAssemblyHelper
    {
        #region 构造函数
        Assembly assembly = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ExecutingAssemblyHelper()
        {
            assembly = Assembly.GetExecutingAssembly();

        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="path">dll,exe路径</param>
        public ExecutingAssemblyHelper(string path)
        {
            if (File.Exists(path))
                assembly = Assembly.LoadFile(path);
        }

        #endregion
        #region 私有方法
        private void GetAssemblyCommon<T>(Action<T> assemblyFacotry) where T : Attribute
        {
            if (assembly != null)
            {
                object[] _attributes = assembly.GetCustomAttributes(typeof(T), false);
                if (_attributes.Length > 0)
                {
                    T _attribute = (T)_attributes[0];
                    assemblyFacotry(_attribute);
                }
            }
        }
        #endregion
        #region 获取应用程序集的标题
        /// <summary>
        /// 获取应用程序集的标题
        /// </summary>
        /// <returns>程序集的标题</returns>
        public string GetTitle()
        {
            string _title = string.Empty;
            GetAssemblyCommon<AssemblyTitleAttribute>(_ass => _title = _ass.Title);
            if (string.IsNullOrEmpty(_title))
            {
                _title = Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
            return _title;
        }
        #endregion
        #region 获取应用程序产品名称
        /// <summary>
        /// 获取应用程序产品名称
        /// </summary>
        /// <returns>产品名称</returns>
        public string GetProductName()
        {
            string _product = string.Empty;
            GetAssemblyCommon<AssemblyProductAttribute>(_ass => _product = _ass.Product);
            return _product;
        }
        #endregion
        #region 获取应用程序版本
        /// <summary>
        /// 获取应用程序版本
        /// </summary>
        /// <returns>版本</returns>
        public string GetVersion()
        {
            return assembly.GetName().Version.ToString();
        }
        #endregion
        #region 获取应用程序说明
        /// <summary>
        /// 获取应用程序说明
        /// </summary>
        /// <returns>说明</returns>
        public string GetDescription()
        {
            string _description = string.Empty;
            GetAssemblyCommon<AssemblyDescriptionAttribute>(_ass => _description = _ass.Description);
            return _description;
        }
        #endregion
        #region 获取应用程序版权信息
        /// <summary>
        /// 获取应用程序版权信息
        /// </summary>
        /// <returns>版权信息</returns>
        public string GetCopyright()
        {
            string _copyright = string.Empty;
            GetAssemblyCommon<AssemblyCopyrightAttribute>(_ass => _copyright = _ass.Copyright);
            return _copyright;
        }
        #endregion
        #region 获取应用程序公司名称
        /// <summary>
        /// 获取应用程序公司名称
        /// </summary>
        /// <returns>公司名称</returns>
        public string GetCompany()
        {
            string _company = string.Empty;
            GetAssemblyCommon<AssemblyCompanyAttribute>(_ass => _company = _ass.Company);
            return _company;
        }
        #endregion
        #region 获取应用程序显示名称
        /// <summary>
        /// 获取应用程序显示名称
        /// </summary>
        /// <returns>应用程序显示名称</returns>
        public string GetAppFullName()
        {
            return assembly.FullName.ToString();
        }
        #endregion
    }
}
