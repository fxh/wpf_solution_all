﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace Infrastructure.Utils.Helpers
{
    /// <summary>
    /// JavaScriptSerializer 帮助类
    /// </summary>
    public static class JaveScriptSerializerHelper
    {
        #region 处理Json字符串的时间格式问题
        /// <summary>
        /// 处理JsonString的时间格式问题
        /// <para>eg:ScriptSerializerHelper.ParseJsonTime(@"[{'getTime':'\/Date(1419564257428)\/'}]", "yyyyMMdd hh:mm:ss");==>[{'getTime':'20141226 11:24:17'}]</para>
        /// <para>参考：http://www.cnphp6.com/archives/35773 </para>
        /// </summary>
        /// <param name="jsonString">Json字符串</param>
        /// <param name="formart">时间格式化类型</param>
        /// <returns>处理好的Json字符串</returns>
        public static string ParseJsonTime(this string jsonString, string formart)
        {
            if (!string.IsNullOrEmpty(jsonString))
            {
                jsonString = Regex.Replace(jsonString, @"\\/Date\((\d+)\)\\/", match =>
                {
                    DateTime _dateTime = new DateTime(1970, 1, 1);
                    _dateTime = _dateTime.AddMilliseconds(long.Parse(match.Groups[1].Value));
                    _dateTime = _dateTime.ToLocalTime();
                    return _dateTime.ToString(formart);
                });
            }
            return jsonString;
        }
        /// <summary>
        /// 处理JsonString的时间格式问题【时间格式：yyyy-MM-dd HH:mm:ss】
        /// <para>参考：http://www.cnphp6.com/archives/35773 </para>
        /// </summary>
        /// <param name="jsonString">Json字符串</param>
        /// <returns>处理好的Json字符串</returns>
        public static string ParseJsonTime(this string jsonString)
        {
            return ParseJsonTime(jsonString, "yyyy-MM-dd HH:mm:ss");
        }
        #endregion
        #region 利用JavaScriptSerializer将对象序列化成JSON
        /// <summary>
        /// 利用JavaScriptSerializer将对象序列化成JSON字符串
        /// <para>eg:ScriptSerializerHelper.Serialize<Person>(_personList);</para>
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="entityList">对象集合</param>
        /// <returns>json</returns>
        public static string Serialize<T>(this IEnumerable<T> entityList) where T : class
        {
            string _jsonString = string.Empty;
            if (entityList != null)
            {
                JavaScriptSerializer _serializerHelper = new JavaScriptSerializer();
                _serializerHelper.MaxJsonLength = int.MaxValue;
                _jsonString = _serializerHelper.Serialize(entityList);
            }
            return _jsonString;
        }
        #endregion
        #region 利用JavaScriptSerializer将json字符串反序列化
        /// <summary>
        ///利用JavaScriptSerializer将json字符串反序列化
        /// <para>eg:List<Person> _result = (List<Person>)ScriptSerializerHelper.Deserialize<Person>(_jsonString);</para>
        /// </summary>
        /// <typeparam name="T">泛型</typeparam>
        /// <param name="jsonString"></param>
        /// <returns>IEnumerable</returns>
        public static IEnumerable<T> Deserialize<T>(this string jsonString) where T : class
        {
            IEnumerable<T> _list = null;
            if (!string.IsNullOrEmpty(jsonString))
            {
                JavaScriptSerializer _serializerHelper = new JavaScriptSerializer();
                _list = _serializerHelper.Deserialize<IEnumerable<T>>(jsonString);
            }
            return _list;
        }
        #endregion
    }
}
