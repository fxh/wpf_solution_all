﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Infrastructure.Utils.Helpers
{
    public  static class TraceHelper
    {
        public static void Write(BooleanSwitch booleanSwitch, params object[] values)
        {
            if (!booleanSwitch.Enabled)
                return;
            for (int i = 1; i < values.Length; i += 2)
            {
                Trace.WriteLine(string.Format("======{0}:\n{1}", GetStringSafe(values[i - 1]), GetStringSafe(values[i])));
            }
        }
        static string GetStringSafe(object obj)
        {
            return obj == null ? "<null>" : obj.ToString();
        }
    }
}
