using System;
using System.Diagnostics;

namespace Infrastructure.Utils.Helpers
{
    /// <summary>
    /// �¼�������
    /// </summary>
	public static class EventHelper
	{
		public delegate T CreateEventArguments<T>() where T : EventArgs;
		[DebuggerHidden]
		public static void Raise(EventHandler handler, object sender)
		{
			if (handler != null)
			{
				handler(sender, EventArgs.Empty);
			}
		}
		[DebuggerHidden]
		public static void Raise<T>(EventHandler<T> handler, object sender, T e) where T : EventArgs
		{
			if (handler != null)
			{
				handler(sender, e);
			}
		}
		[DebuggerHidden]
		public static void Raise<T>(EventHandler<T> handler, object sender, EventHelper.CreateEventArguments<T> createEventArguments) where T : EventArgs
		{
			ArgumentHelper.AssertNotNull<EventHelper.CreateEventArguments<T>>(createEventArguments, "createEventArguments");
			if (handler != null)
			{
				handler(sender, createEventArguments());
			}
		}
		[DebuggerHidden, Obsolete("This API will be removed in a future version of The Helper Trinity.")]
		public static void Raise(Delegate handler, object sender, EventArgs e)
		{
			if (handler != null)
			{
				handler.DynamicInvoke(new object[]
				{
					sender,
					e
				});
			}
		}
		[DebuggerHidden]
		public static void BeginRaise(EventHandler handler, object sender, AsyncCallback callback, object asyncState)
		{
			if (handler != null)
			{
				new Action<EventHandler, object>(EventHelper.Raise).BeginInvoke(handler, sender, callback, asyncState);
			}
		}
		[DebuggerHidden]
		public static void BeginRaise<T>(EventHandler<T> handler, object sender, T e, AsyncCallback callback, object asyncState) where T : EventArgs
		{
			if (handler != null)
			{
				new Action<EventHandler<T>, object, T>(EventHelper.Raise<T>).BeginInvoke(handler, sender, e, callback, asyncState);
			}
		}
		[DebuggerHidden]
		public static void BeginRaise<T>(EventHandler<T> handler, object sender, EventHelper.CreateEventArguments<T> createEventArguments, AsyncCallback callback, object asyncState) where T : EventArgs
		{
			ArgumentHelper.AssertNotNull<EventHelper.CreateEventArguments<T>>(createEventArguments, "createEventArguments");
			if (handler != null)
			{
				new Action<EventHandler<T>, object, EventHelper.CreateEventArguments<T>>(EventHelper.Raise<T>).BeginInvoke(handler, sender, createEventArguments, callback, asyncState);
			}
		}
		[DebuggerHidden, Obsolete("This API will be removed in a future version of The Helper Trinity.")]
		public static void BeginRaise(Delegate handler, object sender, EventArgs e, AsyncCallback callback, object asyncState)
		{
			if (handler != null)
			{
				new Action<Delegate, object, EventArgs>(EventHelper.Raise).BeginInvoke(handler, sender, e, callback, asyncState);
			}
		}
	}
}
