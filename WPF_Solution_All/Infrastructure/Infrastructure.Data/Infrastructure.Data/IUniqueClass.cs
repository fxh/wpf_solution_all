﻿namespace Infrastructure.Data
{
    /// <summary>
    /// 带唯一编号的类
    /// </summary>
    public interface IUniqueClass
    {
        int Id { get; set; }
    }
}
