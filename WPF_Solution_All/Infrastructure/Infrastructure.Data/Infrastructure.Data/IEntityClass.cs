﻿namespace Infrastructure.Data
{
    /// <summary>
    /// 拥有唯一编号和名称的实体类
    /// </summary>
    public interface IEntityClass : IUniqueClass
    {
        string Name { get; set; }
    }
}
