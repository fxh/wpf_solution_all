﻿using Infrastructure.Localization.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.Validation
{
    /// <summary>
    /// 常规删除数据验证
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericDeleteValidator<T> : SpecificationValidator<T> where T : class
    {
        #region  Members 成员变量
        private readonly Func<T, bool> _validationFunction;
        private readonly string _modelName;
        private readonly string _entityName;
        #endregion

        #region Constructors 构造函数
        /// <summary>
        /// 初始化一个常规删除数据验证
        /// </summary>
        /// <param name="validationFunction"></param>
        /// <param name="modelName"></param>
        /// <param name="entityName"></param>
        public GenericDeleteValidator(Func<T, bool> validationFunction, string modelName, string entityName)
        {
            _validationFunction = validationFunction;
            _modelName = modelName;
            _entityName = entityName;
        }
        #endregion

        #region  Base Class Overrides 基类方法重写
        public override string GetErrorMessage(T model)
        {
            var result = _validationFunction.Invoke(model);
            return result ? string.Format(Resources.DeleteErrorUsedBy_f, _modelName, _entityName) : "";
        }
        #endregion
    }
}
