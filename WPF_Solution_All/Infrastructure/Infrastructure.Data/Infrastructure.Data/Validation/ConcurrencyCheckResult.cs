﻿using System;

namespace Infrastructure.Data.Validation
{
    /// <summary>
    /// 并发数据验证结果
    /// </summary>
    public class ConcurrencyCheckResult
    {
        public string ErrorMessage { get; set; }
        public SuggestedOperation SuggestedOperation { get; set; }

        /// <summary>
        /// 创建并发检查结果
        /// </summary>
        /// <param name="suggestedOperation"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ConcurrencyCheckResult Create(SuggestedOperation suggestedOperation, string errorMessage = "")
        {
            return new ConcurrencyCheckResult { ErrorMessage = errorMessage, SuggestedOperation = suggestedOperation };
        }

        /// <summary>
        /// 建议退出
        /// </summary>
        /// <param name="errorMessaage"></param>
        /// <returns></returns>
        public static ConcurrencyCheckResult Break(string errorMessaage)
        {
            return Create(SuggestedOperation.Break, errorMessaage);
        }

        /// <summary>
        /// 建议刷新
        /// </summary>
        /// <returns></returns>
        public static ConcurrencyCheckResult Refresh()
        {
            return Create(SuggestedOperation.Refresh);
        }

        /// <summary>
        /// 建议继续
        /// </summary>
        /// <returns></returns>
        public static ConcurrencyCheckResult Continue()
        {
            return Create(SuggestedOperation.Continue);
        }
    }
}
