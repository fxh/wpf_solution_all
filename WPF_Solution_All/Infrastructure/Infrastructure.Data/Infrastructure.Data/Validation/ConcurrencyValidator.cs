﻿using System;

namespace Infrastructure.Data.Validation
{
    /// <summary>
    /// 并发数据验证
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ConcurrencyValidator<T> where T : class
    {
        #region Abstract Methods 抽象方法
        /// <summary>
        /// 获取并发数据验证结果
        /// </summary>
        /// <param name="current"></param>
        /// <param name="loaded"></param>
        /// <returns></returns>
        public abstract ConcurrencyCheckResult GetErrorMessage(T current, T loaded);
        #endregion
    }
}
