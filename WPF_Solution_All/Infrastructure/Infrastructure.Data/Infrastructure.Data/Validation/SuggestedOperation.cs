﻿using System;

namespace Infrastructure.Data.Validation
{
    /// <summary>
    /// 建议处理操作
    /// </summary>
    public enum SuggestedOperation
    {
        /// <summary>
        /// 停止
        /// </summary>
        Break,
        /// <summary>
        /// 刷新
        /// </summary>
        Refresh,
        /// <summary>
        /// 继续
        /// </summary>
        Continue
    }
}
