﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data
{
    /// <summary>
    /// 实体类-唯一编号和名称
    /// </summary>
    public abstract class EntityClass : UniqueClass, IEntityClass
    {
        public string Name { get; set; }
    }
}
