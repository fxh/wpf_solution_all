﻿namespace Infrastructure.Data
{
    /// <summary>
    /// 唯一编号类
    /// </summary>
    public abstract class UniqueClass : IUniqueClass
    {
        public int Id { get; set; }
    }
}
