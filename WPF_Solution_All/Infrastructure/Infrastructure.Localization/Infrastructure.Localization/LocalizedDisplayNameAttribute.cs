﻿using Infrastructure.Localization.Properties;
using System;
using System.ComponentModel;
using System.Linq;

namespace Infrastructure.Localization
{
    /// <summary>
    /// 定义一个从Resources中读取Key值作为DisplayName的特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        private readonly string _resourceKey;
        /// <summary>
        /// 显示名称
        /// </summary>
        public override string DisplayName
        {
            get
            {
                string text = Resources.ResourceManager.GetString(this._resourceKey);
                if (string.IsNullOrEmpty(text))
                {
                    text = this._resourceKey.Select(delegate(char x)
                    {
                        if (!char.IsLower(x))
                        {
                            return string.Format(" {0}", x);
                        }
                        return x.ToString();
                    }).Aggregate((string x, string y) => x + y).Trim();
                }
                return text;
            }
        }
        public LocalizedDisplayNameAttribute(string resourceKey)
        {
            this._resourceKey = resourceKey;
        }
        public LocalizedDisplayNameAttribute()
        {
            Type type = base.GetType();
            this._resourceKey = type.Name;
        }
    }
}
