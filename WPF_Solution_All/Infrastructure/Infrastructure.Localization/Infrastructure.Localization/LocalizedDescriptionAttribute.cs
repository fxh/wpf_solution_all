﻿using Infrastructure.Localization.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Localization
{
    /// <summary>
    ///  定义一个从Resources中读取Key值作为Description的特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class LocalizedDescriptionAttribute : DescriptionAttribute
    {
        private readonly string _resourceKey;
        /// <summary>
        /// 描述
        /// </summary>
        public override string Description
        {
            get
            {
                string text = Resources.ResourceManager.GetString(this._resourceKey);
                if (string.IsNullOrEmpty(text))
                {
                    text = this._resourceKey.Select(delegate(char x)
                    {
                        if (!char.IsLower(x))
                        {
                            return string.Format(" {0}", x);
                        }
                        return x.ToString();
                    }).Aggregate((string x, string y) => x + y).Trim();
                }
                return text;
            }
        }

        /// <summary>
        /// 本地化描述
        /// </summary>
        /// <param name="resourceKey">资源键值</param>
        public LocalizedDescriptionAttribute(string resourceKey)
        {
            _resourceKey = resourceKey;
        }

        public LocalizedDescriptionAttribute()
        {
            var type = GetType();
            _resourceKey = type.Name;
        }

    }
}
