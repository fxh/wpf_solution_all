﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Standards
{
    public class Common
    {
        #region  Members 成员变量
        #endregion

        #region Constructors 构造函数
        #endregion

        #region  Properties 属性

        #endregion

        #region  Base Class Overrides 基类方法重写

        #endregion

        #region Event Handlers 处理事件
        #endregion

        #region Events 委托事件或路由事件
        #endregion

        #region Methods 方法
        #endregion

        #region Abstract Methods 抽象方法
        #endregion
    }
}
